"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from fotoj.models import *
from siriuso.utils.admin_mixins import TekstoMixin


#Аминка фотогалерей
# Форма для многоязычных названий типов файлов изображений
class FotojFotoDosieroTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = FotojFotoDosieroTipo
        fields = [field.name for field in FotojFotoDosieroTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы файлов изображений
@admin.register(FotojFotoDosieroTipo)
class FotojFotoDosieroTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoDosieroTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = FotojFotoDosieroTipo


# Фотогалереи пользователей
# Форма для многоязычных названий типов альбомов пользователей
class FotojAlbumoUzantoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = FotojAlbumoUzantoTipo
        fields = [field.name for field in FotojAlbumoUzantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы альбомов пользователей
@admin.register(FotojAlbumoUzantoTipo)
class FotojAlbumoUzantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoUzantoTipoFormo
    list_display = ('nomo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoUzantoTipo


# Форма для многоязычных названий и описаний альбомов пользователей
class FotojAlbumoUzantoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojAlbumoUzanto
        fields = [field.name for field in FotojAlbumoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

# Альбомы пользователей
@admin.register(FotojAlbumoUzanto)
class FotojAlbumoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoUzantoFormo
    list_display = ('nomo_teksto', 'priskribo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoUzanto


# Форма для многоязычных описаний фотографий пользователей
class FotojFotoUzantoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojFotoUzanto
        fields = [field.name for field in FotojFotoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Фотографии пользователей
@admin.register(FotojFotoUzanto)
class FotojFotoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoUzantoFormo
    list_display = ('priskribo_teksto', 'albumo', 'uuid')

    class Meta:
        model = FotojFotoUzanto


# Фотолалереи сообществ
# Форма для многоязычных названий типов альбомов сообществ
class FotojAlbumoKomunumoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = FotojAlbumoKomunumoTipo
        fields = [field.name for field in FotojAlbumoKomunumoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы альбомов сообществ
@admin.register(FotojAlbumoKomunumoTipo)
class FotojAlbumoKomunumoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoKomunumoTipoFormo
    list_display = ('nomo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoKomunumoTipo


# Форма для многоязычных названий и описаний альбомов сообществ
class FotojAlbumoKomunumoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojAlbumoKomunumo
        fields = [field.name for field in FotojAlbumoKomunumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Альбомы сообществ
@admin.register(FotojAlbumoKomunumo)
class FotojAlbumoKomunumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojAlbumoKomunumoFormo
    list_display = ('nomo_teksto', 'priskribo_teksto', 'uuid')

    class Meta:
        model = FotojAlbumoKomunumo


# Форма для многоязычных описаний фотографий сообществ
class FotojFotoKomunumoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = FotojFotoKomunumo
        fields = [field.name for field in FotojFotoKomunumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Фотографии сообществ
@admin.register(FotojFotoKomunumo)
class FotojFotoKomunumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = FotojFotoKomunumoFormo
    list_display = ('priskribo_teksto', 'albumo', 'uuid')

    class Meta:
        model = FotojFotoKomunumo
