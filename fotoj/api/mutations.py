"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction
from django.core.files.base import ContentFile
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from .schema import *
from siriuso.utils.hash import get_sha256
from siriuso.api.types import ErrorNode
from siriuso.utils.thumbnailer import get_image_properties, get_thumbnail, get_thumbnail_content
from main.models import Uzanto
from informiloj.models import InformilojLingvo
from komunumoj.models import Komunumo
from PIL import Image
import graphene

# Максимальный размер загружаемого изображения
MAX_IMAGE_SIZE = 1024 * 1024 * 10  # 10 МБ


# Находим альбом по UUID сообщества или пользователя
def get_albumo(uuid, tipo='baza'):
    try:
        return FotojAlbumoKomunumo.objects.get(posedanto_id=uuid, tipo__kodo=tipo), FotojFotoKomunumo, KomunumojAliro
    except FotojAlbumoKomunumo.DoesNotExist:
        pass

    try:
        return FotojAlbumoUzanto.objects.get(posedanto__uuid=uuid, tipo__kodo=tipo), FotojFotoUzanto, UzantojAliro
    except FotojAlbumoUzanto.DoesNotExist:
        pass

    return None


# добавление фото в альбом сообщества или пользователя
class AlshutiMuroBildo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    bildo = graphene.Field(FotojFotoDosieroNode)

    class Arguments:
        bildo = graphene.String()
        muro_posedanto_uuid = graphene.UUID(required=True)
        komentado_aliro_kodo = graphene.String(required=True)

    @staticmethod
    def mutate(root, info, muro_posedanto_uuid, komentado_aliro_kodo, **kwargs):
        request = info.context
        message = None
        bildo = None
        status = False
        user = info.context.user
        albumo = None
        foto_modelo = None
        komentado_aliro = None

        if user.is_authenticated:
            albumo_data = get_albumo(muro_posedanto_uuid)

            if not albumo_data:
                message = _('Для стены не найден альбом для загрузки изображений')
            else:
                albumo, foto_modelo, aliro_modelo = albumo_data
                # Проверяем наличие прав
                try:
                    komentado_aliro = aliro_modelo.objects.get(kodo=komentado_aliro_kodo, forigo=False, arkivo=False,
                                                               publikigo=True)
                except aliro_modelo.DoesNotExist:
                    message = _('Указан неверный тип доступа на комментирование')

            if 'bildo' in request.FILES and not message:
                bildo_file = request.FILES['bildo']

                # Проверяем размер файла:
                if bildo_file.size > MAX_IMAGE_SIZE:
                    message = _('Размер файла больше 25МБ')

                if bildo_file.content_type.split('/')[0].lower() != 'image':
                    message = _('Файл не является изображением')

                if not message:
                    hash = get_sha256(bildo_file)

                    with transaction.atomic():
                        tipo = FotojFotoDosieroTipo.objects.get(kodo='normala', forigo=False, arkivo=False,
                                                                publikigo=True)

                        try:
                            bildo = FotojFotoDosiero.objects.get(hash=hash, forigo=False, arkivo=False, publikigo=True)
                        except FotojFotoDosiero.DoesNotExist:
                            bildo = FotojFotoDosiero.objects.create(
                                hash=hash, forigo=False, arkivo=False, publikigo=True, publikiga_dato=timezone.now(),
                                posedanto=user, bildo_baza=bildo_file, tipo=tipo
                            )

                        # Проверяем, есть ли данное изображение в альбоме
                        if not foto_modelo.objects.filter(dosiero=bildo, forigo=False):
                            m_args = {'autoro': user} if issubclass(foto_modelo, FotojFotoKomunumo) else {}

                            # Создаём запись о изображении в альбоме
                            foto_modelo.objects.create(
                                posedanto=albumo.posedanto,
                                albumo=albumo,
                                dosiero=bildo,
                                komentado_aliro=komentado_aliro,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                arkivo=False,
                                forigo=False,
                                **m_args
                            )

                        status = True
                        message = _('Изображение успешно загружено')
        else:
            message = _('Требуется авторизация')

        return AlshutiMuroBildo(status=status, message=message, bildo=bildo)


# загрузка аватара сообщества или пользователя
class InstaliAvataron(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    avataro = graphene.Field(FotojFotoDosieroNode)

    class Arguments:
        uzanto_id = graphene.Int()
        komunumo_uuid = graphene.UUID()
        bildo_baza = graphene.String()
        bildo_avataro = graphene.String()
        bildo_avataro_min = graphene.String()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        dosiero = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uzanto_id' not in kwargs and 'komunumo_uuid' not in kwargs:
                message = _('Не указан ни один из параметров "uzantoId" и "komunumoUuid"')
            elif 'uzanto_id' in kwargs and 'komunumo_uuid' in kwargs:
                message = _('Должен быть указан только один из параметров: "uzantoId" или "komunumoUuid"')

            if not message:
                if 'bildoBaza' not in info.context.FILES:
                    errors.append(
                        ErrorNode(
                            field='bildoBaza',
                            message=_('Файл изображения отсутствует')
                        )
                    )
                else:
                    file_prop = get_image_properties(info.context.FILES['bildoBaza'])

                    if info.context.FILES['bildoBaza'].size > MAX_IMAGE_SIZE:
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message='{} {}{}'.format(
                                    _('Файл аватара не должен быть больше'),
                                    MAX_IMAGE_SIZE,
                                    _('Мбайт')
                                )
                            )
                        )
                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'bildoAvataro' in info.context.FILES:
                    file_prop = get_image_properties(info.context.FILES['bildoAvataro'])

                    if info.context.FILES['bildoAvataro'].size > 1024 * 1024 * 2.5:
                        errors.append(
                            ErrorNode(
                                field='bildoAvataro',
                                message=_('Файл аватара не должен быть больше 2,5 Мбайт')
                            )
                        )

                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoAvataro',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoAvataro',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'bildoAvataroMin' in info.context.FILES:
                    file_prop = get_image_properties(info.context.FILES['bildoAvataroMin'])

                    if info.context.FILES['bildoAvataroMin'].size > 1024 * 1024 * 2.5:
                        errors.append(
                            ErrorNode(
                                field='bildoAvataroMin',
                                message=_('Файл аватара не должен быть больше 2,5 Мбайт')
                            )
                        )

                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoAvataroMin',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoAvataroMin',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'uzanto_id' in kwargs:
                    try:
                        celo = Uzanto.objects.get(id=kwargs.get('uzanto_id'))
                        celo_albumo = FotojAlbumoUzanto.objects.get(posedanto=celo, tipo__kodo='avataro', forigo=False)
                        celo_foto = FotojFotoUzanto
                        celo_avataro = FotojUzantojAvataro
                        celo_aliro = UzantojAliro.objects.get(kodo='chiuj')
                        celo_rajto = 'main.povas_shanghi_uzantan_avataron'
                    except Uzanto.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Пользователь с таким ID не найден')
                            )
                        )
                    except FotojAlbumoUzanto.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Альбом для аватар не найден')
                            )
                        )
                else:
                    try:
                        celo = Komunumo.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False)
                        celo_albumo = FotojAlbumoKomunumo.objects.get(posedanto=celo, tipo__kodo='avataro',
                                                                      forigo=False)
                        celo_foto = FotojFotoKomunumo
                        celo_avataro = FotojKomunumoAvataro
                        celo_aliro = KomunumojAliro.objects.get(kodo='chiuj')
                        celo_rajto = 'komunumoj.povas_shanghi_komunuman_avataron'
                    except Komunumo.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='komunumoUuid',
                                message=_('Сообщество с таким UUID не найдено')
                            )
                        )
                    except FotojAlbumoKomunumo.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Альбом для аватар не найден')
                            )
                        )

                if celo and not (
                    uzanto.has_perm(celo_rajto)
                    or uzanto.has_perm(celo_rajto, celo)

                ):
                    message = _('Недостаточно прав')

                if not len(errors) and not message:
                    with transaction.atomic():
                        tipo = FotojFotoDosieroTipo.objects.get(kodo='normala', forigo=False, arkivo=False,
                                                                publikigo=True)
                        lingvo = InformilojLingvo.objects.get(kodo='eo_XX')
                        hash = get_sha256(info.context.FILES['bildoBaza'])

                        try:
                            originalo = FotojFotoDosiero.objects.get(hash=hash, forigo=False, arkivo=False,
                                                                     publikigo=True)
                            bildo_baza = (info.context.FILES['bildoAvataro']
                                          if 'bildoAvataro' in info.context.FILES
                                          else get_thumbnail(info.context.FILES['bildoBaza'], (160, 160)))

                        except FotojFotoDosiero.DoesNotExist:
                            originalo = None
                            bildo_baza = info.context.FILES['bildoBaza']

                        dosiero = FotojFotoDosiero.objects.create(
                            tipo=tipo,
                            posedanto=uzanto,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            originalo=originalo,
                            bildo_baza=bildo_baza,
                        )

                        if 'bildoAvataro' in info.context.FILES:
                            bildo_e = get_thumbnail_content(
                                info.context.FILES['bildoAvataro'],
                                (160, 160),
                                dosiero.bildo_baza.name,
                                'e'
                            )
                        else:
                            bildo_e = get_thumbnail_content(
                                dosiero.bildo_baza,
                                (160, 160),
                                dosiero.bildo_baza.name,
                                'e'
                            )

                        if 'bildoAvataroMin' in info.context.FILES:
                            bildo_f = get_thumbnail_content(
                                info.context.FILES['bildoAvataroMin'],
                                (50, 50),
                                dosiero.bildo_baza.name,
                                'f'
                            )
                        else:
                            bildo_f = get_thumbnail_content(
                                dosiero.bildo_baza,
                                (50, 50),
                                dosiero.bildo_baza.name,
                                'f'
                            )

                        dosiero.bildo_e = bildo_e
                        dosiero.bildo_f = bildo_f
                        dosiero.save()

                        args = {'autoro': uzanto, } if isinstance(celo, Komunumo) else {}

                        avataro = celo_foto.objects.create(
                            posedanto=celo,
                            albumo=celo_albumo,
                            dosiero=dosiero,
                            komentado_aliro=celo_aliro,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            **args
                        )

                        celo_avataro.objects.filter(posedanto=celo).update(chefa_varianto=False)
                        celo_avataro.objects.create(
                            avataro=avataro,
                            posedanto=celo,
                            komentado_aliro=celo_aliro,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            chefa_varianto=True,
                            lingvo=lingvo,
                            **args
                        )
                        status = True
                        message = _('Аватара успешно установлена')

                elif not message:
                    message = _('Переданы неверные аргументы')
        else:
            message = _('Требуется авторизация')

        return InstaliAvataron(status=status, message=message, errors=errors, avataro=dosiero)


# загрузка в фотогалереи сообщества или пользователя
class InstaliKovrilon(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    kovrilo = graphene.Field(FotojFotoDosieroNode)

    class Arguments:
        uzanto_id = graphene.Int()
        komunumo_uuid = graphene.UUID()
        bildo_baza = graphene.String()
        bildo_kovrilo = graphene.String()
        bildo_kovrilo_min = graphene.String()

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = []
        dosiero = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uzanto_id' not in kwargs and 'komunumo_uuid' not in kwargs:
                message = _('Не указан ни один из параметров "uzantoId" и "komunumoUuid"')
            elif 'uzanto_id' in kwargs and 'komunumo_uuid' in kwargs:
                message = _('Должен быть указан только один из параметров: "uzantoId" или "komunumoUuid"')

            if not message:
                if 'bildoBaza' not in info.context.FILES:
                    errors.append(
                        ErrorNode(
                            field='bildoBaza',
                            message=_('Файл изображения отсутствует')
                        )
                    )
                else:
                    file_prop = get_image_properties(info.context.FILES['bildoBaza'])

                    if info.context.FILES['bildoBaza'].size > MAX_IMAGE_SIZE:
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message='{} {}{}'.format(
                                    _('Файл аватара не должен быть больше'),
                                    MAX_IMAGE_SIZE,
                                    _('Мбайт')
                                )
                            )
                        )
                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoBaza',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'bildoKovrilo' in info.context.FILES:
                    file_prop = get_image_properties(info.context.FILES['bildoKovrilo'])

                    if info.context.FILES['bildoKovrilo'].size > 1024 * 1024 * 2.5:
                        errors.append(
                            ErrorNode(
                                field='bildoKovrilo',
                                message=_('Файл аватара не должен быть больше 2,5 Мбайт')
                            )
                        )

                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoKovrilo',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoKovrilo',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'bildoKovriloMin' in info.context.FILES:
                    file_prop = get_image_properties(info.context.FILES['bildoKovriloMin'])

                    if info.context.FILES['bildoKovriloMin'].size > 1024 * 1024 * 2.5:
                        errors.append(
                            ErrorNode(
                                field='bildoKovriloMin',
                                message=_('Файл аватара не должен быть больше 2,5 Мбайт')
                            )
                        )

                    if not file_prop:
                        errors.append(
                            ErrorNode(
                                field='bildoKovriloMin',
                                message=_('Файл не является изображением')
                            )
                        )

                    elif file_prop['format'] not in ('GIF', 'PNG', 'JPEG'):
                        errors.append(
                            ErrorNode(
                                field='bildoKovriloMin',
                                message=_('Файл аватара должен иметь формат "GIF", "PNG" или "JPEG"')
                            )
                        )

                if 'uzanto_id' in kwargs:
                    try:
                        celo = Uzanto.objects.get(id=kwargs.get('uzanto_id'))
                        celo_albumo = FotojAlbumoUzanto.objects.get(posedanto=celo, tipo__kodo='kovrilo', forigo=False)
                        celo_foto = FotojFotoUzanto
                        celo_kovrilo = FotojUzantojKovrilo
                        celo_aliro = UzantojAliro.objects.get(kodo='chiuj')
                        celo_rajto = 'main.povas_shanghi_uzantan_kovrilon'
                    except Uzanto.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Пользователь с таким ID не найден')
                            )
                        )
                    except FotojAlbumoUzanto.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Альбом для аватар не найден')
                            )
                        )
                else:
                    try:
                        celo = Komunumo.objects.get(uuid=kwargs.get('komunumo_uuid'), forigo=False)
                        celo_albumo = FotojAlbumoKomunumo.objects.get(posedanto=celo, tipo__kodo='kovrilo',
                                                                      forigo=False)
                        celo_foto = FotojFotoKomunumo
                        celo_kovrilo = FotojKomunumoKovrilo
                        celo_aliro = KomunumojAliro.objects.get(kodo='chiuj')
                        celo_rajto = 'komunumoj.povas_shanghi_komunuman_kovrilon'
                    except Komunumo.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='komunumoUuid',
                                message=_('Сообщество с таким UUID не найдено')
                            )
                        )
                    except FotojAlbumoKomunumo.DoesNotExist:
                        errors.append(
                            ErrorNode(
                                field='uzantoId',
                                message=_('Альбом для аватар не найден')
                            )
                        )

                if celo and not (
                    uzanto.has_perm(celo_rajto)
                    or uzanto.has_perm(celo_rajto, celo)

                ):
                    message = _('Недостаточно прав')

                if not len(errors) and not message:
                    with transaction.atomic():
                        tipo = FotojFotoDosieroTipo.objects.get(kodo='normala', forigo=False, arkivo=False,
                                                                publikigo=True)
                        lingvo = InformilojLingvo.objects.get(kodo='eo_XX')
                        hash = get_sha256(info.context.FILES['bildoBaza'])

                        try:
                            originalo = FotojFotoDosiero.objects.get(hash=hash, forigo=False, arkivo=False,
                                                                     publikigo=True)
                            bildo_baza = (info.context.FILES['bildoKovrilo']
                                          if 'bildoKovrilo' in info.context.FILES
                                          else get_thumbnail(info.context.FILES['bildoBaza'], (800, 290)))

                        except FotojFotoDosiero.DoesNotExist:
                            originalo = None
                            bildo_baza = info.context.FILES['bildoBaza']

                        dosiero = FotojFotoDosiero.objects.create(
                            tipo=tipo,
                            posedanto=uzanto,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            originalo=originalo,
                            bildo_baza=bildo_baza,
                        )

                        if 'bildoKovrilo' in info.context.FILES:
                            bildo_e = get_thumbnail_content(
                                info.context.FILES['bildoKovrilo'],
                                (800, 290),
                                dosiero.bildo_baza.name,
                                'e'
                            )
                        else:
                            bildo_e = get_thumbnail_content(
                                dosiero.bildo_baza,
                                (800, 290),
                                dosiero.bildo_baza.name,
                                'e'
                            )

                        if 'bildoKovriloMin' in info.context.FILES:
                            bildo_f = get_thumbnail_content(
                                info.context.FILES['bildoKovriloMin'],
                                (400, 145),
                                dosiero.bildo_baza.name,
                                'f'
                            )
                        else:
                            bildo_f = get_thumbnail_content(
                                dosiero.bildo_baza,
                                (400, 145),
                                dosiero.bildo_baza.name,
                                'f'
                            )

                        dosiero.bildo_e = bildo_e
                        dosiero.bildo_f = bildo_f
                        dosiero.save()

                        args = {'autoro': uzanto, } if isinstance(celo, Komunumo) else {}

                        kovrilo = celo_foto.objects.create(
                            posedanto=celo,
                            albumo=celo_albumo,
                            dosiero=dosiero,
                            komentado_aliro=celo_aliro,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            **args
                        )

                        celo_kovrilo.objects.filter(posedanto=celo).update(chefa_varianto=False)
                        celo_kovrilo.objects.create(
                            kovrilo=kovrilo,
                            posedanto=celo,
                            komentado_aliro=celo_aliro,
                            publikigo=True,
                            publikiga_dato=timezone.now(),
                            arkivo=False,
                            forigo=False,
                            chefa_varianto=True,
                            lingvo=lingvo,
                            **args
                        )
                        status = True
                        message = _('Аватара успешно установлена')

                elif not message:
                    message = _('Переданы неверные аргументы')
        else:
            message = _('Требуется авторизация')

        return InstaliKovrilon(status=status, message=message, errors=errors, kovrilo=dosiero)


class FotojMutations(graphene.ObjectType):
    alshuti_muro_bildo = AlshutiMuroBildo.Field()
    instali_avataron = InstaliAvataron.Field()
    instali_kovrilon = InstaliKovrilon.Field()
