"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель категорий рабочих пространств
class RedaktuLaborspacoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_kategorioj = graphene.Field(LaborspacoKategorioNode,
        description=_('Созданная/изменённая категория рабочих пространств'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            laborspaco_kategorioj = LaborspacoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    laborspaco_kategorioj.realeco.set(realeco)
                                else:
                                    laborspaco_kategorioj.realeco.clear()

                            laborspaco_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_kategorioj = LaborspacoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                laborspaco_kategorioj.forigo = kwargs.get('forigo', laborspaco_kategorioj.forigo)
                                laborspaco_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_kategorioj.arkivo = kwargs.get('arkivo', laborspaco_kategorioj.arkivo)
                                laborspaco_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_kategorioj.publikigo = kwargs.get('publikigo', laborspaco_kategorioj.publikigo)
                                laborspaco_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        laborspaco_kategorioj.realeco.set(realeco)
                                    else:
                                        laborspaco_kategorioj.realeco.clear()

                                laborspaco_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoKategorio(status=status, message=message, laborspaco_kategorioj=laborspaco_kategorioj)


# Модель типов рабочих пространств
class RedaktuLaborspacoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_tipoj = graphene.Field(LaborspacoTipoNode, 
        description=_('Созданный/изменённый тип рабочих пространств'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            laborspaco_tipoj = LaborspacoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    laborspaco_tipoj.realeco.set(realeco)
                                else:
                                    laborspaco_tipoj.realeco.clear()

                            laborspaco_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_tipoj = LaborspacoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                laborspaco_tipoj.forigo = kwargs.get('forigo', laborspaco_tipoj.forigo)
                                laborspaco_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_tipoj.arkivo = kwargs.get('arkivo', laborspaco_tipoj.arkivo)
                                laborspaco_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_tipoj.publikigo = kwargs.get('publikigo', laborspaco_tipoj.publikigo)
                                laborspaco_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        laborspaco_tipoj.realeco.set(realeco)
                                    else:
                                        laborspaco_tipoj.realeco.clear()

                                laborspaco_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoTipo(status=status, message=message, laborspaco_tipoj=laborspaco_tipoj)


# Модель статусов рабочих пространств
class RedaktuLaborspacoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_statusoj = graphene.Field(LaborspacoStatusoNode, 
        description=_('Созданный/изменённый статусов рабочих пространств'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            laborspaco_statusoj = LaborspacoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            laborspaco_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_statusoj = LaborspacoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:

                                laborspaco_statusoj.forigo = kwargs.get('forigo', laborspaco_statusoj.forigo)
                                laborspaco_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_statusoj.arkivo = kwargs.get('arkivo', laborspaco_statusoj.arkivo)
                                laborspaco_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_statusoj.publikigo = kwargs.get('publikigo', laborspaco_statusoj.publikigo)
                                laborspaco_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                laborspaco_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoStatuso(status=status, message=message, laborspaco_statusoj=laborspaco_statusoj)


# Модель рабочих пространств
class RedaktuLaborspaco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco = graphene.Field(LaborspacoNode, 
        description=_('Созданное/изменённое рабочее пространство'))
    laborspaco_posedantoj = graphene.Field(LaborspacoPosedantoNode, 
        description=_('Созданный/изменённый владелец рабочего пространства'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий рабочих пространств'))
        tipo_id = graphene.Int(description=_('Тип рабочих пространств'))
        statuso_id = graphene.Int(description=_('Статус рабочих пространств'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец рабочего пространства'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец рабочего пространства'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец рабочего пространства'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца рабочего пространства'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца рабочего пространства'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco = None
        realeco = None
        kategorio = LaborspacoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_komunumo = None
        uzanto = info.context.user
        laborspaco_posedantoj = None
        posedanto_perm = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not (message or kwargs.get('uuid', False)):
                    posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_laborspacoj')
                    # получаем пользователя, для которого создаётся пространство
                    if not message:
                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                if not posedanto_perm:
                                    message = _('Недостаточно прав')
                                else:
                                    message = _('Неверный владелец/пользователь рабочего пространства')
                    # если пользователь владелец пространства, по которому создаётся рабочее пространство, то можно создавать
                    # проверяем по полю posedanto
                    if not posedanto_perm and not message:
                        if posedanto_uzanto == uzanto:
                            posedanto_perm = True
                    if posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = LaborspacoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории рабочих пространств'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = LaborspacoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except LaborspacoTipo.DoesNotExist:
                                    message = _('Неверный тип рабочих пространств')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = LaborspacoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except LaborspacoStatuso.DoesNotExist:
                                    message = _('Неверный статус рабочих пространств')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            laborspaco = Laborspaco.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                tipo = tipo,
                                statuso = statuso,
                                pozicio = kwargs.get('pozicio', None),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    laborspaco.kategorio.set(kategorio)
                                else:
                                    laborspaco.kategorio.clear()

                            laborspaco.save()

                            status = True
                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = LaborspacoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца рабочего пространства'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = LaborspacoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except LaborspacoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца рабочего пространства'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец рабочего пространства')

                                if not message:
                                    if 'posedanto_komunumo_id' in kwargs:
                                        try:
                                            posedanto_komunumo = Komunumo.objects.get(
                                                id=kwargs.get('posedanto_komunumo_id'),
                                                forigo=False,
                                                arkivo=False
                                            )
                                        except Komunumo.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверное сообщество-владелец рабочего пространства'),'posedanto_komunumo_id')

                                if not message:
                                    laborspaco_posedantoj = LaborspacoPosedanto.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        realeco=realeco,
                                        laborspaco=laborspaco,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_komunumo = posedanto_komunumo,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    laborspaco_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')
    
                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                elif not message:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) 
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco = Laborspaco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if ((not (uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj', laborspaco)))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj', laborspaco)):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = LaborspacoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории рабочих пространств'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = LaborspacoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoTipo.DoesNotExist:
                                        message = _('Неверный тип рабочих пространств')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = LaborspacoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoStatuso.DoesNotExist:
                                        message = _('Неверный статус рабочих пространств')

                            if not message:

                                laborspaco.forigo = kwargs.get('forigo', laborspaco.forigo)
                                laborspaco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco.arkivo = kwargs.get('arkivo', laborspaco.arkivo)
                                laborspaco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco.publikigo = kwargs.get('publikigo', laborspaco.publikigo)
                                laborspaco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco.realeco = realeco if kwargs.get('realeco_id', False) else laborspaco.realeco
                                laborspaco.statuso = statuso if kwargs.get('statuso_id', False) else laborspaco.statuso
                                laborspaco.tipo = tipo if kwargs.get('tipo_id', False) else laborspaco.tipo
                                laborspaco.pozicio = kwargs.get('pozicio', laborspaco.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        laborspaco.kategorio.set(kategorio)
                                    else:
                                        laborspaco.kategorio.clear()

                                laborspaco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Laborspaco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspaco(status=status, message=message, laborspaco=laborspaco, laborspaco_posedantoj=laborspaco_posedantoj)


# Модель типов владельцев рабочих пространств
class RedaktuLaborspacoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_posedantoj_tipoj = graphene.Field(LaborspacoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев рабочих пространств'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            laborspaco_posedantoj_tipoj = LaborspacoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            laborspaco_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_posedantoj_tipoj = LaborspacoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                laborspaco_posedantoj_tipoj.forigo = kwargs.get('forigo', laborspaco_posedantoj_tipoj.forigo)
                                laborspaco_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_posedantoj_tipoj.arkivo = kwargs.get('arkivo', laborspaco_posedantoj_tipoj.arkivo)
                                laborspaco_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_posedantoj_tipoj.publikigo = kwargs.get('publikigo', laborspaco_posedantoj_tipoj.publikigo)
                                laborspaco_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                laborspaco_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoPosedantoTipo(status=status, message=message, laborspaco_posedantoj_tipoj=laborspaco_posedantoj_tipoj)


# Модель статусов владельцев рабочих пространств
class RedaktuLaborspacoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_posedantoj_statusoj = graphene.Field(LaborspacoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца рабочих пространств'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            laborspaco_posedantoj_statusoj = LaborspacoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            laborspaco_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_posedantoj_statusoj = LaborspacoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                laborspaco_posedantoj_statusoj.forigo = kwargs.get('forigo', laborspaco_posedantoj_statusoj.forigo)
                                laborspaco_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_posedantoj_statusoj.arkivo = kwargs.get('arkivo', laborspaco_posedantoj_statusoj.arkivo)
                                laborspaco_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_posedantoj_statusoj.publikigo = kwargs.get('publikigo', laborspaco_posedantoj_statusoj.publikigo)
                                laborspaco_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                laborspaco_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoPosedantoStatuso(status=status, message=message, laborspaco_posedantoj_statusoj=laborspaco_posedantoj_statusoj)


# Модель владельцев рабочих пространств
class RedaktuLaborspacoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_posedantoj = graphene.Field(LaborspacoPosedantoNode, 
        description=_('Созданный/изменённый владелец рабочего пространства'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        laborspaco_uuid = graphene.String(description=_('Рабочее пространство'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец рабочего пространства'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец рабочего пространства'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец рабочего пространства'))
        tipo_id = graphene.Int(description=_('Тип владельца рабочего пространства'))
        statuso_id = graphene.Int(description=_('Статус владельца рабочего пространства'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_posedantoj = None
        realeco = None
        laborspaco = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_komunumo = None
        tipo = None
        statuso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if not message:
                        if 'laborspaco_uuid' in kwargs:
                            try:
                                laborspaco = Laborspaco.objects.get(uuid=kwargs.get('laborspaco_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Laborspaco.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверное рабочее пространство'),'laborspaco_uuid')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'laborspaco_uuid')
                    # владелец пространства может добавлять пользователя
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_posedantoj', laborspaco):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец рабочего пространства'),'posedanto_organizo_uuid')

                        if not message:
                            if 'posedanto_komunumo_id' in kwargs:
                                try:
                                    posedanto_komunumo = Komunumo.objects.get(
                                        id=kwargs.get('posedanto_komunumo_id'),
                                        forigo=False,
                                        arkivo=False
                                    )
                                except Komunumo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное сообщество-владелец рабочего пространства'),'posedanto_komunumo_id')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный параллельный мир'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = LaborspacoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except LaborspacoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца рабочих пространств')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = LaborspacoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except LaborspacoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца рабочих пространств')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                message = _('Неверный владелец/пользователь рабочего пространства')

                        if not message:
                            laborspaco_posedantoj = LaborspacoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                laborspaco = laborspaco,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                posedanto_komunumo = posedanto_komunumo,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_posedantoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_posedantoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            laborspaco_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('posedanto_komunumo_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('laborspaco_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_posedantoj = LaborspacoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # владелец пространства может править
                            count_posedantoj = LaborspacoPosedanto.objects.filter(laborspaco=laborspaco_posedantoj.laborspaco, posedanto_uzanto=uzanto,
                                                                                  arkivo=False, forigo=False, publikigo=True).count()
                            if (not (uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_posedantoj') or count_posedantoj)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_posedantoj') or count_posedantoj):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь рабочего пространства')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец рабочего пространства')

                            if not message:
                                if 'posedanto_komunumo_id' in kwargs:
                                    try:
                                        posedanto_komunumo = Komunumo.objects.get(
                                            id=kwargs.get('posedanto_komunumo_id'),
                                            forigo=False,
                                            arkivo=False
                                        )
                                    except Komunumo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное сообщество-владелец рабочего пространства'),'posedanto_komunumo_id')

                            if not message:
                                if 'laborspaco_uuid' in kwargs:
                                    try:
                                        laborspaco = Laborspaco.objects.get(uuid=kwargs.get('laborspaco_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Laborspaco.DoesNotExist:
                                        message = _('Неверное рабочее пространство')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = LaborspacoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца рабочих пространств')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = LaborspacoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца рабочих пространств')

                            if not message:

                                laborspaco_posedantoj.forigo = kwargs.get('forigo', laborspaco_posedantoj.forigo)
                                laborspaco_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_posedantoj.arkivo = kwargs.get('arkivo', laborspaco_posedantoj.arkivo)
                                laborspaco_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_posedantoj.publikigo = kwargs.get('publikigo', laborspaco_posedantoj.publikigo)
                                laborspaco_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else laborspaco_posedantoj.realeco
                                laborspaco_posedantoj.laborspaco = realeco if kwargs.get('laborspaco_uuid', False) else laborspaco_posedantoj.laborspaco
                                laborspaco_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else laborspaco_posedantoj.posedanto_uzanto
                                laborspaco_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else laborspaco_posedantoj.posedanto_organizo
                                laborspaco_posedantoj.posedanto_komunumo = posedanto_komunumo if kwargs.get('posedanto_komunumo_id', False) else laborspaco_posedantoj.posedanto_komunumo
                                laborspaco_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else laborspaco_posedantoj.statuso
                                laborspaco_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else laborspaco_posedantoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_posedantoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_posedantoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                laborspaco_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoPosedanto(status=status, message=message, laborspaco_posedantoj=laborspaco_posedantoj)


# Модель типов связей рабочих пространств между собой
class RedaktuLaborspacoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_ligilo_tipoj = graphene.Field(LaborspacoLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей рабочих пространств между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            laborspaco_ligilo_tipoj = LaborspacoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(laborspaco_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(laborspaco_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            laborspaco_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_ligilo_tipoj = LaborspacoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                laborspaco_ligilo_tipoj.forigo = kwargs.get('forigo', laborspaco_ligilo_tipoj.forigo)
                                laborspaco_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_ligilo_tipoj.arkivo = kwargs.get('arkivo', laborspaco_ligilo_tipoj.arkivo)
                                laborspaco_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_ligilo_tipoj.publikigo = kwargs.get('publikigo', laborspaco_ligilo_tipoj.publikigo)
                                laborspaco_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(laborspaco_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(laborspaco_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                laborspaco_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoLigiloTipo(status=status, message=message, laborspaco_ligilo_tipoj=laborspaco_ligilo_tipoj)


# Модель связей рабочих пространств между собой
class RedaktuLaborspacoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco_ligilo = graphene.Field(LaborspacoLigiloNode, 
        description=_('Создание/изменение связи рабочих пространств между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID рабочего пространства владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемого рабочего пространства'))
        tipo_id = graphene.Int(description=_('ID типа связи рабочих пространств'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        laborspaco_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_laborspacoj_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Laborspaco.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Laborspaco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное рабочее пространство'),'posedanto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = Laborspaco.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Laborspaco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное рабочее пространство'),'ligilo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = LaborspacoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except LaborspacoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи рабочих пространств между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            laborspaco_ligilo = LaborspacoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            laborspaco_ligilo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_id', False) or kwargs.get('ligilo_id', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            laborspaco_ligilo = LaborspacoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_laborspacoj_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_laborspacoj_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = Laborspaco.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Laborspaco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное рабочее пространство'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = Laborspaco.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Laborspaco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное рабочее пространство'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = LaborspacoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи рабочих пространств между собой'),'tipo_id')

                            if not message:

                                laborspaco_ligilo.forigo = kwargs.get('forigo', laborspaco_ligilo.forigo)
                                laborspaco_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                laborspaco_ligilo.arkivo = kwargs.get('arkivo', laborspaco_ligilo.arkivo)
                                laborspaco_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                laborspaco_ligilo.publikigo = kwargs.get('publikigo', laborspaco_ligilo.publikigo)
                                laborspaco_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                laborspaco_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else laborspaco_ligilo.posedanto
                                laborspaco_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else laborspaco_ligilo.ligilo
                                laborspaco_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else laborspaco_ligilo.tipo

                                laborspaco_ligilo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except LaborspacoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuLaborspacoLigilo(status=status, message=message, laborspaco_ligilo=laborspaco_ligilo)


# Модель категорий кластеров
class RedaktuKlasteroKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klasteroj_kategorioj = graphene.Field(KlasteroKategorioNode,
        description=_('Созданная/изменённая категория кластера'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klasteroj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klasteroj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            klasteroj_kategorioj = KlasteroKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klasteroj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klasteroj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    klasteroj_kategorioj.realeco.set(realeco)
                                else:
                                    klasteroj_kategorioj.realeco.clear()

                            klasteroj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klasteroj_kategorioj = KlasteroKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                klasteroj_kategorioj.forigo = kwargs.get('forigo', klasteroj_kategorioj.forigo)
                                klasteroj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klasteroj_kategorioj.arkivo = kwargs.get('arkivo', klasteroj_kategorioj.arkivo)
                                klasteroj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klasteroj_kategorioj.publikigo = kwargs.get('publikigo', klasteroj_kategorioj.publikigo)
                                klasteroj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klasteroj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klasteroj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klasteroj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        klasteroj_kategorioj.realeco.set(realeco)
                                    else:
                                        klasteroj_kategorioj.realeco.clear()

                                klasteroj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroKategorio(status=status, message=message, klasteroj_kategorioj=klasteroj_kategorioj)


# Модель типов кластеров
class RedaktuKlasteroTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klasteroj_tipoj = graphene.Field(KlasteroTipoNode, 
        description=_('Созданный/изменённый тип кластера'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klasteroj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klasteroj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            klasteroj_tipoj = KlasteroTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klasteroj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klasteroj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    klasteroj_tipoj.realeco.set(realeco)
                                else:
                                    klasteroj_tipoj.realeco.clear()

                            klasteroj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klasteroj_tipoj = KlasteroTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                klasteroj_tipoj.forigo = kwargs.get('forigo', klasteroj_tipoj.forigo)
                                klasteroj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klasteroj_tipoj.arkivo = kwargs.get('arkivo', klasteroj_tipoj.arkivo)
                                klasteroj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klasteroj_tipoj.publikigo = kwargs.get('publikigo', klasteroj_tipoj.publikigo)
                                klasteroj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klasteroj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klasteroj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klasteroj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        klasteroj_tipoj.realeco.set(realeco)
                                    else:
                                        klasteroj_tipoj.realeco.clear()

                                klasteroj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroTipo(status=status, message=message, klasteroj_tipoj=klasteroj_tipoj)


# Модель статусов кластеров
class RedaktuKlasteroStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klasteroj_statusoj = graphene.Field(KlasteroStatusoNode, 
        description=_('Созданный/изменённый статус кластера'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klasteroj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klasteroj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            klasteroj_statusoj = KlasteroStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klasteroj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klasteroj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            klasteroj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klasteroj_statusoj = KlasteroStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                klasteroj_statusoj.forigo = kwargs.get('forigo', klasteroj_statusoj.forigo)
                                klasteroj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klasteroj_statusoj.arkivo = kwargs.get('arkivo', klasteroj_statusoj.arkivo)
                                klasteroj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klasteroj_statusoj.publikigo = kwargs.get('publikigo', klasteroj_statusoj.publikigo)
                                klasteroj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klasteroj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klasteroj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klasteroj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                klasteroj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroStatuso(status=status, message=message, klasteroj_statusoj=klasteroj_statusoj)


class LaborspacojUUID(graphene.InputObjectType):
    laborspaco = graphene.String(description=_('UUID рабочего пространства в которое входит кластер'))
    pozicio = graphene.Int(description=_('Позиция в списке'))
    nomo = graphene.String(description=_('Название'))
    priskribo = graphene.String(description=_('Описание'))

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.laborspaco, get_enhavo(self.nomo, empty_values=True)[0])



# сохраняем кластеры
def klasteroj_save(klasteroj):

    def eventoj():
        pass

    klasteroj.save()
    transaction.on_commit(eventoj)



def LaborspacojUUID_to_Laborspacoj(laborspacoj_uuid):
    """Перевод из параметра списка laborspacoj_uuid в массив laborspacoj
    с проверкой наличия таких записей
    """
    errors = list()
    message = None
    laborspacoj = []
    if laborspacoj_uuid:
        for laborspaco in laborspacoj_uuid:
            try:
                laborspacoj.append({
                    "laborspaco":Laborspaco.objects.get(uuid=laborspaco.laborspaco, forigo=False,
                                                    arkivo=False, publikigo=True),
                    "pozicio":laborspaco.pozicio,
                    "nomo":laborspaco.nomo,
                    "priskribo":laborspaco.priskribo,
                    })
            except Laborspaco.DoesNotExist:
                errors.append(ErrorNode(
                    field=laborspaco,
                    modelo='LaborspacojUUID_to_Laborspacoj',
                    message=_('Неверное рабочее пространство')
                ))
                message = _('Неверное рабочее пространство: ' + laborspaco.laborspaco)
                break
    return errors, message, laborspacoj


# Модель кластеров
class RedaktuKlastero(graphene.Mutation):
    status = graphene.Boolean()
    errors = graphene.List(ErrorNode)
    message = graphene.String()
    klasteroj = graphene.Field(KlasteroNode, 
        description=_('Созданный/изменённый кластер'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        laborspacoj_uuid = graphene.List(LaborspacojUUID, description=_('Рабочие пространства, в которые входит кластер'))
        # laborspaco_uuid = graphene.String(description=_('Рабочее пространство, в которое входит кластер'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий кластеров'))
        tipo_id = graphene.Int(description=_('Тип кластеров'))
        statuso_id = graphene.Int(description=_('Статус кластеров'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец кластера'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец кластера'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца кластера'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца кластера'))


    @staticmethod
    def kontrolanta_argumentoj(root, info, **kwargs):
        """Проверка аргументов
        Что бы не проверять аргументы в create и в edit, проверим отдельно

        Args:
            дублируем как и в create и в edit
        """
        # возвращаемые данные:
        errors = list()
        # проверяем наличие записей с таким кодом
        if ('realeco' not in kwargs) and ('realeco_id' in kwargs):
            try:
                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                arkivo=False, publikigo=True)
                kwargs['realeco'] = realeco
            except Realeco.DoesNotExist:
                errors.append(ErrorNode(
                    field='realeco_id',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный параллельный мир')
                ))
        if ('tipo' not in kwargs) and ('tipo_id' in kwargs):
            try:
                tipo = KlasteroTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['tipo'] = tipo
            except KlasteroTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='tipo_id',
                    modelo='KlasteroTipo',
                    message=_('Неверный тип кластера')
                ))
        if ('statuso' not in kwargs) and ('statuso_id' in kwargs):
            try:
                statuso = KlasteroStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['statuso'] = statuso
            except KlasteroStatuso.DoesNotExist:
                errors.append(ErrorNode(
                    field='statuso_id',
                    modelo='KlasteroStatuso',
                    message=_('Неверный статус кластера')
                ))

        return errors, kwargs


    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        klasteroj = None
        laborspacoj = []
        kategorio = KlasteroKategorio.objects.none()
        uzanto = info.context.user
        posedanto_uzanto = None
        kwargs_posedanto = {}
        posedanto_perm = None # если пользователь владелец рабочего пространства

        if uzanto.is_authenticated:
            # Создаём новую запись
            if not kwargs.get('uuid', False):
                if 'laborspacoj_uuid' in kwargs:
                    errors, message, laborspacoj = LaborspacojUUID_to_Laborspacoj(kwargs.get('laborspacoj_uuid'))
                # else: требование заполнения пространство отключено по желанию Владимира 08.11.2023
                #     errors.append(ErrorNode(
                #         field='laborspacoj_uuid',
                #         modelo='RedaktuKlastero',
                #         message=_('Поле обязательно для заполнения')
                #     ))
                posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_klasteroj')
                if not posedanto_perm and not len(errors):
                    # владелец пространства имеет полные права
                    # если подключают несколько пространств, то должен быть владельцем всех пространств.
                    # если не является владельцем какого либо пространства, то идёт ошибка
                    for laborspaco in laborspacoj:
                        posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_klasteroj', laborspaco['laborspaco'])
                        if not posedanto_perm:
                            break
                    if not ('laborspacoj_uuid' in kwargs) and not posedanto_perm and not len(errors):
                        # если нет рабочего пространства, то разрешение на создание есть ещё у того, кто создаёт для себя
                        # получаем пользователя, для которого создаётся кластер
                        if ('posedanto_uzanto_id' in kwargs and (kwargs.get('posedanto_tipo_id', False)) and
                                    kwargs.get('posedanto_statuso_id', False)):
                            kwargs_posedanto['posedanto_uzanto_id'] = kwargs['posedanto_uzanto_id']
                            kwargs_posedanto['tipo_id'] = kwargs['posedanto_tipo_id']
                            kwargs_posedanto['statuso_id'] = kwargs['posedanto_statuso_id']
                            errors, kwargs_posedanto = RedaktuKlasteroPosedanto.kontrolanta_argumentoj(root, info, **kwargs_posedanto)
                            
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                if not posedanto_perm:
                                    errors.append(ErrorNode(
                                        field='',
                                        modelo='Klastero',
                                        message=_('Недостаточно прав')
                                    ))
                                else:
                                    errors.append(ErrorNode(
                                        field='posedanto_uzanto_id',
                                        modelo='Uzanto',
                                        message=_('Неверный владелец/пользователь рабочего пространства')
                                    ))
                            # если пользователь владелец кластера без связи с рабочим пространством, по которому создаётся кластер, то можно создавать
                            # проверяем по полю posedanto
                            if not len(errors):
                                if posedanto_uzanto == uzanto:
                                    posedanto_perm = True
                if posedanto_perm and not len(errors):
                    # Проверяем наличие всех полей
                    if kwargs.get('forigo', False) and not len(errors):
                        errors.append(ErrorNode(
                            field='forigo',
                            modelo='Klastero',
                            message=_('При создании записи не допустимо указание поля')
                        ))

                    if kwargs.get('arkivo', False) and not len(errors):
                        errors.append(ErrorNode(
                            field='arkivo',
                            modelo='Klastero',
                            message=_('При создании записи не допустимо указание поля')
                        ))
                    
                    if not len(errors):
                        if not kwargs.get('nomo'):
                            errors.append(ErrorNode(
                                field='nomo',
                                modelo='Klastero',
                                message=_('Поле обязательно для заполнения')
                            ))

                    if not len(errors):
                        if not kwargs.get('priskribo'):
                            errors.append(ErrorNode(
                                field='priskribo',
                                modelo='Klastero',
                                message=_('Поле обязательно для заполнения')
                            ))

                    if not len(errors):
                        if 'kategorio' not in kwargs:
                            errors.append(ErrorNode(
                                field='kategorio',
                                modelo='Klastero',
                                message=_('Поле обязательно для заполнения')
                            ))
                        elif not len(kwargs.get('kategorio')):
                            errors.append(ErrorNode(
                                field='kategorio',
                                modelo='Klastero',
                                message=_('Необходимо указать хотя бы одну группу')
                            ))
                        else:
                            kategorio_id = set(kwargs.get('kategorio'))

                            if len(kategorio_id):
                                kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    errors.append(ErrorNode(
                                        field='{}: {}'.format('kategorio',str(dif)),
                                        modelo='Klastero',
                                        message=_('Указаны несуществующие категории кластеров')
                                    ))

                    # проверяем наличие записей с таким кодом
                    if not len(errors):
                        errors, kwargs = RedaktuKlastero.kontrolanta_argumentoj(root, info, **kwargs)
                        if 'tipo' not in kwargs:
                            errors.append(ErrorNode(
                                field='tipo_id',
                                message=_('Поле обязательно для заполнения')
                            ))
                        if 'statuso' not in kwargs:
                            errors.append(ErrorNode(
                                field='statuso_id',
                                message=_('Поле обязательно для заполнения')
                            ))

                    if not len(errors):
                        with transaction.atomic():
                            klasteroj = Klastero.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = kwargs.get('realeco'),
                                tipo = kwargs.get('tipo'),
                                statuso = kwargs.get('statuso'),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                                pozicio = kwargs.get('pozicio', None),
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klasteroj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klasteroj.priskribo, 
                                            kwargs.get('priskribo'), 
                                            info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    klasteroj.kategorio.set(kategorio)
                                else:
                                    klasteroj.kategorio.clear()
                            klasteroj.save()
                            if len(laborspacoj)>0:
                                for laborspaco in laborspacoj:
                                    sci = KlasteroLaborspacoLigilo.objects.create(
                                        laborspaco=laborspaco["laborspaco"],
                                        klastero=klasteroj,
                                        pozicio=laborspaco["pozicio"] if laborspaco["pozicio"] else None,
                                        nomo=laborspaco["nomo"] if laborspaco["nomo"] else None,
                                        priskribo=laborspaco["priskribo"] if laborspaco["priskribo"] else None
                                    )
                                klasteroj.laborspacoj.all()
                            

                            klasteroj_save(klasteroj)
                            kwargs['klastero'] = klasteroj

                        # далее блок добавления владельца
                        if (kwargs.get('posedanto_tipo_id', False)):
                            kwargs['posedanto_perm'] = True
                            if 'tipo' in kwargs_posedanto:
                                kwargs['tipo'] = kwargs_posedanto['tipo']
                                kwargs['statuso'] = kwargs_posedanto['statuso']
                            else:
                                kwargs['tipo_id'] = kwargs['posedanto_tipo_id']
                                kwargs['statuso_id'] = kwargs['posedanto_statuso_id']
                            status, message, errors, klastero_posedantoj = RedaktuKlasteroPosedanto.create(root, info, **kwargs)
                            if status:
                                message = _('Обе записи созданы')

                        if not message:
                            message = _('Запись создана')

                        status = True
                    else:
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)

        else:
            message = _('Требуется авторизация')
            errors.append(ErrorNode(
                message=_('Требуется авторизация')
            ))
        return status, message, errors, klasteroj

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        klasteroj = None
        realeco = None
        uzanto = info.context.user
        if not uzanto.is_authenticated:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuKlastero',
                message=_('Требуется авторизация')
            ))
            message = _('Требуется авторизация')
            return status, message, errors, klasteroj
        with transaction.atomic():
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)  or kwargs.get('laborspacoj_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')
                        errors.append(ErrorNode(
                            modelo='RedaktuKlastero',
                            message=_('Не задано ни одно поле для изменения')
                        ))

                    if not len(errors):
                        # Ищем запись для изменения
                        try:
                            klasteroj = Klastero.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not (uzanto.has_perm('laborspacoj.povas_forigi_klasteroj', klasteroj))
                                    and kwargs.get('forigo', False)):
                                errors.append(ErrorNode(
                                    message=_('Недостаточно прав для удаления')
                                ))
                            elif not (uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj', klasteroj)):
                                errors.append(ErrorNode(
                                    message=_('Недостаточно прав для изменения')
                                ))

                            if not len(errors):
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            errors.append(ErrorNode(
                                                field=str(dif),
                                                message=_('Указаны несуществующие категории рабочих пространств')
                                            ))

                            # проверяем наличие записей с таким кодом
                            errors, kwargs = RedaktuKlastero.kontrolanta_argumentoj(root, info, **kwargs)

                            if not len(errors):
                                if 'laborspacoj_uuid' in kwargs:
                                    errors, message, laborspacoj = LaborspacojUUID_to_Laborspacoj(kwargs.get('laborspacoj_uuid'))

                            if not len(errors):

                                klasteroj.forigo = kwargs.get('forigo', klasteroj.forigo)
                                klasteroj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klasteroj.arkivo = kwargs.get('arkivo', klasteroj.arkivo)
                                klasteroj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klasteroj.publikigo = kwargs.get('publikigo', klasteroj.publikigo)
                                klasteroj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klasteroj.realeco = kwargs.get('realeco', klasteroj.realeco)
                                klasteroj.statuso = kwargs.get('statuso', klasteroj.statuso)
                                klasteroj.tipo = kwargs.get('tipo', klasteroj.tipo)
                                klasteroj.pozicio = kwargs.get('pozicio', klasteroj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klasteroj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klasteroj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        klasteroj.kategorio.set(kategorio)
                                    else:
                                        klasteroj.kategorio.clear()

                                if 'laborspacoj_uuid' in kwargs:
                                    klasteroj.laborspacoj.clear()
                                    if len(laborspacoj)>0:
                                        for laborspaco in laborspacoj:
                                            sci = KlasteroLaborspacoLigilo.objects.create(
                                                laborspaco=laborspaco["laborspaco"],
                                                klastero=klasteroj,
                                                pozicio=laborspaco["pozicio"] if laborspaco["pozicio"] else None,
                                                nomo=laborspaco["nomo"] if laborspaco["nomo"] else None,
                                                priskribo=laborspaco["priskribo"] if laborspaco["priskribo"] else None
                                            )
                                        klasteroj.laborspacoj.all()

                                klasteroj_save(klasteroj)
                                status = True

                                message = _('Запись успешно изменена')
                            else:
                                message = _('Nevalida argumentvaloroj')
                                for error in errors:
                                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                        error.message, _('в поле'), error.field)
                        except Klastero.DoesNotExist:
                            message = _('Запись не найдена')

        return status, message, errors, klasteroj

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        klasteroj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, klasteroj = RedaktuKlastero.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, klasteroj = RedaktuKlastero.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuKlastero',
                message=_('Требуется авторизация')
            ))

        return RedaktuKlastero(status=status, message=message, errors=errors, klasteroj=klasteroj)


# Модель типов владельцев кластеров
class RedaktuKlasteroPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klasteroj_posedantoj_tipoj = graphene.Field(KlasteroPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев кластеров'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klasteroj_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klasteroj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            klasteroj_posedantoj_tipoj = KlasteroPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klasteroj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klasteroj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            klasteroj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klasteroj_posedantoj_tipoj = KlasteroPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                klasteroj_posedantoj_tipoj.forigo = kwargs.get('forigo', klasteroj_posedantoj_tipoj.forigo)
                                klasteroj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klasteroj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', klasteroj_posedantoj_tipoj.arkivo)
                                klasteroj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klasteroj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', klasteroj_posedantoj_tipoj.publikigo)
                                klasteroj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klasteroj_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klasteroj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klasteroj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                klasteroj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroPosedantoTipo(status=status, message=message, klasteroj_posedantoj_tipoj=klasteroj_posedantoj_tipoj)


# Модель статусов владельцев кластеров
class RedaktuKlasteroPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klastero_posedantoj_statusoj = graphene.Field(KlasteroPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца кластера'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klastero_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klasteroj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            klastero_posedantoj_statusoj = KlasteroPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klastero_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klastero_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            klastero_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klastero_posedantoj_statusoj = KlasteroPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                klastero_posedantoj_statusoj.forigo = kwargs.get('forigo', klastero_posedantoj_statusoj.forigo)
                                klastero_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klastero_posedantoj_statusoj.arkivo = kwargs.get('arkivo', klastero_posedantoj_statusoj.arkivo)
                                klastero_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klastero_posedantoj_statusoj.publikigo = kwargs.get('publikigo', klastero_posedantoj_statusoj.publikigo)
                                klastero_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klastero_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klastero_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klastero_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                klastero_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroPosedantoStatuso(status=status, message=message, klastero_posedantoj_statusoj=klastero_posedantoj_statusoj)


# Модель владельцев кластеров
class RedaktuKlasteroPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klastero_posedantoj = graphene.Field(KlasteroPosedantoNode, 
        description=_('Созданный/изменённый владелец кластера'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        klastero_uuid = graphene.String(description=_('Кластер'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец кластера'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец кластера'))
        tipo_id = graphene.Int(description=_('Тип владельца кластера'))
        statuso_id = graphene.Int(description=_('Статус владельца кластера'))


    @staticmethod
    def kontrolanta_argumentoj(root, info, **kwargs):
        """Проверка аргументов
        Что бы не проверять аргументы в create и в edit, проверим отдельно

        Args:
            дублируем как и в create и в edit
        """
        # возвращаемые данные:
        errors = list()
        # проверяем наличие записей с таким кодом
        if ('posedanto_uzanto' not in kwargs) and ('posedanto_uzanto_id' in kwargs):
            try:
                posedanto_uzanto = Uzanto.objects.get(
                    id=kwargs.get('posedanto_uzanto_id'), 
                    is_active=True,
                    konfirmita=True
                )
                kwargs['posedanto_uzanto'] = posedanto_uzanto
            except Uzanto.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_uzanto_id',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный владелец/пользователь кластера')
                ))
        if ('posedanto_organizo' not in kwargs) and ('posedanto_organizo_uuid' in kwargs):
            try:
                posedanto_organizo = Organizo.objects.get(
                    uuid=kwargs.get('posedanto_organizo_uuid'),
                    forigo=False,
                    arkivo=False,  
                    publikigo=True
                )
                kwargs['posedanto_organizo'] = posedanto_organizo
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_organizo_uuid',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверная организация владелец кластера')
                ))
        if ('realeco' not in kwargs) and ('realeco_id' in kwargs):
            try:
                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                arkivo=False, publikigo=True)
                kwargs['realeco'] = realeco
            except Realeco.DoesNotExist:
                errors.append(ErrorNode(
                    field='realeco_id',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный параллельный мир')
                ))
        if ('tipo' not in kwargs) and ('tipo_id' in kwargs):
            try:
                tipo = KlasteroPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                arkivo=False, publikigo=True)
                kwargs['tipo'] = tipo
            except KlasteroPosedantoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='tipo_id',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный тип владельца кластера')
                ))
        if ('statuso' not in kwargs) and ('statuso_id' in kwargs):
            try:
                statuso = KlasteroPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                arkivo=False, publikigo=True)
                kwargs['statuso'] = statuso
            except KlasteroPosedantoStatuso.DoesNotExist:
                errors.append(ErrorNode(
                    field='statuso_id',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный статус владельца кластера')
                ))
        if ('klastero' not in kwargs) and ('klastero_uuid' in kwargs):
            try:
                klastero = Klastero.objects.get(uuid=kwargs.get('klastero_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                kwargs['klastero'] = klastero
            except Klastero.DoesNotExist:
                errors.append(ErrorNode(
                    field='klastero_uuid',
                    modelo='RedaktuKlasteroPosedanto',
                    message=_('Неверный кластер')
                ))
        return errors, kwargs


    @staticmethod
    def create(root, info, **kwargs):
        """
        Можно вызвать с добавленными параметрами:
        klastero - сслыка на объект модели Klastero
        posedanto_perm - разрешение на создание объекта
        posedanto_uzanto
        realeco

        """
        status = False
        errors = list()
        message = None
        klastero_posedantoj = None
        klastero = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца кластера

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_klasteroj_posedantoj')
                if 'klastero' in kwargs:
                    klastero = kwargs.get('klastero')
                    if not posedanto_perm and 'posedanto_perm' in kwargs:
                        posedanto_perm = kwargs.get('posedanto_perm')
                elif 'klastero_uuid' in kwargs:
                    try:
                        klastero = Klastero.objects.get(uuid=kwargs.get('klastero_uuid'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except Klastero.DoesNotExist:
                        errors.append(ErrorNode(
                            field='klastero_uuid',
                            message=_('Неверное рабочее пространство')
                        ))
                else:
                    errors.append(ErrorNode(
                        field='klastero_uuid',
                        message=_('Поле обязательно для заполнения')
                    ))
                if not posedanto_perm and klastero:
                    # владелец пространства имеет полные права
                    # если подключают несколько пространств, то должен быть владельцем всех пространств.
                    # если не является владельцем какого либо пространства, то идёт ошибка
                    for laborspaco in klastero.laborspacoj.all():
                        posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_klasteroj_posedantoj', laborspaco)
                        if not posedanto_perm:
                            break
                if posedanto_perm and not len(errors):
                    # Проверяем наличие всех полей
                    if 'forigo' in kwargs:
                        errors.append(ErrorNode(
                            field='forigo',
                            message=_('При создании записи не допустимо указание поля')
                        ))

                    if 'arkivo' in kwargs:
                        errors.append(ErrorNode(
                            field='arkivo',
                            message=_('При создании записи не допустимо указание поля')
                        ))
                    
                    # проверяем наличие записей с таким кодом и добавляем значения по умолчанию
                    errors, kwargs = RedaktuKlasteroPosedanto.kontrolanta_argumentoj(root, info, **kwargs)
                    # Проверяем наличие обязательных для заполнения полей
                    if 'tipo' not in kwargs:
                        errors.append(ErrorNode(
                            field='tipo_id',
                            message=_('Поле обязательно для заполнения')
                        ))
                    if 'statuso' not in kwargs:
                        errors.append(ErrorNode(
                            field='statuso_id',
                            message=_('Поле обязательно для заполнения')
                        ))

                    if not len(errors):
                        klastero_posedantoj = KlasteroPosedanto.objects.create(
                            forigo = False,
                            arkivo = False,
                            realeco = kwargs.get('realeco'),
                            klastero = kwargs.get('klastero'),
                            posedanto_uzanto = kwargs.get('posedanto_uzanto'),
                            posedanto_organizo = kwargs.get('posedanto_organizo'),
                            tipo = kwargs.get('tipo'),
                            statuso = kwargs.get('statuso'),
                            publikigo = kwargs.get('publikigo', False),
                            publikiga_dato = timezone.now()
                        )

                        klastero_posedantoj.save()

                        status = True
                        message = _('Запись создана')
                    else:
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                elif len(errors):
                    message = _('Недостаточно прав')
                    errors.append(ErrorNode(
                        modelo='RedaktuDokumentoContract',
                        message=_('Недостаточно прав')
                    ))
                else:
                    message = _('Nevalida argumentvaloroj')
                    for error in errors:
                        message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                            error.message, _('в поле'), error.field)
        else:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuDokumentoContract',
                message=_('Требуется авторизация')
            ))
            message = _('Требуется авторизация')

        return status, message, errors, klastero_posedantoj

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        klasteroj = None
        realeco = None
        uzanto = info.context.user
        if not uzanto.is_authenticated:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuKlasteroPosedanto',
                message=_('Требуется авторизация')
            ))
            message = _('Требуется авторизация')
            return status, message, errors, klasteroj
        with transaction.atomic():
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('klastero_uuid', False)):
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuKlasteroPosedanto',
                            message=_('Не задано ни одно поле для изменения')
                        ))

                    if not len(errors):
                        # Ищем запись для изменения
                        try:
                            klastero_posedantoj = KlasteroPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # владелец пространства имеет полные права
                            posedanto_perm = LaborspacoPosedanto.objects.filter(laborspaco=klastero_posedantoj.klastero.laborspaco,
                                                                                posedanto_uzanto=uzanto, arkivo=False,
                                                                                forigo=False, publikigo=True).count()

                            if (not (uzanto.has_perm('laborspacoj.povas_forigi_klasteroj_posedantoj') or posedanto_perm)
                                    and kwargs.get('forigo', False)):
                                errors.append(ErrorNode(
                                    field='',
                                    modelo='RedaktuKlasteroPosedanto',
                                    message=_('Не Недостаточно прав для удаления')
                                ))
                            elif not (uzanto.has_perm('laborspacoj.povas_shanghi_klasteroj_posedantoj') or posedanto_perm):
                                errors.append(ErrorNode(
                                    field='',
                                    modelo='RedaktuKlasteroPosedanto',
                                    message=_('Недостаточно прав для изменения')
                                ))

                            if not len(errors):
                                # проверяем наличие записей с таким кодом и добавляем значения по умолчанию
                                errors, kwargs = RedaktuKlasteroPosedanto.kontrolanta_argumentoj(root, info, **kwargs)

                            if not len(errors):

                                klastero_posedantoj.forigo = kwargs.get('forigo', klastero_posedantoj.forigo)
                                klastero_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klastero_posedantoj.arkivo = kwargs.get('arkivo', klastero_posedantoj.arkivo)
                                klastero_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klastero_posedantoj.publikigo = kwargs.get('publikigo', klastero_posedantoj.publikigo)
                                klastero_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klastero_posedantoj.realeco = kwargs.get('realeco', klastero_posedantoj.realeco) 
                                klastero_posedantoj.klastero = kwargs.get('klastero', klastero_posedantoj.klastero)
                                klastero_posedantoj.posedanto_uzanto = kwargs.get('posedanto_uzanto', klastero_posedantoj.posedanto_uzanto)
                                klastero_posedantoj.posedanto_organizo = kwargs.get('posedanto_organizo', klastero_posedantoj.posedanto_organizo)
                                klastero_posedantoj.statuso = kwargs.get('statuso', klastero_posedantoj.statuso)
                                klastero_posedantoj.tipo = kwargs.get('tipo', klastero_posedantoj.tipo)

                                klastero_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                            else:
                                message = _('Nevalida argumentvaloroj')
                                for error in errors:
                                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                        error.message, _('в поле'), error.field)
                        except KlasteroPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
                            errors.append(ErrorNode(
                                field='uuid',
                                modelo='KlasteroPosedanto',
                                message=_('Запись не найдена')
                            ))

        return status, message, errors, klastero_posedantoj

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        klastero_posedantoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, klastero_posedantoj = RedaktuKlasteroPosedanto.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, klastero_posedantoj = RedaktuKlasteroPosedanto.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuKlasteroPosedanto',
                message=_('Требуется авторизация')
            ))

        return RedaktuKlasteroPosedanto(status=status, message=message, errors=errors, klastero_posedantoj=klastero_posedantoj)


# Модель добавления множества кластеров с их владельцами
class RedaktuKreiKlasterojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klasteroj = graphene.Field(graphene.List(KlasteroNode), 
        description=_('Создание списка кластеров с владельцами'))

    class Arguments:
        # параметры, одинаковые для всех кластеров
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        laborspaco_uuid = graphene.String(description=_('Рабочее пространство в которое входит кластер'))
        tipo_id = graphene.Int(description=_('Тип кластеров)'))
        # параметры кластеров в перечне (могут быть разные для разных кластеров)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий кластеров')))
        statuso_id = graphene.List(graphene.Int, description=_('Статус кластеров'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владелец кластеров
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца кластеров'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца кластеров'))
        posedanto_uzanto_id = graphene.Int(description=_('ID владельца-лица кластеров'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация-владелец кластеров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klasteroj = []
        realeco = None
        laborspaco = None
        kategorio = KlasteroKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_organizo = None
        posedanto_uzanto = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца кластера

        if uzanto.is_authenticated:
            with transaction.atomic():
                # получаем пользователя, для которого создаётся кластер
                # обязательно наличие либо владельца, либо организация-владельца
                if not (('posedanto_organizo_uuid' in kwargs) or ('posedanto_uzanto_id' in kwargs)):
                    message = _('одно из полей обязательно: posedanto_uzanto_id или posedanto_organizo_uuid')
                elif 'posedanto_uzanto_id' in kwargs:
                    try:
                        posedanto_uzanto = Uzanto.objects.get(
                            id=kwargs.get('posedanto_uzanto_id'), 
                            is_active=True,
                            konfirmita=True
                        )
                    except Uzanto.DoesNotExist:
                        if not (uzanto.has_perm('laborspacoj.povas_krei_klasteroj')):
                            message = _('Недостаточно прав')
                        else:
                            message = _('Неверный владелец/пользователь кластера')
                # если пользователь владелец кластера, по которому создаётся кластер, то можно создавать
                # проверяем по полю posedanto
                if not (uzanto.has_perm('laborspacoj.povas_krei_klasteroj')) and not message:
                    if posedanto_uzanto == uzanto:
                        posedanto_perm = uzanto
                if uzanto.has_perm('laborspacoj.povas_krei_klasteroj') or posedanto_perm:
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif not len(kwargs.get('kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории кластеров'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'laborspaco_uuid' in kwargs:
                            try:
                                laborspaco = Laborspaco.objects.get(uuid=kwargs.get('laborspaco_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Laborspaco.DoesNotExist:
                                message = _('Неверное рабочее пространство в который входит кластер')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'laborspaco_uuid')

                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = KlasteroTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KlasteroTipo.DoesNotExist:
                                message = _('Неверный тип кластера')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')


                    if not message:
                        if 'posedanto_tipo_id' in kwargs:
                            try:
                                posedanto_tipo = KlasteroPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except KlasteroPosedantoTipo.DoesNotExist:
                                message = _('Неверный тип владельца кластеров')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_tipo_id')

                    if not message:
                        if 'posedanto_statuso_id' in kwargs:
                            try:
                                posedanto_statuso = KlasteroPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except KlasteroPosedantoStatuso.DoesNotExist:
                                message = _('Неверный статус владельца кластеров')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')


                    if not kwargs.get('pozicio',False):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    if (not message) and kwargs.get('pozicio',False):
                        pozicioj = kwargs.get('pozicio')
                        statusoj = kwargs.get('statuso_id')
                        i = 0
                        for pozic in pozicioj:
                            klastero = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KlasteroStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KlasteroStatuso.DoesNotExist:
                                        message = _('Неверный статус кластеров')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                            else:
                                break

                            if not message:
                                klastero = Klastero.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    laborspaco = laborspaco,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klastero.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klastero.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = KlasteroKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)

                                    if kategorio:
                                        klastero.kategorio.set(kategorio)
                                    else:
                                        klastero.kategorio.clear()

                                klasteroj_save(klastero)
                                klasteroj.append(klastero)

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец кластера')

                                if not message:
                                    klastero_posedantoj = KlasteroPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        klastero = klastero,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_organizo = posedanto_organizo,
                                        posedanto_uzanto = posedanto_uzanto,
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )

                                    klastero_posedantoj.save()

                                status = True
                            
                            i += 1

                        if status:
                            message = _('Запись создана')
                else:
                    message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiKlasterojPosedanto(status=status, message=message, klasteroj=klasteroj)


# Модель добавления рабочего пространства с владельцем и множества кластеров с их владельцами
class RedaktuKreiLaborspacoKlasterojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    laborspaco = graphene.Field(LaborspacoNode, 
        description=_('Создание рабочего пространства с владельцем и списка кластеров с владельцами'))
    klasteroj = graphene.Field(graphene.List(KlasteroNode), 
        description=_('Создание списка кластеров с владельцами'))

    class Arguments:
        # параметры, одинаковые для рабочего пространства и всех кластеров
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # параметры для рабочего пространства
        prj_nomo = graphene.String(description=_('Название'))
        prj_priskribo = graphene.String(description=_('Описание'))
        prj_kategorio = graphene.List(graphene.Int,description=_('Список категорий рабочих пространств'))
        prj_tipo_id = graphene.Int(description=_('Тип рабочих пространств'))
        prj_statuso_id = graphene.Int(description=_('Статус рабочих пространств'))
        # параметры для создания владельца рабочего пространства
        posedanto_uzanto_id = graphene.Int(description=_('ID владельца-лица рабочего пространства'))
        prj_posedanto_organizo_uuid = graphene.String(description=_('Организация-владелец рабочего пространства'))
        prj_posedanto_tipo_id = graphene.Int(description=_('Тип владельца рабочего пространства'))
        prj_posedanto_statuso_id = graphene.Int(description=_('Статус владельца рабочего пространства'))
        # параметры, одинаковые для всех кластеров
        tipo_id = graphene.Int(description=_('Тип кластеров'))
        stokejo_uuid = graphene.String(description=_('Место хранения при передаче объектов'))
        # параметры кластеров в перечне (могут быть разные для разных кластеров)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий кластеров')))
        statuso_id = graphene.List(graphene.Int, description=_('Статус кластеров'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владелец кластеров
        # владельцев может быть много
        posedanto_tipo_id = graphene.List(graphene.Int, description=_('Тип владельца кластеров'))
        posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца кластеров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        message_krei = None
        laborspaco = None
        posedanto_organizo = None
        posedanto_uzanto = None
        klasteroj = []
        realeco = None
        kategorio = KlasteroKategorio.objects.none()
        prj_kategorio = LaborspacoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца кластера

        if uzanto.is_authenticated:
            with transaction.atomic():
                posedanto_perm = uzanto.has_perm('laborspacoj.povas_krei_laborspacoj')
                if not kwargs.get('pozicio',False):
                    message = '{} "{}"'.format(_('Поле обязательно для заполнения (должен быть хотя бы один кластер)'),'pozicio')
                # получаем пользователя, для которого создаётся пространство
                if not message:
                    # обязательно наличие либо владельца, либо организация-владельца
                    if not (('posedanto_organizo_uuid' in kwargs) or ('posedanto_uzanto_id' in kwargs)):
                        message = _('одно из полей обязательно: posedanto_uzanto_id или posedanto_organizo_uuid')
                    elif 'posedanto_uzanto_id' in kwargs:
                        try:
                            posedanto_uzanto = Uzanto.objects.get(
                                id=kwargs.get('posedanto_uzanto_id'), 
                                is_active=True,
                                konfirmita=True
                            )
                        except Uzanto.DoesNotExist:
                            if not posedanto_perm:
                                message = _('Недостаточно прав')
                            else:
                                message = _('Неверный владелец/пользователь рабочего пространства')
                # если пользователь владелец пространства, по которому создаётся рабочее пространство, то можно создавать
                # проверяем по полю posedanto
                if not posedanto_perm and not message:
                    if posedanto_uzanto == uzanto:
                        posedanto_perm = True
                if posedanto_perm:
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('prj_nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_nomo')

                    if not message:
                        if 'prj_kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_kategorio')
                        elif not len(kwargs.get('prj_kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'prj_kategorio')
                        else:
                            kategorio_id = set(kwargs.get('prj_kategorio'))

                            if len(kategorio_id):
                                prj_kategorio = LaborspacoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(prj_kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории рабочих пространств'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'prj_tipo_id' in kwargs:
                            try:
                                tipo = LaborspacoTipo.objects.get(id=kwargs.get('prj_tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except LaborspacoTipo.DoesNotExist:
                                message = _('Неверный тип рабочих пространств')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_tipo_id')

                    if not message:
                        if 'prj_statuso_id' in kwargs:
                            try:
                                statuso = LaborspacoStatuso.objects.get(id=kwargs.get('prj_statuso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except LaborspacoStatuso.DoesNotExist:
                                message = _('Неверный статус рабочих пространств')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_statuso_id')

                    if not message:
                        laborspaco = Laborspaco.objects.create(
                            forigo=False,
                            arkivo=False,
                            realeco = realeco,
                            tipo = tipo,
                            statuso = statuso,
                            publikigo = True,
                            publikiga_dato = timezone.now()
                        )

                        if (kwargs.get('prj_nomo', False)):
                            set_enhavo(laborspaco.nomo, kwargs.get('prj_nomo'), info.context.LANGUAGE_CODE)
                        if (kwargs.get('prj_priskribo', False)):
                            set_enhavo(laborspaco.priskribo, 
                                        kwargs.get('prj_priskribo'), 
                                        info.context.LANGUAGE_CODE)
                        if 'prj_kategorio' in kwargs:
                            if prj_kategorio:
                                laborspaco.kategorio.set(prj_kategorio)
                            else:
                                laborspaco.kategorio.clear()

                        laborspaco.save()

                        status = True
                        # далее блок добавления владельца
                        if (kwargs.get('posedanto_uzanto_id', False) or kwargs.get('prj_posedanto_organizo_uuid', False)):
                            if not message:
                                if 'prj_posedanto_tipo_id' in kwargs:
                                    try:
                                        posedanto_tipo = LaborspacoPosedantoTipo.objects.get(id=kwargs.get('prj_posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца рабочего пространства'),'prj_posedanto_tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_tipo_id')

                            if not message:
                                if 'prj_posedanto_statuso_id' in kwargs:
                                    try:
                                        posedanto_statuso = LaborspacoPosedantoStatuso.objects.get(id=kwargs.get('prj_posedanto_statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except LaborspacoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца рабочего пространства'),'prj_posedanto_statuso_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_statuso_id')

                            if not message:
                                if 'prj_posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('prj_posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец рабочего пространства')

                            if not message:
                                laborspaco_posedantoj = LaborspacoPosedanto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco=realeco,
                                    laborspaco=laborspaco,
                                    posedanto_organizo = posedanto_organizo,
                                    posedanto_uzanto = posedanto_uzanto,
                                    tipo = posedanto_tipo,
                                    statuso = posedanto_statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now()
                                )

                                laborspaco_posedantoj.save()

                                status = True
                                message_krei = _('Записи рабочего пространства и владельца созданы')

                        if not message_krei:
                            message_krei = _('Запись рабочего пространства создана')

                pozicioj = kwargs.get('pozicio')
                if pozicioj and laborspaco and (not message) and (uzanto.has_perm('laborspacoj.povas_krei_klasteroj')):
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        else:
                            if len(kwargs.get('nomo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество наименований меньше количества позиций'),len(kwargs.get('nomo')),len(pozicioj))


                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')
                        else:
                            if len(kwargs.get('priskribo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество описаний меньше количества позиций'),len(kwargs.get('priskribo')),len(pozicioj))

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif len(kwargs.get('kategorio'))<len(pozicioj):
                            message = '{} "{} < {}"'.format(_('количество категорий меньше количества позиций'),len(kwargs.get('kategorio')),len(pozicioj))
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории кластеров'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    tipo = None
                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = KlasteroTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except KlasteroTipo.DoesNotExist:
                                message = _('Неверный тип кластеров')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                    if not message:
                        klastero_posedanto_tipo_id = set(kwargs.get('posedanto_tipo_id',[]))
                        if len(klastero_posedanto_tipo_id):
                            klastero_posedanto_tipo = KlasteroPosedantoTipo.objects.filter(id__in=klastero_posedanto_tipo_id, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(klastero_posedanto_tipo.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(klastero_posedanto_tipo_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие типы владельцев'),
                                                str(dif)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны типы владельцев'),
                                            'posedanto_tipo_id'
                            )

                    if not message:
                        posedanto_statuso_id = set(kwargs.get('posedanto_statuso_id',[]))
                        if len(posedanto_statuso_id):
                            posedanto_statuso = KlasteroPosedantoStatuso.objects.filter(id__in=posedanto_statuso_id, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(posedanto_statuso.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(posedanto_statuso_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие статусы владельцев'),
                                                str(dif)
                                )
                            elif len(posedanto_statuso_id)<len(klastero_posedanto_tipo):
                                message='{}: {}<{}'.format(
                                                _('количество статусов владельцев меньше количества типов владельцев кластеров'),
                                                len(posedanto_statuso_id),
                                                len(klastero_posedanto_tipo)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны статусы владельцев'),
                                            'posedanto_statuso_id'
                            )

                    if (not message) and (not kwargs.get('pozicio',False)):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    statusoj = kwargs.get('statuso_id')
                    if len(statusoj)<len(pozicioj):
                        message = '{} "{} < {}"'.format(_('количество статусов меньше количества позиций'),len(statusoj),len(pozicioj))
                    if (not message) and kwargs.get('pozicio',False):
                        i = 0
                        statuso = None
                        status = False
                        for pozic in pozicioj:
                            klastero = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = KlasteroStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KlasteroStatuso.DoesNotExist:
                                        message = _('Неверный статус кластеров')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                                    break
                            else:
                                break

                            if not message:
                                klastero = Klastero.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    laborspaco = laborspaco,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klastero.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klastero.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = KlasteroKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = KlasteroKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True, arkivo=False)

                                    if kategorio:
                                        klastero.kategorio.set(kategorio)
                                    else:
                                        klastero.kategorio.clear()

                                klasteroj_save(klastero)
                                klasteroj.append(klastero)

                                for posedanto_tipo in klastero_posedanto_tipo:
                                    klastero_posedantoj = KlasteroPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        klastero = klastero,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso[i],
                                        posedanto_organizo = posedanto_organizo,
                                        posedanto_uzanto = posedanto_uzanto,
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )
                                    klastero_posedantoj.save()
                                status = True
                            i += 1
                            if message:
                                break

                        if status and not message:
                            message_krei = '{}, {}'.format(message_krei,_('Запись кластеров создана'))
                elif (not laborspaco) and (not message):
                    message = _('Рабочее пространство не создано')
                elif not (uzanto.has_perm('laborspacoj.povas_krei_klasteroj') or posedanto_perm) and (not message):
                    message = _('Недостаточно прав')
                if not message:
                    message = message_krei
                elif message_krei:
                    message = "{}, и ошибка - {}".format(message_krei, message)
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiLaborspacoKlasterojPosedanto(
            status=status, 
            message=message, 
            laborspaco=laborspaco, 
            klasteroj=klasteroj
        )


# Модель типов связей рабочих пространств между собой
class RedaktuKlasteroLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klastero_ligilo_tipoj = graphene.Field(KlasteroLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей рабочих пространств между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klastero_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klastero_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            klastero_ligilo_tipoj = KlasteroLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(klastero_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(klastero_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            klastero_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klastero_ligilo_tipoj = KlasteroLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klastero_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klastero_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                klastero_ligilo_tipoj.forigo = kwargs.get('forigo', klastero_ligilo_tipoj.forigo)
                                klastero_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klastero_ligilo_tipoj.arkivo = kwargs.get('arkivo', klastero_ligilo_tipoj.arkivo)
                                klastero_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klastero_ligilo_tipoj.publikigo = kwargs.get('publikigo', klastero_ligilo_tipoj.publikigo)
                                klastero_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klastero_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(klastero_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(klastero_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                klastero_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroLigiloTipo(status=status, message=message, klastero_ligilo_tipoj=klastero_ligilo_tipoj)


# Модель связей рабочих пространств между собой
class RedaktuKlasteroLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    klastero_ligilo = graphene.Field(KlasteroLigiloNode, 
        description=_('Создание/изменение связи рабочих пространств между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID рабочего пространства владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемого рабочего пространства'))
        tipo_id = graphene.Int(description=_('ID типа связи рабочих пространств'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        klastero_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('laborspacoj.povas_krei_klastero_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Klastero.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Klastero.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный кластер'),'posedanto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = Klastero.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Klastero.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный кластер'),'ligilo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KlasteroLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KlasteroLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи кластеров между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            klastero_ligilo = KlasteroLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            klastero_ligilo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            klastero_ligilo = KlasteroLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('laborspacoj.povas_forigi_klastero_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('laborspacoj.povas_shanghi_klastero_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = Klastero.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Klastero.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный кластер'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = Klastero.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Klastero.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный кластер'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KlasteroLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KlasteroLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи кластеров между собой'),'tipo_id')

                            if not message:

                                klastero_ligilo.forigo = kwargs.get('forigo', klastero_ligilo.forigo)
                                klastero_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                klastero_ligilo.arkivo = kwargs.get('arkivo', klastero_ligilo.arkivo)
                                klastero_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                klastero_ligilo.publikigo = kwargs.get('publikigo', klastero_ligilo.publikigo)
                                klastero_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                klastero_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else klastero_ligilo.posedanto
                                klastero_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else klastero_ligilo.ligilo
                                klastero_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else klastero_ligilo.tipo

                                klastero_ligilo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KlasteroLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKlasteroLigilo(status=status, message=message, klastero_ligilo=klastero_ligilo)


class LaborspacojMutations(graphene.ObjectType):
    redaktu_laborspaco_kategorio = RedaktuLaborspacoKategorio.Field(
        description=_('''Создаёт или редактирует категории рабочих пространств''')
    )
    redaktu_laborspaco_tipo = RedaktuLaborspacoTipo.Field(
        description=_('''Создаёт или редактирует типы рабочих пространств''')
    )
    redaktu_laborspaco_statuso = RedaktuLaborspacoStatuso.Field(
        description=_('''Создаёт или редактирует статусы рабочих пространств''')
    )
    redaktu_laborspaco = RedaktuLaborspaco.Field(
        description=_('''Создаёт или редактирует рабочие пространства''')
    )
    redaktu_laborspaco_posedantoj_tipo = RedaktuLaborspacoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев рабочих пространств''')
    )
    redaktu_laborspaco_posedantoj_statuso = RedaktuLaborspacoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев рабочих пространств''')
    )
    redaktu_laborspaco_posedantoj = RedaktuLaborspacoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев рабочие пространства''')
    )
    redaktu_laborspaco_ligilo_tipo = RedaktuLaborspacoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей рабочих пространств между собой''')
    )
    redaktu_laborspaco_ligilo = RedaktuLaborspacoLigilo.Field(
        description=_('''Создаёт или редактирует связи рабочих пространств между собой''')
    )
    redaktu_klastero_kategorio = RedaktuKlasteroKategorio.Field(
        description=_('''Создаёт или редактирует категории кластеров''')
    )
    redaktu_klastero_tipo = RedaktuKlasteroTipo.Field(
        description=_('''Создаёт или редактирует типы кластеров''')
    )
    redaktu_klastero_statuso = RedaktuKlasteroStatuso.Field(
        description=_('''Создаёт или редактирует статусы кластеров''')
    )
    redaktu_klastero = RedaktuKlastero.Field(
        description=_('''Создаёт или редактирует кластер''')
    )
    redaktu_klastero_posedantoj_tipo = RedaktuKlasteroPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев кластеров''')
    )
    redaktu_klastero_posedantoj_statuso = RedaktuKlasteroPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев кластеров''')
    )
    redaktu_klastero_posedantoj = RedaktuKlasteroPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев кластеров''')
    )
    redaktu_krei_klastero_posedanto = RedaktuKreiKlasterojPosedanto.Field(
        description=_('''Создаёт список кластеров с владельцами''')
    )
    redaktu_krei_laborspaco_klastero_posedanto = RedaktuKreiLaborspacoKlasterojPosedanto.Field(
        description=_('''Создание рабочего пространства с владельцем и списка кластеров с владельцами''')
    )
    redaktu_klastero_ligilo_tipo = RedaktuKlasteroLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей кластеров между собой''')
    )
    redaktu_klastero_ligilo = RedaktuKlasteroLigilo.Field(
        description=_('''Создаёт или редактирует связи кластеров между собой''')
    )

