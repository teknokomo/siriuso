"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.forms import widgets
from informiloj.models import InformilojLingvo


class LingvoTextWidget(widgets.Widget):
    template_name = 'siriuso/forms/widgets/lingvo_text.html'

    def get_context(self, name, value, attrs):
        context = {
            'lingvoj': InformilojLingvo.objects.filter(forigo=False).order_by('nomo')
        }

        context.update(super(LingvoTextWidget, self).get_context(name, value,attrs))
        return context


class LingvoInputWidget(LingvoTextWidget):
    template_name = 'siriuso/forms/widgets/lingvo_input.html'
