"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
# Задаем переменную окружения, содержащую название файла настроек нашего проекта.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'siriuso.settings')

# создаем экземпляр приложения 
app = Celery('siriuso')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
# загружаем конфигурацию из настроек нашего проекта. Параметр namespace определяет префикс, который
# мы будем добавлять для всех настроек, связанных с Celery. Таким об-
# разом, в файле settings.py можно будет задавать конфигурацию Celery
# через настройки вида CELERY_, например CELERY_BROKER_URL
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
# вызываем процесс поиска и загрузки асинхронных задач по
# нашему проекту. Celery пройдет по всем приложениям, указанным в на-
# стройке INSTALLED_APPS, и попытается найти файл tasks.py, чтобы загру-
# зить код задач
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
