"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

# Middleware для корректного отражения языкового контекста запроса
def SiriusoAsgiUzantoLingvo(next_middleware, root, info, *args, **kwargs):
    """My custome GraphQL middleware."""
    # Invoke next middleware.
    from django.utils import translation

    def get_header(key, default=None):
        res = list(filter(lambda x: x[0].decode('utf-8').lower() == key.lower(), info.context.headers))
        return res[0][1].decode('utf-8') if len(res) else default

    info.context.LANGUAGE_CODE = translation.get_language()

    lang_header = get_header('x-client-lang')

    if lang_header:
        if translation.check_for_language(lang_header):
            translation.activate(lang_header)

            if hasattr(info.context, 'session'):
                info.context.session[translation.LANGUAGE_SESSION_KEY] = lang_header
                info.context.session.modified = True
                info.context.session.save()

    lang_header = get_header('x-content-lang')

    if lang_header and translation.check_for_language(lang_header):
            info.context.CONTENT_LANGUAGE_CODE = lang_header

    return next_middleware(root, info, *args, **kwargs)
