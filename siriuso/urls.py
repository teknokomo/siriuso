"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

"""siriuso URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  re_path(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  re_path(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  re_path(r'^blog/', include('blog.urls'))
"""

from django.urls import path, re_path, include #in new django 2.0 'url'=>'re_path'
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.http import response
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from django.contrib.auth import views
import pathlib

from registrado.api.schema import mutations

admin.autodiscover()
admin.site.enable_nav_sidebar = False


def graphiql(request):
    """Trivial view to serve the `graphiql.html` file."""
    del request
    graphiql_filepath = pathlib.Path(__file__).absolute().parent.parent / "main/templates/siriuso/graphiql.html"
    with open(graphiql_filepath) as f:
        return response.HttpResponse(f.read())


# GraphQL API
graphql_urls = [
    path('', GraphQLView.as_view(graphiql=True)),
    path('registrado/', csrf_exempt(GraphQLView.as_view(graphiql=True, schema=mutations))),
    path('jwt/', csrf_exempt(GraphQLView.as_view(graphiql=True))),
]
#     path('', GraphQLView.as_view(graphiql=settings.DEBUG)),
#     path('registrado/', csrf_exempt(GraphQLView.as_view(graphiql=settings.DEBUG, schema=mutations))),
# ]

# if settings.DEBUG:
graphql_urls.append(path('ws/', graphiql))

urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    # вход в админку
    path('admin/', admin.site.urls),
    # path('test/', main_views.Test.as_view(), name='test_page'),
    path('', include('django.contrib.auth.urls')),
    # GraphQL API
    path('api/v1.1/', include((graphql_urls, 'graphene_django'), namespace='api11')),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)