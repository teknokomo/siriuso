"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import json


class CallableEncoder(json.JSONEncoder):
    """
    Позволяет кодировать в JSON вызываемые объекты
    """
    def default(self, o):
        if callable(o):
            return o()

        return super().default(o)
