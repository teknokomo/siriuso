"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from graphene_permissions.mixins import AuthNode, AuthMutation
from graphene_permissions.permissions import AllowAuthenticated
from django.db.models import AutoField, IntegerField
from django.utils.translation import gettext_lazy as _
from graphql_relay.node.node import from_global_id
from graphene.utils.str_converters import to_camel_case
import graphene
import inspect
import re

from .types import ErrorNode
from .exceptions import *


class SiriusoAuthNode(AuthNode):
    permission_classes = (AllowAuthenticated,)


class SiriusoAuthMutation(AuthMutation):
    permission_classes = (AllowAuthenticated,)


class SiriusoObjectId:
    """
    Миксин добавляет поле objId, которое содержит оригенальное
    значение поля id модели, в которой id не является pk
    """
    obj_id = graphene.Int(source='id')


class SiriusoPermissions:
    rajtoj = graphene.List(graphene.String)

    @staticmethod
    def resolve_rajtoj(root, info, **kwargs):
        uzanto = info.context.user
        return uzanto.get_all_permissions(root)


class SiriusoMutation(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)

    # кэш соответствия класса, его методов мутаций и их флагов
    __subclass_cache__ = dict()

    # зарегистрированные действия
    actions = lambda: dict()

    @classmethod
    def mutate(cls, info, *args, **kwargs):
        # Определяем список доступных действий для мутации
        if isinstance(cls.actions, dict):
            if cls not in cls.__subclass_cache__:
                margs = set(cls._meta.arguments.keys())
                members = tuple(
                    member[0] for member in inspect.getmembers(
                            cls, predicate=lambda v: inspect.isfunction(v) or inspect.ismethod(v)
                    )
                )

                functions = sorted([
                    (func, conds) for func, conds in cls.actions.items()
                    if (func in members
                        and len(conds.get('args', tuple())) == len(set(conds.get('args', tuple())) & margs)
                        and len(conds.get('required', tuple())) == len(set(conds.get('required', tuple())) & margs))
                ], key=lambda v: len(v[1].get('args')), reverse=True)

                cls.__subclass_cache__[cls] = functions
            else:
                functions = cls.__subclass_cache__[cls]
        else:
            raise ValueError(
                'class "%s" attribute "actions": %s' % (cls.__name__, _('Значение должно быть типа "dict"'))
            )

        func_name = None
        default_func_name = None

        for func, conds in functions:
            if len(conds.get('args', tuple())):
                if len(conds.get('args')) == len(set(conds.get('args')) & set(kwargs.keys())):
                    func_name = func
                    errors = list()
                    # Проверяем наличие обязательных аргументов
                    if len(conds.get('required', tuple())) != len(set(conds.get('required')) & set(kwargs.keys())):
                        errors.extend([
                            ErrorNode(field=to_camel_case(arg), message=_('Аргумент должен быть задан'))
                            for arg in set(conds.get('required', tuple())) - set(kwargs.keys())
                        ])

                    # Проверяем отсутствие запрещённых аргментов
                    if len(set(conds.get('deny_args', tuple())) & set(kwargs.keys())):
                        errors.extend([
                            ErrorNode(field=to_camel_case(arg), message=_('Нельзя использовать данный аргумент'))
                            for arg in set(conds.get('deny_args', tuple())) & set(kwargs.keys())
                        ])

                    if len(errors):
                        return cls(status=False, message=_('Неверные значения аргументов'), errors=errors)

                    break

            elif not default_func_name:
                default_func_name = func
            else:
                print('%(cls)s: %(action)s - %(message)'.format(
                    cls=cls.__name__, action=func, message=_('действие не используется')
                ))

        if func_name or default_func_name:
            func_name = func_name or default_func_name
            return getattr(cls, func_name)(info, **kwargs)

        raise MutationActionNotFound('class "%s": %s' % (cls.__name__,
                                                         _('Для заданных параметров не удалось определить действие')))
