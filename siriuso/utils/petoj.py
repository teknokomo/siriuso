"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import current_app
from django.utils.translation import gettext_lazy as _
from main.models import SistemaPeto


class TaskDoesNotExistError(Exception):
    pass


def peti(tasko, autoro=None, args=None, kwargs=None):
    nomo_funk = {
        nomo: func
        for nomo, func in current_app.tasks.items()
        if not nomo.startswith('celery.') and (nomo == tasko or func == tasko)
    }

    if len(nomo_funk):
        tasko_nomo, tasko_funk = nomo_funk.popitem()
        funk_args = args or list()
        funk_kwargs = kwargs or dict()
        t = tasko_funk.apply_async(countdown=1, args=funk_args, kwargs=funk_kwargs)
        SistemaPeto.objects.create(
            tasko_id=t.id,
            autoro=autoro,
            tasko=tasko_nomo,
            statuso=t.status,
            parametroj={
                'args': args or list(),
                'kwargs': kwargs or dict(),
            }
        )
        return t
    else:
        raise TaskDoesNotExistError(_('Task "%s" does not exist') % str(tasko))
