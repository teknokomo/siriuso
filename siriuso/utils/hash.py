"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.core.files.uploadedfile import InMemoryUploadedFile, TemporaryUploadedFile
from django.core.files.base import ContentFile
from functools import partial
import hashlib


def get_hash(uploaded_file, algorithm):
    """
    Расчитывает хэш для загруженного файла при помощи указанного алгоритма
    """
    if isinstance(uploaded_file, (InMemoryUploadedFile, TemporaryUploadedFile, ContentFile)):
        buf_size = 1024 * 64
        hash_alg = getattr(hashlib, algorithm)()
        uploaded_file.seek(0)

        while True:
            data = uploaded_file.read(buf_size)

            if not data:
                break

            hash_alg.update(data)

        uploaded_file.seek(0)
        return hash_alg.hexdigest()

    return None


# MD5
get_md5 = partial(get_hash, algorithm='md5')
# SHA1
get_sha1 = partial(get_hash, algorithm='sha1')
# SHA224
get_sha224 = partial(get_hash, algorithm='sha224')
# SHA256
get_sha256 = partial(get_hash, algorithm='sha256')
# SHA384
get_sha384 = partial(get_hash, algorithm='sha384')
# SHA512
get_sha512 = partial(get_hash, algorithm='sha512')
