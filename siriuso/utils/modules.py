"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import hashlib
import secrets
import requests
import string
import random
import re
import json

from django.apps import apps
from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from django.db.models import Q
from django.utils.encoding import iri_to_uri
from django.http.request import HttpRequest
from channels_graphql_ws.scope_as_context import ScopeAsContext
from channels.http import AsgiRequest
from binascii import hexlify
from urllib.parse import quote, urlencode, urljoin, urlsplit

import os


SIRIUSO_TOKENS_KEY_PREFIX = 'siriuso.tokens'


def get_request_context(request):
    """
    Возвращает контекст текущего запроса (контекст для ключа: HTTP_USER_AGENT).
    Не путать с контекстом для Http ответа!
    :param request:
    :return: str or None
    """
    context = None
    if hasattr(request, 'META'):
        context = request.META.get('HTTP_USER_AGENT', None)

        if context is not None:
            m = hashlib.sha256()
            m.update(str(context).encode('utf-8'))
            context = m.hexdigest()

    return context


def generate_user_token(uzanto, request=None, validity=None, start_date=timezone.now(),
                        urlsafe=False, length=32):
    """
    Генерирует и возвращает в виде строки
    токен для аутентификации пользователя

    :param uzanto: Uzanto инстанс
    :param request: Текущий запрос
    :param validity: Период действия токена в секундах
    :param start_date: Дата и время начала действия токена
    :param urlsafe: Признак для генерации URL-безопасного токена
    :param length: Длина ключа от 8 до 64 символов
    :type uzanto: main.models.Uzanto
    :type request: WSGIRequest or None
    :type validity: int or None
    :type start_date: datetime.datetime
    :type urlsafe: bool
    :type length: int
    :return: Token or NoneType object
    :rtype: string or None
    """
    _len = max(min(length, 64), 8)
    token = secrets.token_hex(_len) if not urlsafe else secrets.token_urlsafe(_len)

    kwargs = {
        'uzanto': uzanto,
        'kunteksto': get_request_context(request) if request is not None else None,
        'fina_dato': start_date + timezone.timedelta(seconds=validity) if validity is not None else None,
        'shlosilo': hashlib.sha256(token.encode('utf-8')).hexdigest(),
    }

    if start_date:
        kwargs['starta_dato'] = start_date

    UzantaShlosilo = apps.get_model('main', 'UzantaShlosilo')
    uzanto_shlosilo = UzantaShlosilo(**kwargs)
    uzanto_shlosilo.save()
    return token


def get_user_by_token(request):
    """
    Возвращает пользователя по токену авторизации

    :param token:
    :param context:
    :type token: str
    :type context: str or None
    :return: Uzanto инстанс или None
    :rtype: main.models.Uzanto or None
    """
    uzanto = None

    if hasattr(request, 'META') and 'HTTP_X_AUTH_TOKEN' in request.META:
        UzantaShlosilo = apps.get_model('main', 'UzantaShlosilo')
        token = request.META.get('HTTP_X_AUTH_TOKEN')
        now = timezone.now()
        try:
            uzanta_shlosilo = (UzantaShlosilo.objects
                               .select_related('uzanto')
                               .get(Q(kunteksto__isnull=True) | Q(kunteksto='') |
                                    Q(kunteksto=get_request_context(request)),
                                    shlosilo=hashlib.sha256(token.encode('utf-8')).hexdigest()))
            # проверяем период действия
            if now <= (getattr(uzanta_shlosilo, 'fina_dato') or now):
                if now >= uzanta_shlosilo.starta_dato:
                    uzanto = uzanta_shlosilo.uzanto
            else:
                # удаляем ключ, т.к. срок действия истёк
                uzanta_shlosilo.delete()

        except UzantaShlosilo.DoesNotExist:
            pass
    return uzanto


def cancel_token(request):
    """
    Отменяет действие токена

    :param token: Токен авторизации пользователя
    :return: True or False
    :rtype: bool
    """
    token = request.META.get('HTTP_X_AUTH_TOKEN')
    UzantaShlosilo = apps.get_model('main', 'UzantaShlosilo')

    try:
        uzanta_shlosilo = (UzantaShlosilo.objects
                           .select_related('uzanto')
                           .get(shlosilo=hashlib.sha256(token.encode('utf-8')).hexdigest()))
        uzanta_shlosilo.delete()
    except UzantaShlosilo.DoesNotExist:
        return False

    return True


def set_token_session(token, request):
    token_key = '%s.%s' % (SIRIUSO_TOKENS_KEY_PREFIX, token)
    token_session = request.session.session_key
    cache.set(token_key, token_session, timeout=request.session.get_expiry_age())


def get_token_session(token):
    token_key = '%s.%s' % (SIRIUSO_TOKENS_KEY_PREFIX, token)
    session_key = cache.get(token_key)

    return session_key


# Значение полей Lingvo по умолчанию
def default_lingvo():
    return {
        'enhavo': [],
        'lingvo': {},
        'chefa_varianto': None
    }


# Возвращает нормализованный код локали
def lingvo_kodo_normaligo(kodo):
    if kodo and re.search(r'[-_]', kodo):
        kodoj = re.split(r'[-_]', kodo)
        normo = '{}_{}'.format(kodoj[0], kodoj[1].upper())
    elif not kodo:
        normo = settings.LANGUAGE_CODE
    else:
        normo = kodo

    return normo


# Возвращает кортеж c текстовым контентом в первой позиции и кодом локали во втором
def get_enhavo(enhavo_kampo, lingvo=None, empty_values=False):
    enhavo = None
    lingvo_enhavo = None
    lingvo_kodo = lingvo_kodo_normaligo(lingvo)

    if lingvo_kodo:
        lingvo_enhavo = lingvo_kodo if lingvo_kodo in enhavo_kampo['lingvo'] else enhavo_kampo.get('chefa_varianto')

        if lingvo_enhavo:
            idx = enhavo_kampo['lingvo'][lingvo_enhavo]
            enhavo = enhavo_kampo['enhavo'][idx]
    elif 'chefa_varianto' in enhavo_kampo and enhavo_kampo['chefa_varianto']:
        lingvo_enhavo = enhavo_kampo['chefa_varianto']
        idx = enhavo_kampo['lingvo'][lingvo_enhavo]

        if idx < len(enhavo_kampo['enhavo']):
            enhavo = enhavo_kampo['enhavo'][idx]

    if not lingvo_enhavo and not enhavo and empty_values:
        lingvo_enhavo = ''
        enhavo = ''

    return tuple([enhavo, lingvo_enhavo]) if enhavo is not None and lingvo_enhavo is not None else None


# Изменяет мультиязычное JSON поле
def set_enhavo(enhavo_kampo, teksto=None, lingvo=None, chefa=False):
    lingvo_kodo = lingvo_kodo_normaligo(lingvo)
    lingvo_enhavo = lingvo_kodo or enhavo_kampo['chefa_varianto']

    if lingvo_enhavo is None:
        return False

    if enhavo_kampo['chefa_varianto'] is None:
        enhavo_kampo['chefa_varianto'] = lingvo_enhavo

    if lingvo_enhavo in enhavo_kampo['lingvo']:
        idx = enhavo_kampo['lingvo'][lingvo_enhavo]

        if teksto:
            enhavo_kampo['enhavo'][idx] = teksto
        elif chefa:
            enhavo_kampo['chefa_varianto'] = lingvo_enhavo
        elif idx in enhavo_kampo['enhavo'] and lingvo_enhavo in enhavo_kampo['lingvo']:
            del enhavo_kampo['enhavo'][idx]
            del enhavo_kampo['lingvo'][lingvo_enhavo]

            for key, val in enhavo_kampo['lingvo'].items():
                if val > idx:
                    enhavo_kampo['lingvo'][key] = val - 1
    elif teksto:
        enhavo_kampo['lingvo'].update({lingvo_enhavo: len(enhavo_kampo['enhavo'])})
        enhavo_kampo['enhavo'].append(teksto)

    if chefa and lingvo_enhavo in enhavo_kampo['lingvo']:
        enhavo_kampo['chefa_varianto'] = lingvo_enhavo

    return True


# Возвращает кортеж c текстовым контентом в первой позиции и кодом локали во втором для описания изменений
def get_priskribo(enhavo_kampo, lingvo=None, empty_values=False):
    enhavo = None
    lingvo_enhavo = None
    lingvo_kodo = lingvo_kodo_normaligo(lingvo)

    if 'priskribo_chefa_varianto' in enhavo_kampo:
        if lingvo_kodo:
            lingvo_enhavo = (lingvo_kodo if lingvo_kodo in enhavo_kampo['priskribo_lingvo']
                             else enhavo_kampo.get('priskribo_chefa_varianto'))

            if lingvo_enhavo:
                idx = enhavo_kampo['priskribo_lingvo'][lingvo_enhavo]
                enhavo = enhavo_kampo['priskribo_enhavo'][idx]
        elif 'priskribo_chefa_varianto' in enhavo_kampo and enhavo_kampo['priskribo_chefa_varianto']:
            lingvo_enhavo = enhavo_kampo['priskribo_chefa_varianto']
            idx = enhavo_kampo['priskribo_lingvo'][lingvo_enhavo]

            if idx < len(enhavo_kampo['priskribo_enhavo']):
                enhavo = enhavo_kampo['priskribo_enhavo'][idx]

    if not lingvo_enhavo and not enhavo and empty_values:
        lingvo_enhavo = ''
        enhavo = ''

    return tuple([enhavo, lingvo_enhavo]) if enhavo is not None and lingvo_enhavo is not None else None


# Изменяет мультиязычное JSON поле с описанием изменений
def set_priskribo(enhavo_kampo, teksto=None, lingvo=None, chefa=False):
    if 'priskribo_chefa_varianto' not in enhavo_kampo:
        enhavo_kampo['priskribo_chefa_varianto'] = None

    if 'priskribo_lingvo' not in enhavo_kampo:
        enhavo_kampo['priskribo_lingvo'] = dict()

    if 'priskribo_enhavo' not in enhavo_kampo:
        enhavo_kampo['priskribo_enhavo'] = dict()

    lingvo_kodo = lingvo_kodo_normaligo(lingvo)
    lingvo_enhavo = lingvo_kodo or enhavo_kampo['priskribo_chefa_varianto']

    if lingvo_enhavo is None:
        return False

    if enhavo_kampo['priskribo_chefa_varianto'] is None:
        enhavo_kampo['priskribo_chefa_varianto'] = lingvo_enhavo

    if lingvo_enhavo in enhavo_kampo['priskribo_lingvo']:
        idx = enhavo_kampo['priskribo_lingvo'][lingvo_enhavo]

        if teksto:
            enhavo_kampo['priskribo_enhavo'][idx] = teksto
        elif chefa:
            enhavo_kampo['priskribo_chefa_varianto'] = lingvo_enhavo
        elif idx in enhavo_kampo['priskribo_enhavo'] and lingvo_enhavo in enhavo_kampo['priskribo_lingvo']:
            del enhavo_kampo['priskribo_enhavo'][idx]
            del enhavo_kampo['priskribo_lingvo'][lingvo_enhavo]

            for key, val in enhavo_kampo['priskribo_lingvo'].items():
                if val > idx:
                    enhavo_kampo['priskribo_lingvo'][key] = val - 1
    elif teksto:
        enhavo_kampo['priskribo_lingvo'].update({lingvo_enhavo: len(enhavo_kampo['priskribo_enhavo'])})
        enhavo_kampo['priskribo_enhavo'].update({len(enhavo_kampo['priskribo_enhavo']): teksto})

    if chefa and lingvo_enhavo in enhavo_kampo['priskribo_lingvo']:
        enhavo_kampo['priskribo_chefa_varianto'] = lingvo_enhavo

    return True


# Возвращает результат проверки reCaptcha google
# def check_reCaptcha(response, site=None):
#     print('=== site = ',site)
#     secret = settings.RECAPTCHA_PRIVATE_KEY

#     if site and isinstance(settings.RECAPTCHA_SITES, dict):
#         secret = settings.RECAPTCHA_SITES.get(site, secret)

#     data = {
#         'secret': secret,
#         'response': response
#     }
#     resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
#     resp = resp.json()
#     result = resp['success']

#     return result

# Возвращает результат проверки Captcha yandex
def check_reCaptcha(token, ip):
    resp = requests.get(
        "https://captcha-api.yandex.ru/validate",
        {
            "secret": settings.SMARTCAPTCHA_SERVER_KEY,
            "token": token,
            "ip": ip
        },
        timeout=1
    )
    server_output = resp.content.decode()
    if resp.status_code != 200:
        print(f"Allow access due to an error: code={resp.status_code}; message={server_output}", file=sys.stderr)
        return False
    return json.loads(server_output)["status"] == "ok"


# Генерация случайного названия и переименование загружаемых картинок
def dosiero_nomo(instance, filename, subdir=None):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    path_prefix = ('{}/'.format(instance._meta.app_label)
                   if not subdir else '{}/{}/'.format(instance._meta.app_label, subdir))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return path_prefix + '{0}.{1}'.format(rnd_string, filename.split('.')[-1])


def generate_hex_id(model_name):
    model = model_name.split('.')
    model = apps.get_model(model[0], model[1])

    while (True):
        hid = hexlify(os.urandom(8))

        try:
            model.objects.get(hid=hid)
        except model.DoesNotExist:
            return hid.decode()


def get_lang_kodo(request):
    return request.CONTENT_LANGUAGE_CODE if hasattr(request, 'CONTENT_LANGUAGE_CODE') else request.LANGUAGE_CODE


# Возвращает абсолютный путь для указанного адреса
def build_absolute_uri(request, location=None):
    if isinstance(request, (HttpRequest, AsgiRequest)):
        return request.build_absolute_uri(location)
    elif isinstance(request, ScopeAsContext):
        """request_meta = {h[0].decode('utf-8').upper(): h[1].decode('utf-8') for h in request.headers}

        if settings.USE_X_FORWARDED_HOST and (
                'X_FORWARDED_HOST' in request_meta):
            host = request_meta['X_FORWARDED_HOST']
        elif 'HOST' in request_meta:
            host = request_meta['HOST'].split(':')[:1]
        else:
            return None

        if settings.USE_X_FORWARDED_PORT and (
                'X_FORWARDED_PORT' in request_meta):
            port = request_meta['X_FORWARDED_PORT']
        elif 'HOST' in request_meta:
            port = request_meta['HOST'][1:]
        else:
            return None
        """
        return iri_to_uri(location)

    return None
