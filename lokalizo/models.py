"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
import random, string
from django.db import models
from django.db.models import Q, Max
from django.utils.translation import gettext_lazy as _
from main.models import SiriusoBazaAbstrakta2, SiriusoBazaAbstraktaKomunumoj, \
    SiriusoTipoAbstrakta, Uzanto
from django.urls import reverse
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo

import functools


# Локализация

# Переводчик
class LokalizoTradukisto(SiriusoBazaAbstrakta2):

    # переводчик
    # поле связи 1к1, одновременно и PK
    posedanto = models.OneToOneField(Uzanto, on_delete=models.CASCADE, unique=True)
    # tradukisto = models.OneToOneField(Uzanto, on_delete=models.CASCADE, primary_key=True)
    
    # уровень переводчика
    nivelo = models.IntegerField(_('La nivelo tradukisto'),default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'lokalizo_tradukisto'
        # читабельное название модели, в единственном числе
        verbose_name = _('Lokaligo tradukisto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Lokaligo tradukistoj')
        # права
        permissions = (
            ('povas_vidi_lokalizo_tradukiston', _('Povas vidi lokalizo tradukistoj')),
            ('povas_krei_lokalizo_tradukiston', _('Povas krei lokalizo tradukistoj')),
            ('povas_forigi_lokalizo_tradukiston', _('Povas forigu lokalizo tradukistoj')),
            ('povas_shanghi_lokalizo_tradukiston', _('Povas ŝanĝi lokalizo tradukistoj')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Типы элементов локализации, использует абстрактный класс SiriusoTipoAbstrakta
class LokalizoElementoTipo(SiriusoTipoAbstrakta):

    # название многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'lokalizo_elementoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de elementoj de lokalizo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de elementoj de lokalizo')
        # права
        permissions = (
            ('povas_vidi_lokalizo_elementon_tipon', _('Povas vidi lokalizo elementon tipon')),
            ('povas_krei_lokalizo_elementon_tipon', _('Povas krei lokalizo elementon tipon')),
            ('povas_forigi_lokalizo_elementon_tipon', _('Povas forigu lokalizo elementon tipon')),
            ('povas_shanghi_lokalizo_elementon_tipon', _('Povas ŝanĝi lokalizo elementon tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()

# Элементы локализации
class LokalizoElemento(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # тип элемента локализации
    tipo = models.ForeignKey(LokalizoElementoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код
    kodo = models.CharField(_('Kodo'), max_length=32)

    # оригинальный и/или переведённый текст многоязычный в JSON формате
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)
    
    # автор перевода или старший по переводу - autoro

    # количество голосов за данный перевод
    vochdoni = models.ManyToManyField(LokalizoTradukisto, verbose_name=_('nombro de voĉdonoj por la plej bona traduko'), 
                blank=True)
    
    #указатель на оригинальный текст
    originala = models.ForeignKey('self', verbose_name=_('Originala teksto'), blank=True, default=None,
                             on_delete=models.CASCADE)


    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'lokalizo_elementoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Elemento de lokalizo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Elementoj de lokalizo')
        # права
        permissions = (
            ('povas_vidi_lokalizo_elementon', _('Povas vidi lokalizo elementon')),
            ('povas_krei_lokalizo_elementon', _('Povas krei lokalizo elementon')),
            ('povas_forigi_lokalizo_elementon', _('Povas forigu lokalizo elementon')),
            ('povas_shanghi_lokalizo_elementon', _('Povas ŝanĝi lokalizo elementon')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id этой модели
        return '{}'.format(self.id)

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(LokalizoElemento, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                           update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения исследований
            all_perms = set(perms.user_registrita_perms(apps=('lokalizo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'lokalizo.povas_vidi_lokalizo_elementon', 'lokalizo.povas_krei_lokalizo_elementon',
                'lokalizo.povas_forigi_lokalizo_elementon', 'lokalizo.povas_shanghi_lokalizo_elementon'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='lokalizo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('lokalizo.povas_vidi_lokalizo_elementon')
                    or user_obj.has_perm('lokalizo.povas_vidi_lokalizo_elementon')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('lokalizo.povas_vidi_lokalizo_elementon'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
