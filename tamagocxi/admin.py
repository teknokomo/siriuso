"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from .models import *


# Типы Тамагочи
@admin.register(TamagocxiTipo)
class TamagocxiTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'kodo', 'uuid')
    exclude = ('nomo',)

    class Meta:
        model = TamagocxiTipo


# Степени Тамагочи
@admin.register(TamagocxiGrado)
class TamagocxiGradoAdmin(admin.ModelAdmin):
    list_display = ('grado', 'publikigo', 'uuid')

    class Meta:
        model = TamagocxiGrado


# Виды Тамагочи
@admin.register(TamagocxiSpeco)
class TamagocxiSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = TamagocxiSpeco


# Перечень Тамагочи (справочник Тамагочи со всеми характеристиками, ими будет награждение)
@admin.register(TamagocxiPremio)
class TamagocxiPremioAdmin(admin.ModelAdmin):
    list_display = ('speco', 'tipo', 'grado', 'publikigo', 'uuid')

    class Meta:
        model = TamagocxiPremio


# Условия награждения (справочник)
@admin.register(TamagocxiKondicho)
class TamagocxiKondichoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'publikigo', 'uuid')
    exclude = ('nomo', 'priskribo')

    class Meta:
        model = TamagocxiKondicho


# Награждение (перечень всех присвоений Тамагочи пользователям)
@admin.register(TamagocxiPremiado)
class TamagocxiPremiadoAdmin(admin.ModelAdmin):
    list_display = ('premiita', 'premio', 'publikigo', 'kondicho')

    class Meta:
        model = TamagocxiPremiado
