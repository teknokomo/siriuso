"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import shared_task
from django.utils.translation import gettext_lazy as _
from uzantoj.sciigoj import sendu_sciigon

from .models import TamagocxiPremiado

@shared_task
def sciigi_tamagocxi(premiado_uuid):
    try:
        premiado = (TamagocxiPremiado.objects.get(uuid=premiado_uuid, forigo=False))
        kondicho = premiado.kondicho
    except TamagocxiPremiado.DoesNotExist:
        return False

    uzantoj = {premiado.premiita.id,}
    
    # Получен новый Тамагочи за <условия награждения>
    teksto = 'Получен новый Тамагочи за %(nomo)s'
    parametroj = {'nomo': {'obj': kondicho, 'field': 'nomo'}}
    _('Получен новый Тамагочи за %(nomo)s')


    return sendu_sciigon(teksto, to=uzantoj, objektoj=(premiado,kondicho,), teksto_parametroj=parametroj)

    return 0
