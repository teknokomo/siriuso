"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene # сам Graphene
from graphene_django import DjangoObjectType # Класс описания модели Django для Graphene
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions # Миксины для описания типов  Graphene
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField # Коннектор для получения данных
from siriuso.api.types import SiriusoLingvo # Объект Graphene для представления мультиязычного поля
from ..models import * # модели приложения

# Типы наград
class TamagocxiTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа награды'))

    class Meta:
        model = TamagocxiTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Степени наград
class TamagocxiGradoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'grado': ['icontains',]
    }


    class Meta:
        model = TamagocxiGrado
        filter_fields = {
            'uuid': ['exact'],
            'grado': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)

# Виды наград
class TamagocxiSpecoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование вида награды'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание вида награды'))

    class Meta:
        model = TamagocxiSpeco
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (graphene.relay.Node,)

# Перечень наград (ими будет награждение)
class TamagocxiPremioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',]
    }

    class Meta:
        model = TamagocxiPremio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'speco__kodo': ['exact'],
            'speco__uuid': ['exact'],
            'tipo__kodo': ['exact'],
            'tipo__uuid': ['exact'],
            'grado__grado': ['exact'],
            'grado__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Условия награждения
class TamagocxiKondichoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование условия'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание условия'))

    class Meta:
        model = TamagocxiKondicho
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (graphene.relay.Node,)

# Награждение (перечень всех присвоений наград)
class TamagocxiPremiadoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains',]
    }

    class Meta:
        model = TamagocxiPremiado
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'premiita__id': ['exact'],
            'premiita__uuid': ['exact'],
            'premio__id': ['exact'],
            'premio__uuid': ['exact'],
            'kondicho__kodo': ['exact'],
            'kondicho__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

class TamagocxiQuery(graphene.ObjectType):
    tamagocxi_tipoj = SiriusoFilterConnectionField(TamagocxiTipoNode,
                                                          description=_('Выводит все доступные типы наград'))
    tamagocxi_gradoj = SiriusoFilterConnectionField(TamagocxiGradoNode,
                                                          description=_('Выводит все доступные степени наград'))
    tamagocxi_specoj = SiriusoFilterConnectionField(TamagocxiSpecoNode,
                                                          description=_('Выводит все доступные виды наград'))
    tamagocxi_premioj = SiriusoFilterConnectionField(TamagocxiPremioNode,
                                                          description=_('Выводит весь доступный перечень наград'))
    tamagocxi_kondichoj = SiriusoFilterConnectionField(TamagocxiKondichoNode,
                                                          description=_('Выводит все доступные условия награждения'))
    tamagocxi_premiadoj = SiriusoFilterConnectionField(TamagocxiPremiadoNode,
                                                          description=_('Выводит перечень всех присвоений наград'))
