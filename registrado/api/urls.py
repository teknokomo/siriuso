"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.urls import path, re_path, include
from registrado.api import views
from rest_framework import routers
from .views import *


#router = routers.SimpleRouter(trailing_slash=True)
#router.register(r'ensaluti', EnsalutiView, base_name='ensaluti')

urlpatterns = [
    path('ensaluti/', EnsalutiView.as_view(), name="ensaluti"),
    path('elsaluti/', ElsalutiView.as_view(), name="elsaluti"),
]
