$(function(){


	//Выбор нескольких чекбоксов
	//=================================================================
	$('.t_bazo_chiuj').each(function(){

		var	$e = $(this),
			$checkboxes = $e.find('.t_bazo_selection_checkbox[data-required="true"]'),
			$button = $e.find('#t_bazo_selection_submit');

		function checkSelection(){
			if(!$e.find('input.bad-value').length && $e.find('.t_bazo_selection_checkbox[data-required="true"] > input[type="checkbox"]:checked').length == $e.find('.t_bazo_selection_checkbox[data-required="true"]').length)
				$button.removeAttr('disabled');
			else
				$button.attr('disabled', 'disabled');
		}
			
		checkSelection();
			
		$checkboxes.each(function(){
			
			var	$c = $(this);
			
			$c.change(function(){
				checkSelection();
			});
			
		});
	
	});
	
	
});