"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.dispatch.dispatcher import Signal
from django.contrib.auth import authenticate, logout, login
from django.forms import forms
from main.models import Uzanto
from .tokens import UserToken, send_confirmation_token
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import requires_csrf_token, csrf_protect
from django.views.decorators.cache import never_cache
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.utils.http import is_safe_url
from django.urls import reverse
from datetime import datetime
from siriuso.views.decorators import auth_user_redirect
from siriuso.views import SiriusoTemplateView
import re
from django.utils import translation

from informiloj.models import InformilojLingvo


# Сигналы при регистрации и подтверждении регистрации
uzanto_registrita = Signal()
uzanto_konfirmita = Signal()
# uzanto_registrita = Signal(providing_args=['uzanto', 'request'])
# uzanto_konfirmita = Signal(providing_args=['uzanto', 'request'])
