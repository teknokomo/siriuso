"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.urls import path, re_path, include
from uzantoj.api.views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'uzantoj', UzantoView, base_name='uzantoj')

urlpatterns = [
    path('', include(router.urls)),
    path('mi/', MiView.as_view({'get': 'retrieve'}), name='uzanto_mi'),
]
