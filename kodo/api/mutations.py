"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import KodoPagxoTipoNode, KodoPagxoNode, KodoKategorioTipoNode, KodoKategorioNode
from ..models import *

class RedaktuKodoKategorioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio_tipo = graphene.Field(KodoKategorioTipoNode, description=_('Созданная/изменённая запись типа категории Энциклопедии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kodo.povas_krei_kategorian_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            KodoKategorioTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except KodoKategorioTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = KodoKategorioTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = KodoKategorioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kodo.povas_forigi_kategorion')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kodo.povas_shanghi_kategorion'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KodoKategorioTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKodoKategorioTipo(status=status, message=message, kategorio_tipo=tipo)

class RedaktuKodoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio = graphene.Field(KodoKategorioNode, description=_('Созданная/изменённая категории Академии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа категории'))
        kodo = graphene.String(description=_('Код'))
        nomo = graphene.String(description=_('Название категории'))
        priskribo = graphene.String(description=_('Описание категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kodo.povas_krei_kategorion'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('tipo_kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = KodoKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except KodoKategorioTipo.DoesNotExist:
                                message = _('Неверный тип страницы')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not (kwargs.get('kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            kategorio = KodoKategorio.objects.create(
                                kodo=kwargs.get('kodo'),
                                autoro=uzanto,
                                tipo=tipo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            kategorio.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('priskribo', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('kodo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = KodoKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except KodoKategorioTipo.DoesNotExist:
                            message = _('Неверный тип страницы')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            kategorio = KodoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kodo.povas_forigi_kategorion')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kodo.povas_shanghi_kategorion'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'tipo', 'kodo', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                kategorio.tipo = tipo or kategorio.tipo
                                kategorio.kodo = kwargs.get('kodo', kategorio.kodo)
                                kategorio.forigo = kwargs.get('forigo', kategorio.forigo)
                                kategorio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kategorio.arkivo = kwargs.get('arkivo', kategorio.arkivo)
                                kategorio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kategorio.publikigo = kwargs.get('publikigo', kategorio.publikigo)
                                kategorio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kategorio.lasta_autoro = uzanto
                                kategorio.lasta_dato = timezone.now()

                                if kwargs.get('priskribo', False):
                                    set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                if kwargs.get('nomo', False):
                                    set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')

                                kategorio.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except KodoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKodoKategorio(status=status, message=message, kategorio=kategorio)

class RedaktuKodoPagxoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo_tipo = graphene.Field(KodoPagxoTipoNode, description=_('Созданная/изменённая запись типа страницы Академии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kodo.povas_krei_pagxan_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            KodoPagxoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except KodoPagxoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = KodoPagxoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = KodoPagxoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kodo.povas_forigi_pagxan_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kodo.povas_shanghi_pagxan_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KodoPagxoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKodoPagxoTipo(status=status, message=message, pagxo_tipo=tipo)


class RedaktuKodoPagxo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    pagxo = graphene.Field(KodoPagxoNode, description=_('Созданная/изменённая страницы Академии'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа страницы'))
        kategorioj_id = graphene.List(graphene.String, 
            description=_('Список id категорий'))
        nomo = graphene.String(description=_('Название страницы'))
        teksto = graphene.String(description=_('Текст страницы'))
        kodo = graphene.String(description=_('Код'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        pagxo = None
        tipo = None
        novo_kategorioj = []
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kodo.povas_krei_pagxan'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = KodoPagxoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                         arkivo=False, publikigo=True)
                                except KodoPagxoTipo.DoesNotExist:
                                    message = _('Неверный тип страницы')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not (kwargs.get('kodo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if kwargs.get('kategorioj_id', False):
                                for kategorioj in kwargs.get('kategorioj_id'):
                                    try:
                                        novo_kategorioj.append(KodoKategorio.objects.get(id=kategorioj, forigo=False,
                                                                     arkivo=False, publikigo=True))
                                    except KodoKategorio.DoesNotExist:
                                        message = _('Неверная категория '+kategorioj)

                        if not message:
                            pagxo = KodoPagxo.objects.create(
                                kodo=kwargs.get('kodo'),
                                autoro=uzanto,
                                tipo=tipo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = []

                            if kwargs.get('teksto', False):
                                set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                update_fields.append('teksto')
                            if kwargs.get('nomo', False):
                                set_enhavo(pagxo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                update_fields.append('nomo')
                            pagxo.kategorio.set(novo_kategorioj)
                            pagxo.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('teksto', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('nomo', False) or kwargs.get('kategorioj_kodo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('kodo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = KodoPagxoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except KodoPagxoTipo.DoesNotExist:
                            message = _('Неверный тип страницы')

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('kategorioj_id', False):
                            for kategorioj in kwargs.get('kategorioj_id'):
                                try:
                                    novo_kategorioj.append(KodoKategorio.objects.get(id=kategorioj, forigo=False,
                                                                arkivo=False, publikigo=True))
                                except KodoKategorio.DoesNotExist:
                                    message = _('Неверная категория '+kategorioj)

                    # Ищем запись для изменения
                    if not message:
                        try:
                            pagxo = KodoPagxo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kodo.povas_forigi_pagxan')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kodo.povas_shanghi_pagxan'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'tipo', 'kodo', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                pagxo.kodo = kwargs.get('kodo', pagxo.kodo)
                                pagxo.tipo = tipo or pagxo.tipo
                                pagxo.forigo = kwargs.get('forigo', pagxo.forigo)
                                pagxo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                pagxo.arkivo = kwargs.get('arkivo', pagxo.arkivo)
                                pagxo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                pagxo.publikigo = kwargs.get('publikigo', pagxo.publikigo)
                                pagxo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                pagxo.lasta_autoro = uzanto
                                pagxo.lasta_dato = timezone.now()

                                if kwargs.get('nomo', False):
                                    set_enhavo(pagxo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')

                                if kwargs.get('teksto', False):
                                    set_enhavo(pagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                    update_fields.append('teksto')

                                pagxo.kategorio.set(novo_kategorioj)

                                pagxo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except KodoPagxo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKodoPagxo(status=status, message=message, pagxo=pagxo)


class KodoMutations(graphene.ObjectType):
    redaktu_kodo_kategorio_tipo = RedaktuKodoKategorioTipo.Field(
        description=_('''Создаёт или редактирует типы категорий кодекса''')
    )
    redaktu_kodo_kategorio = RedaktuKodoKategorio.Field(
        description=_('''Создаёт или редактирует категорию кодекса''')
    )
    redaktu_kodo_pagxo_tipo = RedaktuKodoPagxoTipo.Field(
        description=_('''Создаёт или редактирует типы страниц кодекса''')
    )
    redaktu_kodo_pagxo = RedaktuKodoPagxo.Field(
        description=_('''Создаёт или редактирует страницы кодекса''')
    )

