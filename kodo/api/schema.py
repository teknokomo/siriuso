"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene
from graphene_django import DjangoObjectType
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from graphene_permissions.permissions import AllowAny
from django.utils.translation import gettext_lazy as _
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from ..models import *

from siriuso.utils import lingvo_kodo_normaligo, get_lang_kodo, perms

from versioj.models import VersioKodoPagxo
from versioj.api.schema import VersioKodoPagxoNode

class KodoKategorioTipoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Тип категории энциклопедии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains', ]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа категории'))

    class Meta:
        model = KodoKategorioTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class KodoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    """
    Категория энциклопедии
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование категории'))
    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание категории'))

    class Meta:
        model = KodoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__kodo': ['exact', 'icontains', 'istartswith'],
            'tipo__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

class KodoPagxoTipoNode(SiriusoAuthNode, DjangoObjectType):
    """
    Тип страницы кодекса
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа страницы'))

    class Meta:
        model = KodoPagxoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


class KodoPagxoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    """
    Страница кодекса
    """
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование страницы'))
    teksto = graphene.Field(SiriusoLingvo, description=_('Текст страницы кодекса'))
    versioj = SiriusoFilterConnectionField(VersioKodoPagxoNode, description=_('Версии страницы'))

    class Meta:
        model = KodoPagxo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'tipo__kodo': ['exact', 'icontains', 'istartswith'],
            'tipo__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'kategorio__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioKodoPagxo

        perm_name = 'versioj.povas_vidi_kodo_pagxo_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

class KodoQuery(graphene.ObjectType):
    kodoj_kategorio_tipoj = SiriusoFilterConnectionField(KodoKategorioTipoNode,
                                                          description=_('Выводит все доступные типы категорий кодекса'))
    kodoj_kategorioj = SiriusoFilterConnectionField(KodoKategorioNode,
                                                    description=_('Выводит все доступные категории кодекса'))
    kodoj_pagxoj_tipoj = SiriusoFilterConnectionField(KodoPagxoTipoNode,
                                                          description=_('Выводит все доступные типы страниц кодекса'))
    kodoj_pagxoj = SiriusoFilterConnectionField(KodoPagxoNode,
                                                    description=_('Выводит все доступные страницы кодекса'))
