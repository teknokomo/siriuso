"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, Realeco
from main.models import Uzanto


# Типы организаций в Универсо, использует абстрактный класс UniversoBazaMaks
class OrganizoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='organizoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'organizoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de organizoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de organizoj')
        # права
        permissions = (
            ('povas_vidi_organizoj_tipoj', _('Povas vidi tipoj de organizoj')),
            ('povas_krei_organizoj_tipoj', _('Povas krei tipoj de organizoj')),
            ('povas_forigi_organizoj_tipoj', _('Povas forigi tipoj de organizoj')),
            ('povas_sxangxi_organizoj_tipoj', _('Povas ŝanĝi tipoj de organizoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(OrganizoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('organizoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'organizoj.povas_vidi_organizoj_tipoj', 
                'organizoj.povas_krei_organizoj_tipoj',
                'organizoj.povas_forigi_organizoj_tipoj',
                'organizoj.povas_sxangxi_organizoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='organizoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('organizoj.povas_vidi_organizoj_tipoj')
                    or user_obj.has_perm('organizoj.povas_vidi_organizoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('organizoj.povas_vidi_organizoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Организации в Универсо, использует абстрактный класс UniversoBazaMaks
class Organizo(UniversoBazaMaks):

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # тип организации Универсо
    tipo = models.ForeignKey(OrganizoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                            encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                 encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='organizoj_realeco_ligiloj')

    # ИНН
    inn = models.CharField(_('INN'), max_length=50, default=None, blank=True, null=True)

    # КПП
    kpp = models.CharField(_('KPP'), max_length=9, default=None, blank=True, null=True)

    # код 
    kodo = models.CharField(_('Kodo'), max_length=16, default=None, blank=True, null=True)

    # КодПоОКПО
    okpo = models.CharField(_('OKPO'), max_length=10, default=None, blank=True, null=True)

    # Налоговый Номер
    imposta_numero = models.CharField(_('Imposta Numero'), max_length=50, default=None, blank=True, null=True)

    # Регистрационный Номер
    registronumero = models.CharField(_('Registronumero'), max_length=50, default=None, blank=True, null=True)
    
    # Email Уведомлений
    email = models.CharField(_('E-Mail'), max_length=100, default=None, blank=True, null=True)

    # Свидетельство Серия Номер
    akto_serio_numero = models.CharField(_('Akto serio numero'), max_length=25, default=None, blank=True, null=True)

    # Свидетельство Дата Выдачи
    akto_dato_eligo = models.DateTimeField(_('Akto eligo de dato'), blank=True, null=True)

    # Дата Регистрации - дата
    dato_registrado = models.DateTimeField(_('Dato de registrado'), blank=True, null=True)

    # Дата Создания - дата
    dato_faro = models.DateTimeField(_('Dato de faro'), blank=True, null=True)

    # Таблицы:
    #     КонтактнаяИнформация
    #     ДополнительныеРеквизиты
    #     ИсторияКПП
    #     ИсторияНаименований
    #     ИсторияКонтактнойИнформации

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'organizoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Organizo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Organizoj')
        # права
        permissions = (
            ('povas_vidi_organizoj', _('Povas vidi organizoj')),
            ('povas_krei_organizoj', _('Povas krei organizoj')),
            ('povas_forigi_organizoj', _('Povas forigi organizoj')),
            ('povas_sxangxi_organizoj', _('Povas ŝanĝi organizoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('organizoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'organizoj.povas_vidi_organizoj', 
                'organizoj.povas_krei_organizoj',
                'organizoj.povas_forigi_organizoj',
                'organizoj.povas_sxangxi_organizoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='organizoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('organizoj.povas_vidi_organizoj')
                    or user_obj.has_perm('organizoj.povas_vidi_organizoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # если член организации, то выдаём по нему данные
                cond = Q(organizomembro__uzanto__id=user_obj.id)
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('organizoj.povas_vidi_organizoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Членство в организациях Универсо, использует абстрактный класс UniversoBazaMaks
class OrganizoMembro(UniversoBazaMaks):

    # организация Универсо
    organizo = models.ForeignKey(Organizo, verbose_name=_('Organizo'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # пользователь член организации
    uzanto = models.ForeignKey(Uzanto, verbose_name=_('Uzanto'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'organizoj_membroj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Membro de organizo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Membro de organizo')
        # права
        permissions = (
            ('povas_vidi_organizoj_membroj', _('Povas vidi membroj de organizoj')),
            ('povas_krei_organizoj_membroj', _('Povas krei membroj de organizoj')),
            ('povas_forigi_organizoj_membroj', _('Povas forigi membroj de organizoj')),
            ('povas_sxangxi_organizoj_membroj', _('Povas ŝanĝi membroj de organizoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{} {} {}'.format(self.uuid, self.uzanto, self.organizo)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('organizoj_membroj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'organizoj_membroj.povas_vidi_organizoj_membroj',
                'organizoj_membroj.povas_krei_organizoj_membroj',
                'organizoj_membroj.povas_forigi_organizoj_membroj',
                'organizoj_membroj.povas_sxangxi_organizoj_membroj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='organizoj_membroj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('organizoj_membroj.povas_vidi_organizoj_membroj')
                    or user_obj.has_perm('organizoj_membroj.povas_vidi_organizoj_membroj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('organizoj_membroj.povas_vidi_organizoj_membroj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
