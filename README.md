# Внимание!!! Репозиторий переехал!!!

В связи с усложнением использования GitLab с территории России, в том числе, из-за того, что GitLab препятствует регистрации новых пользователей из России, репозиторий перенесён на сервис МосХаб https://hub.mos.ru/teknokomo

**В данном репозитории устаревшая информация!!!**

# Siriuso #

Движок умной социальной сети Техноком - первого блока Всемирной автоматизированной системы Техноком

Чтобы присоединиться к работе над движком напишите, пожалуйста, нашей команде в Телеграмме через бот "Роботун": https://t.me/universo_pro_bot

## Описание структуры ##

Описание структуры движка находится в Вики соотвествующего мезопроекта https://dok.universo.pro/ru/latest/django-servilo/bazo/index.html

## Использование Docker ##

В корне проекта находится файлы:
- docker-compose.yaml.local - отвечает за запуск проекта на локальной машине разработчика через Django-webserver. Разработчик сразу видит свои изменения.
- docker-compose.yaml.server - отвечает за запуск проекта через gunicorn. Аналог серверного/серверный запуск.
Один из файлов переименовываем в docker-compose.yaml.

Настройки базы в settings.py
```
'HOST': 'postgres',
'PORT': '5432',
'NAME': 'siriuso',
'USER': 'siriuso',
'PASSWORD': 'oGLOWo8nd3',
```
После запуска будет развернуто 6 (шесть) контейнеров:
1. Postgresql с доступностом по localhost:5432
2. django с доступом по localhost:8000
    local - Django-webserver
     или
    server - запуск проекта через gunicorn
3. redis
4. rabbitmq
5. celery
6. flower

### Linux ###

На Linux потребуется установить docker и docker-compose

Для запуска нужно перейти в корень проекта и выполнить
```
sudo docker-compose up --build
```
После чего нужно подождать пока скачаются образы с docker hub и соберутся контейнеры

Для управления можно использовать те же комманды что и обычно с доступом в контейнер
```
sudo docker exec -it siriuso_app-django python3 manage.py
```
**Например:**
```
sudo docker exec -it siriuso_app-django python3 manage.py makemigrations  # Подготовка миграций
sudo docker exec -it siriuso_app-django python3 manage.py migrate         # Применение миграцй
sudo docker exec -it siriuso_app-django python3 manage.py createsuperuser # Создание супер пользователя
sudo docker exec -it siriuso_app-django python3 manage.py loaddata        # Загрузка начальных данных
```

### Windows ###

У кого Windows напишите инструкцию

### PyCharm ###
