"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from .apps_signals import konferenca_komento_poshtita, konferenca_komenti_forigita
from .tasks import *
from django.dispatch import receiver


@receiver(konferenca_komento_poshtita)
def komento_poshtita1(sender, komento, ago_uzanto, **kwargs):
    send_email_comment_publication.delay(komento.uuid)


@receiver(konferenca_komenti_forigita)
def komento_f(sender, komento, ago_uzanto, **kwargs):
    if komento.autoro != ago_uzanto:
        print('===> Работает АДМИН! <===')
