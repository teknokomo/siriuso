"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils.translation import gettext_lazy as _

from django.test import TestCase
from .models import KonferencojKategorioTipo

class KonferencojKategorioTipoTest(TestCase):
    '''тест полей KonferencojKategorioTipo'''
    
    # def setUpTestData(cls):
    @classmethod
    def setUpTestData(cls):
        KonferencojKategorioTipo.objects.create(nomo='Name')

    def test_first_name_label(self):
        tipo=KonferencojKategorioTipo.objects.get(nomo='Name')
        nomo = tipo._meta.get_field('nomo').verbose_name
        self.assertEquals(nomo,_('Имя'))
        # self.assertEquals(nomo,_('nomo'))

    def test_object_name_is_last_name_comma_first_name(self):
        tipo=KonferencojKategorioTipo.objects.get(nomo='Name')
        expected_object_name = '{}'.format(tipo.nomo)
        self.assertEquals(expected_object_name,'Name')
        # self.assertEquals(expected_object_name,str(tipo))

    