"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene # сам Graphene
from graphene_django import DjangoObjectType # Класс описания модели Django для Graphene
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions # Миксины для описания типов  Graphene
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField # Коннектор для получения данных
from siriuso.api.types import SiriusoLingvo # Объект Graphene для представления мультиязычного поля
from siriuso.utils import lingvo_kodo_normaligo, get_lang_kodo
from ..models import * # модели приложения

from versioj.models import VersioKonferencojTemoKomento, VersioKonferencojTemo, VersioKonferencojKategorio
from versioj.api.schema import VersioKonferencojTemoKomentoNode, VersioKonferencojTemoNode, VersioKonferencojKategorioNode


# Комментарии тем конференций групп
class KonferencojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('текст комментария'))
    versioj = SiriusoFilterConnectionField(VersioKonferencojTemoKomentoNode, description=_('Версии страницы'))

    class Meta:
        model = KonferencojTemoKomento
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'komento__id': ['exact'],
            'komento__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioKonferencojTemoKomento

        perm_name = 'versioj.povas_vidi_konferencoj_temon_komenton_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Типы тем конференций (справочник)
class KonferencojTemoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа конференции'))

    class Meta:
        model = KonferencojTemoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


# Темы конференций групп
class KonferencojTemoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название темы'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание темы'))

    komentoj = SiriusoFilterConnectionField(KonferencojTemoKomentoNode,
                                            description=_('Выводит список комментариев темы'))

    tuta_komentoj = graphene.Int(description=_('Общее количество комментариев в теме'))
    versioj = SiriusoFilterConnectionField(VersioKonferencojTemoNode, description=_('Версии тем'))

    class Meta:
        model = KonferencojTemo
        filter_fields = {
            'uuid': ['exact'],
            'fermita': ['exact'],
            'fiksa': ['exact'],
            'fiksa_listo': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'kategorio__posedanto__id': ['exact'],
            'kategorio__uuid': ['exact'],
            'kategorio__posedanto__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_komentoj(self, info, **kwargs):
        return KonferencojTemoKomento.objects.filter(posedanto=self)

    def resolve_tuta_komentoj(self, info, **kwargs):
        return KonferencojTemoKomento.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioKonferencojTemo

        perm_name = 'versioj.povas_vidi_konferencoj_temon_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


# Типы категорий тем (справочник)
class KonferencojKategorioTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа категории'))

    class Meta:
        model = KonferencojKategorioTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


# Категории тем групп
class KonferencojKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название категории'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание категории'))

    temoj = SiriusoFilterConnectionField(KonferencojTemoNode, description=_('Выводит список тем категории'))

    tuta_temoj = graphene.Int(description=_('Общее количество тем в категории'))

    versioj = SiriusoFilterConnectionField(VersioKonferencojKategorioNode, description=_('Версии категорий'))

    class Meta:
        model = KonferencojKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_temoj(self, info, **kwargs):
        return KonferencojTemo.objects.filter(kategorio=self)

    def resolve_tuta_temoj(self, info, **kwargs):
        return KonferencojTemo.objects.filter(kategorio=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioKonferencojKategorio

        perm_name = 'versioj.povas_vidi_konferencoj_kategorion_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


# Таблица подписок пользователей на Темы конференций
class KonferencojSciigojNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = KonferencojSciigoj
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class KonferencojQuery(graphene.ObjectType):
    konferencoj_kategorioj_tipoj = SiriusoFilterConnectionField(
        KonferencojKategorioTipoNode,
        description=_('Выводит все доступные типы категорий конференций')
    )
    konferencoj_kategorioj = SiriusoFilterConnectionField(
        KonferencojKategorioNode,
        description=_('Выводит все доступные категории конференций')
    )
    konferencoj_temoj_tipoj = SiriusoFilterConnectionField(
        KonferencojTemoTipoNode,
        description=_('Выводит все доступные типы тем конференций')
    )
    konferencoj_temoj = SiriusoFilterConnectionField(
        KonferencojTemoNode,
        description=_('Выводит все доступные темы конферений')
    )
    konferencoj_temoj_komentoj = SiriusoFilterConnectionField(
        KonferencojTemoKomentoNode,
        description=_('Выводит все доступные комментарии к темам конференций')
    )
    konferencoj_sciigoj = SiriusoFilterConnectionField(
       KonferencojSciigojNode,
       description=_('Выводит всех подписанных пользователей к темам конференций')
    )
