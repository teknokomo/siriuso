"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils.translation import gettext_lazy as _
from celery import shared_task
from uzantoj.sciigoj import sendu_sciigon
from mesagxilo.models import MesagxiloBabilejo
from main.models import Uzanto


@shared_task
def sciigi_mesagxilo(escepto_uuid, babilejo_uuid):
    babilejo = MesagxiloBabilejo.objects.get(uuid=babilejo_uuid, forigo=False,
                                            arkivo=False, publikigo=True)
    escepto = Uzanto.objects.get(uuid=escepto_uuid, is_active=True, konfirmita=True)
    uzantoj =  Uzanto.objects.filter(
                    mesagxilo_mesagxilopartoprenanto_partoprenanto__babilejo=babilejo,
                    mesagxilo_mesagxilopartoprenanto_partoprenanto__arkivo=False,
                    mesagxilo_mesagxilopartoprenanto_partoprenanto__forigo=False
                                ).exclude(
                                    id=escepto.id
                                )
                    
    # Пришло новое сообщение в чате <название_чата>
    teksto = 'Nova mesaĝo en konversacio %(nomo)s'
    parametroj = {'nomo': {'obj': babilejo, 'field': 'nomo'}}
    _('Nova mesaĝo en konversacio %(nomo)s')


    if uzantoj:
        uzantoj = list(uzantoj.values_list('id', flat=True))
        return sendu_sciigon(teksto, to=uzantoj, objektoj=(babilejo,), teksto_parametroj=parametroj)

    return 0
