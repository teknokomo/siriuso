"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ChangelogConfig(AppConfig):
    # Название модуля
    name = 'changelog'
    # Визуальное название, будет отображаться в админке, потому делаем переводимым
    verbose_name = _('Changelog')
