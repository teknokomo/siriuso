"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Q


from siriuso.utils import perms
from main.models import Uzanto, SiriusoBazaAbstrakta3
from objektoj.models import Objekto
from universo_bazo.models import Realeco

# пользователи подключены на websocket, т.е. в текущий момент играют/общаются
# и на какие подписки подписаны
class UniversoUzantojWebsocket(SiriusoBazaAbstrakta3):

    # владелец (пользователь)
    posedanto = models.OneToOneField(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # поднято соединение по вебсокету
    online = models.BooleanField(_('Online'), blank=True, null=True, default=False)

    # подписан на чат
    subscription_mesagxilo = models.BooleanField(_('Subscription mesagxilo'), blank=True, null=True, default=False)
    
    # подписан на станцию
    subscription_kosmostacio = models.BooleanField(_('Subscription kosmostacio'), blank=True, null=True, default=False)
    
    # на какую космостанцию подписан - объект Универсо
    kosmostacio = models.ForeignKey(Objekto, verbose_name=_('Kosmostacio'), blank=True, null=True, default=None,
                                on_delete=models.CASCADE)
    
    # подписан на космос
    subscription_kosmo = models.BooleanField(_('Subscription kosmo'), blank=True, null=True, default=False)

    # перечень кубов космоса, на которые подписан
    kuboj = models.CharField(_('Kuboj'), max_length=64, blank=True, null=True, default=False) 
    
    # на какой параллельный мир подписан
    realeco = models.ForeignKey(Realeco, verbose_name=_('Realeco de Universo'), blank=True, null=True,
                                on_delete=models.CASCADE)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'universo_uzantoj_websocket'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanto websocket')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantoj websocketoj')
        # права
        permissions = (
            ('povas_vidi_universo_uzantoj_websocket', _('Povas vidi uzantoj websocket de Universo')),
            ('povas_krei_universo_uzantoj_websocket', _('Povas krei uzantoj websocket de Universo')),
            ('povas_forigi_universo_uzantoj_websocket', _('Povas forigi uzantoj websocket de Universo')),
            ('povas_shangxi_universo_uzantoj_websocket', _('Povas ŝanĝi uzantoj websocket de Universo')),
        )

    def __str__(self):
        return "{} - {}".format(str(self.uuid), str(self.online))

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('universo_uzantoj_websocket',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'universo_uzantoj_websocket.povas_vidi_universo_uzantoj_websocket', 
                'universo_uzantoj_websocket.povas_krei_universo_uzantoj_websocket',
                'universo_uzantoj_websocket.povas_forigi_universo_uzantoj_websocket', 
                'universo_uzantoj_websocket.povas_shangxi_universo_uzantoj_websocket'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='universo_uzantoj_websocket', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('universo_uzantoj_websocket.povas_vidi_universo_uzantoj_websocket')
                    or user_obj.has_perm('universo_uzantoj_websocket.povas_vidi_universo_uzantoj_websocket')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                cond = Q(posedanto=user_obj.id)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('universo_uzantoj_websocket.povas_vidi_universo_uzantoj_websocket'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

