"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json
from django.contrib.admin.sites import AdminSite

from objektoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from siriuso.utils.admin_mixins import TekstoMixin


AdminSite.empty_value_display = '(None)'


class TekstoMixinPosedantoj(TekstoMixin):

    def posedantoj_(self, obj):
        posedantoj = ObjektoPosedanto.objects.filter(objekto=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if user.posedanto_uzanto: 
                if i:
                    idj = "{}".format(user.posedanto_uzanto.id)
                    i = False
                else:
                    idj = "{}; {}".format(idj, user.posedanto_uzanto.id)
            if user.posedanto_organizo: 
                if i:
                    idj = "{}".format(self.teksto(obj=user.posedanto_organizo, field='nomo'))
                    i = False
                else:
                    idj = "{}; {}".format(idj, self.teksto(obj=user.posedanto_organizo, field='nomo'))
        return idj
    posedantoj_.short_description = 'Владелец'

    def posedantoj__(self, obj):
        posedantoj = ObjektoLigilo.objects.filter(posedanto_stokejo=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if i:
                idj = "{}".format(user.posedanto)
                i = False
            else:
                idj = "{}; {}".format(idj, user.posedanto)
        return idj
    posedantoj__.short_description = 'Подсоединение к'


# Форма мест хранения в объектах
class ObjektoStokejoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=False)
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )
    class Meta:
        model = ObjektoStokejo
        fields = [field.name for field in ObjektoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Форма мест хранения в объектах
class ObjektoStokejoInlineFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoTextWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoStokejo
        fields = [field.name for field in ObjektoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владелец модели объекта
class ObjektoPosedantoInline(admin.TabularInline):
    model = ObjektoPosedanto
    fk_name = 'objekto'
    extra = 1

# Места хранения в объектах модели объекта
class ObjektoStokejoInline(admin.TabularInline):
    model = ObjektoStokejo
    fk_name = 'posedanto_objekto'
    # form = ObjektoStokejoInlineFormo
    extra = 1

# Владелец модели объекта
class ObjektoLigiloInline(admin.TabularInline):
    model = ObjektoLigilo
    fk_name = 'posedanto'
    extra = 1


# Форма объектов
class ObjektoSxablonoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoSxablono
        fields = [field.name for field in ObjektoSxablono._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Объекты
@admin.register(ObjektoSxablono)
class ObjektoSxablonoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoSxablonoFormo
    list_display = ('nomo_teksto','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = ObjektoSxablono


# Форма объектов
class ObjektoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = Objekto
        fields = [field.name for field in Objekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Объекты
@admin.register(Objekto)
class ObjektoAdmin(admin.ModelAdmin, TekstoMixinPosedantoj):
    form = ObjektoFormo
    list_display = ('nomo_teksto','posedantoj_','priskribo_teksto','realeco','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    inlines = (ObjektoPosedantoInline, ObjektoStokejoInline, ObjektoLigiloInline)
    list_filter = ('sxablono_sistema','realeco')
    save_on_top = True
    save_as = True

    class Meta:
        model = Objekto


# Форма типов владельцев объектов
class ObjektoPosedantoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoPosedantoTipo
        fields = [field.name for field in ObjektoPosedantoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев объектов
@admin.register(ObjektoPosedantoTipo)
class ObjektoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoPosedantoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ObjektoPosedantoTipo


# Форма статусов владельца в рамках владения объектом
class ObjektoPosedantoStatusoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoPosedantoStatuso
        fields = [field.name for field in ObjektoPosedantoStatuso._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев в рамках владения объектом
@admin.register(ObjektoPosedantoStatuso)
class ObjektoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoPosedantoStatusoFormo
    list_display = ('nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']
    save_on_top = True

    class Meta:
        model = ObjektoPosedantoStatuso


# Форма владельцев объектов
class ObjektoPosedantoFormo(forms.ModelForm):

    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ObjektoPosedanto
        fields = [field.name for field in ObjektoPosedanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы объектов
@admin.register(ObjektoPosedanto)
class ObjektoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoPosedantoFormo
    list_display = ('uuid','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','realeco')
    save_on_top = True

    class Meta:
        model = ObjektoPosedanto


# Места хранения в объектах
@admin.register(ObjektoStokejo)
class ObjektoStokejoAdmin(admin.ModelAdmin, TekstoMixinPosedantoj):
    form = ObjektoStokejoFormo
    list_display = ('uuid','posedantoj__','nomo_teksto','priskribo_teksto','id','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    fieldsets = (
            ('Система/Шаблон', {
                'classes': ('collapse', 'wide',),
                'fields': ('sistema', ('sxablono_sistema', 'sxablono_sistema_id', 'sxablono_sistema_priskribo'))
            }),
            ('Служебная информация', {
                'classes': ('wide',),
                'fields': (('publikigo', 'publikiga_dato',), ('arkivo', 'arkiva_dato', ))
            }),
            ('Основные данные', {
                'fields': ('posedanto_objekto', 'id', 'tipo', 'nomo', 'priskribo'),
            }),
        )
    save_on_top = True

    class Meta:
        model = ObjektoStokejo


# Форма типов связей объектов между собой
class ObjektoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoLigiloTipo
        fields = [field.name for field in ObjektoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей объектов между собой
@admin.register(ObjektoLigiloTipo)
class ObjektoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ObjektoLigiloTipo


# Форма связи объектов между собой
class ObjektoLigiloFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ObjektoLigilo
        fields = [field.name for field in ObjektoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Связь объектов между собой
@admin.register(ObjektoLigilo)
class ObjektoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoLigiloFormo
    list_display = ('uuid','posedanto_nomo','konektilo_posedanto','ligilo_nomo','konektilo_ligilo','tipo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','tipo')
    search_fields = ('uuid',)
    search_help_text = 'uuid'
    class Meta:
        model = ObjektoLigilo


# 
class ObjektoUzantoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ObjektoUzanto
        fields = [field.name for field in ObjektoUzanto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# 
@admin.register(ObjektoUzanto)
class ObjektoUzantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoUzantoFormo
    list_display = ('uuid','autoro','objekto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema','realeco')
    class Meta:
        model = ObjektoUzanto


# Форма температурных режимов хранения/транспортировки
class ObjektoTemperaturaReghimoFormo(forms.ModelForm):
    
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    predefined_nomo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Predefined nomo'), 
        required=False
    )

    class Meta:
        model = ObjektoTemperaturaReghimo
        fields = [field.name for field in ObjektoTemperaturaReghimo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_predefined_nomo(self):
        out = self.cleaned_data['predefined_nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Температурный режим хранения/транспортировки
@admin.register(ObjektoTemperaturaReghimo)
class ObjektoTemperaturaReghimoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoTemperaturaReghimoFormo
    list_display = ('uuid', 'kodo', 'priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = ObjektoTemperaturaReghimo


# Форма типов контейнеров
class ObjektoTipoKonteneroFormo(forms.ModelForm):
    
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    komento = forms.CharField(widget=LingvoInputWidget(), label=_('Лomento'), required=False)
    predefined_nomo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Predefined nomo'), 
        required=False
    )

    class Meta:
        model = ObjektoTipoKontenero
        fields = [field.name for field in ObjektoTipoKontenero._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_komento(self):
        out = self.cleaned_data['komento']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_predefined_nomo(self):
        out = self.cleaned_data['predefined_nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы контейнеров
@admin.register(ObjektoTipoKontenero)
class ObjektoTipoKonteneroAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoTipoKonteneroFormo
    list_display = ('uuid', 'kodo', 'priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = ObjektoTipoKontenero


# Форма типов упаковок
class ObjektoTipoPakumoFormo(forms.ModelForm):
    
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = ObjektoTipoPakumo
        fields = [field.name for field in ObjektoTipoPakumo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы упаковок
@admin.register(ObjektoTipoPakumo)
class ObjektoTipoPakumoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ObjektoTipoPakumoFormo
    list_display = ('uuid', 'kodo', 'priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = ObjektoTipoPakumo


