"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_protect
from .models import InformilojRegiono
from django.db.models import F, Q

@csrf_protect
def informiloj_ajax(request):
    response = {'status': 'err', 'err': 'Bad AJAX request'}

    if request.method == 'POST'\
            and 'filtro' in request.POST \
            and 'informilo' in request.POST:
        if request.POST['informilo'] == 'regiono':
            regionoj = InformilojRegiono.objects\
                .values('uuid', 'nomo__enhavo')\
                .annotate(nomo=F('nomo__enhavo'))\
                .filter(lando=request.POST['filtro'], forigo=False)\
                .order_by('nomo')\
                .values('uuid', 'nomo')

            informilo = [{'uuid':'', 'nomo': '--------'}]
            informilo.extend(list(regionoj))

            response = {'status': 'ok',
                        'filtro': request.POST['filtro'],
                        'informilo': informilo
                        }

    return JsonResponse(response)