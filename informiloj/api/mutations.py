"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
import requests
from django.conf import settings

from siriuso.utils import set_enhavo
from siriuso.api.types import ErrorNode
from ..models import InformilojUrboj
from .schema import InformilojUrbojNode


# Импорт справочника местонахождения / местоположения из 1с
class ImportoInformilojUrboj1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(InformilojUrbojNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        url = settings.ODATA_URL+'Catalog_СКЛ_Города?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('informiloj.povas_krei_informilo_urboj'):
            posedanto = None
            komunumo = None
            uzanto = None

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                errors = list()
                tipo_retpowto = None
                tipo_telefono = None
                tipo_adreso = None
                for js in jsj['value']:
                    tipo = None
                    speco = None

                    uuid = js['Ref_Key']
                    try:
                        importo = InformilojUrboj.objects.get(uuid=uuid, forigo=False)
                        next # такой есть, идём к следующему
                    except InformilojUrboj.DoesNotExist:
                        pass

                        kodo = js['Code']
                        nomo = js['Description']

                        if not len(errors):
                            importo = InformilojUrboj.objects.create(
                                uuid=uuid,
                                forigo=False,
                                kodo=kodo,
                            )
                            if nomo:
                                set_enhavo(importo.nomo, nomo, 'ru_RU')

                            importo.save()
                            kvanto += 1

                        else:
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoInformilojUrboj1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoInformilojUrboj1c(status=status, message=message, errors=errors, importo=importo)


class InformilojMutations(graphene.ObjectType):
    importo_informiloj_urboj1c = ImportoInformilojUrboj1c.Field(
        description=_('''Импорт справочника местонахождения / местоположения из 1с''')
    )
