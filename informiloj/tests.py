"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.test import TestCase
from .models import InformilojLandoNomo
class InformilojModelTest(TestCase):

    def test_meta_settings(self):
        '''тест: проверка мета информации'''
        self.assertEqual(InformilojLandoNomo.posedanto_kampo, 'nomo')
        
