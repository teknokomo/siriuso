"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from .models import *


# Функционал задач здоровья

# Типы реализаций задач здоровья
class SanoTaskoRealigoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoRealigoTipo

admin.site.register(SanoTaskoRealigoTipo, SanoTaskoRealigoTipoAdmin)


# Типы направлений задач здоровья
class SanoDirektoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoDirektoTipo

admin.site.register(SanoDirektoTipo, SanoDirektoTipoAdmin)


# Виды направлений задач здоровья
class SanoDirektoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoDirektoSpeco

admin.site.register(SanoDirektoSpeco, SanoDirektoSpecoAdmin)


# Типы этапов задач здоровья
class SanoEtapoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoEtapoTipo

admin.site.register(SanoEtapoTipo, SanoEtapoTipoAdmin)


# Виды этапов задач здоровья
class SanoEtapoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoEtapoSpeco

admin.site.register(SanoEtapoSpeco, SanoEtapoSpecoAdmin)


# Типы задач здоровья
class SanoTaskoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoTipo

admin.site.register(SanoTaskoTipo, SanoTaskoTipoAdmin)


# Виды задач здоровья
class SanoTaskoSpecoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'kodo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = SanoTaskoSpeco

admin.site.register(SanoTaskoSpeco, SanoTaskoSpecoAdmin)


# Глобальные задачи здоровья

# Направления глобальных задач здоровья
class SanoXeneralaDirektoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaDirekto

admin.site.register(SanoXeneralaDirekto, SanoXeneralaDirektoAdmin)


# Этапы глобальных задач здоровья
class SanoXeneralaEtapoAdmin(admin.ModelAdmin):
    list_display = ('id', 'priskribo', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaEtapo

admin.site.register(SanoXeneralaEtapo, SanoXeneralaEtapoAdmin)


# Глобальные задачи здоровья
class SanoXeneralaTaskoAdmin(admin.ModelAdmin):
    list_display = ('id', 'autoro', 'publikigo', 'uuid',)
    exclude = ('id', 'priskribo',)

    class Meta:
        model = SanoXeneralaTasko

admin.site.register(SanoXeneralaTasko, SanoXeneralaTaskoAdmin)
