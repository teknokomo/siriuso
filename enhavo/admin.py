"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from .models import *


@admin.register(EnhavoEnhavoTipo)
class EnhavoEnhavoTipoAdmin(admin.ModelAdmin):
    list_display = ('nomo', 'uuid',)
    exclude = ('nomo',)

    class Meta:
        model = EnhavoEnhavoTipo


# Контент
@admin.register(EnhavoEnhavo)
class EnhavoEnhavoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nomo', 'uuid',)
    exclude = ('nomo', 'teksto', 'id')

    class Meta:
        model = EnhavoEnhavo
