"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django import template
from django.utils.html import escape, conditional_escape
from django.utils.safestring import mark_safe
from django.template.defaultfilters import stringfilter
from django.utils.translation import gettext_lazy as _

register = template.Library()

import re
readmore_showscript = ''.join([
"this.parentNode.style.display='none';",
"this.parentNode.parentNode.getElementsByClassName('more')[0].style.display='inline';",
"return false;",
]);

@register.filter(needs_autoescape=True)
@stringfilter
def read_more(s, show_words, autoescape=True):
    """Split text after so many words, inserting a "more" link at the end.

    Relies on JavaScript to react to the link being clicked and on classes
    found in Bootstrap to hide elements.
    """
    show_words = int(show_words)
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    words = esc(s).split()

    if len(words) <= show_words:
        return s

    insertion = (
        # The see more link...
        '<div class="read-more">&hellip;'
        '    <a href="#">'
        '        <i class="fa fa-plus-square gray" title="Show All"></i>'
        '    </a>'
        '</div>'
        # The call to hide the rest...
        '<div class="more hidden">'
    )

    # wrap the more part
    words.insert(show_words, insertion)
    words.append('</div>')
    return mark_safe(' '.join(words))
'''
@register.filter
def readmore(txt, showwords=15):
    global readmore_showscript
    showchars=100
    words = re.split(r' ', escape(txt))
    for i in range(len(words)):
       if len(words[i])>=showchars:
          words[i]='{}{}{}'.format(words[i][:5],'-',words[i][-5:])
    Read_more=_('Показать полностью')
    txt=mark_safe(' '.join(words))
    if len(words) <= showwords:
        return txt

    # wrap the more part
    words.insert(showwords, '<div class="more" style="display:none;">')
    words.append('</div>')

    # insert the readmore part
    words.insert(showwords, '<div class="readmore"><a href="#" class="readmore_link" onclick="')
    words.insert(showwords+1, readmore_showscript)
    words.insert(showwords+2, '{}{}{}'.format('">', Read_more, '...</a>'))
    words.insert(showwords+3, '</div>')
    return mark_safe(' '.join(words))

readmore.is_safe = True
'''
@register.filter
def readmore(txt, showwords=15):
    global readmore_showscript
    showchars=200
    #words = re.split(r' ', escape(txt))
    words =sum([word.split('\n') for word in escape(txt).split(' ')],[])
    Read_more=_('Показать полностью')
    Long_word=_('показать длинное слово')
    
    def longword(word):
       word='{}{}{}{}{}{}'.format(word[:10],'<span class="readmore">... <a href="#" onclick="',readmore_showscript,'{}{}{}'.format('">', Long_word, '</a>'),'</span>','{}{}{}'.format('<span class="more" style="display:none;">',word[10:],'</span>'))
       return word

    for i in range(len(words)):
       if len(words[i])>=showchars:
          words[i]=longword(words[i])

    txt=mark_safe(' '.join(words))

    if len(words) <= showwords:
        return txt

    # wrap the more part
    words.insert(showwords, '<div class="more" style="display:none;">')
    words.append('</div>')

    # insert the readmore part
    words.insert(showwords, '<div class="readmore"> <a href="#" class="readmore_link" onclick="')
    words.insert(showwords+1, readmore_showscript)
    words.insert(showwords+2, '{}{}{}'.format('">', Read_more, '...</a>'))
    words.insert(showwords+3, '</div>')
    return mark_safe(' '.join(words))

readmore.is_safe = True


