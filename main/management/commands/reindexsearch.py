"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.core.management.base import BaseCommand
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from komunumoj import models
from komunumoj import search


class Command(BaseCommand):
    help = 'Reindex Elasticsearch database'

    def handle(self, *args, **options):
        init_list = (search.KomunumojSociaprojektoIndex,)
        for obj in init_list:
            obj.init()

        index_list = (models.KomunumojSociaprojekto,)
        es = Elasticsearch(hosts='elasticsearch', timeout=20)
        for obj in index_list:
            index = obj.objects.all().iterator()
            bulk(client=es, actions=(b.indexing() for b in index))
        self.stdout.write(self.style.SUCCESS('Successfully reindex elasticserch database'))
