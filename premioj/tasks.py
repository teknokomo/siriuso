"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import shared_task
from django.utils.translation import gettext_lazy as _
from uzantoj.sciigoj import sendu_sciigon

from .models import PremiojPremiado

@shared_task
def sciigi_premioj(premiado_uuid):
    try:
        premiado = (PremiojPremiado.objects.get(uuid=premiado_uuid, forigo=False))
        kondicho = premiado.kondicho
    except PremiojPremiado.DoesNotExist:
        return False

    uzantoj = {premiado.premiita.id,}
    
    # Получена новая награда за <условия награждения>
    teksto = 'Получена новая награда за %(nomo)s'
    parametroj = {'nomo': {'obj': kondicho, 'field': 'nomo'}}
    _('Получена новая награда за %(nomo)s')


    return sendu_sciigon(teksto, to=uzantoj, objektoj=(premiado,kondicho,), teksto_parametroj=parametroj)

    return 0
