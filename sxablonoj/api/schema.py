"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель типов глобальных шаблонов пользователей
class SxablonoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = SxablonoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель глобальных шаблонов пользователей
class SxablonoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = Sxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class SxablonoQuery(graphene.ObjectType):
    uzanto_sxablono_tipo = SiriusoFilterConnectionField(
        SxablonoTipoNode,
        description=_('Выводит все доступные модели типов глобальных шаблонов пользователей')
    )
    uzanto_sxablono = SiriusoFilterConnectionField(
        SxablonoNode,
        description=_('Выводит все доступные модели глобальных шаблонов пользователей')
    )
