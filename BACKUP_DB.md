# Резервное копирование Siriuso
[[_TOC_]]

# Создание резервной копии
Сделать дамп БД в виде скрипта SQL
```
pg_dump -h 127.0.0.1 -U siriuso siriuso > /backup/$(date +"%Y-%m-%d_%H-%M-%S")_siriuso_postgresql.sql
```

# Восстановление из резервной копии
Пересоздать БД
```
dropdb -h 127.0.0.1 -U siriuso siriuso
createdb -h 127.0.0.1 -U siriuso siriuso
```
Загрузить данные из последней копии
```
psql -h 127.0.0.1 -U siriuso -d siriuso < $(ls -t /backup/*.sql | head -1)
```
$(ls -t /backup/*.sql | head -1) означает:
- -t - сортировать файлы по времени создания начиная от самых новых
- /backup/*.sql - взять из директории backup файлы с расширением sql
- head -1 - из полученного списка взять первое имя файла
