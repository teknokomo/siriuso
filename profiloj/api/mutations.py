"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .schema import *
from ..models import *


# Модель пользовательских профилей
class RedaktuProfilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    profilo = graphene.Field(ProfiloNode, description=_('Созданный/изменённый профиль пользователя'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        retnomo = graphene.String(description=_('никнейм'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def create(root, uzanto, info, **kwargs):
        status = False
        message = None
        errors = list()
        if not uzanto:
            uzanto = info.context.user
        profilo = None

        with transaction.atomic():
            # Создаём новую запись
            # Т.к. пользователь создаёт запись по себе, то проверкв наличия прав не требуется
            # if uzanto.has_perm('profiloj.povas_krei_profiloj'):
            # Проверяем наличие всех полей
            if kwargs.get('forigo', False) and not message:
                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

            if kwargs.get('arkivo', False) and not message:
                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

            if not kwargs.get('retnomo', False):
                message = '{} "{}"'.format(
                    _('При создании записи поле обязательно для заполнения'),
                    'retnomo'
                )

            try:
                Profilo.objects.get(retnomo=kwargs.get('retnomo'))
                message = _('Такой никнейм уже используется')
            except:
                pass

            if not message:
                profilo = Profilo.objects.create(
                    forigo=False,
                    arkivo=False,
                    siriuso_uzanto = uzanto,
                    publikigo=kwargs.get('publikigo', False),
                    publikiga_dato=timezone.now(),
                    retnomo=kwargs.get('retnomo')
                )

                profilo.save()

                status = True
                message = _('Запись создана')
            
        return status, message, errors, profilo

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        uzanto = info.context.user
        profilo = None

        with transaction.atomic():
            # Изменяем запись
            if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                    or kwargs.get('retnomo', False)):
                message = _('Не задано ни одно поле для изменения')

            try:
                Profilo.objects.get(retnomo=kwargs.get('retnomo'))
                message = _('Такой никнейм уже используется')
            except:
                pass

            if not message:
                # Ищем запись для изменения
                try:
                    profilo = Profilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                    if (not uzanto.has_perm('profiloj.povas_forigi_profiloj')
                            and kwargs.get('forigo', False)):
                        message = _('Недостаточно прав для удаления')
                    elif not uzanto.has_perm('profiloj.povas_shanghi_profiloj'):
                        if uzanto != profilo.siriuso_uzanto:
                            message = _('Недостаточно прав для изменения')

                    if not message:
                        profilo.forigo = kwargs.get('forigo', profilo.forigo)
                        profilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                        profilo.arkivo = kwargs.get('arkivo', profilo.arkivo)
                        profilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                        profilo.publikigo = kwargs.get('publikigo', profilo.publikigo)
                        profilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                        profilo.retnomo = kwargs.get('retnomo', profilo.retnomo)

                        profilo.save()
                        status = True
                        message = _('Запись успешно изменена')
                except Profilo.DoesNotExist:
                    message = _('Запись не найдена')

        return status, message, errors, profilo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        profilo = None
        errors = list()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, profilo = RedaktuProfilo.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, profilo = RedaktuProfilo.create(root, False, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return RedaktuProfilo(status=status, message=message, profilo=profilo)


class ProfiloMutations(graphene.ObjectType):
    redaktu_profilo = RedaktuProfilo.Field(
        description=_('''Создаёт или редактирует профили пользователей ''')
    )
