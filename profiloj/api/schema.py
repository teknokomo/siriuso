"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoPermissions  # Миксины для описания типов  Graphene
from ..models import *  # модели приложения


# Модель пользовательских профилей
class ProfiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = Profilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'siriuso_uzanto__id': ['exact'],
            'siriuso_uzanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class ProfiloQuery(graphene.ObjectType):
    profilo = SiriusoFilterConnectionField(
        ProfiloNode,
        description=_('Выводит все доступные модели пользовательских профилей')
    )
