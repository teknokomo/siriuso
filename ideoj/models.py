"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.db.models import Max
from django.utils.translation import gettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from main.models import SiriusoBazaAbstraktaKomunumoj
from main.models import SiriusoKomentoAbstrakta
from main.models import SiriusoTipoAbstrakta
from komunumoj.models import KomunumojAliro
from komunumoj.models import Komunumo
import sys


# Функционал идей для общественных проектов
# Типы категорий идей, использует абстрактный класс SiriusoTipoAbstrakta
class IdeojSociaprojektoKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sociaprojektoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')


# Категории идей общественных проектов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class IdeojSociaprojektoKategorio(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (общественный проект)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип категории
    tipo = models.ForeignKey(IdeojSociaprojektoKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категорий, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sociaprojektoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(IdeojSociaprojektoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                      using=using, update_fields=update_fields)


# Типы идей (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class IdeojSociaprojektoIdeoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sociaprojektoj_ideoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ideoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ideoj')


# Идеи общественных проектов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class IdeojSociaprojektoIdeo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (общественный проект)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(IdeojSociaprojektoKategorio, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип идеи
    tipo = models.ForeignKey(IdeojSociaprojektoIdeoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий идей, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов идей, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # тема закрыта (да или нет)
    fermita = models.BooleanField(_('Fermita'), blank=False, default=False)

    # кому разрешено писать в закрытую тему
    komentado_aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # тема закреплена (да или нет)
    fiksa = models.BooleanField(_('Fiksa'), blank=False, default=False)

    # позиция среди закреплённых тем
    fiksa_listo = models.IntegerField(_('Fiksa listo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sociaprojektoj_ideoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ideo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ideoj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(IdeojSociaprojektoIdeo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)


# Комментарии идей общественных проектов, использует абстрактный класс SiriusoKomentoAbstrakta
class IdeojSociaprojektoIdeoKomento(SiriusoKomentoAbstrakta):

    # владелец (идея)
    posedanto = models.ForeignKey(IdeojSociaprojektoIdeo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # текст в таблице текстов комментариев идей, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sociaprojektoj_ideoj_komentoj'


# Функционал идей для советов
# Типы категорий идей, использует абстрактный класс SiriusoTipoAbstrakta
class IdeojSovetoKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sovetoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')


# Категории идей советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class IdeojSovetoKategorio(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип категории
    tipo = models.ForeignKey(IdeojSovetoKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категорий, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sovetoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(IdeojSovetoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)


# Типы идей (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class IdeojSovetoIdeoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sovetoj_ideoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ideoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ideoj')


# Идеи советов, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class IdeojSovetoIdeo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (совет)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(IdeojSovetoKategorio, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип идеи
    tipo = models.ForeignKey(IdeojSovetoIdeoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий идей, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # текст в таблице текстов идей, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # тема закрыта (да или нет)
    fermita = models.BooleanField(_('Fermita'), blank=False, default=False)

    # кому разрешено писать в закрытую тему
    komentado_aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # тема закреплена (да или нет)
    fiksa = models.BooleanField(_('Fiksa'), blank=False, default=False)

    # позиция среди закреплённых тем
    fiksa_listo = models.IntegerField(_('Fiksa listo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sovetoj_ideoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ideo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ideoj')

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(IdeojSovetoIdeo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)


# Комментарии идей советов, использует абстрактный класс SiriusoKomentoAbstrakta
class IdeojSovetoIdeoKomento(SiriusoKomentoAbstrakta):

    # владелец (идея)
    posedanto = models.ForeignKey(IdeojSovetoIdeo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # текст в таблице текстов комментариев идей, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'ideoj_sovetoj_ideoj_komentoj'
