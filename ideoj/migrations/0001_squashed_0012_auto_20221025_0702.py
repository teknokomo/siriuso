# Generated by Django 3.2.16 on 2022-12-04 07:50

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import siriuso.models.postgres
import siriuso.utils.modules
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('main', '0001_squashed_0021_auto_20221025_0702'),
        ('informiloj', '0001_initial'),
        ('komunumoj', '0001_squashed_0033_auto_20221013_0421'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='IdeojSociaprojektoKategorioTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektokategoriotipo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de kategorioj',
                'verbose_name_plural': 'Tipoj de kategorioj',
                'db_table': 'ideoj_sociaprojektoj_kategorioj_tipoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSociaprojektoKategorio',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('aliro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumojaliro', verbose_name='Aliro')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektokategorio_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumo', verbose_name='Posedanto')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsociaprojektokategoriotipo', verbose_name='Tipo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsociaprojektokategorio_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('priskribo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektokategorio_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Kategorio',
                'verbose_name_plural': 'Kategorioj',
                'db_table': 'ideoj_sociaprojektoj_kategorioj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSociaprojektoIdeoTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideotipo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de ideoj',
                'verbose_name_plural': 'Tipoj de ideoj',
                'db_table': 'ideoj_sociaprojektoj_ideoj_tipoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSociaprojektoIdeo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('fermita', models.BooleanField(default=False, verbose_name='Fermita')),
                ('fiksa', models.BooleanField(default=False, verbose_name='Fiksa')),
                ('fiksa_listo', models.IntegerField(blank=True, null=True, verbose_name='Fiksa listo')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideo_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('kategorio', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsociaprojektokategorio', verbose_name='Kategorio')),
                ('komentado_aliro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumojaliro', verbose_name='Komentada aliro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumo', verbose_name='Posedanto')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsociaprojektoideotipo', verbose_name='Tipo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsociaprojektoideo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('teksto', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Ideo',
                'verbose_name_plural': 'Ideoj',
                'db_table': 'ideoj_sociaprojektoj_ideoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSociaprojektoIdeoKomento',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideokomento_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsociaprojektoideo', verbose_name='Posedanto')),
                ('respondo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideokomento_respondo', to='ideoj.ideojsociaprojektoideokomento', verbose_name='Respondo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsociaprojektoideokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('teksto', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsociaprojektoideokomento_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'db_table': 'ideoj_sociaprojektoj_ideoj_komentoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSovetoKategorioTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetokategoriotipo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de kategorioj',
                'verbose_name_plural': 'Tipoj de kategorioj',
                'db_table': 'ideoj_sovetoj_kategorioj_tipoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSovetoKategorio',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('aliro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumojaliro', verbose_name='Aliro')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetokategorio_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumo', verbose_name='Posedanto')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsovetokategoriotipo', verbose_name='Tipo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsovetokategorio_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('priskribo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Priskribo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetokategorio_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Kategorio',
                'verbose_name_plural': 'Kategorioj',
                'db_table': 'ideoj_sovetoj_kategorioj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSovetoIdeoTipo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('kodo', models.CharField(max_length=16, verbose_name='Kodo')),
                ('speciala', models.BooleanField(default=False, verbose_name='Speciala')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Nomo')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideotipo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Tipo de ideoj',
                'verbose_name_plural': 'Tipoj de ideoj',
                'db_table': 'ideoj_sovetoj_ideoj_tipoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSovetoIdeo',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('id', models.IntegerField(default=0, unique=True, verbose_name='ID')),
                ('fermita', models.BooleanField(default=False, verbose_name='Fermita')),
                ('fiksa', models.BooleanField(default=False, verbose_name='Fiksa')),
                ('fiksa_listo', models.IntegerField(blank=True, null=True, verbose_name='Fiksa listo')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideo_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('kategorio', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsovetokategorio', verbose_name='Kategorio')),
                ('komentado_aliro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumojaliro', verbose_name='Komentada aliro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='komunumoj.komunumo', verbose_name='Posedanto')),
                ('tipo', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsovetoideotipo', verbose_name='Tipo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsovetoideo_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('nomo', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Titolo')),
                ('teksto', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'verbose_name': 'Ideo',
                'verbose_name_plural': 'Ideoj',
                'db_table': 'ideoj_sovetoj_ideoj',
            },
        ),
        migrations.CreateModel(
            name='IdeojSovetoIdeoKomento',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, verbose_name='UUID')),
                ('krea_dato', models.DateTimeField(auto_now_add=True, verbose_name='Krea dato')),
                ('forigo', models.BooleanField(blank=True, default=False, verbose_name='Forigo')),
                ('foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Foriga dato')),
                ('a_foriga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Aŭtomata foriga dato')),
                ('publikigo', models.BooleanField(default=False, verbose_name='Publikigis')),
                ('publikiga_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Dato de publikigo')),
                ('arkivo', models.BooleanField(default=False, verbose_name='Arkiva')),
                ('arkiva_dato', models.DateTimeField(blank=True, default=None, null=True, verbose_name='Arkiva dato')),
                ('autoro', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideokomento_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Aŭtoro')),
                ('posedanto', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='ideoj.ideojsovetoideo', verbose_name='Posedanto')),
                ('respondo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideokomento_respondo', to='ideoj.ideojsovetoideokomento', verbose_name='Respondo')),
                ('lasta_autoro', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='ideoj_ideojsovetoideokomento_lasta_autoro', to=settings.AUTH_USER_MODEL, verbose_name='Modifita de')),
                ('lasta_dato', models.DateTimeField(blank=True, null=True, verbose_name='Dato de lasta modifo')),
                ('teksto', models.JSONField(blank=True, default=siriuso.utils.modules.default_lingvo, encoder=siriuso.models.postgres.CallableEncoder, verbose_name='Teksto')),
                ('wablomo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='ideoj_ideojsovetoideokomento_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono')),
            ],
            options={
                'db_table': 'ideoj_sovetoj_ideoj_komentoj',
            },
        ),
    ]
