"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.core.management.base import BaseCommand
from django.contrib.gis.gdal import DataSource
from geo.models import PlanetOsmPolygon
from django.contrib.gis.db.models.functions import AsGeoJSON
from django.core.serializers import serialize


class Command(BaseCommand):
    help = 'Fill OSM database'

    # def add_arguments(self, parser):
    #     parser.add_argument('filename', type=str)

    # def handle(self, *args, **options):
    #     ds = DataSource(options['filename'])
    #     for layer in ds[3].field_types:
    #         print(layer)

    def handle(self, *args, **options):
        s = (PlanetOsmPolygon.objects.using('osm')
             .filter(tags__contains={'ISO3166-1': 'RU'})
             .annotate(json=AsGeoJSON('way')))
        ss = serialize('geojson', s,
                          geometry_field='way',
                          fields=('name',))
        print(ss[:1000])
        # filename = 'kaliningrad-latest.osm.pbf'
        # s1 = '''osm2pgsql -j --match-only --hstore-add-index -c -d siriuso
        #         -H localhost -P 5434 -U siriuso -W {}'''.format(filename)