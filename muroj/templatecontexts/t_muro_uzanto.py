"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from .muro_uzanto import context_kamaradoj, context_statistikoj


def context_mobile_kamaradoj(request, **kwargs):
    return context_kamaradoj(request, **kwargs)


def context_mobile_statistikoj(request, **kwargs):
    return context_statistikoj(request, **kwargs)