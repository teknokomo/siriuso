"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from .organizo import context_organizo_statistikoj, context_organizo_kohereco


def context_mobile_organizo_statistikoj(request, **kwargs):
    return context_organizo_statistikoj(request, **kwargs)


def context_mobile_organizo_kohereco(request, **kwargs):
    return context_organizo_kohereco(request, **kwargs)