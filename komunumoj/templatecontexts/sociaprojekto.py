"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models import Count, F, Q
from komunumoj.models import *
from itertools import chain


def context_projekto_statistikoj(request, **kwargs):
    context = {}

    if 'projekto_id' in kwargs or 'sociaprojekto_id' in kwargs or 'kom_id' in kwargs:
        projekto_id = kwargs['projekto_id'] if 'projekto_id' in kwargs else kwargs.get('sociaprojekto_id')
        projekto_id = projekto_id or kwargs.get('kom_id')
        sociaprojekto = KomunumojSociaprojekto.objects\
            .values('uuid', 'nomo__enhavo', 'priskribo__enhavo', 'id','komunumojsociaprojektoavataro__bildo',
                    'komunumojsociaprojektoavataro__bildo_min', 'komunumojsociaprojektokovrilo__bildo')\
            .filter(id=projekto_id, forigo=False)

        if sociaprojekto.count():
            sociaprojekto = sociaprojekto[0]
            queryset_total = KomunumojSociaprojektoMembro.objects.values('posedanto__id')

            total = queryset_total.filter(posedanto__id=projekto_id, forigo=False, autoro__konfirmita=True,
                                          autoro__is_superuser=False).aggregate(total=Count('posedanto__id'))['total']

            procentoj = 100*total/100000

            if not procentoj:
                procentoj = 1
            elif procentoj < 2:
                procentoj = 2
            else:
                procentoj = "%.2f" % (procentoj)

            context['sociaprojekto'] = sociaprojekto
            context['statistikoj'] = {'tuta': total,
                                      'procentoj': procentoj,
                                      }

            if request.user.is_authenticated:
                try:
                    uzanto_aligis = (KomunumojSociaprojektoMembro.objects
                                     .get(autoro_id=request.user.id, posedanto__id=projekto_id, forigo=False))
                    uzanto_aligis = [aligis.kodo for aligis in uzanto_aligis.muro_sciigo.all()]
                except KomunumojSociaprojektoMembro.DoesNotExist:
                    uzanto_aligis = False

                context['statistikoj']['uzanto_aligis'] = uzanto_aligis

                # Временное!!!
                # Определяем права пользователя для сообщества
                try:
                    uzanto_aliro = KomunumojSociaprojektoMembro.objects\
                        .values('tipo__kodo')\
                        .get(autoro_id=request.user.id, posedanto_id=sociaprojekto['uuid'], forigo=False)
                    context['uzanto_aliro'] = uzanto_aliro['tipo__kodo']
                    context['tipoj_membroj'] = ['membro', 'membro-adm', 'membro-mod']
                    context['adm_membroj'] = ['membro-adm', 'membro-mod', 'administranto', 'moderiganto']
                except KomunumojSociaprojektoMembro.DoesNotExist:
                    pass

    return context


def context_projekto_kohereco(request, **kwargs):
    context = {}

    if 'projekto_id' in kwargs:
        def make_query(obj, nomo_enhavo, avataro_bildo_min, filter):
            return (obj.objects
                    .annotate(nomo__enhavo=nomo_enhavo,
                              avataro_bildo_min=avataro_bildo_min)
                    .filter(filter, forigo=False))

        projektoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsociaprojektoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_sociaprojekto__posedanto_sociaprojekto__id=kwargs['projekto_id'])
        }
        projektoj = make_query(KomunumojSociaprojekto, **projektoj_dict)

        sovetoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojsovetoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_soveto__posedanto_sociaprojekto__id=kwargs['projekto_id'])
        }
        sovetoj = make_query(KomunumojSoveto, **sovetoj_dict)

        organizo_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojorganizoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_organizo__posedanto_sociaprojekto__id=kwargs['projekto_id'])
        }
        organizoj = make_query(KomunumojOrganizo, **organizo_dict)

        grupoj_dict = {
            'nomo_enhavo': F('nomo__enhavo'),
            'avataro_bildo_min': F('komunumojgrupoavataro__bildo_min'),
            'filter': Q(komunumoj_komunumojkohereco_kohera_grupo__posedanto_sociaprojekto__id=kwargs['projekto_id'])
        }
        grupoj = make_query(KomunumojGrupo, **grupoj_dict)

        context['komunumoj'] = list(chain(sovetoj, organizoj, projektoj, grupoj))

    return context
