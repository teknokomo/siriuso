"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from .soveto import context_soveto_statistikoj, context_soveto_kohereco


def context_mobile_soveto_statistikoj(request, **kwargs):
    return context_soveto_statistikoj(request, **kwargs)


def context_mobile_soveto_kohereco(request, **kwargs):
    return context_soveto_kohereco(request, **kwargs)