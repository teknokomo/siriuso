"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import re
from graphene import (relay, ObjectType, String, DateTime,
                      Int, Boolean, Field)
from graphene_django import DjangoObjectType
from graphene_permissions.permissions import AllowAny
from django.db.models import Q, F, Value, CharField
from django.utils.translation import gettext_lazy as _

from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from ..models import *
from uzantoj.api.schema import UzantoNode
from fotoj.api.schema import FotojFotoDosieroNode
from fotoj.models import FotojFotoDosiero, FotojKomunumoAvataro, FotojKomunumoKovrilo
from main.models import Uzanto

class KomunumoProprietoj(ObjectType):
    __parent = None
    __qs = None

    def __init__(self, parent, *args, **kwargs):
        self.__parent = parent
        super().__init__(*args, **kwargs)

    # Всего пользователей
    tuta = Int()
    # Целевое количество пользователей (увеличивается по достижении)
    postulita = Int()
    # Признак участия текущего пользователя в сообществе
    mia = Boolean()
    # Тип участия текущего пользователя в сообществе
    membra_tipo = String()
    # Список участников
    membroj = SiriusoFilterConnectionField(UzantoNode)
    # Рейтинг по количеству Записей на Стене
    rating = Int()
    # Дата последней активности на Стене
    aktiva_dato = DateTime()

    def __queryset(self):
        if self.__qs:
            return self.__qs

        if isinstance(self.__parent, Komunumo):
            self.__qs = (Uzanto.objects.filter(komunumoj_komunumomembro_autoro__posedanto_id=self.__parent.uuid,
                                               komunumoj_komunumomembro_autoro__forigo=False,
                                               is_active=True, konfirmita=True)
                         .annotate(membra_tipo=F('komunumoj_komunumomembro_autoro__tipo__kodo')))
        else:
            self.__qs = Uzanto.objects.none()

        return self.__qs

    def resolve_postulita(self, info):
        tuta = self.__queryset().count()

        for postulita in (1000000000, 100000000, 10000000, 1000000, 100000, 10000):
            if tuta > postulita:
                return postulita * 10
        return 10000

    def resolve_tuta(self, info):
        return self.__queryset().count()

    def resolve_mia(self, info):
        if info.context.user.is_authenticated:
            return bool(self.__queryset().filter(id=info.context.user.id).count())
        return False

    def resolve_membra_tipo(self, info):
        qs = self.__queryset().filter(id=info.context.user.id)

        if qs.count():
            return qs[0].membra_tipo
        return None

    def resolve_membroj(self, info, **kwargs):
        return self.__queryset()

    def resolve_rating(self, info, **kwargs):
        return getattr(self.__parent, 'rating', None)

    def resolve_aktiva_dato(self, info, **kwargs):
        return getattr(self.__parent, 'aktiva_dato', None)


class KomunumojBildo(ObjectType):
    """
    Абстрактное поле для отражения аватара и обложки Сообщетсва
    """
    __source = None

    bildo = String()
    bildo_min = String()
    bildo_maks = String()

    def __init__(self, field, informo=False, *args, **kwargs):
        filter_args = {'forigo': False}

        if not informo:
            filter_args.update({
                'aktiva': True,
                'arkivo': False,
                'chefa_varianto': True
            })

        self.__source = field.filter(**filter_args)
        self.__source = self.__source[0] if self.__source.count() else None
        super().__init__(*args, **kwargs)

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        image = getattr(bildo, 'url') if bildo else default
        return request.build_absolute_uri(image) if image else None

    def resolve_bildo(self, info):
        if self.__source:
            return KomunumojBildo.__resolve_bildo(info.context, getattr(self.__source, 'bildo'))
        return None

    def resolve_bildo_min(self, info):
        if self.__source and hasattr(self.__source, 'bildo_min'):
            return KomunumojBildo.__resolve_bildo(info.context, getattr(self.__source, 'bildo_min'))
        return None

    def resolve_bildo_maks(self, info):
        if self.__source and hasattr(self.__source, 'bildo_maks'):
            return KomunumojBildo.__resolve_bildo(info.context, getattr(self.__source, 'bildo_maks'))
        return self.resolve_bildo(info)


class KomunumojMixin:
    """
    Миксин для добавления полей статистики
    и участников ко всем сообществам
    """
    statistiko = Field(KomunumoProprietoj)
    nomo = Field(SiriusoLingvo)
    priskribo = Field(SiriusoLingvo)
    informo = Field(SiriusoLingvo)
    informo_bildo = Field(KomunumojBildo)
    kontaktuloj = SiriusoFilterConnectionField(UzantoNode)

    @staticmethod
    def __get_qs(parent, name):
        for rel in parent._meta.related_objects:
            if re.search(r'%s$' % name, rel.name):
                model = rel.related_model
                return model.objects.filter(posedanto=parent)
        return None

    def resolve_statistiko(self, info):
        return KomunumoProprietoj(parent=self)

    def resolve_informo(self, info):
        qs = KomunumojMixin.__get_qs(self, 'informo')

        if qs is not None:
            qs = qs.filter(forigo=False, arkivo=False)
            return qs[0].teksto if qs else None
        return None

    def resolve_informo_bildo(self, info):
        informo = KomunumojMixin.__get_qs(self, 'informo')

        if informo is not None:
            informo = informo.filter(forigo=False, arkivo=False)
            qs = KomunumojMixin.__get_qs(informo[0], 'bildo') if informo else None

            if qs is not None:
                qs = qs.filter(forigo=False, arkivo=False)
                return KomunumojBildo(qs, informo=True)

        return None

    def resolve_kontaktuloj(self, info):
        q_dict = {
            'query': Q(komunumoj_komunumomembro_autoro__posedanto=self,
                       komunumoj_komunumomembro_autoro__kontaktulo=True,
                       komunumoj_komunumomembro_autoro__forigo=False,
                       is_active=True, konfirmita=True),
            'annotate': F('komunumoj_komunumomembro_autoro__kontakta_informo')
        }

        if q_dict:
            kontaktuloj = Uzanto.objects.filter(q_dict['query']).annotate(kontakta_informo=q_dict['annotate'])
        else:
            kontaktuloj = None

        if not kontaktuloj:
            kontaktuloj = (
                Uzanto.objects.filter(id=13)
                    .annotate(
                    kontakta_informo=Value(
                        _('Пишите на <a href="/im/direct/13">Технокоме</a><br/>' +
                          'Телеграм <a href="tg://resolve?domain=vladimir_levadnij">@vladimir_levadnij</a><br />' +
                          'Почта <a href="mailto:miru-mir@tehnokom.su">miru-mir@tehnokom.su</a>'),
                        output_field=CharField()
                    )
                )
            )

        return kontaktuloj


class KomunumojBildoMixin:
    """
    Миксин разрешает файлы изображений как абсолютный URL
    """

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        image = getattr(bildo, 'url') if bildo else default
        return request.build_absolute_uri(image) if image else None

    def resolve_bildo(self, info):
        default = ('/static/main/images/defaulte-avataro.png' if isinstance(self, KomunumoAvataro)
                   else '/static/main/images/defaulte-kovrilo.png')
        return KomunumojBildoMixin.__resolve_bildo(info.context, getattr(self, 'bildo'), default)

    def resolve_bildo_min(self, info):
        default = ('/static/main/images/defaulte-avataro-min.png' if isinstance(self, KomunumoAvataro) else None)
        return KomunumojBildoMixin.__resolve_bildo(info.context, getattr(self, 'bildo_min'), default)

    def resolve_bildo_maks(self, info):
        return KomunumojBildoMixin.__resolve_bildo(info.context, getattr(self, 'bildo_maks'))


class KomunumojAliroNode(SiriusoAuthNode, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)

    class Meta:
        model = KomunumojAliro
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


################################
class KomunumoTipoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)

    kvanto_komomunumoj = Int(description=_('Общее количество сообществ данного типа'))

    class Meta:
        model = KomunumoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)

    def resolve_kvanto_komomunumoj(self, info, **kwargs):
        return Komunumo.objects.filter(tipo=self, forigo=False, arkivo=False, publikigo=True).count()


# Сообщества
class KomunumoNode(SiriusoAuthNode, SiriusoObjectId, KomunumojMixin, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    search_fields = ('id', 'nomo__enhavo__icontains', 'priskribo__enhavo__icontains')

    avataro = Field(FotojFotoDosieroNode)

    kovrilo = Field(FotojFotoDosieroNode)

    class Meta:
        model = Komunumo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'tipo__kodo': ['exact', 'in'],
            'tipo__nomo__kodo': ['exact', 'icontains', 'istartswith'],
            'speco__kodo': ['exact', 'in'],
            'grava': ['exact'],
        }
        interfaces = (relay.Node,)

    @staticmethod
    def resolve_kohereco(root, info, **kwargs):
        return root.kohereco.all() or Komunumo.objects.filter(id=13)

    def resolve_avataro(self, info):
        default = FotojFotoDosiero(
                bildo_baza='/static/main/images/defaulte-avataro.png',
                bildo_f='/static/main/images/defaulte-avataro-min.png',
                forigo=False, arkivo=False
            )
        qs = FotojKomunumoAvataro.objects.filter(posedanto=self,
                arkivo=False, chefa_varianto=True, forigo=False)
        qs = qs[0].avataro.dosiero if qs else default
        return qs


    def resolve_kovrilo(self, info):
        default = FotojFotoDosiero(
                bildo_baza='/static/main/images/defaulte-kovrilo.png',
                forigo=False, arkivo=False
            )
        qs = FotojKomunumoKovrilo.objects.filter(posedanto=self,
                arkivo=False, chefa_varianto=True, forigo=False)
        qs = qs[0].kovrilo.dosiero if qs else default
        return qs



# Типы членов сообществ
class KomunumoMembroTipoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    # search_fields = ('nomo__enhavo__icontains')

    nomo = Field(SiriusoLingvo, description=_('Название'))

    class Meta:
        model = KomunumoMembroTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (relay.Node,)


# Сообщества
class KomunumoMembroNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    search_fields = ()

    class Meta:
        model = KomunumoMembro
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'tipo__kodo': ['exact', 'in'],
            'posedanto__id': ['exact'],
            # 'publikigo': ['exact'],
        }
        interfaces = (relay.Node,)


class KomunumoQuery(ObjectType):
    komunumoj = SiriusoFilterConnectionField(KomunumoNode)
    komunumoj_aliroj = SiriusoFilterConnectionField(KomunumojAliroNode)
    komunumoj_tipoj = SiriusoFilterConnectionField(
        KomunumoTipoNode,
        description=_('Выводит список типов сообществ')
    )
    komunumo_membro = SiriusoFilterConnectionField(
        KomunumoMembroNode,
        description=_('Выводит список членов сообществ')
    )
    komunumo_membro_tipo = SiriusoFilterConnectionField(
        KomunumoMembroTipoNode,
        description=_('Выводит список типов членов сообществ')
    )
