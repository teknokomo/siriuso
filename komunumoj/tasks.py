"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from celery import shared_task
from django.db import models
from django.utils import timezone

from .models import Komunumo, KomunumoTakso
from muroj.models import MuroEnskribo


@shared_task
def kom_takso(en_dato=None, kom_uuid=None):
    # Формирует кэш рейтингов всех активных сообществ
    komunumoj = Komunumo.objects.all() if kom_uuid is None else Komunumo.objects.filter(posedanto_id=kom_uuid)
    cnt = 0

    for kom in komunumoj:
        print('{} of {}'.format(cnt + 1, komunumoj.count()))
        if en_dato is None:
            enskriboj = MuroEnskribo.objects.filter(
                posedanto=kom,
                forigo=False,
                publikigo=True,
                arkivo=False
            )
        else:
            enskriboj = MuroEnskribo.objects.filter(
                models.Q(posedanto=kom),
                models.Q(publikiga_dato__lte=en_dato),
                models.Q(arkiva_dato__isnull=True) |
                models.Q(arkiva_dato__gte=en_dato),
                models.Q(foriga_dato__isnull=True) |
                models.Q(foriga_dato__gte=en_dato),
            )

        if enskriboj:
            aktiva_dato = enskriboj.order_by('-publikiga_dato')[0]
            aktiva_dato = aktiva_dato.publikiga_dato
        else:
            aktiva_dato = kom.krea_dato

        KomunumoTakso.objects.create(
            komunumo=kom,
            takso=enskriboj.count(),
            takso_dato=en_dato or timezone.now(),
            aktiva_dato=aktiva_dato
        )
        cnt += 1

    return {'tuta': komunumoj.count(), 'fiksita': cnt}


@shared_task
def kom_takso_purigado(delta_hours=6):
    # Удаляет записи рейтингов сообществ, которые старше 6 часов
    dato = timezone.now() - timezone.timedelta(hours=delta_hours)
    taksoj = KomunumoTakso.objects.filter(krea_dato__lte=dato)

    cnt = taksoj.count()

    if taksoj:
        taksoj.delete()

    return { 'restas': KomunumoTakso.objects.all().count(), 'forigita ': cnt }
