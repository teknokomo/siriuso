"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class KomunumojConfig(AppConfig):
    name = 'komunumoj'
    verbose_name = _('Komunumoj')

    # def ready(self):
    #     import komunumoj.signals
