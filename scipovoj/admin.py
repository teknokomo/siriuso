"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import Realeco
from siriuso.utils.admin_mixins import TekstoMixin


# Форма категорий навыков
class ScipovoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ScipovoKategorio
        fields = [field.name for field in ScipovoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий навыков
@admin.register(ScipovoKategorio)
class ScipovoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = ScipovoKategorioFormo
    list_display = ('nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ScipovoKategorio


# Форма типов навыков
class ScipovoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = ScipovoTipo
        fields = [field.name for field in ScipovoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы навыков
@admin.register(ScipovoTipo)
class ScipovoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ScipovoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ScipovoTipo


# Форма уровней навыков
class ScipovoNiveloFormo(forms.ModelForm):
    
    class Meta:
        model = ScipovoNivelo
        fields = [field.name for field in ScipovoNivelo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Уровни навыков
@admin.register(ScipovoNivelo)
class ScipovoNiveloAdmin(admin.ModelAdmin, TekstoMixin):
    form = ScipovoNiveloFormo
    list_display = ('uuid', 'nivelo','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = ScipovoNivelo


# Форма навыков
class ScipovoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Категория навыков'),
        queryset=ScipovoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = Scipovo
        fields = [field.name for field in Scipovo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# навыкы
@admin.register(Scipovo)
class ScipovoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ScipovoFormo
    list_display = ('nomo_teksto','autoro_nn','priskribo_teksto','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = Scipovo


# Форма навыков объектов
class ScipovoObjektoFormo(forms.ModelForm):
    
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(), 
        label=_('Priskribo de sistema ŝablono'), 
        required=False
    )

    class Meta:
        model = ScipovoObjekto
        fields = [field.name for field in ScipovoObjekto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# навыкы
@admin.register(ScipovoObjekto)
class ScipovoObjektoAdmin(admin.ModelAdmin, TekstoMixin):
    form = ScipovoObjektoFormo
    list_display = ('uuid','objekto','scipovo','nivelo','sxablono_sistema_id','sxablono_sistema','sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)
    class Meta:
        model = ScipovoObjekto
