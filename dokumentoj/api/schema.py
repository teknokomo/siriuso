"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from urllib.parse import urljoin

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения
from taskoj.api.schema import TaskojProjektoNode, TaskojProjektoPosedantoNode
from taskoj.models import TaskojProjektoPosedanto, TaskojProjekto, TaskojProjektoStatuso


# Модель файлов документов
class DokumentoDosieroNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'formo__enhavo': ['contains', 'icontains'],
        'statuso__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    formo = graphene.Field(SiriusoLingvo, description=_('Вид печатной формы'))

    statuso = graphene.Field(SiriusoLingvo, description=_('Состояние'))

    dosiero_url = graphene.String(description=_('URL файла'))

    class Meta:
        model = DokumentoDosiero
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_dosiero_url(self, info, **kwargs):
        return 'https://{}{}'.format(get_current_site(request=info.context).domain,
                                            urljoin(settings.MEDIA_URL, str(self.dosiero)))
        

# Модель связей категорий документов между собой
class DokumentoKategorioLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = DokumentoKategorioLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'ligilo__id': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий документов
class DokumentoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Связь категорий документов между собой
    ligilo =  SiriusoFilterConnectionField(DokumentoKategorioLigiloNode,
        description=_('Выводит связи категорий документов между собой'))

    class Meta:
        model = DokumentoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'dokumentoj_dokumentokategorioligilo_ligilo': ['isnull']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return DokumentoKategorioLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)


# Модель типов связей категорий документов между собой
class DokumentoKategorioLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoKategorioLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов документов
class DokumentoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель видов документов
class DokumentoSpecoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoSpeco
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов мест хранения документов
class DokumentoStokejoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoStokejoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель логических мест хранения документов
class DokumentoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов цен документов
class DokumentoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов цен документов
class DokumentoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель цен документов
class DokumentoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = DokumentoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей документов между собой
class DokumentoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей документов между собой
class DokumentoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = DokumentoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель характеристик документов экспедирования грузаММП
class DokumentoEkspedoKargoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'numero_indikatoro__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование'))
    numero_indikatoro = graphene.Field(SiriusoLingvo, description=_('Серийный номер датчика'))

    class Meta:
        model = DokumentoEkspedoKargo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель характеристик документов экспедирования
class DokumentoEkspedoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'adreso_elsxargxado__enhavo': ['contains', 'icontains'],
        'adreso_kargado__enhavo': ['contains', 'icontains'],
        'komento__enhavo': ['contains', 'icontains'],
        'komento_adreso_elsxargxado__enhavo': ['contains', 'icontains'],
        'komento_adreso_kargado__enhavo': ['contains', 'icontains'],
        'komento_nuligo__enhavo': ['contains', 'icontains'],
        'kombatanto_adresoj__enhavo': ['contains', 'icontains'],
        'kombatanto_adresoj_elsxargxado__enhavo': ['contains', 'icontains'],
        'priskribo_kargo__enhavo': ['contains', 'icontains'],
        'organizo_adreso__enhavo': ['contains', 'icontains'],
        'organizo_adreso_elsxargxado__enhavo': ['contains', 'icontains'],
        'telefono_kliento__enhavo': ['contains', 'icontains'],
        'telefono_adreso__enhavo': ['contains', 'icontains'],
        'telefono_adreso_elsxargxado__enhavo': ['contains', 'icontains'],
        'aldona_kondicxaro__enhavo': ['contains', 'icontains'],
        'impostokvoto_kliento__enhavo': ['contains', 'icontains'],
        'impostokvoto_transportisto__enhavo': ['contains', 'icontains'],
        'priskribo_kalkulado__enhavo': ['contains', 'icontains'],
        'numero_frajtoletero__enhavo': ['contains', 'icontains'],
        'priskribo_dangxera_kargo__enhavo': ['contains', 'icontains'],
        'klaso_dangxera__enhavo': ['contains', 'icontains'],
        'aldone_postulo_hc__enhavo': ['contains', 'icontains'],
        'urboj_recivado_nomo__enhavo': ['contains', 'icontains'],
        'urboj_transdono_nomo__enhavo': ['contains', 'icontains'],
        'aliaj_servo__enhavo': ['contains', 'icontains'],
        'fordono_kargo_fio__enhavo': ['contains', 'icontains'],
        'nomo_kargo__enhavo': ['contains', 'icontains'],
        'konservejo_temperatura_reghimo_alia__enhavo': ['contains', 'icontains'],
        'konservejo_tasko__enhavo': ['contains', 'icontains'],
        'konservejo_komento__enhavo': ['contains', 'icontains'],
        'priskribo_kliento__enhavo': ['contains', 'icontains'],
        'numero_frajtoletero_kliento__enhavo': ['contains', 'icontains'],
        'email_ekspedinto__enhavo': ['contains', 'icontains'],
        'email_ricevanto__enhavo': ['contains', 'icontains'],
        'tipo_mendo__enhavo': ['contains', 'icontains'],
    }

    adreso_elsxargxado = graphene.Field(SiriusoLingvo, description=_('Адрес выгрузки'))
    adreso_kargado = graphene.Field(SiriusoLingvo, description=_('Адрес загрузки'))
    komento = graphene.Field(SiriusoLingvo, description=_('Комментарий'))
    komento_adreso_elsxargxado = graphene.Field(SiriusoLingvo, description=_('Комментарий адреса выгрузки'))
    komento_adreso_kargado = graphene.Field(SiriusoLingvo, description=_('Комментарий адреса загрузки'))
    komento_nuligo = graphene.Field(SiriusoLingvo, description=_('Комментарий отмены'))
    kombatanto_adresoj = graphene.Field(SiriusoLingvo, description=_('Контактное лицо по адресу'))
    kombatanto_adresoj_elsxargxado = graphene.Field(SiriusoLingvo, description=_('Контактное лицо по адресу выгрузки'))
    priskribo_kargo = graphene.Field(SiriusoLingvo, description=_('Описание груза'))
    organizo_adreso = graphene.Field(SiriusoLingvo, description=_('Организация по адресу'))
    organizo_adreso_elsxargxado = graphene.Field(SiriusoLingvo, description=_('Организация по адресу выгрузки'))
    telefono_kliento = graphene.Field(SiriusoLingvo, description=_('Телефон заказчика'))
    telefono_adreso = graphene.Field(SiriusoLingvo, description=_('Телефон по адресу'))
    telefono_adreso_elsxargxado = graphene.Field(SiriusoLingvo, description=_('Телефон по адресу выгрузки'))
    aldona_kondicxaro = graphene.Field(SiriusoLingvo, description=_('Дополнительные условия'))
    impostokvoto_kliento = graphene.Field(SiriusoLingvo, description=_('Ставка НДС заказчика'))
    impostokvoto_transportisto = graphene.Field(SiriusoLingvo, description=_('Ставка НДС перевозчика'))
    priskribo_kalkulado = graphene.Field(SiriusoLingvo, description=_('Комментарий расчета'))
    numero_frajtoletero = graphene.Field(SiriusoLingvo, description=_('Номер накладной СК'))
    priskribo_dangxera_kargo = graphene.Field(SiriusoLingvo, description=_('Описание опасного груза'))
    klaso_dangxera = graphene.Field(SiriusoLingvo, description=_('Класс опасности'))
    aldone_postulo_hc = graphene.Field(SiriusoLingvo, description=_('Дополнительные требования ХЦ'))
    urboj_recivado_nomo = graphene.Field(SiriusoLingvo, description=_('Город приемки текстом'))
    urboj_transdono_nomo = graphene.Field(SiriusoLingvo, description=_('Город передачи текстом'))
    aliaj_servo = graphene.Field(SiriusoLingvo, description=_('Иные услуги'))
    fordono_kargo_fio = graphene.Field(SiriusoLingvo, description=_('ФИО передающего груз'))
    nomo_kargo = graphene.Field(SiriusoLingvo, description=_('Наименование груза'))
    konservejo_temperatura_reghimo_alia = graphene.Field(SiriusoLingvo, description=_('Иной температурный режим в складе'))
    konservejo_tasko = graphene.Field(SiriusoLingvo, description=_('Задание в складе'))
    konservejo_komento = graphene.Field(SiriusoLingvo, description=_('Комментарий в складе'))
    priskribo_kliento = graphene.Field(SiriusoLingvo, description=_('Комментарий клиента в складе'))
    numero_frajtoletero_kliento = graphene.Field(SiriusoLingvo, description=_('Номер накладной клиента'))
    email_ekspedinto = graphene.Field(SiriusoLingvo, description=_('Email отправителя'))
    email_ricevanto = graphene.Field(SiriusoLingvo, description=_('Email получателя'))
    tipo_mendo = graphene.Field(SiriusoLingvo, description=_('рл тип заявки'))

    ekspedo_kargo = SiriusoFilterConnectionField(DokumentoEkspedoKargoNode,
        description=_('Выводит грузы ММП характеристик документа экспедирования'))
    
    class Meta:
        model = DokumentoEkspedo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'kliento__uuid': ['exact'],
            'kodo': ['exact', 'icontains'],
            'uid': ['exact', 'icontains'],
            'dokumento__speco__id': ['exact', 'icontains'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ekspedo_kargo(self, info, **kwargs):
        return DokumentoEkspedoKargo.objects.filter(ekspedo=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель документов
class DokumentoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    # Связь документов между собой
    ligilo =  SiriusoFilterConnectionField(DokumentoLigiloNode,
        description=_('Выводит связи документов между собой, владельцами которых является документ'))

    ekspedo =  SiriusoFilterConnectionField(DokumentoEkspedoNode,
        description=_('Выводит характеристки экспедирования данного документа'))
    
    projekto = SiriusoFilterConnectionField(TaskojProjektoNode,
        description=_('Выводит проекты данного документа'))

    projekto_posedanto = SiriusoFilterConnectionField(TaskojProjektoPosedantoNode,
        description=_('Выводит владельцев проектов данного документа'))

    class Meta:
        model = Dokumento
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'speco__id': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'taskoj_taskojprojektoposedanto_posedanto_dokumento__uuid': ['exact'],
            'taskoj_taskojprojektoposedanto_posedanto_dokumento__posedanto_organizo__uuid': ['exact'],
            'dokumentoj_dokumentoekspedo_dokumento__kliento__uuid': ['exact'],
            'dokumentoj_dokumentoekspedo_dokumento__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_ligilo(self, info, **kwargs):
        return DokumentoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_ekspedo(self, info, **kwargs):
        return DokumentoEkspedo.objects.filter(dokumento=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_projekto(self, info, **kwargs):
        projekto = TaskojProjektoPosedanto.objects.filter(posedanto_dokumento=self, forigo=False, arkivo=False, 
            publikigo=True).values('projekto')
        return TaskojProjekto.objects.filter(uuid__in=projekto, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_projekto_posedanto(self, info, **kwargs):
        return TaskojProjektoPosedanto.objects.filter(posedanto_dokumento=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель видов услуг экспедирования
class DokumentoSpecoServoEkspedoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'tipo_kosto__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
        'tipo_servo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    tipo_kosto = graphene.Field(SiriusoLingvo, description=_('Тип цены'))
    tipo_servo = graphene.Field(SiriusoLingvo, description=_('Тип посреднической услуги'))

    class Meta:
        model = DokumentoSpecoServoEkspedo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'kodo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель характеристик документов договоры контрагентов
class DokumentoContractNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = DokumentoContract
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class DokumentojQuery(graphene.ObjectType):
    dokumento_dosiero = SiriusoFilterConnectionField(
        DokumentoDosieroNode,
        description=_('Выводит все доступные модели категорий документов')
    )
    dokumento_kategorio = SiriusoFilterConnectionField(
        DokumentoKategorioNode,
        description=_('Выводит все доступные модели категорий документов')
    )
    dokumento_kategorio_ligiloj_tipoj = SiriusoFilterConnectionField(
        DokumentoKategorioLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей категорий документов между собой')
    )
    dokumento_kategorio_ligiloj = SiriusoFilterConnectionField(
        DokumentoKategorioLigiloNode,
        description=_('Выводит все доступные модели связей категорий документов между собой')
    )
    dokumento_tipo = SiriusoFilterConnectionField(
        DokumentoTipoNode,
        description=_('Выводит все доступные модели типов документов')
    )
    dokumento_speco = SiriusoFilterConnectionField(
        DokumentoSpecoNode,
        description=_('Выводит все доступные модели видов документов')
    )
    dokumento_stokejo_tipo = SiriusoFilterConnectionField(
        DokumentoStokejoTipoNode,
        description=_('Выводит все доступные модели типов мест хранения документов')
    )
    dokumento_stokejo = SiriusoFilterConnectionField(
        DokumentoStokejoNode,
        description=_('Выводит все доступные модели логических мест хранения документов')
    )
    dokumento_posedanto_tipo = SiriusoFilterConnectionField(
        DokumentoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев документов')
    )
    dokumento_posedanto_statuso = SiriusoFilterConnectionField(
        DokumentoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца в рамках владения документом')
    )
    dokumento_posedanto = SiriusoFilterConnectionField(
        DokumentoPosedantoNode,
        description=_('Выводит все доступные модели владельцев документов')
    )
    dokumento = SiriusoFilterConnectionField(
        DokumentoNode,
        description=_('Выводит все доступные документы')
    )
    dokumento_ligilo_tipo = SiriusoFilterConnectionField(
        DokumentoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей документов между собой')
    )
    dokumento_ligilo = SiriusoFilterConnectionField(
        DokumentoLigiloNode,
        description=_('Выводит все доступные модели связей документов между собой')
    )
    dokumento_ekspedo = SiriusoFilterConnectionField(
        DokumentoEkspedoNode,
        description=_('Выводит все доступные модели характеристик документов экспедирования'),
        serchi=graphene.String(),
        margheno_serchi=graphene.String() #(marĝeno - поле) список полей поиска через пробел
    )
    dokumento_ekspedo_kargo = SiriusoFilterConnectionField(
        DokumentoEkspedoKargoNode,
        description=_('Выводит все доступные модели характеристик документов экспедирования грузаММП')
    )
    dokumento_speco_servo_ekspedo = SiriusoFilterConnectionField(
        DokumentoSpecoServoEkspedoNode,
        description=_('Выводит все доступные модели видов услуг экспедирования')
    )
    dokumento_contract = SiriusoFilterConnectionField(
        DokumentoContractNode,
        description=_('Выводит все доступные модели характеристик документов договоры контрагентов')
    )

    @staticmethod
    def resolve_dokumento_ekspedo(root, info, **kwargs):
        if 'serchi' in kwargs and 'margheno_serchi' in kwargs:
            margheno_serchi = list(kwargs.get('margheno_serchi').split(sep=None, maxsplit=-1))
            cond_args = []
            masivo_args = []
            for serchi in margheno_serchi:
                # Комбинированные условия
                if serchi == 'adreso_kargado': # Адрес Загрузки
                    masivo_args.append(Q(adreso_kargado__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'adreso_elsxargxado': # Адрес Выгрузки
                    masivo_args.append(Q(adreso_elsxargxado__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'ekspedinto': # Грузоотправитель
                    ekspedinto = Organizo.objects.filter(nomo__enhavo__icontains=kwargs.get('serchi')).values_list('uuid', flat=True)
                    masivo_args.append(Q(ekspedinto__in=ekspedinto))
                elif serchi == 'ricevanto': # Грузополучатель
                    ricevanto = Organizo.objects.filter(nomo__enhavo__icontains=kwargs.get('serchi')).values_list('uuid', flat=True)
                    masivo_args.append(Q(ricevanto__in=ricevanto))
                elif serchi == 'kodo': # номер заявки
                    masivo_args.append(Q(kodo__contains=kwargs.get('serchi')))
                elif serchi == 'organizo_adreso': # ОрганизацияПоАдресу
                    masivo_args.append(Q(organizo_adreso__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'organizo_adreso_elsxargxado': # ОрганизацияПоАдресуРазгрузки
                    masivo_args.append(Q(organizo_adreso_elsxargxado__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'priskribo_kargo': # ОписаниеГруза
                    masivo_args.append(Q(priskribo_kargo__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'urboj_recivado': # ГородПриемки   и    urboj_recivado_nomo - ГородПриемки_nomo
                    urboj_recivado = InformilojUrboj.objects.filter(nomo__enhavo__icontains=kwargs.get('serchi')).values_list('uuid', flat=True)
                    masivo_args.append(Q(urboj_recivado__in=urboj_recivado))
                    masivo_args.append(Q(urboj_recivado_nomo__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'urboj_transdono': # ГородПередачи   и   urboj_transdono_nomo - ГородПередачи_nomo
                    urboj_transdono = InformilojUrboj.objects.filter(nomo__enhavo__icontains=kwargs.get('serchi')).values_list('uuid', flat=True)
                    masivo_args.append(Q(urboj_transdono__in=urboj_transdono))
                    masivo_args.append(Q(urboj_transdono_nomo__enhavo__icontains=kwargs.get('serchi')))
                elif serchi == 'prj_statuso': # Грузополучатель
                    prj_statuso = TaskojProjektoStatuso.objects.filter(nomo__enhavo__icontains=kwargs.get('serchi')).values_list('uuid', flat=True)
                    projekto = TaskojProjekto.objects.filter(statuso__in=prj_statuso).values_list('uuid', flat=True)
                    dokumento = TaskojProjektoPosedanto.objects.filter(projekto__in=projekto).values_list('posedanto_dokumento__uuid', flat=True)
                    masivo_args.append(Q(dokumento__in=dokumento))
                elif serchi == 'telefono': # telefono_kliento - ТелефонЗаказчика   и   telefono_adreso - ТелефонПоАдресу   и
                    # telefono_adreso_elsxargxado - ТелефонПоАдресуРазгрузки
                    # нормально работает выборка до 12 параметров, далее вываливает всё. По этой причине телефоны объединили
                    masivo_args.append(Q(telefono_kliento__enhavo__icontains=kwargs.get('serchi'))|
                                       Q(telefono_adreso__enhavo__icontains=kwargs.get('serchi'))|
                                       Q(telefono_adreso_elsxargxado__enhavo__icontains=kwargs.get('serchi')))
                    
            if len(masivo_args) == 1:
                cond_args.append(masivo_args[0])
            elif len(masivo_args) == 2:
                cond_args.append(masivo_args[0] | masivo_args[1])
            elif len(masivo_args) == 3:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2])
            elif len(masivo_args) == 4:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3])
            elif len(masivo_args) == 5:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4])
            elif len(masivo_args) == 6:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5])
            elif len(masivo_args) == 7:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6])
            elif len(masivo_args) == 8:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7])
            elif len(masivo_args) == 9:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8])
            elif len(masivo_args) == 10:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8] | masivo_args[9])
            elif len(masivo_args) == 11:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8] | masivo_args[9] | masivo_args[10])
            elif len(masivo_args) == 12:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8] | masivo_args[9] | masivo_args[10] | masivo_args[11])
            elif len(masivo_args) == 13:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8] | masivo_args[9] | masivo_args[10] | masivo_args[11] |
                    masivo_args[12])
            elif len(masivo_args) == 14:
                cond_args.append(masivo_args[0] | masivo_args[1] | masivo_args[2] | masivo_args[3] | masivo_args[4] | masivo_args[5] |
                    masivo_args[6] | masivo_args[7] | masivo_args[8] | masivo_args[9] | masivo_args[10] | masivo_args[11] |
                    masivo_args[12] | masivo_args[13])
            return DokumentoEkspedo.objects.filter(*cond_args)
        return DokumentoEkspedo.objects.all()


