"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
import requests
from datetime import datetime
import json
from django.conf import settings
import os 
from django.core.mail import send_mail
from django.template.loader import render_to_string

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo, get_enhavo
from .schema import *
from ..models import *
from ..tasks import task_dokumento_ekspedo_al_1c
from taskoj.models import TaskojProjektoStatuso, TaskojTaskoStatuso
from taskoj.api.mutations import RedaktuKreiTaskojProjektoTaskojPosedanto
from organizoj.models import OrganizoMembro
from .subscription import DokumentoEkspedoEventoj
from .mutation1c import ImportoDokumentoEkspedo1c, dokumento_ekspedo_save
import siriuso.app_logger as _logger

logger = _logger.get_logger(__name__)

# Модель файлов документов
class RedaktuDokumentoDosiero(graphene.Mutation):
    """ 
    Добавление файлов к документам
    """
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    dokumento_dosiero = graphene.Field(DokumentoDosieroNode)

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        formo = graphene.String(description=_('Вид печатной формы'))
        statuso = graphene.String(description=_('Состояние (статус)'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        dosiero = graphene.String(description=_('Присоединенный файл'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        uzanto = info.context.user
        dokumento_dosiero = None
        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_dosiero'):
            if not kwargs.get('nomo', False):
                errors.append(ErrorNode(
                    field='nomo',
                    message=_('Поле обязательно для заполнения')
                ))
            if not kwargs.get('formo', False):
                errors.append(ErrorNode(
                    field='formo',
                    message=_('Поле обязательно для заполнения')
                ))
            if kwargs.get('forigo', False):
                errors.append(ErrorNode(
                    field='forigo',
                    message=_('При создании записи не допустимо указание поля')
                ))
            if kwargs.get('arkivo', False):
                errors.append(ErrorNode(
                    field='arkivo',
                    message=_('При создании записи не допустимо указание поля')
                ))
            if 'dosiero' in info.context.FILES:
                dosiero = info.context.FILES['dosiero']
            else:
                errors.append(ErrorNode(
                    field='dosiero',
                    message=_('Отсутствует присоединённый файл')
                ))

            if not len(errors):
                if kwargs.get('uuid', False):
                    dokumento_dosiero = DokumentoDosiero.objects.create(
                        uuid=kwargs.get('uuid'),
                        forigo=False,
                        arkivo=False,
                        publikigo=kwargs.get('publikigo', False),
                        publikiga_dato=timezone.now(),
                        dosiero=dosiero,
                    )
                else:
                    dokumento_dosiero = DokumentoDosiero.objects.create(
                        forigo=False,
                        arkivo=False,
                        publikigo=kwargs.get('publikigo', False),
                        publikiga_dato=timezone.now(),
                        dosiero=dosiero,
                    )

                if (kwargs.get('nomo', False)):
                    set_enhavo(dokumento_dosiero.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                if (kwargs.get('formo', False)):
                    set_enhavo(dokumento_dosiero.formo, kwargs.get('formo'), info.context.LANGUAGE_CODE)
                if (kwargs.get('statuso', False)):
                    set_enhavo(dokumento_dosiero.statuso, kwargs.get('statuso'), info.context.LANGUAGE_CODE)

                dokumento_dosiero.save()
                status = True
                message = _('dosiero sukcese kreiĝis')
                errors = list()
            else:
                message = _('Nevalida argumentvaloroj')
                for error in errors:
                    message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                        error.message, _('в поле'), error.field)
        else:
            message = _('Недостаточно прав')

        return status, message, errors, dokumento_dosiero

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        dokumento_dosiero = None
        user = info.context.user
        if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                or kwargs.get('nomo', False) or kwargs.get('formo', False)
                or kwargs.get('statuso', False) or 'dosiero' in info.context.FILES):
            errors.append(ErrorNode(
                field='',
                modelo='DokumentoDosiero',
                message=_('Не задано ни одно поле для изменения')
            ))
            message=_('Не задано ни одно поле для изменения')
            return status, message, errors, dokumento_dosiero
        # проверяем наличие записи, если нет, то переходим в создание
        try:
            dokumento_dosiero = DokumentoDosiero.objects.get(uuid=kwargs.get('uuid'))
            if (user.has_perm('dokumentoj.povas_shangxi_dokumentoj_dosiero')
                    or user.has_perm('dokumentoj.povas_forigi_dokumentoj_dosiero')):
                # тут пришла замена файла
                logger.info('=== пришла замена файла или пришел статус === ДОДЕЛАТЬ !!!!????')

                if not len(errors):

                    dokumento_dosiero.forigo = kwargs.get('forigo', dokumento_dosiero.forigo)
                    dokumento_dosiero.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                    dokumento_dosiero.arkivo = kwargs.get('arkivo', dokumento_dosiero.arkivo)
                    dokumento_dosiero.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                    dokumento_dosiero.publikigo = kwargs.get('publikigo', dokumento_dosiero.publikigo)
                    dokumento_dosiero.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                    if (kwargs.get('nomo', False)):
                        set_enhavo(dokumento_dosiero.nomo, kwargs.get('nomo'), 
                                    info.context.LANGUAGE_CODE)
                    if (kwargs.get('formo', False)):
                        set_enhavo(dokumento_dosiero.formo, kwargs.get('formo'), 
                                    info.context.LANGUAGE_CODE)
                    if (kwargs.get('statuso', False)):
                        set_enhavo(dokumento_dosiero.statuso, kwargs.get('statuso'), 
                                    info.context.LANGUAGE_CODE)
                    if 'dosiero' in info.context.FILES:
                        # удаляем старый файл
                        file_location = str(settings.BASE_DIR) + dokumento_dosiero.dosiero.url
                        if os.path.isfile(file_location): 
                            os.remove(file_location) 
                            logger.info("success") 
                        else: 
                            logger.info("File doesn't exists! ", file_location)

                        # добавляем новый файл
                        dokumento_dosiero.dosiero = info.context.FILES['dosiero']

                    dokumento_dosiero.save()
                    status = True
                    message = _('Запись успешно изменена')
                
                pass
            else:
                message = _('Недостаточно прав')
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        except DokumentoDosiero.DoesNotExist:
            status, message, errors, dokumento_dosiero = RedaktuDokumentoDosiero.create(root, info, **kwargs)


        return status, message, errors, dokumento_dosiero

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        uzanto = info.context.user
        dokumento_dosiero = None

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, dokumento_dosiero = RedaktuDokumentoDosiero.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, dokumento_dosiero = RedaktuDokumentoDosiero.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoDosiero(status=status, message=message, errors=errors, dokumento_dosiero=dokumento_dosiero)


# Модель категорий документов
class RedaktuDokumentoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_kategorioj = graphene.Field(DokumentoKategorioNode,
        description=_('Созданная/изменённая категория документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            dokumentoj_kategorioj = DokumentoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    dokumentoj_kategorioj.realeco.set(realeco)
                                else:
                                    dokumentoj_kategorioj.realeco.clear()

                            dokumentoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoKategorio')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_kategorioj = DokumentoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoKategorio')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_kategorioj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoKategorio')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                dokumentoj_kategorioj.forigo = kwargs.get('forigo', dokumentoj_kategorioj.forigo)
                                dokumentoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_kategorioj.arkivo = kwargs.get('arkivo', dokumentoj_kategorioj.arkivo)
                                dokumentoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_kategorioj.publikigo = kwargs.get('publikigo', dokumentoj_kategorioj.publikigo)
                                dokumentoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        dokumentoj_kategorioj.realeco.set(realeco)
                                    else:
                                        dokumentoj_kategorioj.realeco.clear()

                                dokumentoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoKategorio(status=status, message=message, dokumentoj_kategorioj=dokumentoj_kategorioj)


# Модель типов связей категорий документов между собой
class RedaktuDokumentoKategorioLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = graphene.Field(DokumentoKategorioLigiloTipoNode,
        description=_('Созданная/изменённая модель типов связей категорий документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = DokumentoKategorioLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoKategorioLigiloTipo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj = DokumentoKategorioLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoKategorioLigiloTipo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoKategorioLigiloTipo')

                            if not message:

                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.forigo = kwargs.get('forigo', dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.forigo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkivo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikigo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoKategorioLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoKategorioLigiloTipo(status=status, message=message, dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj=dokumentoj_kategorioj_dokumentoj_ligiloj_tipoj)


# Модель связей категорий документов между собой
class RedaktuDokumentoKategorioLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_kategorioj_dokumentoj_ligiloj = graphene.Field(DokumentoKategorioLigiloNode,
        description=_('Созданная/изменённая модель связей категорий документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('ID категории документов владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемой категории документов'))
        tipo_id = graphene.Int(description=_('ID типа связи категорий документов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_kategorioj_dokumentoj_ligiloj = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_kategorioj_dokumentoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = DokumentoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная категория документов владельца связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указана категория документов владелец связи'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = DokumentoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная связываемая категория документов'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указана связываемая категория документов'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = DokumentoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoKategorioLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи категорий документов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи категорий документов'),'tipo_id')

                        if not message:
                            dokumentoj_kategorioj_dokumentoj_ligiloj = DokumentoKategorioLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            dokumentoj_kategorioj_dokumentoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoKategorioLigilo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_kategorioj_dokumentoj_ligiloj = DokumentoKategorioLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_kategorioj_dokumentoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoKategorioLigilo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_kategorioj_dokumentoj_ligiloj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoKategorioLigilo')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = DokumentoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная категория документов владельца связи'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = DokumentoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная связываемая категория документов'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = DokumentoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoKategorioLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи категорий документов'),'tipo_id')

                            if not message:

                                dokumentoj_kategorioj_dokumentoj_ligiloj.forigo = kwargs.get('forigo', dokumentoj_kategorioj_dokumentoj_ligiloj.forigo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj.arkivo = kwargs.get('arkivo', dokumentoj_kategorioj_dokumentoj_ligiloj.arkivo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj.publikigo = kwargs.get('publikigo', dokumentoj_kategorioj_dokumentoj_ligiloj.publikigo)
                                dokumentoj_kategorioj_dokumentoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_kategorioj_dokumentoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_id', False) else dokumentoj_kategorioj_dokumentoj_ligiloj.posedanto
                                dokumentoj_kategorioj_dokumentoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_id', False) else dokumentoj_kategorioj_dokumentoj_ligiloj.ligilo
                                dokumentoj_kategorioj_dokumentoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else dokumentoj_kategorioj_dokumentoj_ligiloj.tipo

                                dokumentoj_kategorioj_dokumentoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoKategorioLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoKategorioLigilo(status=status, message=message, dokumentoj_kategorioj_dokumentoj_ligiloj=dokumentoj_kategorioj_dokumentoj_ligiloj)


# Модель типов документов
class RedaktuDokumentoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_tipoj = graphene.Field(DokumentoTipoNode, 
        description=_('Созданный/изменённый тип документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            dokumentoj_tipoj = DokumentoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    dokumentoj_tipoj.realeco.set(realeco)
                                else:
                                    dokumentoj_tipoj.realeco.clear()

                            dokumentoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoTipo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_tipoj = DokumentoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoTipo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_tipoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoTipo')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                dokumentoj_tipoj.forigo = kwargs.get('forigo', dokumentoj_tipoj.forigo)
                                dokumentoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_tipoj.arkivo = kwargs.get('arkivo', dokumentoj_tipoj.arkivo)
                                dokumentoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_tipoj.publikigo = kwargs.get('publikigo', dokumentoj_tipoj.publikigo)
                                dokumentoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        dokumentoj_tipoj.realeco.set(realeco)
                                    else:
                                        dokumentoj_tipoj.realeco.clear()

                                dokumentoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoTipo(status=status, message=message, dokumentoj_tipoj=dokumentoj_tipoj)


# Модель видов документов
class RedaktuDokumentoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_specoj = graphene.Field(DokumentoSpecoNode,
        description=_('Созданный/изменённый вид документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_specoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_specoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            dokumentoj_specoj = DokumentoSpeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_specoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_specoj.priskribo,
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    dokumentoj_specoj.realeco.set(realeco)
                                else:
                                    dokumentoj_specoj.realeco.clear()

                            dokumentoj_specoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoSpeco')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_specoj = DokumentoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoSpeco')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_specoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoSpeco')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                dokumentoj_specoj.forigo = kwargs.get('forigo', dokumentoj_specoj.forigo)
                                dokumentoj_specoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_specoj.arkivo = kwargs.get('arkivo', dokumentoj_specoj.arkivo)
                                dokumentoj_specoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_specoj.publikigo = kwargs.get('publikigo', dokumentoj_specoj.publikigo)
                                dokumentoj_specoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_specoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_specoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_specoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        dokumentoj_specoj.realeco.set(realeco)
                                    else:
                                        dokumentoj_specoj.realeco.clear()

                                dokumentoj_specoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoSpeco(status=status, message=message, dokumentoj_specoj=dokumentoj_specoj)


# Модель типов мест хранения документов
class RedaktuDokumentoStokejoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_stokejoj_tipoj = graphene.Field(DokumentoStokejoTipoNode, 
        description=_('Созданный/изменённый тип мест хранения документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_stokejoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_stokejoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            dokumentoj_stokejoj_tipoj = DokumentoStokejoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_stokejoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_stokejoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoStokejoTipo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_stokejoj_tipoj = DokumentoStokejoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_stokejoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoStokejoTipo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_stokejoj_tipoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoStokejoTipo')
                            if not message:

                                dokumentoj_stokejoj_tipoj.forigo = kwargs.get('forigo', dokumentoj_stokejoj_tipoj.forigo)
                                dokumentoj_stokejoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_stokejoj_tipoj.arkivo = kwargs.get('arkivo', dokumentoj_stokejoj_tipoj.arkivo)
                                dokumentoj_stokejoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_stokejoj_tipoj.publikigo = kwargs.get('publikigo', dokumentoj_stokejoj_tipoj.publikigo)
                                dokumentoj_stokejoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_stokejoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_stokejoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_stokejoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoStokejoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoStokejoTipo(status=status, message=message, dokumentoj_stokejoj_tipoj=dokumentoj_stokejoj_tipoj)


# Модель документов
class RedaktuDokumento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj = graphene.Field(DokumentoNode, 
        description=_('Созданный/изменённый документ'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий документов'))
        tipo_id = graphene.Int(description=_('Код типа документов'))
        speco_id = graphene.Int(description=_('Код вида документов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj = None
        realeco = Realeco.objects.none()
        kategorio = DokumentoKategorio.objects.none()
        tipo = None
        speco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = DokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория документов'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = DokumentoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип документов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип документов'),'tipo_id')

                        if not message:
                            if 'speco_id' in kwargs:
                                try:
                                    speco = DokumentoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoSpeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный вид документов'),'speco_id')
                            else:
                                message = '{}: {}'.format(_('Не указан вид документов'),'speco_id')

                        if not message:
                            dokumentoj = Dokumento.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                speco = speco,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    dokumentoj.realeco.set(realeco)
                                else:
                                    dokumentoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    dokumentoj.kategorio.set(kategorio)
                                else:
                                    dokumentoj.kategorio.clear()

                            dokumentoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumento')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('speco_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj = Dokumento.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumento')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumento')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = DokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория документов'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = DokumentoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoTipo.DoesNotExist:
                                        message = _('Неверный тип документов')
                            if not message:
                                if 'speco_id' in kwargs:
                                    try:
                                        speco = DokumentoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoSpeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный вид документов'),'speco_id')

                            if not message:

                                dokumentoj.forigo = kwargs.get('forigo', dokumentoj.forigo)
                                dokumentoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj.arkivo = kwargs.get('arkivo', dokumentoj.arkivo)
                                dokumentoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj.publikigo = kwargs.get('publikigo', dokumentoj.publikigo)
                                dokumentoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj.autoro = uzanto
                                dokumentoj.tipo = tipo if kwargs.get('tipo_id', False) else dokumentoj.tipo
                                dokumentoj.speco = speco if kwargs.get('speco_id', False) else dokumentoj.speco

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        dokumentoj.realeco.set(realeco)
                                    else:
                                        dokumentoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        dokumentoj.kategorio.set(kategorio)
                                    else:
                                        dokumentoj.kategorio.clear()

                                dokumentoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Dokumento.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumento(status=status, message=message, dokumentoj=dokumentoj)


# Модель логических мест хранения документов
class RedaktuDokumentoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_stokejoj = graphene.Field(DokumentoStokejoNode, 
        description=_('Созданные/изменённые логические места хранения документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('Ресурс владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения документов на основе которого создано это место хранения в документе'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        tipo = None
        dokumentoj_stokejoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = DokumentoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoStokejoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип места хранения документов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип места хранения документов'),'tipo_id')

                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Dokumento.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Dokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный документ владелец места хранения'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указан документ владелец места хранения'),'posedanto_id')

                        if not message:
                            dokumentoj_stokejoj = DokumentoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                tipo=tipo,
                                posedanto=posedanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoStokejo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_uuid', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_stokejoj = DokumentoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoStokejo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_stokejoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoStokejo')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = DokumentoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoStokejoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип места хранения документов'),'tipo_id')
                                else:
                                    message = '{}: {}'.format(_('Не указан тип места хранения документов'),'tipo_id')

                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = Dokumento.objects.get(id=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Dokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный документ владелец места хранения'),'posedanto_uuid')
                                else:
                                    message = '{}: {}'.format(_('Не указан документ владелец места хранения'),'posedanto_uuid')

                            if not message:

                                dokumentoj_stokejoj.forigo = kwargs.get('forigo', dokumentoj_stokejoj.forigo)
                                dokumentoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_stokejoj.arkivo = kwargs.get('arkivo', dokumentoj_stokejoj.arkivo)
                                dokumentoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_stokejoj.publikigo = kwargs.get('publikigo', dokumentoj_stokejoj.publikigo)
                                dokumentoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else dokumentoj_stokejoj.tipo
                                dokumentoj_stokejoj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else dokumentoj_stokejoj.posedanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoStokejo(status=status, message=message, dokumentoj_stokejoj=dokumentoj_stokejoj)


# Модель типов владельцев документов
class RedaktuDokumentoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_posedantoj_tipoj = graphene.Field(DokumentoPosedantoTipoNode,
        description=_('Созданный/изменённый тип владельцев документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'nomo')

                        if not message:
                            dokumentoj_posedantoj_tipoj = DokumentoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_posedantoj_tipoj.nomo, kwargs.get('nomo'),
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_posedantoj_tipoj.priskribo,
                                           kwargs.get('priskribo'),
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoPosedantoTipo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_posedantoj_tipoj = DokumentoPosedantoTipo.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoPosedantoTipo')
                            elif not uzanto.has_perm(
                                    'dokumentoj.povas_shanghi_dokumentoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoPosedantoTipo')
                            if not message:

                                dokumentoj_posedantoj_tipoj.forigo = kwargs.get('forigo',
                                                                                       dokumentoj_posedantoj_tipoj.forigo)
                                dokumentoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo',
                                                                                                              False) else None
                                dokumentoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo',
                                                                                       dokumentoj_posedantoj_tipoj.arkivo)
                                dokumentoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo',
                                                                                                              False) else None
                                dokumentoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo',
                                                                                          dokumentoj_posedantoj_tipoj.publikigo)
                                dokumentoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get(
                                    'publikigo', False) else None
                                dokumentoj_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_posedantoj_tipoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoPosedantoTipo(status=status, message=message,
                                                   dokumentoj_posedantoj_tipoj=dokumentoj_posedantoj_tipoj)


# Модель статусов владельца в рамках владения документом
class RedaktuDokumentoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_posedantoj_statusoj = graphene.Field(DokumentoPosedantoStatusoNode,
        description=_('Созданный/изменённый тип статусов владельцев документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'nomo')

                        if not message:
                            dokumentoj_posedantoj_statusoj = DokumentoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_posedantoj_statusoj.nomo, kwargs.get('nomo'),
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_posedantoj_statusoj.priskribo,
                                           kwargs.get('priskribo'),
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoPosedantoStatuso')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_posedantoj_statusoj = DokumentoPosedantoStatuso.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm(
                                    'dokumentoj.povas_forigi_dokumentoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoPosedantoStatuso')
                            elif not uzanto.has_perm(
                                    'dokumentoj.povas_shanghi_dokumentoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoPosedantoStatuso')
                            if not message:

                                dokumentoj_posedantoj_statusoj.forigo = kwargs.get('forigo',
                                                                                          dokumentoj_posedantoj_statusoj.forigo)
                                dokumentoj_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get(
                                    'forigo', False) else None
                                dokumentoj_posedantoj_statusoj.arkivo = kwargs.get('arkivo',
                                                                                          dokumentoj_posedantoj_statusoj.arkivo)
                                dokumentoj_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get(
                                    'arkivo', False) else None
                                dokumentoj_posedantoj_statusoj.publikigo = kwargs.get('publikigo',
                                                                                             dokumentoj_posedantoj_statusoj.publikigo)
                                dokumentoj_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get(
                                    'publikigo', False) else None
                                dokumentoj_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_posedantoj_statusoj.nomo, kwargs.get('nomo'),
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_posedantoj_statusoj.priskribo, kwargs.get('priskribo'),
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoPosedantoStatuso(status=status, message=message,
                                                      dokumentoj_posedantoj_statusoj=dokumentoj_posedantoj_statusoj)


# Модель владельцев документов
class RedaktuDokumentoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_posedantoj = graphene.Field(DokumentoPosedantoNode, 
        description=_('Созданный/изменённый владелец документов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        dokumento_id = graphene.Int(description=_('Объект владения'))
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец документа'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец документа'))
        parto = graphene.Int(description=_('Размер доли владения'))
        tipo_id = graphene.Int(description=_('Тип владельца документа'))
        statuso_id = graphene.Int(description=_('Статус владельца документа'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto_uzanto = None
        realeco = None
        dokumento = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        dokumentoj_posedantoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_posedantoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                          arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность'), 'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'realeco_id')

                        if not message:
                            if (kwargs.get('posedanto_uzanto_id', False)):
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'),
                                        publikigo=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')

                        if not message:
                            if (kwargs.get('dokumento_id', False)):
                                try:
                                    dokumento = Dokumento.objects.get(
                                        id=kwargs.get('dokumento_id'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except Dokumento.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный документ владения'), 'dokumento_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'dokumento_id')

                        if not message:
                            if (kwargs.get('posedanto_organizo_uuid', False)):
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец документа'),
                                                               'posedanto_organizo_uuid')

                        if not message:
                            if (kwargs.get('tipo_id', False)):
                                try:
                                    tipo = DokumentoPosedantoTipo.objects.get(
                                        id=kwargs.get('tipo_id'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except DokumentoPosedantoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип владельца документа'), 'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'tipo_id')

                        if not message:
                            if (kwargs.get('statuso_id', False)):
                                try:
                                    statuso = DokumentoPosedantoStatuso.objects.get(
                                        id=kwargs.get('statuso_id'),
                                        forigo=False,
                                        arkivo=False,
                                        publikigo=True
                                    )
                                except DokumentoPosedantoStatuso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный статус владельца документа'), 'statuso_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'), 'statuso_id')

                        if not message:
                            dokumentoj_posedantoj = DokumentoPosedanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto=posedanto_uzanto,
                                dokumento=dokumento,
                                realeco=realeco,
                                posedanto_organizo=posedanto_organizo,
                                parto=kwargs.get('parto', 100),
                                tipo=tipo,
                                statuso=statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            dokumentoj_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoPosedanto')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False)
                            or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('dokumento_uuid', False) or kwargs.get('parto', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_posedantoj = DokumentoPosedanto.objects.get(uuid=kwargs.get('uuid'),
                                                                                                forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_posedantoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoPosedanto')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_posedantoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoPosedanto')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                              arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность'), 'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'),
                                            publikigo=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')

                            if not message:
                                if (kwargs.get('dokumento_uuid', False)):
                                    try:
                                        dokumento = Dokumento.objects.get(
                                            uuid=kwargs.get('dokumento_uuid'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except Dokumento.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный документ владения'), 'dokumento_uuid')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация владелец документа'),
                                                                   'posedanto_organizo_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = DokumentoPosedantoTipo.objects.get(
                                            id=kwargs.get('tipo_id'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except DokumentoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца документа'), 'tipo_id')

                            if not message:
                                if (kwargs.get('statuso_id', False)):
                                    try:
                                        statuso = DokumentoPosedantoStatuso.objects.get(
                                            id=kwargs.get('statuso_id'),
                                            forigo=False,
                                            arkivo=False,
                                            publikigo=True
                                        )
                                    except DokumentoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца документа'), 'statuso_id')

                            if not message:
                                dokumentoj_posedantoj.forigo = kwargs.get('forigo', dokumentoj_posedantoj.forigo)
                                dokumentoj_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else dokumentoj_posedantoj.foriga_dato
                                dokumentoj_posedantoj.arkivo = kwargs.get('arkivo', dokumentoj_posedantoj.arkivo)
                                dokumentoj_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else dokumentoj_posedantoj.arkiva_dato
                                dokumentoj_posedantoj.publikigo = kwargs.get('publikigo', dokumentoj_posedantoj.publikigo)
                                dokumentoj_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else dokumentoj_posedantoj.publikiga_dato
                                dokumentoj_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else dokumentoj_posedantoj.realeco
                                dokumentoj_posedantoj.dokumento = dokumento if kwargs.get('dokumento_uuid', False) else dokumentoj_posedantoj.dokumento
                                dokumentoj_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else dokumentoj_posedantoj.posedanto_uzanto
                                dokumentoj_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else dokumentoj_posedantoj.posedanto_organizo
                                dokumentoj_posedantoj.parto = kwargs.get('parto', dokumentoj_posedantoj.parto)
                                dokumentoj_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else dokumentoj_posedantoj.tipo
                                dokumentoj_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else dokumentoj_posedantoj.statuso

                                dokumentoj_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoPosedanto(status=status, message=message,
                                               dokumentoj_posedantoj=dokumentoj_posedantoj)
    

# Модель типов связей документов между собой
class RedaktuDokumentoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_ligiloj_tipoj = graphene.Field(DokumentoLigiloTipoNode, 
        description=_('Созданная/изменённая модель типов связей документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_ligiloj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            dokumentoj_ligiloj_tipoj = DokumentoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoLigiloTipo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_ligiloj_tipoj = DokumentoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoLigiloTipo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoLigiloTipo')
                            if not message:

                                dokumentoj_ligiloj_tipoj.forigo = kwargs.get('forigo', dokumentoj_ligiloj_tipoj.forigo)
                                dokumentoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', dokumentoj_ligiloj_tipoj.arkivo)
                                dokumentoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', dokumentoj_ligiloj_tipoj.publikigo)
                                dokumentoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_ligiloj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(dokumentoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                dokumentoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoLigiloTipo(status=status, message=message, dokumentoj_ligiloj_tipoj=dokumentoj_ligiloj_tipoj)


# Модель связей документов между собой
class RedaktuDokumentoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    dokumentoj_ligiloj = graphene.Field(DokumentoLigiloNode, 
        description=_('Созданная/изменённая модель связей документов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('Ресурс владелец связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('Место хранения владельца связи'))
        ligilo_id = graphene.Int(description=_('Связываемый документ'))
        tipo_id = graphene.Int(description=_('Тип связи документов'))
        konektilo = graphene.Int(description=_('Разъём (слот), который занимает этот документ у родительского документа'))
        priskribo = graphene.String(description=_('Описание'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        dokumentoj_ligiloj = None
        posedanto = None
        posedanto_stokejo = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Dokumento.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Dokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный документ владелец связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указан документ владелец связи'),'posedanto_id')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = DokumentoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoStokejo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = Dokumento.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Dokumento.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный связываемый документ'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан связываемый документ'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = DokumentoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except DokumentoLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи документов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи документов'),'tipo_id')

                        if not message:
                            dokumentoj_ligiloj = DokumentoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                posedanto_stokejo=posedanto_stokejo,
                                ligilo=ligilo,
                                tipo=tipo,
                                konektilo=kwargs.get('konektilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('priskribo', False)):
                                set_enhavo(dokumentoj_ligiloj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            dokumentoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав RedaktuDokumentoLigilo')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uuid', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            dokumentoj_ligiloj = DokumentoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления RedaktuDokumentoLigilo')
                            elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_ligiloj'):
                                message = _('Недостаточно прав для изменения RedaktuDokumentoLigilo')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = Dokumento.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Dokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный документ владелец связи'),'posedanto_uuid')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = DokumentoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoStokejo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_uuid' in kwargs:
                                    try:
                                        ligilo = Dokumento.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Dokumento.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный связываемый документ'),'ligilo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = DokumentoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except DokumentoLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи документов'),'tipo_id')

                            if not message:

                                dokumentoj_ligiloj.forigo = kwargs.get('forigo', dokumentoj_ligiloj.forigo)
                                dokumentoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                dokumentoj_ligiloj.arkivo = kwargs.get('arkivo', dokumentoj_ligiloj.arkivo)
                                dokumentoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                dokumentoj_ligiloj.publikigo = kwargs.get('publikigo', dokumentoj_ligiloj.publikigo)
                                dokumentoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                dokumentoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else dokumentoj_ligiloj.posedanto
                                dokumentoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else dokumentoj_ligiloj.posedanto_stokejo
                                dokumentoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_uuid', False) else dokumentoj_ligiloj.ligilo
                                dokumentoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else dokumentoj_ligiloj.tipo
                                dokumentoj_ligiloj.konektilo = kwargs.get('konektilo',  dokumentoj_ligiloj.konektilo)

                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(dokumentoj_ligiloj.priskribo, 
                                            kwargs.get('priskribo'), 
                                            info.context.LANGUAGE_CODE)

                                dokumentoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except DokumentoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoLigilo(status=status, message=message, dokumentoj_ligiloj=dokumentoj_ligiloj)


# сохраняем проект
def projekto_save(dokumento_ekspedo, projekto):

    def eventoj():
        DokumentoEkspedoEventoj.dokumento_projekto(dokumento_ekspedo.kliento, dokumento_ekspedo, projekto)

    projekto.save()
    transaction.on_commit(eventoj)


def send_email_raportado(emailto, nomo, priskribo, organizo='', retposhto=''):
    """
    отправка письма о новом статусе документа экспедирования
    """
    from_email = 'test-mailer@tmail.tehnokom.su'
    email_subject = _('Изменение статуса документа экспедирования')
    email_body = """%s
""" % ( nomo,
                                    )
    if priskribo:
        email_body = """%s
%s """ % (email_body, 
            priskribo)
    if organizo:
        email_body = """%s
%s""" % (email_body, 
            organizo)
    if retposhto:
        email_body = """%s
%s""" % (email_body, 
            retposhto)
    mail_to = ['magcourier@mail.ru']
    mail_to.append(emailto)
    data = {'nomo': nomo,
            'priskribo': priskribo,
            'organizo': organizo,
            'retposhto': retposhto}
    html_body = render_to_string('send_email/send_email_raportado.html', data)
    result = send_mail(email_subject,
              email_body,
              from_email,
              mail_to,
              fail_silently=True,
              html_message=html_body
              )


# Модель документов экспедирования
class RedaktuDokumentoEkspedo(graphene.Mutation):
    """
    Версия 1
    03.02.2022
    Сделано только добавление/замены ссылки на файлы (dosiero - DokumentoDosiero)
    """
    status = graphene.Boolean()
    errors = graphene.List(ErrorNode)
    message = graphene.String()
    dokumento_ekspedo = graphene.Field(DokumentoEkspedoNode, 
        description=_('Созданная/изменённая модель документов экспедирования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        
        dosiero_uuid = graphene.String(description=_('UUID присоединённого файла'))

        prj_statuso_uuid = graphene.String(description=_('UUID нового статуса заявки'))

        # для создание характеристик экспедирования
        # kodo = graphene.String(description=_('Kodo')) создаётся в 1с
        adreso_elsxargxado = graphene.String(description=_('Адрес выгрузки'))
        adreso_kargado = graphene.String(description=_('Адрес загрузки'))
        valuto_kliento_id = graphene.Int(description=_('Валюта заказчика'))
        pezo = graphene.Int(description=_('Вес'))
        tempo_elsxargxado = graphene.DateTime(description=_("Время выгрузки с"))
        tempo_s = graphene.DateTime(description=_("Время с"))
        ekspedinto_uuid = graphene.String(description=_('Грузоотправитель'))
        ricevanto_uuid = graphene.String(description=_('Грузополучатель'))
        kontrakto_kliento_uuid = graphene.String(description=_('Договор заказчика'))
        kliento_uuid = graphene.String(description=_('Заказчик'))
        kliento_sciigi = graphene.Boolean(description=_("Признак: заказчик извещен"))
        komento = graphene.String(description=_('Комментарий'))
        komento_adreso_elsxargxado = graphene.String(description=_('Комментарий адреса выгрузки'))
        komento_adreso_kargado = graphene.String(description=_('Комментарий адреса загрузки'))
        komento_nuligo = graphene.String(description=_('Комментарий отмены'))
        kombatanto_kliento_uuid = graphene.String(description=_('Контактное лицо заказчика'))
        kombatanto_adresoj = graphene.String(description=_('Контактное лицо по адресу загрузки'))
        kombatanto_adresoj_elsxargxado = graphene.String(description=_('Контактное лицо по адресу разгрузки'))
        volumeno = graphene.Float(description=_('Объем'))
        priskribo_kargo = graphene.String(description=_('Описание груза'))
        organizo_uuid = graphene.String(description=_('uuid обслуживающей организации'))
        organizo_adreso = graphene.String(description=_('Организация по адресу'))
        organizo_adreso_elsxargxado = graphene.String(description=_('Организация по адресу разгрузки'))
        # ПричинаОтмены_Key - ссылка ??? (причи́н||а kaŭzo; kialo) (отме́н||а nuligo)
        # kialo_nuligo_key = models.UUIDField(_('Kialo nuligo'), default=None, blank=True, null=True)
        kosto_kargo = graphene.Float(description=_('Стоимость груза'))
        monsumo_kliento = graphene.Float(description=_('Сумма заказчика'))
        monsumo_kliento_fiksa_mane = graphene.Boolean(description=_('Сумма заказчика установлена вручную'))
        monsumo_transportisto = graphene.Float(description=_('Сумма перевозчика'))
        monsumo_transportisto_fiksa_mane = graphene.Boolean(description=_('Сумма перевозчика установлена вручную'))
        telefono_kliento = graphene.String(description=_('Телефон заказчика'))
        telefono_adreso = graphene.String(description=_('Телефон по адресу'))
        telefono_adreso_elsxargxado = graphene.String(description=_('Телефон по адресу разгрузки'))
        fakto = graphene.Boolean(description=_('Факт'))
        kosto_kliento = graphene.Float(description=_('Цена заказчика'))
        kosto_transportisto = graphene.Float(description=_('Цена перевозчика'))
        partopago_kliento = graphene.Float(description=_('Аванс заказчика'))
        partopago_transportisto = graphene.Float(description=_('Аванс перевозчика'))
        aldona_kondicxaro = graphene.String(description=_('Дополнительные условия'))
        # АвторЛогист_Key - ссылка
        # autoro_logist_key = models.UUIDField(_('Autoro logist key'), default=None, blank=True, null=True)
        impostokvoto_kliento = graphene.String(description=_('Ставка НДС заказчика'))
        impostokvoto_transportisto = graphene.String(description=_('Ставка НДС перевозчика'))
        priskribo_kalkulado = graphene.String(description=_('Комментарий расчета'))
        rendevuejo = graphene.Boolean(description=_('Сборный груз'))
        numero_frajtoletero = graphene.String(description=_('Номер накладной СК'))
        pezo_fakto = graphene.Float(description=_('Вес фактический'))
        pezo_kalkula = graphene.Float(description=_('Вес расчетный'))
        dangxera_kargo = graphene.Boolean(description=_('Опасный груз'))
        priskribo_dangxera_kargo = graphene.String(description=_('Описание опасного груза'))
        klaso_dangxera = graphene.String(description=_('Класс опасности'))
        retropasxo_tsd = graphene.Boolean(description=_('Возврат ТСД'))
        akcepto_liveri_nelabora_tempo = graphene.Boolean(description=_('Прием доставка в нерабочее время'))
        aldone_pakumo = graphene.Boolean(description=_('Дополнительная упаковка'))
        temperatura_reghimo_uuid = graphene.String(description=_('UUID температурного режима'))
        # СозданиеХЦ_Key (Создание - faro)
        # faro_hc_key = models.UUIDField(_('Faro HC UUID'), default=None, blank=True, null=True)
        indikatoro_temperatura = graphene.Boolean(description=_('Датчик температуры'))
        livero_tp = graphene.Boolean(description=_('Предоставление ТП'))
        aldone_postulo_hc = graphene.String(description=_('Дополнительные требования ХЦ'))
        urboj_recivado_uuid = graphene.String(description=_('UUID города приемки'))
        urboj_recivado_nomo = graphene.String(description=_('Город приемки'))
        urboj_transdono_uuid = graphene.String(description=_('UUID города передачи'))
        urboj_transdono_nomo = graphene.String(description=_('Город передачи'))
        aliaj_servo = graphene.String(description=_('Иные услуги'))
        fordono_kargo_fio = graphene.String(description=_('Передача груза ФИО'))
        fordono_kargo_dato = graphene.Date(description=_("ФПередача груза дата"))
        fordono_kargo_tempo = graphene.Time(description=_("ФПередача груза время"))
        konservejo_ricevado_dato = graphene.Date(description=_("СКЛ дата приемки"))
        konservejo_ricevado_tempo_de = graphene.Time(description=_("СКЛ время приемки с"))
        konservejo_ricevado_tempo_en = graphene.Time(description=_("СКЛ время приемки по"))
        konservejo_fordono_dato = graphene.Date(description=_("СКЛ дата передачи"))
        konservejo_fordono_tempo_de = graphene.Time(description=_("СКЛ время передачи с"))
        konservejo_fordono_tempo_en = graphene.Time(description=_("СКЛ время передачи по"))
        asekurado = graphene.Boolean(description=_('Требуется страхование'))
        horoj_sxargxistoj = graphene.Int(description=_('Количество часов грузчики'))
        kvanto_sxargxistoj = graphene.Int(description=_('Количество грузчиков'))
        kvanto_pecoj = graphene.Int(description=_('Количество мест'))
        sxargxisto_rigilaro = graphene.Int(description=_('Требуются грузчики такелажники'))
        en_peza_kategorio = graphene.Boolean(description=_('Тяжеловес'))
        nomo_kargo = graphene.String(description=_('Наименование груза'))
        # Транспорт_Key (тра́нспорт trajno)
        # trajno_key = models.UUIDField(_('Trajno UUID'), default=None, blank=True, null=True)
        # Водитель_Key (Водитель kondukisto)
        # kondukisto_key = models.UUIDField(_('Kondukisto UUID'), default=None, blank=True, null=True)
        konservejo_temperatura_reghimo_1 = graphene.Boolean(description=_('скл температурный режим 1'))
        konservejo_temperatura_reghimo_2 = graphene.Boolean(description=_('скл температурный режим 2'))
        konservejo_temperatura_reghimo_3 = graphene.Boolean(description=_('скл температурный режим 3'))
        konservejo_temperatura_reghimo_4 = graphene.Boolean(description=_('скл температурный режим 4'))
        konservejo_temperatura_reghimo_alia = graphene.String(description=_('скл температурный режим иной'))
        konservejo_temperatura_reghimo_5 = graphene.Boolean(description=_('скл температурный режим 5'))
        konservejo_temperatura_reghimo_6 = graphene.Boolean(description=_('скл температурный режим 6'))
        konservejo_tasko = graphene.String(description=_('скл задание'))
        uid = graphene.String(description=_('УИД'))
        konservejo_komento = graphene.String(description=_('скл комментарий'))
        liveranto_uuid = graphene.String(description=_('UUID рл поставщик'))
        liveranto_kontrakto_uuid = graphene.String(description=_('Kontrakto liveranto UUID'))
        kombatanto_kontrakto_uuid = graphene.String(description=_('UUID рл контактное лицо поставщика'))
        trajno_venigo_dato = graphene.Date(description=_("рл дата подачи транспорт"))
        trajno_venigo_tempo = graphene.Time(description=_("рл время подачи транспорт"))
        trajno_finigho_dato = graphene.Date(description=_("рл дата окончания транспорт"))
        trajno_finigho_tempo = graphene.Time(description=_("рл время окончания транспорт"))
        # ТранспортСоСкладаГК_Key (тра́нспорт trajno)
        # trajno_konservejo_gk_key = models.UUIDField(_('Trajno konservejo gk UUID'), default=None, blank=True, null=True)
        # ВодительСоСкладаГК_Key (Водитель kondukisto)
        # kondukisto_konservejo_gk_key = models.UUIDField(_('Kondukisto konservejo gk UUID'), default=None, blank=True, null=True)
        sensoro_temperatura_2 = graphene.Boolean(description=_('Датчик температуры 2'))
        sensoro_temperatura_3 = graphene.Boolean(description=_('Датчик температуры 3'))
        sensoro_temperatura_4 = graphene.Boolean(description=_('Датчик температуры 4'))
        sensoro_temperatura_5 = graphene.Boolean(description=_('Датчик температуры 5'))
        sensoro_temperatura_alia = graphene.Boolean(description=_('Датчик температуры иной'))
        Livero_tp_2 = graphene.Boolean(description=_('Предоставление ТП 2'))
        Livero_tp_3 = graphene.Boolean(description=_('Предоставление ТП 3'))
        Livero_tp_4 = graphene.Boolean(description=_('Предоставление ТП 4'))
        Livero_tp_5 = graphene.Boolean(description=_('Предоставление ТП 5'))
        Livero_tp_alia = graphene.Boolean(description=_('Предоставление ТП иной'))
        priskribo_kliento = graphene.String(description=_('скл комментарий клиента'))
        numero_frajtoletero_kliento = graphene.String(description=_('Номер накладной клиента'))
        email_ekspedinto = graphene.String(description=_('Email отправителя'))
        email_ricevanto = graphene.String(description=_('Email получателя'))
        plenumado_dato = graphene.DateTime(description=_("Дата на исполнении"))
        fiksa_termoskribi = graphene.Boolean(description=_('рл стационарный термописец'))
        fiksa_termoskribi_2 = graphene.Boolean(description=_('рл стационарный термописец 2'))
        fiksa_termoskribi_3 = graphene.Boolean(description=_('рл стационарный термописец 3'))
        fiksa_termoskribi_4 = graphene.Boolean(description=_('рл стационарный термописец 4'))
        fiksa_termoskribi_5 = graphene.Boolean(description=_('рл стационарный термописец 5'))
        fiksa_termoskribi_alia = graphene.Boolean(description=_('рл стационарный термописец иной'))
        tipo_mendo = graphene.String(description=_('рл тип заявки'))
                    # "ИдМаршрута": 1,

            # рл_ПодразделениеЗаказчика_Key (Подразделение  (отдел, раздел, секция) sekcio)
            # sekcio_kliento_key = models.UUIDField(_('Sekcio kliento UUID'), default=None, blank=True, null=True)
        sxargxistoj_sxargxado = graphene.Boolean(description=_('рл грузчики погрузка'))
        sxargxistoj_elsxargxado = graphene.Boolean(description=_('рл грузчики выгрузка'))
        # # параметры создаваемого проекта
        # projekto_statuso_id = graphene.Int(description=_('Статус проекта')) # по умолчанию = 7
        # # параметры создаваемой задачи
        # tasko_numero = graphene.List(graphene.Int,description=_('Порядковый номер задач')) # "LineNumber": 1, # обязательное поле для маршрута
        # tasko_kom_dato = graphene.List(graphene.DateTime,description=_('Дата начала выполнения задач')) # "ДатаНачала": 	"2021-09-02T09:00:00",
        # tasko_fin_dato = graphene.List(graphene.DateTime,description=_('Дата окончания выполнения задач')) #  "ДатаОкончания": "2021-09-03T13:00:00",
        # tasko_kom_adreso = graphene.List(graphene.String,description=_('Адрес начала выполнения задач')) # "АдресНачала": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
        #     # "АдресНачала_Type": "Edm.String",
        # tasko_fin_adreso = graphene.List(graphene.String,description=_('Адрес окончания выполнения задач')) # "АдресКонца": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
        #     # "АдресКонца_Type": "Edm.String",
        # tasko_statuso_id = graphene.List(graphene.Int,description=_('Статус задач')) # "Состояние_Key": "67896102-d228-11e5-80f8-000c298958d9",
        # tasko_priskribo = graphene.List(graphene.String,description=_('Комментарии задач')) # "Комментарий": "принимаем до 12-00",
        # tasko_nomo = graphene.List(graphene.String,description=_('Название задач'))# "ИдентификаторМаршрута": "Маршрут № 1"},
        # # параметры перевозимого груза
        # kargo_numero = graphene.List(graphene.Int,description=_('Порядковый номер'))
        # kargo_nomo = graphene.List(graphene.String,description=_('Наименование'))
        # kargo_kvanto_pecoj = graphene.List(graphene.Int,description=_('Количество мест'))
        # kargo_longo = graphene.List(graphene.Float,description=_('Длина'))
        # kargo_largho = graphene.List(graphene.Float,description=_('Ширина'))
        # kargo_alto = graphene.List(graphene.Float,description=_('Высота'))
        # kargo_pezo_fakta = graphene.List(graphene.Float,description=_('Вес фактический'))
        # kargo_volumeno = graphene.List(graphene.Float,description=_('Объем'))
        # kargo_pezo_volumena = graphene.List(graphene.Float,description=_('Вес объемный'))
        # kargo_tipo_pakumoj_uuid = graphene.List(graphene.String,description=_('UUID типа упаковки'))
        # kargo_indikatoro = graphene.List(graphene.Boolean,description=_('Нуженли датчик'))
        # kargo_numero_indikatoro = graphene.List(graphene.String,description=_('Серийный номер датчика'))
        # kargo_temperatura_reghimo_uuid = graphene.List(graphene.String,description=_('UUID терморежима'))
        # kargo_grave = graphene.List(graphene.Boolean,description=_('Опасный/Неопасный'))
        # kargo_retropasxo_pakumo = graphene.List(graphene.Boolean,description=_('Возврат упаковки'))
        # kargo_retropasxo_indikatoro = graphene.List(graphene.Boolean,description=_('Возврат датчика'))

    @staticmethod
    def kontrolanta_argumentoj(root, info, **kwargs):
        """Проверка аргументов
        Что бы не проверять аргументы в create и в edit, проверим отдельно

        Args:
            дублируем как и в create и в edit
        """
        # uzanto = info.context.user
        # возвращаемые данные:
        errors = list()
        # проверяем наличие записей с таким кодом
        if 'kliento_uuid' in kwargs:
                try:
                    kliento = Organizo.objects.get(uuid=kwargs.get('kliento_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['kliento'] = kliento
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='kliento_uuid',
                        message=_('Неверный uuid организации')
                    ))
        if 'organizo_uuid' in kwargs: # Организация_Key
            if not len(errors):
                try:
                    organizo = Organizo.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['organizo'] = organizo
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='organizo_uuid',
                        message=_('Неверный uuid')
                    ))
        if 'kontrakto_kliento_uuid' in kwargs:#ДоговорЗаказчика_Key
            if not len(errors):
                try:
                    kontrakto_kliento = DokumentoContract.objects.get(uuid=kwargs.get('kontrakto_kliento_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['kontrakto_kliento'] = kontrakto_kliento
                except DokumentoContract.DoesNotExist:
                    errors.append(ErrorNode(
                        field='kontrakto_kliento_uuid',
                        message=_('Неверный uuid в DokumentoContract')
                    ))
        if 'kombatanto_kliento_uuid' in kwargs: # КонтактноеЛицоЗаказчика
            if not len(errors):
                try:
                    kombatanto_kliento = Kombatanto.objects.get(uuid=kwargs.get('kombatanto_kliento_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['kombatanto_kliento'] = kombatanto_kliento
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='kombatanto_kliento_uuid',
                        message=_('Неверный uuid')
                    ))
        if 'urboj_recivado_uuid' in kwargs: # ГородПриемки
            if not len(errors):
                try:
                    urboj_recivado = InformilojUrboj.objects.get(uuid=kwargs.get('urboj_recivado_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['urboj_recivado'] = urboj_recivado
                except InformilojUrboj.DoesNotExist:
                    errors.append(ErrorNode(
                        field='urboj_recivado_uuid',
                        message=_('Неверный uuid')
                    ))
        if 'urboj_transdono_uuid' in kwargs: # ГородПередачи
            if not len(errors):
                try:
                    urboj_transdono = InformilojUrboj.objects.get(uuid=kwargs.get('urboj_transdono_uuid'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['urboj_transdono'] = urboj_transdono
                except InformilojUrboj.DoesNotExist:
                    errors.append(ErrorNode(
                        field='urboj_transdono_uuid',
                        message=_('Неверный uuid')
                    ))
        if kwargs.get('valuto_kliento_id', False): # ВалютаЗаказчика_Key
            try:
                valuto_kliento = MonoValuto.objects.get(id=kwargs.get('valuto_kliento_id'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['valuto_kliento'] = valuto_kliento
            except MonoValuto.DoesNotExist:
                errors.append(ErrorNode(
                    field='valuto_kliento_id',
                    message=_('Неверный id валюты')
                ))
        if kwargs.get('ekspedinto_uuid', False):
            try:
                ekspedinto = Organizo.objects.get(uuid=kwargs.get('ekspedinto_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['ekspedinto'] = ekspedinto
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='ekspedinto_uuid',
                    message=_('Неверный uuid организации')
                ))
        if (not len(errors)) and kwargs.get('ricevanto_uuid', False):
            try:
                ricevanto = Organizo.objects.get(uuid=kwargs.get('ricevanto_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['ricevanto'] = ricevanto
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='ricevanto_uuid',
                    message=_('Неверный uuid организации')
                ))
        if (not len(errors)) and kwargs.get('temperatura_reghimo_uuid', False):
            try:
                temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=kwargs.get('temperatura_reghimo_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['temperatura_reghimo'] = temperatura_reghimo
            except ObjektoTemperaturaReghimo.DoesNotExist:
                errors.append(ErrorNode(
                    field='temperatura_reghimo_uuid',
                    message=_('Неверный uuid температурного режима')
                ))
        if (not len(errors)) and kwargs.get('liveranto_uuid', False):
            try:
                liveranto = Organizo.objects.get(uuid=kwargs.get('liveranto_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['liveranto'] = liveranto
            except Organizo.DoesNotExist:
                errors.append(ErrorNode(
                    field='liveranto_uuid',
                    message=_('Неверный uuid организации')
                ))
        if (not len(errors)) and kwargs.get('liveranto_kontrakto_uuid', False):
            try:
                liveranto_kontrakto = DokumentoContract.objects.get(uuid=kwargs.get('liveranto_kontrakto_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['liveranto_kontrakto'] = liveranto_kontrakto
            except DokumentoContract.DoesNotExist:
                errors.append(ErrorNode(
                    field='liveranto_kontrakto_uuid',
                    message=_('Неверный uuid договора поставщика в DokumentoContract')
                ))
        if (not len(errors)) and kwargs.get('kombatanto_kontrakto_uuid', False):
            try:
                kombatanto_kontrakto = Kombatanto.objects.get(uuid=kwargs.get('kombatanto_kontrakto_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['kombatanto_kontrakto'] = kombatanto_kontrakto
            except Kombatanto.DoesNotExist:
                errors.append(ErrorNode(
                    field='kombatanto_kontrakto_uuid',
                    message=_('Неверный uuid контактного лица поставщика')
                ))
        return errors, kwargs


    @staticmethod
    def argumentoj_kreanta(root, info, **kwargs):
        """argumentoj kiam kreanta - аргументы при создании 

        Args:
            root (_type_): _description_
            info (_type_): _description_
        """
        errors = list()
        # проверяем обязательные параметры
        # if not ('rendevuejo' in kwargs): #СборныйГруз
        #     errors.append(ErrorNode(
        #         field='rendevuejo',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('telefono_kliento' in kwargs): #ТелефонЗаказчика
        #     errors.append(ErrorNode(
        #         field='telefono_kliento',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('volumeno' in kwargs): # Объем
        #     errors.append(ErrorNode(
        #         field='volumeno',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('pezo' in kwargs): #Вес
        #     errors.append(ErrorNode(
        #         field='pezo',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('priskribo_kargo' in kwargs): # ОписаниеГруза
        #     errors.append(ErrorNode(
        #         field='priskribo_kargo',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('kosto_kargo' in kwargs): # СтоимостьГруза
        #     errors.append(ErrorNode(
        #         field='kosto_kargo',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('monsumo_kliento' in kwargs): # СуммаЗаказчика
        #     errors.append(ErrorNode(
        #         field='monsumo_kliento',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('monsumo_transportisto' in kwargs): # СуммаПеревозчика
        #     errors.append(ErrorNode(
        #         field='monsumo_transportisto',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        if not ('adreso_kargado' in kwargs): # АдресЗагрузки
            errors.append(ErrorNode(
                field='adreso_kargado',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('adreso_elsxargxado' in kwargs): # АдресВыгрузки
            errors.append(ErrorNode(
                field='adreso_elsxargxado',
                message=_('Поле обязательно для заполнения')
            ))
        # if not ('tempo_s' in kwargs): # ВремяС
        #     errors.append(ErrorNode(
        #         field='tempo_s',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        # if not ('tempo_elsxargxado' in kwargs): # ВремяВыгрузкиС
        #     errors.append(ErrorNode(
        #         field='tempo_elsxargxado',
        #         message=_('Поле обязательно для заполнения')
        #     ))
        if not ('konservejo_ricevado_dato' in kwargs): # СКЛ_ДатаПриемки
            errors.append(ErrorNode(
                field='konservejo_ricevado_dato',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('konservejo_ricevado_tempo_de' in kwargs): # СКЛ_ВремяПриемкиС
            errors.append(ErrorNode(
                field='konservejo_ricevado_tempo_de',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('konservejo_ricevado_tempo_en' in kwargs): # СКЛ_ВремяПриемкиПо
            errors.append(ErrorNode(
                field='konservejo_ricevado_tempo_en',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('konservejo_fordono_dato' in kwargs): # СКЛ_ДатаПередачи
            errors.append(ErrorNode(
                field='konservejo_fordono_dato',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('konservejo_fordono_tempo_de' in kwargs): # СКЛ_ВремяПередачиС
            errors.append(ErrorNode(
                field='konservejo_fordono_tempo_de',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('konservejo_fordono_tempo_en' in kwargs): # СКЛ_ВремяПередачиПо
            errors.append(ErrorNode(
                field='konservejo_fordono_tempo_en',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('organizo_adreso' in kwargs): # ОрганизацияПоАдресу
            errors.append(ErrorNode(
                field='organizo_adreso',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('organizo_adreso_elsxargxado' in kwargs): # ОрганизацияПоАдресуРазгрузки
            errors.append(ErrorNode(
                field='organizo_adreso_elsxargxado',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('kombatanto_adresoj' in kwargs): # КонтактноеЛицоПоАдресу
            errors.append(ErrorNode(
                field='kombatanto_adresoj',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('kombatanto_adresoj_elsxargxado' in kwargs): # КонтактноеЛицоПоАдресуРазгрузки
            errors.append(ErrorNode(
                field='kombatanto_adresoj_elsxargxado',
                message=_('Поле обязательно для заполнения')
            ))
        if not ('organizo_uuid' in kwargs): # Организация_Key
            errors.append(ErrorNode(
                field='organizo_uuid',
                message=_('Поле обязательно для заполнения')
            ))
        if (not ('urboj_recivado_uuid' in kwargs) and # ГородПриемки
           not ('urboj_recivado_nomo' in kwargs)): # ГородПриемки_nomo
            errors.append(ErrorNode(
                field='urboj_recivado_uuid или urboj_recivado_nomo',
                message=_('Одно из полей обязательно для заполнения')
            ))
        if (not ('urboj_transdono_uuid' in kwargs) and # ГородПередачи
            not ('urboj_transdono_nomo' in kwargs)): # ГородПередачи_nomo
            errors.append(ErrorNode(
                field='urboj_transdono_uuid или urboj_transdono_nomo',
                message=_('Одно из полей обязательно для заполнения')
            ))
        return errors, kwargs


    @staticmethod
    def create(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_ekspedo = None
        uzanto = info.context.user
        membro = None # признак сотрудника компании

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Создавать может член организации-владельца документа
                    # находим клиента организации
                    if not (uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo')):
                        kliento = None
                        if 'kliento_uuid' in kwargs:
                            try:
                                kliento = Organizo.objects.get(uuid=kwargs.get('kliento_uuid'), forigo=False,
                                                            arkivo=False, publikigo=True)
                            except Organizo.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='kliento_uuid',
                                    message=_('Неверный uuid организации')
                                ))
                            try:
                                membro = OrganizoMembro.objects.get(organizo=kliento, uzanto=uzanto, forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except OrganizoMembro.DoesNotExist: # не является членом организации
                                membro = None
                        if not membro: # если не сотрудник клиента, то проверяем организацию-исполнителя
                            organizo = None
                            if 'organizo_uuid' in kwargs: # Организация_Key
                                if not len(errors):
                                    try:
                                        organizo = Organizo.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                    except Organizo.DoesNotExist:
                                        errors.append(ErrorNode(
                                            field='organizo_uuid',
                                            message=_('Неверный uuid')
                                        ))
                            try:
                                membro = OrganizoMembro.objects.get(organizo=organizo, uzanto=uzanto, forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except OrganizoMembro.DoesNotExist: # не является членом организации
                                membro = None
                        
                    if not (uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo') or membro):
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoEkspedo',
                            message=_('Недостаточно прав для создания')
                        ))
                    else:
                        # проверяем наличие параметров
                        errors, kwargs = RedaktuDokumentoEkspedo.argumentoj_kreanta(root, info, **kwargs)
                        if not len(errors):
                            # проверяем наличие записей с таким кодом и добавляем значения по умолчанию
                            errors, kwargs = RedaktuDokumentoEkspedo.kontrolanta_argumentoj(root, info, **kwargs)

                        # сохраняем в базу
                        if not len(errors):
                            dokumento_ekspedo = DokumentoEkspedo.objects.create(
                                dokumento=kwargs.get('dokumento'),
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo',True),
                                publikiga_dato=timezone.now(),
                                konservejo_ricevado_dato=kwargs.get('konservejo_ricevado_dato'),
                                konservejo_ricevado_tempo_de=kwargs.get('konservejo_ricevado_tempo_de'),
                                konservejo_ricevado_tempo_en=kwargs.get('konservejo_ricevado_tempo_en'),
                                konservejo_fordono_dato=kwargs.get('konservejo_fordono_dato'),
                                konservejo_fordono_tempo_de=kwargs.get('konservejo_fordono_tempo_de'),
                                konservejo_fordono_tempo_en=kwargs.get('konservejo_fordono_tempo_en'),
                            )
                            
                            if kwargs.get('kodo', False):
                                dokumento_ekspedo.kodo = kwargs.get('kodo')
                            if kwargs.get('valuto_kliento', False):
                                dokumento_ekspedo.valuto_kliento=kwargs.get('valuto_kliento')
                            if kwargs.get('pezo', False):
                                dokumento_ekspedo.pezo=kwargs.get('pezo')
                            if kwargs.get('tempo_elsxargxado', False):
                                dokumento_ekspedo.tempo_elsxargxado=kwargs.get('tempo_elsxargxado')
                            if kwargs.get('tempo_s', False):
                                dokumento_ekspedo.tempo_s=kwargs.get('tempo_s')
                            if kwargs.get('ekspedinto', False):
                                dokumento_ekspedo.ekspedinto=kwargs.get('ekspedinto')
                            if kwargs.get('ricevanto', False):
                                dokumento_ekspedo.ricevanto=kwargs.get('ricevanto')
                            if kwargs.get('kontrakto_kliento', False):
                                dokumento_ekspedo.kontrakto_kliento=kwargs.get('kontrakto_kliento')
                            if kwargs.get('kliento', False):
                                dokumento_ekspedo.kliento=kwargs.get('kliento')
                            if kwargs.get('kliento_sciigi', False):
                                dokumento_ekspedo.kliento_sciigi=kwargs.get('kliento_sciigi')
                            if kwargs.get('kombatanto_kliento', False):
                                dokumento_ekspedo.kombatanto_kliento=kwargs.get('kombatanto_kliento')
                            if kwargs.get('volumeno', False):
                                dokumento_ekspedo.volumeno=kwargs.get('volumeno')
                            if kwargs.get('organizo', False):
                                dokumento_ekspedo.organizo=kwargs.get('organizo')
                            if kwargs.get('kosto_kargo', False):
                                dokumento_ekspedo.kosto_kargo=kwargs.get('kosto_kargo')
                            if kwargs.get('monsumo_kliento', False):
                                dokumento_ekspedo.monsumo_kliento=kwargs.get('monsumo_kliento')
                            if kwargs.get('monsumo_kliento_fiksa_mane', False):
                                dokumento_ekspedo.monsumo_kliento_fiksa_mane=kwargs.get('monsumo_kliento_fiksa_mane')
                            if kwargs.get('monsumo_transportisto', False):
                                dokumento_ekspedo.monsumo_transportisto=kwargs.get('monsumo_transportisto')
                            if kwargs.get('monsumo_transportisto_fiksa_mane', False):
                                dokumento_ekspedo.monsumo_transportisto_fiksa_mane=kwargs.get('monsumo_transportisto_fiksa_mane')
                            if kwargs.get('fakto', False):
                                dokumento_ekspedo.fakto=kwargs.get('fakto')
                            if kwargs.get('kosto_kliento', False):
                                dokumento_ekspedo.kosto_kliento=kwargs.get('kosto_kliento')
                            if kwargs.get('kosto_transportisto', False):
                                dokumento_ekspedo.kosto_transportisto=kwargs.get('kosto_transportisto')
                            if kwargs.get('partopago_kliento', False):
                                dokumento_ekspedo.partopago_kliento=kwargs.get('partopago_kliento')
                            if kwargs.get('partopago_transportisto', False):
                                dokumento_ekspedo.partopago_transportisto=kwargs.get('partopago_transportisto')
                            if kwargs.get('pezo_fakto', False):
                                dokumento_ekspedo.pezo_fakto=kwargs.get('pezo_fakto')
                            if kwargs.get('pezo_kalkula', False):
                                dokumento_ekspedo.pezo_kalkula=kwargs.get('pezo_kalkula')
                            if kwargs.get('dangxera_kargo', False):
                                dokumento_ekspedo.dangxera_kargo=kwargs.get('dangxera_kargo')
                            if kwargs.get('retropasxo_tsd', False):
                                dokumento_ekspedo.retropasxo_tsd=kwargs.get('retropasxo_tsd')
                            if kwargs.get('akcepto_liveri_nelabora_tempo', False):
                                dokumento_ekspedo.akcepto_liveri_nelabora_tempo=kwargs.get('akcepto_liveri_nelabora_tempo')
                            if kwargs.get('aldone_pakumo', False):
                                dokumento_ekspedo.aldone_pakumo=kwargs.get('aldone_pakumo')
                            if kwargs.get('temperatura_reghimo', False):
                                dokumento_ekspedo.temperatura_reghimo=kwargs.get('temperatura_reghimo')
                            if kwargs.get('indikatoro_temperatura', False):
                                dokumento_ekspedo.indikatoro_temperatura=kwargs.get('indikatoro_temperatura')
                            if kwargs.get('livero_tp', False):
                                dokumento_ekspedo.livero_tp=kwargs.get('livero_tp')
                            if kwargs.get('urboj_recivado', False):
                                dokumento_ekspedo.urboj_recivado=kwargs.get('urboj_recivado')
                            if kwargs.get('urboj_transdono', False):
                                dokumento_ekspedo.urboj_transdono=kwargs.get('urboj_transdono')
                            if kwargs.get('fordono_kargo_dato', False):
                                dokumento_ekspedo.fordono_kargo_dato=kwargs.get('fordono_kargo_dato')
                            if kwargs.get('fordono_kargo_tempo', False):
                                dokumento_ekspedo.fordono_kargo_tempo=kwargs.get('fordono_kargo_tempo')
                            if kwargs.get('asekurado', False):
                                dokumento_ekspedo.asekurado=kwargs.get('asekurado')
                            if kwargs.get('horoj_sxargxistoj', False):
                                dokumento_ekspedo.horoj_sxargxistoj=kwargs.get('horoj_sxargxistoj')
                            if kwargs.get('kvanto_sxargxistoj', False):
                                dokumento_ekspedo.kvanto_sxargxistoj=kwargs.get('kvanto_sxargxistoj')
                            if kwargs.get('kvanto_pecoj', False):
                                dokumento_ekspedo.kvanto_pecoj=kwargs.get('kvanto_pecoj')
                            if kwargs.get('sxargxisto_rigilaro', False):
                                dokumento_ekspedo.sxargxisto_rigilaro=kwargs.get('sxargxisto_rigilaro')
                            if kwargs.get('en_peza_kategorio', False):
                                dokumento_ekspedo.en_peza_kategorio=kwargs.get('en_peza_kategorio')
                            if kwargs.get('konservejo_temperatura_reghimo_1', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_1=kwargs.get('konservejo_temperatura_reghimo_1')
                            if kwargs.get('konservejo_temperatura_reghimo_2', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_2=kwargs.get('konservejo_temperatura_reghimo_2')
                            if kwargs.get('konservejo_temperatura_reghimo_3', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_3=kwargs.get('konservejo_temperatura_reghimo_3')
                            if kwargs.get('konservejo_temperatura_reghimo_4', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_4=kwargs.get('konservejo_temperatura_reghimo_4')
                            if kwargs.get('konservejo_temperatura_reghimo_alia', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_alia=kwargs.get('konservejo_temperatura_reghimo_alia')
                            if kwargs.get('konservejo_temperatura_reghimo_5', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_5=kwargs.get('konservejo_temperatura_reghimo_5')
                            if kwargs.get('konservejo_temperatura_reghimo_6', False):
                                dokumento_ekspedo.konservejo_temperatura_reghimo_6=kwargs.get('konservejo_temperatura_reghimo_6')
                            if kwargs.get('liveranto', False):
                                dokumento_ekspedo.liveranto=kwargs.get('liveranto')
                            if kwargs.get('liveranto_kontrakto', False):
                                dokumento_ekspedo.liveranto_kontrakto=kwargs.get('liveranto_kontrakto')
                            if kwargs.get('kombatanto_kontrakto', False):
                                dokumento_ekspedo.kombatanto_kontrakto=kwargs.get('kombatanto_kontrakto')
                            if kwargs.get('trajno_venigo_dato', False):
                                dokumento_ekspedo.trajno_venigo_dato=kwargs.get('trajno_venigo_dato')
                            if kwargs.get('trajno_venigo_tempo', False):
                                dokumento_ekspedo.trajno_venigo_tempo=kwargs.get('trajno_venigo_tempo')
                            if kwargs.get('trajno_finigho_dato', False):
                                dokumento_ekspedo.trajno_finigho_dato=kwargs.get('trajno_finigho_dato')
                            if kwargs.get('trajno_finigho_tempo', False):
                                dokumento_ekspedo.trajno_finigho_tempo=kwargs.get('trajno_finigho_tempo')
                            if kwargs.get('sensoro_temperatura_2', False):
                                dokumento_ekspedo.sensoro_temperatura_2=kwargs.get('sensoro_temperatura_2')
                            if kwargs.get('sensoro_temperatura_3', False):
                                dokumento_ekspedo.sensoro_temperatura_3=kwargs.get('sensoro_temperatura_3')
                            if kwargs.get('sensoro_temperatura_4', False):
                                dokumento_ekspedo.sensoro_temperatura_4=kwargs.get('sensoro_temperatura_4')
                            if kwargs.get('sensoro_temperatura_5', False):
                                dokumento_ekspedo.sensoro_temperatura_5=kwargs.get('sensoro_temperatura_5')
                            if kwargs.get('sensoro_temperatura_alia', False):
                                dokumento_ekspedo.sensoro_temperatura_alia=kwargs.get('sensoro_temperatura_alia')
                            if kwargs.get('Livero_tp_2', False):
                                dokumento_ekspedo.Livero_tp_2=kwargs.get('Livero_tp_2')
                            if kwargs.get('Livero_tp_3', False):
                                dokumento_ekspedo.Livero_tp_3=kwargs.get('Livero_tp_3')
                            if kwargs.get('Livero_tp_4', False):
                                dokumento_ekspedo.Livero_tp_4=kwargs.get('Livero_tp_4')
                            if kwargs.get('Livero_tp_5', False):
                                dokumento_ekspedo.Livero_tp_5=kwargs.get('Livero_tp_5')
                            if kwargs.get('Livero_tp_alia', False):
                                dokumento_ekspedo.Livero_tp_alia=kwargs.get('Livero_tp_alia')
                            if kwargs.get('plenumado_dato', False):
                                dokumento_ekspedo.plenumado_dato=kwargs.get('plenumado_dato')
                            if kwargs.get('fiksa_termoskribi', False):
                                dokumento_ekspedo.fiksa_termoskribi=kwargs.get('fiksa_termoskribi')
                            if kwargs.get('fiksa_termoskribi_2', False):
                                dokumento_ekspedo.fiksa_termoskribi_2=kwargs.get('fiksa_termoskribi_2')
                            if kwargs.get('fiksa_termoskribi_3', False):
                                dokumento_ekspedo.fiksa_termoskribi_3=kwargs.get('fiksa_termoskribi_3')
                            if kwargs.get('fiksa_termoskribi_4', False):
                                dokumento_ekspedo.fiksa_termoskribi_4=kwargs.get('fiksa_termoskribi_4')
                            if kwargs.get('fiksa_termoskribi_5', False):
                                dokumento_ekspedo.fiksa_termoskribi_5=kwargs.get('fiksa_termoskribi_5')
                            if kwargs.get('fiksa_termoskribi_alia', False):
                                dokumento_ekspedo.fiksa_termoskribi_alia=kwargs.get('fiksa_termoskribi_alia')
                            if kwargs.get('sxargxistoj_sxargxado', False):
                                dokumento_ekspedo.sxargxistoj_sxargxado=kwargs.get('sxargxistoj_sxargxado')
                            if kwargs.get('sxargxistoj_elsxargxado', False):
                                dokumento_ekspedo.sxargxistoj_elsxargxado=kwargs.get('sxargxistoj_elsxargxado')
                            if kwargs.get('uid', False):
                                dokumento_ekspedo.uid = kwargs.get('uid')


                                
                            if kwargs.get('adreso_elsxargxado', False):
                                set_enhavo(dokumento_ekspedo.adreso_elsxargxado, kwargs.get('adreso_elsxargxado'), 'ru_RU')
                            if kwargs.get('adreso_kargado', False):
                                set_enhavo(dokumento_ekspedo.adreso_kargado, kwargs.get('adreso_kargado'), 'ru_RU')
                            if kwargs.get('komento', False):
                                set_enhavo(dokumento_ekspedo.komento, kwargs.get('komento'), 'ru_RU')
                            if kwargs.get('komento_adreso_elsxargxado', False):
                                set_enhavo(dokumento_ekspedo.komento_adreso_elsxargxado, kwargs.get('komento_adreso_elsxargxado'), 'ru_RU')
                            if kwargs.get('komento_adreso_kargado', False):
                                set_enhavo(dokumento_ekspedo.komento_adreso_kargado, kwargs.get('komento_adreso_kargado'), 'ru_RU')
                            if kwargs.get('komento_nuligo', False):
                                set_enhavo(dokumento_ekspedo.komento_nuligo, kwargs.get('komento_nuligo'), 'ru_RU')
                            if kwargs.get('kombatanto_adresoj', False):
                                set_enhavo(dokumento_ekspedo.kombatanto_adresoj, kwargs.get('kombatanto_adresoj'), 'ru_RU')
                            if kwargs.get('kombatanto_adresoj_elsxargxado', False):
                                set_enhavo(dokumento_ekspedo.kombatanto_adresoj_elsxargxado, kwargs.get('kombatanto_adresoj_elsxargxado'), 'ru_RU')
                            if kwargs.get('priskribo_kargo', False):
                                set_enhavo(dokumento_ekspedo.priskribo_kargo, kwargs.get('priskribo_kargo'), 'ru_RU')
                            if kwargs.get('organizo_adreso', False):
                                set_enhavo(dokumento_ekspedo.organizo_adreso, kwargs.get('organizo_adreso'), 'ru_RU')
                            if kwargs.get('organizo_adreso_elsxargxado', False):
                                set_enhavo(dokumento_ekspedo.organizo_adreso_elsxargxado, kwargs.get('organizo_adreso_elsxargxado'), 'ru_RU')
                            if kwargs.get('telefono_kliento', False):
                                set_enhavo(dokumento_ekspedo.telefono_kliento, kwargs.get('telefono_kliento'), 'ru_RU')
                            if kwargs.get('telefono_adreso', False):
                                set_enhavo(dokumento_ekspedo.telefono_adreso, kwargs.get('telefono_adreso'), 'ru_RU')
                            if kwargs.get('telefono_adreso_elsxargxado', False):
                                set_enhavo(dokumento_ekspedo.telefono_adreso_elsxargxado, kwargs.get('telefono_adreso_elsxargxado'), 'ru_RU')
                            if kwargs.get('aldona_kondicxaro', False):
                                set_enhavo(dokumento_ekspedo.aldona_kondicxaro, kwargs.get('aldona_kondicxaro'), 'ru_RU')
                            if kwargs.get('impostokvoto_kliento', False):
                                set_enhavo(dokumento_ekspedo.impostokvoto_kliento, kwargs.get('impostokvoto_kliento'), 'ru_RU')
                            if kwargs.get('impostokvoto_transportisto', False):
                                set_enhavo(dokumento_ekspedo.impostokvoto_transportisto, kwargs.get('impostokvoto_transportisto'), 'ru_RU')
                            if kwargs.get('priskribo_kalkulado', False):
                                set_enhavo(dokumento_ekspedo.priskribo_kalkulado, kwargs.get('priskribo_kalkulado'), 'ru_RU')
                            if kwargs.get('numero_frajtoletero', False):
                                set_enhavo(dokumento_ekspedo.numero_frajtoletero, kwargs.get('numero_frajtoletero'), 'ru_RU')
                            if kwargs.get('priskribo_dangxera_kargo', False):
                                set_enhavo(dokumento_ekspedo.priskribo_dangxera_kargo, kwargs.get('priskribo_dangxera_kargo'), 'ru_RU')
                            if kwargs.get('klaso_dangxera', False):
                                set_enhavo(dokumento_ekspedo.klaso_dangxera, kwargs.get('klaso_dangxera'), 'ru_RU')
                            if kwargs.get('aldone_postulo_hc', False):
                                set_enhavo(dokumento_ekspedo.aldone_postulo_hc, kwargs.get('aldone_postulo_hc'), 'ru_RU')
                            if kwargs.get('aliaj_servo', False):
                                set_enhavo(dokumento_ekspedo.aliaj_servo, kwargs.get('aliaj_servo'), 'ru_RU')
                            if kwargs.get('fordono_kargo_fio', False):
                                set_enhavo(dokumento_ekspedo.fordono_kargo_fio, kwargs.get('fordono_kargo_fio'), 'ru_RU')
                            if kwargs.get('nomo_kargo', False):
                                set_enhavo(dokumento_ekspedo.nomo_kargo, kwargs.get('nomo_kargo'), 'ru_RU')
                            if kwargs.get('konservejo_tasko', False):
                                set_enhavo(dokumento_ekspedo.konservejo_tasko, kwargs.get('konservejo_tasko'), 'ru_RU')
                            if kwargs.get('konservejo_komento', False):
                                set_enhavo(dokumento_ekspedo.konservejo_komento, kwargs.get('konservejo_komento'), 'ru_RU')
                            if kwargs.get('priskribo_kliento', False):
                                set_enhavo(dokumento_ekspedo.priskribo_kliento, kwargs.get('priskribo_kliento'), 'ru_RU')
                            if kwargs.get('numero_frajtoletero_kliento', False):
                                set_enhavo(dokumento_ekspedo.numero_frajtoletero_kliento, kwargs.get('numero_frajtoletero_kliento'), 'ru_RU')
                            if kwargs.get('email_ekspedinto', False):
                                set_enhavo(dokumento_ekspedo.email_ekspedinto, kwargs.get('email_ekspedinto'), 'ru_RU')
                            if kwargs.get('email_ricevanto', False):
                                set_enhavo(dokumento_ekspedo.email_ricevanto, kwargs.get('email_ricevanto'), 'ru_RU')
                            if kwargs.get('tipo_mendo', False):
                                set_enhavo(dokumento_ekspedo.tipo_mendo, kwargs.get('tipo_mendo'), 'ru_RU')
                            if kwargs.get('urboj_recivado_nomo', False):
                                set_enhavo(dokumento_ekspedo.urboj_recivado_nomo, kwargs.get('urboj_recivado_nomo'), 'ru_RU')
                            if kwargs.get('urboj_transdono_nomo', False):
                                set_enhavo(dokumento_ekspedo.urboj_transdono_nomo, kwargs.get('urboj_transdono_nomo'), 'ru_RU')
                                
                            dokumento_ekspedo_save(dokumento_ekspedo)


                        # errors.append(ErrorNode(
                        #     field='',
                        #     modelo='RedaktuDokumentoEkspedo',
                        #     message=_('Создание новых записей в данной мутации не реализовано')
                        # ))
                            message = _('Создана новая записей документа экспедирования')
                if not message:
                    if len(errors):
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                    else:
                        status = True
                        errors = list()
                        message =  _('Не найдено новых объектов для добавления')

        return status, message, errors, dokumento_ekspedo

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_ekspedo = None
        dosiero = None
        uzanto = info.context.user
        if not uzanto.is_authenticated:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuDokumentoEkspedo',
                message=_('Требуется авторизация')
            ))
            message = _('Требуется авторизация')
            return status, message, errors, dokumento_ekspedo

        with transaction.atomic():
            # Изменяем запись (обязательно должно быть uuid)
            if not (kwargs.get('uuid', False) or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                    or kwargs.get('dosiero_uuid', False) or kwargs.get('prj_statuso_uuid', False)
                    or kwargs.get('uid', False)):
                message = _('Не задано ни одно поле для изменения')
                errors.append(ErrorNode(
                    field='',
                    modelo='RedaktuDokumentoEkspedo',
                    message=_('Не задано ни одно поле для изменения')
                ))

            if not message:
                # Ищем запись для изменения
                try:
                    dokumento_ekspedo = DokumentoEkspedo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                except DokumentoEkspedo.DoesNotExist:
                    try:
                        dokumento_ekspedo = DokumentoEkspedo.objects.get(uid=kwargs.get('uid'), forigo=False)
                    except DokumentoEkspedo.DoesNotExist:
                        dokumento_ekspedo = None
                if dokumento_ekspedo:
                    if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_ekspedo', dokumento_ekspedo)
                            and kwargs.get('forigo', False)):
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoEkspedo',
                            message=_('Недостаточно прав для удаления')
                        ))
                    if not (uzanto.has_perm('dokumentoj.povas_shangxi_dokumentoj_ekspedo', dokumento_ekspedo)):
                        # Редактировать может член организации-владельца документа
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoEkspedo',
                            message=_('Недостаточно прав для изменения')
                        ))
                    # если необходимо обновить статус заявки
                    if kwargs.get('prj_statuso_uuid', False) and not len(errors):
                        # находим соответствующие проекты
                        projekto_posedantoj = TaskojProjektoPosedanto.objects.filter(posedanto_dokumento=dokumento_ekspedo.dokumento, 
                                    forigo=False, arkivo=False, publikigo=True).values('projekto')
                        projektoj = TaskojProjekto.objects.filter(uuid__in=projekto_posedantoj, forigo=False, arkivo=False, 
                            publikigo=True)
                        # если проектов > 1, то ошибка - неизвестно кому менять статус
                        if len(projektoj)>1:
                            errors.append(ErrorNode(
                                field='prj_statuso_uuid',
                                modelo='RedaktuDokumentoEkspedo',
                                message=_('У данной заявки больше одного проекта - неизвестно какому проекту изменять статус')
                            ))
                        elif len(projektoj)==0:
                            # проектов ещё нет, создаём новый
                            # находим текущий статус проекта согласно поля prj_statuso_uuid 
                            statuso_id = 1 # Новый
                            statuso = None
                            try:
                                statuso = TaskojProjektoStatuso.objects.get(uuid=kwargs.get('prj_statuso_uuid'))
                                statuso_id = statuso.id
                            except TaskojProjektoStatuso.DoesNotExist:
                                errors.append(ErrorNode(
                                    field="{}{}".format('prj_statuso_uuid = ',kwargs.get('prj_statuso_uuid')),
                                    modelo='RedaktuDokumentoEkspedo',
                                    message=_('Нет такого статуса проекта в базе')
                                ))
                            
                            # подготавливаем под задачи+проекты+владельцы
                            kwargoj = {
                                'prj_nomo': 'Экспедирование',
                                'prj_kategorio': [7,],
                                'prj_tipo_id': 3,
                                'prj_statuso_id': statuso_id,
                                'prj_posedanto_tipo_id': [1],
                                'prj_posedanto_statuso_id': [1],
                                'posedanto_dokumento_uuid': [dokumento_ekspedo.dokumento.uuid],
                                'prj_posedanto_organizo_uuid': [],
                                
                                'tipo_id': 3,
                                
                                # эти в цикле собираем:
                                'kategorio':[],
                                'pozicio': [],
                                'nomo': [],
                                'priskribo': [],
                                'statuso_id': [],
                                'posedanto_tipo_id':[],
                                'posedanto_statuso_id':[],
                                'kom_dato':[],
                                'fin_dato':[],
                                'kom_adreso':[],
                                'fin_adreso':[]
                            }
                            if dokumento_ekspedo.kliento:
                                kwargoj['prj_posedanto_organizo_uuid'] = [dokumento_ekspedo.kliento.uuid]
                            # поставщика услуг также во владельцы проекта надо ставить
                            if dokumento_ekspedo.organizo:
                                kwargoj['prj_posedanto_tipo_id'].append(1)
                                kwargoj['prj_posedanto_statuso_id'].append(1)
                                kwargoj['posedanto_dokumento_uuid'].append(dokumento_ekspedo.dokumento.uuid)
                                kwargoj['prj_posedanto_organizo_uuid'].append(dokumento_ekspedo.organizo.uuid)

                            # блок создания параметов задач
                            i_tasko = 1
                            kwargoj['pozicio'].append(i_tasko)
                            kwargoj['kategorio'].append(13)
                            kwargoj['nomo'].append('Экспедирование')
                            kwargoj['priskribo'].append('Экспедирование')

                            # находим статус задачи
                            statuso_id = 1 # Новый
                            statuso = None
                            try:
                                statuso = TaskojTaskoStatuso.objects.get(uuid=kwargs.get('prj_statuso_uuid'))
                                statuso_id = statuso.id
                            except TaskojTaskoStatuso.DoesNotExist:
                                errors.append(ErrorNode(
                                    field="{}{}".format('prj_statuso_uuid в задачах = ',kwargs.get('prj_statuso_uuid')),
                                    modelo='RedaktuDokumentoEkspedo',
                                    message=_('Нет такого статуса задачи в базе')
                                ))
                            kwargoj['statuso_id'].append(statuso_id)
                            kwargoj['posedanto_tipo_id'].append(1)
                            kwargoj['posedanto_statuso_id'].append(1)
                            # kwargoj['kom_dato'].append(js_tasko['ДатаНачала'])
                            # kwargoj['fin_dato'].append(js_tasko['ДатаОкончания'])
                            # kwargoj['kom_adreso'].append(js_tasko['АдресНачала'])
                            # kwargoj['fin_adreso'].append(js_tasko['АдресКонца'])
                            
                            # i_tasko += 1
                            if not len(errors):
                                status, message2, errors2, projekto, taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.create(root, info, **kwargoj)
                                # отправляем на почту обновление статуса заказа
                                # Уведомление на почту об изменении статуса заявки
                                send_email_raportado(dokumento_ekspedo.kliento.email,
                                    "Статус заявки " + dokumento_ekspedo.kodo,'был изменён на ' ,str(get_enhavo(statuso.nomo, empty_values=True)[0]))
                                

                        else:
                            # редактируем статус единственного проекта
                            # находим текущий статус проекта согласно поля prj_statuso_uuid 
                            statuso_id = 1 # Новый
                            statuso = None
                            try:
                                statuso = TaskojProjektoStatuso.objects.get(uuid=kwargs.get('prj_statuso_uuid'))
                                statuso_id = statuso.id
                            except TaskojProjektoStatuso.DoesNotExist:
                                errors.append(ErrorNode(
                                    field="{}{}".format('prj_statuso_uuid = ',kwargs.get('prj_statuso_uuid')),
                                    modelo='RedaktuDokumentoEkspedo',
                                    message=_('Нет такого статуса проекта в базе')
                                ))
                            # если статусы отличаются, то заменяем
                            if projektoj[0].statuso.id != statuso_id:
                                projektoj[0].statuso = statuso
                                projekto_save(dokumento_ekspedo, projektoj[0])
                                send_email_raportado(dokumento_ekspedo.kliento.email,
                                    "Статус заявки " + dokumento_ekspedo.kodo,'был изменён на ' ,str(get_enhavo(statuso.nomo, empty_values=True)[0]))
                                status = True
                        if status:
                            message = _('Статус заявки экспедирования успешно изменён')
                        else:
                            status = True
                            message = _('Статус заявки экспедирования не требует изменений')
                    
                    
                    
                    # проверяем наличие записей с таким кодом в списке
                    if not len(errors) and not message:
                        if 'dosiero_uuid' in kwargs:
                            try:
                                dosiero = DokumentoDosiero.objects.get(uuid=kwargs.get('dosiero_uuid'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except DokumentoDosiero.DoesNotExist:
                                # message = '{}: {}'.format(_('Неверный привязанный файл к документу'),'dosiero_uuid')
                                errors.append(ErrorNode(
                                    field='dosiero_uuid',
                                    modelo='RedaktuDokumentoEkspedo',
                                    message=_('Неверный привязанный файл к документу')
                                ))

                    if not len(errors) and not message:

                        dokumento_ekspedo.forigo = kwargs.get('forigo', dokumento_ekspedo.forigo)
                        dokumento_ekspedo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                        dokumento_ekspedo.arkivo = kwargs.get('arkivo', dokumento_ekspedo.arkivo)
                        dokumento_ekspedo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                        dokumento_ekspedo.publikigo = kwargs.get('publikigo', dokumento_ekspedo.publikigo)
                        dokumento_ekspedo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                        if kwargs.get('dosiero_uuid', False):
                            if dosiero:
                                dokumento_ekspedo.dosiero.add(dosiero)
                            else:
                                dokumento_ekspedo.dosiero.clear()

                        dokumento_ekspedo_save(dokumento_ekspedo)
                        status = True
                        message = _('Запись успешно изменена')
                else:
                    message = _('Запись не найдена')
                if not message:
                    if len(errors):
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                    else:
                        status = True
                        errors = list()
                        message =  _('Не найдено новых объектов для добавления')
        return status, message, errors, dokumento_ekspedo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_ekspedo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, dokumento_ekspedo = RedaktuDokumentoEkspedo.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, dokumento_ekspedo = RedaktuDokumentoEkspedo.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return RedaktuDokumentoEkspedo(status=status, message=message, errors=errors, dokumento_ekspedo=dokumento_ekspedo)


# Модель документов экспедирования
class RedaktuDokumentoContract(graphene.Mutation):
    """
    Версия 1
    03.02.2022
    Сделано только добавление/замены ссылки на файлы (dosiero - DokumentoDosiero)
    """
    status = graphene.Boolean()
    errors = graphene.List(ErrorNode)
    message = graphene.String()
    dokumento_contract = graphene.Field(DokumentoContractNode, 
        description=_('Созданная/изменённая модель документов - контрактов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        
        dosiero_uuid = graphene.String(description=_('UUID присоединённого файла'))
        
    @staticmethod
    def create(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_contract = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_contract'):
                        # нет возможности добавить модель на 03.02.2022
                        pass
                    else:
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoContract',
                            message=_('Недостаточно прав')
                        ))
                        message = _('Недостаточно прав RedaktuDokumentoContract')
                    errors.append(ErrorNode(
                        field='',
                        modelo='RedaktuDokumentoContract',
                        message=_('Создание новых записей в данной мутации не реализовано')
                    ))
                    message = _('Создание новых записей в данной мутации не реализовано RedaktuDokumentoContract')
                if not message:
                    if len(errors):
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                    else:
                        status = True
                        errors = list()
                        message =  _('Не найдено новых объектов для добавления')

        return status, message, errors, dokumento_contract

    @staticmethod
    def edit(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_contract = None
        dosiero = None
        uzanto = info.context.user
        if not uzanto.is_authenticated:
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuDokumentoContract',
                message=_('Требуется авторизация')
            ))
            message = _('Требуется авторизация')
            return status, message, errors, dokumento_contract

        with transaction.atomic():
            # Изменяем запись (обязательно должно быть uuid)
            if not (kwargs.get('uuid', False) or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                    or kwargs.get('dosiero_uuid', False)):
                message = _('Не задано ни одно поле для изменения')
                errors.append(ErrorNode(
                    field='',
                    modelo='RedaktuDokumentoContract',
                    message=_('Не задано ни одно поле для изменения')
                ))

            if not message:
                # Ищем запись для изменения
                try:
                    dokumento_contract = DokumentoContract.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                    if (not uzanto.has_perm('dokumentoj.povas_forigi_dokumentoj_contract')
                            and kwargs.get('forigo', False)):
                        # message = _('Недостаточно прав для удаления RedaktuDokumentoContract')
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoContract',
                            message=_('Недостаточно прав для удаления')
                        ))
                    elif not uzanto.has_perm('dokumentoj.povas_shanghi_dokumentoj_contract'):
                        # message = _('Недостаточно прав для изменения RedaktuDokumentoContract')
                        errors.append(ErrorNode(
                            field='',
                            modelo='RedaktuDokumentoContract',
                            message=_('Недостаточно прав для изменения')
                        ))
                    # проверяем наличие записей с таким кодом в списке
                    if not len(errors):
                        if 'dosiero_uuid' in kwargs:
                            try:
                                dosiero = DokumentoDosiero.objects.get(uuid=kwargs.get('dosiero_uuid'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except DokumentoDosiero.DoesNotExist:
                                # message = '{}: {}'.format(_('Неверный привязанный файл к документу'),'dosiero_uuid')
                                errors.append(ErrorNode(
                                    field='dosiero_uuid',
                                    modelo='RedaktuDokumentoContract',
                                    message=_('Неверный привязанный файл к документу')
                                ))

                    if not len(errors):

                        dokumento_contract.forigo = kwargs.get('forigo', dokumento_contract.forigo)
                        dokumento_contract.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                        dokumento_contract.arkivo = kwargs.get('arkivo', dokumento_contract.arkivo)
                        dokumento_contract.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                        dokumento_contract.publikigo = kwargs.get('publikigo', dokumento_contract.publikigo)
                        dokumento_contract.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                        if 'dosiero_uuid' in kwargs:
                            if dosiero:
                                dokumento_contract.dosiero.add(dosiero)
                            else:
                                dokumento_contract.dosiero.clear()

                        dokumento_contract.save()
                        status = True
                        message = _('Запись успешно изменена')
                except DokumentoContract.DoesNotExist:
                    message = _('Запись не найдена')
                if not message:
                    if len(errors):
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                    else:
                        status = True
                        errors = list()
                        message =  _('Не найдено новых объектов для добавления')
        return status, message, errors, dokumento_contract

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        errors = list()
        message = None
        dokumento_contract = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            if 'uuid' in kwargs: 
                # Изменяем запись
                status, message, errors, dokumento_contract = RedaktuDokumentoContract.edit(root, info, **kwargs)
            else:
                # Создаём запись
                status, message, errors, dokumento_contract = RedaktuDokumentoContract.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')
            errors.append(ErrorNode(
                field='',
                modelo='RedaktuDokumentoContract',
                message=_('Требуется авторизация')
            ))

        return RedaktuDokumentoContract(status=status, message=message, errors=errors, dokumento_contract=dokumento_contract)


# Импорт Catalog_новаВидыУслугЭкспедирования "Виды Услуг Экспедирования" из 1с
class ImportoSpecoServoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(DokumentoSpecoServoEkspedoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user

        url = settings.ODATA_URL+'Catalog_новаВидыУслугЭкспедирования?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_speco_servo_ekspedo'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()

                for js in jsj['value']:
                    if len(errors)>0:
                        break
                    uuid = js['Ref_Key']
                    try:
                        importo = DokumentoSpecoServoEkspedo.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except DokumentoSpecoServoEkspedo.DoesNotExist:
                        
                        mono_valuto = None
                        if js['Валюта_Key'] != '00000000-0000-0000-0000-000000000000':
                            try:
                                mono_valuto = MonoValuto.objects.get(uuid=js['Валюта_Key'])
                            except MonoValuto.DoesNotExist:
                                errors.append(ErrorNode(
                                    field='Валюта_Key',
                                    message=_('Нет такой валюты в базе')
                                ))

                        nomo = js['Description']
                        tipo_kosto = js['ТипЦены']
                        tipo_servo = js['ТипПосредническойУслуги']

                        if not len(errors):
                            importo = DokumentoSpecoServoEkspedo.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                kodo=js['Code'],
                                mono_valuto=mono_valuto,
                                maks_tempo=js['МаксВремяЧасПодачи'],
                                normo_mileage_horo=js['НормаПробегаВЧас'],
                                kosto_mileage_kliento=js['ЦенаПерепробегаЗаказчик'],
                                kosto_mileage_transportisto=js['ЦенаПерепробегаПеревозчик'],
                                ne_re_prezenti=js['НеПеревыставлять'],
                                ne_eligi=js['НеВыводитьВРМ'],
                            )
                            if nomo:
                                set_enhavo(importo.nomo, nomo, 'ru_RU')
                            if tipo_kosto:
                                set_enhavo(importo.tipo_kosto, tipo_kosto, 'ru_RU')
                            if tipo_servo:
                                set_enhavo(importo.tipo_servo, tipo_servo, 'ru_RU')

                            importo.save()
                            kvanto += 1
                if not message:
                    if len(errors):
                        message = _('Nevalida argumentvaloroj')
                        for error in errors:
                            message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                error.message, _('в поле'), error.field)
                    else:
                        status = True
                        errors = list()
                        if kvanto > 0:
                            message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                        else:
                            message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Недостаточно прав')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoSpecoServoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return ImportoSpecoServoEkspedo1c(status=status, message=message, errors=errors, importo=importo)


# Импорт Catalog_ДоговорыКонтрагентов из 1с
class ImportoDokumentoContract1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(DokumentoContractNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))
        tipo_dokumento_id = graphene.Int(description=_('ID типа документов'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user
        tipo_dokumento = None
        organizo = None

        url = settings.ODATA_URL+'Catalog_ДоговорыКонтрагентов?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_contract'):
            if not len(errors):
                if 'tipo_dokumento_id' in kwargs:
                    try:
                        tipo_dokumento = DokumentoTipo.objects.get(id=kwargs.get('tipo_dokumento_id'), forigo=False,
                                                            arkivo=False, publikigo=True)
                    except DokumentoTipo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='tipo_dokumento_id',
                            message=_('Неверный тип документов')
                        ))
                else:
                    errors.append(ErrorNode(
                        field='tipo_dokumento_id',
                        message=_('Не указан тип документа для импорта данных')
                    ))

            if not len(errors):
                with transaction.atomic():
                    r = requests.get(url,auth=(login, password), verify=False)
                    kvanto = 0
                    jsj = r.json()

                    for js in jsj['value']:
                        if len(errors)>0:
                            break

                        uuid = js['Ref_Key']
                        
                        try:
                            importo = DokumentoContract.objects.get(uuid=uuid, forigo=False,
                                                                arkivo=False, publikigo=True)
                            next # такой есть, идём к следующему
                        except DokumentoContract.DoesNotExist:
                            publikiga_dato = js['Дата']

                            if not len(errors):
                                dokumento = Dokumento.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    autoro = uzanto,
                                    tipo = tipo_dokumento,
                                    publikigo=True,
                                    publikiga_dato=publikiga_dato
                                )

                            mono_valuto = None
                            if js['ВалютаВзаиморасчетов_Key'] != '00000000-0000-0000-0000-000000000000':
                                try:
                                    mono_valuto = MonoValuto.objects.get(uuid=js['ВалютаВзаиморасчетов_Key'])
                                except MonoValuto.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='ВалютаВзаиморасчетов_Key',
                                        message=_('Нет такой валюты в базе')
                                    ))
                            if js['Организация_Key'] != '00000000-0000-0000-0000-000000000000':
                                try:
                                    organizo = Organizo.objects.get(uuid=js['Организация_Key'])
                                except Organizo.DoesNotExist:
                                    errors.append(ErrorNode(
                                        field='Организация_Key',
                                        message=_('Нет такой организации в базе')
                                    ))

                            komisipago_kalkulo_metodo = js['СпособРасчетаКомиссионногоВознаграждения']
                            tipo_kontrakto = js['ВидДоговора']
                            tipo_agentejo = js['ВидАгентскогоДоговора']
                            nombro = js['Номер']
                            procedo_registrigho = js['ПорядокРегистрацииСчетовФактурНаАвансПоДоговору']
                            direktanto_ordonilo = js['ЗаРуководителяПоПриказу']
                            direktanto_kombaranto = js['РуководительКонтрагента']
                            direktanto_kombaranto_ofico = js['ДолжностьРуководителяКонтрагента']

                            sekso_direktanto_kombaranto = None
                            if js['ПолРуководителяКонтрагента'] == 'Мужской':
                                sekso_direktanto_kombaranto = 'vira'
                            elif js['ПолРуководителяКонтрагента'] == 'Женский':
                                sekso_direktanto_kombaranto = 'virina'

                            dato=datetime.strptime(js['Дата'], "%Y-%m-%dT%H:%M:%S")
                            valideca_periodo=datetime.strptime(js['СрокДействия'], "%Y-%m-%dT%H:%M:%S")

                            if not len(errors):
                                importo = DokumentoContract.objects.create(
                                    uuid=uuid,
                                    forigo=False,
                                    arkivo=False,
                                    publikigo=True,
                                    publikiga_dato=publikiga_dato,
                                    dokumento = dokumento,
                                    kodo=js['Code'],
                                    mono_valuto=mono_valuto,
                                    organizo=organizo,
                                    komisipago=js['ПроцентКомиссионногоВознаграждения'],
                                    kontado_agentejo=js['УчетАгентскогоНДС'],
                                    kalkuto_kondicxa_unioj=js['РасчетыВУсловныхЕдиницах'],
                                    forigi_eksportado=js['УдалитьРеализацияНаЭкспорт'],
                                    dato=dato,
                                    valideca_periodo=valideca_periodo,
                                    fiksita_paga_termino=js['УстановленСрокОплаты'],
                                    paga_termino=js['СрокОплаты'],
                                    valuta=js['Валютный'],
                                    pagado_valuta=js['ОплатаВВалюте'],
                                    subskribi_kontrakton=js['ДоговорПодписан'],
                                    sekso_direktanto_kombaranto=sekso_direktanto_kombaranto,
                                )
                                
                                if komisipago_kalkulo_metodo:
                                    set_enhavo(importo.komisipago_kalkulo_metodo, komisipago_kalkulo_metodo, 'ru_RU')
                                if tipo_kontrakto:
                                    set_enhavo(importo.tipo_kontrakto, tipo_kontrakto, 'ru_RU')
                                if tipo_agentejo:
                                    set_enhavo(importo.tipo_agentejo, tipo_agentejo, 'ru_RU')
                                if nombro:
                                    set_enhavo(importo.nombro, nombro, 'ru_RU')
                                if procedo_registrigho:
                                    set_enhavo(importo.procedo_registrigho, procedo_registrigho, 'ru_RU')
                                if direktanto_ordonilo:
                                    set_enhavo(importo.direktanto_ordonilo, direktanto_ordonilo, 'ru_RU')
                                if direktanto_kombaranto:
                                    set_enhavo(importo.direktanto_kombaranto, direktanto_kombaranto, 'ru_RU')
                                if direktanto_kombaranto_ofico:
                                    set_enhavo(importo.direktanto_kombaranto_ofico, direktanto_kombaranto_ofico, 'ru_RU')

                                importo.save()
                                kvanto += 1
                    if not message:
                        if len(errors):
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                        else:
                            status = True
                            errors = list()
                            if kvanto > 0:
                                message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                            else:
                                message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Недостаточно прав')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoDokumentoContract1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return ImportoDokumentoContract1c(status=status, message=message, errors=errors, importo=importo)


# Добавление документа экспедирования в 1с (Тестовое добавление)
class TestAldoniDokumentoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    aldoni = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        uzanto = info.context.user

        # url = "https://192.168.99.5/skl_test_site/odata/standard.odata/Document_бит_ЗаявкаЭкспедированияММП(guid'eed47c08-3c81-11ec-8bc3-00155d091105')?$format=application/json"
        url = settings.ODATA_URL+'Document_бит_ЗаявкаЭкспедированияММП?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            posedanto = None
            komunumo = None
            uzanto = None

            with transaction.atomic():
                dato = timezone.now()

                datoj = {
                    # "Date": "2021-11-01T08:53:36",
                    "Date": dato.strftime("%Y-%m-%dT%H:%M:%S"),
                    # "УИД": "ORDER890509",
                    # "СборныйГруз": False,
                    # "Заказчик_Key": "82cfd4e8-d187-11e5-80f8-000c298958d9",
                    # "ДоговорЗаказчика_Key": "9262d36f-d188-11e5-80f8-000c298958d9",
                    # "КонтактноеЛицоЗаказчика": "d415c023-df91-11e5-80f8-000c298958d9",
                    "КонтактноеЛицоЗаказчика_Type": "StandardODATA.Catalog_КонтактныеЛица",
                    # "ТелефонЗаказчика": "",
                    # "Организация_Key": "8575bcfe-2ec6-11e5-80ee-000c298958d9",
                    # "Объем": 10,
                    # "Вес": 10,
                    # "ОписаниеГруза": "",
                    # "СтоимостьГруза": 1,
                    # "ВалютаЗаказчика_Key": "00000000-0000-0000-0000-000000000000",
                    # "СуммаЗаказчика": 0,
                    # "СуммаПеревозчика": 0,
                    
                    # "АдресЗагрузки": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
                    "АдресЗагрузки_Type": "Edm.String",
                    # "АдресВыгрузки": "Липецкая обл., г.Липецк, ул.Космонавтов, д.11а",
                    "АдресВыгрузки_Type": "Edm.String",
                    # "ВремяС": "2021-09-02T08:00:00",
                    # "ВремяВыгрузкиС": "2021-09-03T09:00:00",
                    # "СКЛ_ДатаПриемки": "2021-09-02T00:00:00",
                    # "СКЛ_ВремяПриемкиС": "0001-01-01T08:00:00",
                    # "СКЛ_ВремяПриемкиПо": "0001-01-01T16:00:00",
                    # "СКЛ_ДатаПередачи": "2021-09-03T00:00:00",
                    # "СКЛ_ВремяПередачиС": "0001-01-01T09:00:00",
                    # "СКЛ_ВремяПередачиПо": "0001-01-01T16:00:00",
                    # "ГородПриемки": "934bc395-d720-11e5-80f8-000c298958d9",
                    "ГородПриемки_Type": "StandardODATA.Catalog_СКЛ_Города",
                    # "ГородПередачи": 	"8b89bbf0-d720-11e5-80f8-000c298958d9",
                    "ГородПередачи_Type": "StandardODATA.Catalog_СКЛ_Города",
                    # "ОрганизацияПоАдресу": "ООО \"Био-Рад Лаборатории\" Смирнова",
                    # "ОрганизацияПоАдресуРазгрузки": 	"ГУЗ \"Липецкая областная станция переливания крови\"",
                    # "КонтактноеЛицоПоАдресу": "Смирнова Н.С.",
                    # "КонтактноеЛицоПоАдресуРазгрузки": 	"Журавлева Маргарита Александровна, Шмырева Татьяна",
                    # "удалитьРезультатВыполнения_Key": 	"257a1113-da7f-11ea-8ba1-005056a9edf3",
                    # "Маршрут": [
                    #    {
                    #        "LineNumber": 1, # обязательное поле для маршрута
                    #        "ДатаНачала": 	"2021-09-02T09:00:00",
                    #     "ДатаОкончания": "2021-09-03T13:00:00",
                    #     "АдресНачала": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
                        # "АдресНачала_Type": "Edm.String",
                    #     "АдресКонца": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
                        # "АдресКонца_Type": "Edm.String",
                    #     "Состояние_Key": "67896102-d228-11e5-80f8-000c298958d9",
                    #     "Комментарий": "принимаем до 12-00",
                    #     "ИдентификаторМаршрута": "Маршрут № 1"},
                    # ],
                    # "ИдМаршрута": 1,
                }
                    
                    
                    # "ВремяС"
                

                json_string = json.dumps(datoj)
                # logger.info('== json_string == ', json_string)

                # r = requests.delete(url, auth=(login, password), verify=False)
                # headers = {'1C_OData-DataLoadMode': bytes(0)}
                # r = requests.post(url, headers=headers, data=json_string, auth=(login, password), verify=False)
                r = requests.post(url, data=json_string, auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                if jsj.get('Ref_Key',False):
                # errors = list()
                # if not message:
                    status = True
                    errors = list()
                    message = jsj.get('Ref_Key',False)
                else:
                    status = False
                    errors = list()
                    message = jsj
        else:
            message = _('Недостаточно прав')

        return status, message, errors, aldoni

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Создаёи тестовую запись в базе 1С
            status, message, errors, aldoni = TestAldoniDokumentoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return TestAldoniDokumentoEkspedo1c(status=status, message=message, errors=errors, aldoni=aldoni)


# Добавление документа экспедирования вместе с созданием документа, проекта, задачи
class AldoniDokumentoEkspedo(graphene.Mutation):
    """ Вначале данные сохраняются в 1с, получая ответ с uuid - сохраняем себе в базу (так будет синхронизация)
    на 30,03,2022 возникла проблема - слишком долгое сохранение и идёт обрыв связи до получения инфы от 1с о сохранении данных.
    
    Вариант:
    сохраняем в базу с пометкой о неподтверждённом сохранении в 1с. Регулярно проверяем о сохранении в 1с и если нет, то опять отправляем на сохранение
    В УИД записываем id документа (Dokumento) - он уникален по всем документам
    
    Для непроверенных ставим "Вид документа" (DokumentoSpeco) - "На синхронизации с 1с"
    
    Args:
        graphene ([type]): [description]

    Returns:
        [type]: [description]
    """
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    ekspedo = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        # для создания документа
        tipo_dokumento_id = graphene.Int(description=_('ID типа документов (по умолчанию = 4 (Экспедирование))'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров (по умолчанию = [1])'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий документов (по умолчанию = [4])'))
        # для создания владельцев документов
        posedanto_dokumento_kliento_tipo_id = graphene.Int(description=_('ID типа владельцев-клиентов документов (по умолчанию = 2)'))
        posedanto_dokumento_organizo_tipo_id = graphene.Int(description=_('ID типа владельцев-поставщиков документов (по умолчанию = 1)'))
        posedanto_dokumento_statuso_id = graphene.Int(description=_('ID типа  владельцев документов (по умолчанию = 1)'))
        # для создание характеристик экспедирования
        # kodo = graphene.String(description=_('Kodo')) создаётся в 1с
        adreso_elsxargxado = graphene.String(description=_('Адрес выгрузки'))
        adreso_kargado = graphene.String(description=_('Адрес загрузки'))
        valuto_kliento_id = graphene.Int(description=_('Валюта заказчика'))
        pezo = graphene.Int(description=_('Вес'))
        tempo_elsxargxado = graphene.DateTime(description=_("Время выгрузки с"))
        tempo_s = graphene.DateTime(description=_("Время с"))
        ekspedinto_uuid = graphene.String(description=_('Грузоотправитель'))
        ricevanto_uuid = graphene.String(description=_('Грузополучатель'))
        kontrakto_kliento_uuid = graphene.String(description=_('Договор заказчика'))
        kliento_uuid = graphene.String(description=_('Заказчик'))
        kliento_sciigi = graphene.Boolean(description=_("Признак: заказчик извещен"))
        komento = graphene.String(description=_('Комментарий'))
        komento_adreso_elsxargxado = graphene.String(description=_('Комментарий адреса выгрузки'))
        komento_adreso_kargado = graphene.String(description=_('Комментарий адреса загрузки'))
        komento_nuligo = graphene.String(description=_('Комментарий отмены'))
        kombatanto_kliento_uuid = graphene.String(description=_('Контактное лицо заказчика'))
        kombatanto_adresoj = graphene.String(description=_('Контактное лицо по адресу загрузки'))
        kombatanto_adresoj_elsxargxado = graphene.String(description=_('Контактное лицо по адресу разгрузки'))
        volumeno = graphene.Float(description=_('Объем'))
        priskribo_kargo = graphene.String(description=_('Описание груза'))
        organizo_uuid = graphene.String(description=_('uuid обслуживающей организации'))
        organizo_adreso = graphene.String(description=_('Организация по адресу'))
        organizo_adreso_elsxargxado = graphene.String(description=_('Организация по адресу разгрузки'))
        # ПричинаОтмены_Key - ссылка ??? (причи́н||а kaŭzo; kialo) (отме́н||а nuligo)
        # kialo_nuligo_key = models.UUIDField(_('Kialo nuligo'), default=None, blank=True, null=True)
        kosto_kargo = graphene.Float(description=_('Стоимость груза'))
        monsumo_kliento = graphene.Float(description=_('Сумма заказчика'))
        monsumo_kliento_fiksa_mane = graphene.Boolean(description=_('Сумма заказчика установлена вручную'))
        monsumo_transportisto = graphene.Float(description=_('Сумма перевозчика'))
        monsumo_transportisto_fiksa_mane = graphene.Boolean(description=_('Сумма перевозчика установлена вручную'))
        telefono_kliento = graphene.String(description=_('Телефон заказчика'))
        telefono_adreso = graphene.String(description=_('Телефон по адресу'))
        telefono_adreso_elsxargxado = graphene.String(description=_('Телефон по адресу разгрузки'))
        fakto = graphene.Boolean(description=_('Факт'))
        kosto_kliento = graphene.Float(description=_('Цена заказчика'))
        kosto_transportisto = graphene.Float(description=_('Цена перевозчика'))
        partopago_kliento = graphene.Float(description=_('Аванс заказчика'))
        partopago_transportisto = graphene.Float(description=_('Аванс перевозчика'))
        aldona_kondicxaro = graphene.String(description=_('Дополнительные условия'))
        # АвторЛогист_Key - ссылка
        # autoro_logist_key = models.UUIDField(_('Autoro logist key'), default=None, blank=True, null=True)
        impostokvoto_kliento = graphene.String(description=_('Ставка НДС заказчика'))
        impostokvoto_transportisto = graphene.String(description=_('Ставка НДС перевозчика'))
        priskribo_kalkulado = graphene.String(description=_('Комментарий расчета'))
        rendevuejo = graphene.Boolean(description=_('Сборный груз'))
        numero_frajtoletero = graphene.String(description=_('Номер накладной СК'))
        pezo_fakto = graphene.Float(description=_('Вес фактический'))
        pezo_kalkula = graphene.Float(description=_('Вес расчетный'))
        dangxera_kargo = graphene.Boolean(description=_('Опасный груз'))
        priskribo_dangxera_kargo = graphene.String(description=_('Описание опасного груза'))
        klaso_dangxera = graphene.String(description=_('Класс опасности'))
        retropasxo_tsd = graphene.Boolean(description=_('Возврат ТСД'))
        akcepto_liveri_nelabora_tempo = graphene.Boolean(description=_('Прием доставка в нерабочее время'))
        aldone_pakumo = graphene.Boolean(description=_('Дополнительная упаковка'))
        temperatura_reghimo_uuid = graphene.String(description=_('UUID температурного режима'))
        # СозданиеХЦ_Key (Создание - faro)
        # faro_hc_key = models.UUIDField(_('Faro HC UUID'), default=None, blank=True, null=True)
        indikatoro_temperatura = graphene.Boolean(description=_('Датчик температуры'))
        livero_tp = graphene.Boolean(description=_('Предоставление ТП'))
        aldone_postulo_hc = graphene.String(description=_('Дополнительные требования ХЦ'))
        urboj_recivado_uuid = graphene.String(description=_('UUID города приемки'))
        urboj_recivado_nomo = graphene.String(description=_('Город приемки'))
        urboj_transdono_uuid = graphene.String(description=_('UUID города передачи'))
        urboj_transdono_nomo = graphene.String(description=_('Город передачи'))
        aliaj_servo = graphene.String(description=_('Иные услуги'))
        fordono_kargo_fio = graphene.String(description=_('Передача груза ФИО'))
        fordono_kargo_dato = graphene.Date(description=_("ФПередача груза дата"))
        fordono_kargo_tempo = graphene.Time(description=_("ФПередача груза время"))
        konservejo_ricevado_dato = graphene.Date(description=_("СКЛ дата приемки"))
        konservejo_ricevado_tempo_de = graphene.Time(description=_("СКЛ время приемки с"))
        konservejo_ricevado_tempo_en = graphene.Time(description=_("СКЛ время приемки по"))
        konservejo_fordono_dato = graphene.Date(description=_("СКЛ дата передачи"))
        konservejo_fordono_tempo_de = graphene.Time(description=_("СКЛ время передачи с"))
        konservejo_fordono_tempo_en = graphene.Time(description=_("СКЛ время передачи по"))
        asekurado = graphene.Boolean(description=_('Требуется страхование'))
        horoj_sxargxistoj = graphene.Int(description=_('Количество часов грузчики'))
        kvanto_sxargxistoj = graphene.Int(description=_('Количество грузчиков'))
        kvanto_pecoj = graphene.Int(description=_('Количество мест'))
        sxargxisto_rigilaro = graphene.Int(description=_('Требуются грузчики такелажники'))
        en_peza_kategorio = graphene.Boolean(description=_('Тяжеловес'))
        nomo_kargo = graphene.String(description=_('Наименование груза'))
        # Транспорт_Key (тра́нспорт trajno)
        # trajno_key = models.UUIDField(_('Trajno UUID'), default=None, blank=True, null=True)
        # Водитель_Key (Водитель kondukisto)
        # kondukisto_key = models.UUIDField(_('Kondukisto UUID'), default=None, blank=True, null=True)
        konservejo_temperatura_reghimo_1 = graphene.Boolean(description=_('скл температурный режим 1'))
        konservejo_temperatura_reghimo_2 = graphene.Boolean(description=_('скл температурный режим 2'))
        konservejo_temperatura_reghimo_3 = graphene.Boolean(description=_('скл температурный режим 3'))
        konservejo_temperatura_reghimo_4 = graphene.Boolean(description=_('скл температурный режим 4'))
        konservejo_temperatura_reghimo_alia = graphene.String(description=_('скл температурный режим иной'))
        konservejo_temperatura_reghimo_5 = graphene.Boolean(description=_('скл температурный режим 5'))
        konservejo_temperatura_reghimo_6 = graphene.Boolean(description=_('скл температурный режим 6'))
        konservejo_tasko = graphene.String(description=_('скл задание'))
        uid = graphene.String(description=_('УИД'))
        konservejo_komento = graphene.String(description=_('скл комментарий'))
        liveranto_uuid = graphene.String(description=_('UUID рл поставщик'))
        liveranto_kontrakto_uuid = graphene.String(description=_('Kontrakto liveranto UUID'))
        kombatanto_kontrakto_uuid = graphene.String(description=_('UUID рл контактное лицо поставщика'))
        trajno_venigo_dato = graphene.Date(description=_("рл дата подачи транспорт"))
        trajno_venigo_tempo = graphene.Time(description=_("рл время подачи транспорт"))
        trajno_finigho_dato = graphene.Date(description=_("рл дата окончания транспорт"))
        trajno_finigho_tempo = graphene.Time(description=_("рл время окончания транспорт"))
        # ТранспортСоСкладаГК_Key (тра́нспорт trajno)
        # trajno_konservejo_gk_key = models.UUIDField(_('Trajno konservejo gk UUID'), default=None, blank=True, null=True)
        # ВодительСоСкладаГК_Key (Водитель kondukisto)
        # kondukisto_konservejo_gk_key = models.UUIDField(_('Kondukisto konservejo gk UUID'), default=None, blank=True, null=True)
        sensoro_temperatura_2 = graphene.Boolean(description=_('Датчик температуры 2'))
        sensoro_temperatura_3 = graphene.Boolean(description=_('Датчик температуры 3'))
        sensoro_temperatura_4 = graphene.Boolean(description=_('Датчик температуры 4'))
        sensoro_temperatura_5 = graphene.Boolean(description=_('Датчик температуры 5'))
        sensoro_temperatura_alia = graphene.Boolean(description=_('Датчик температуры иной'))
        Livero_tp_2 = graphene.Boolean(description=_('Предоставление ТП 2'))
        Livero_tp_3 = graphene.Boolean(description=_('Предоставление ТП 3'))
        Livero_tp_4 = graphene.Boolean(description=_('Предоставление ТП 4'))
        Livero_tp_5 = graphene.Boolean(description=_('Предоставление ТП 5'))
        Livero_tp_alia = graphene.Boolean(description=_('Предоставление ТП иной'))
        priskribo_kliento = graphene.String(description=_('скл комментарий клиента'))
        numero_frajtoletero_kliento = graphene.String(description=_('Номер накладной клиента'))
        email_ekspedinto = graphene.String(description=_('Email отправителя'))
        email_ricevanto = graphene.String(description=_('Email получателя'))
        plenumado_dato = graphene.DateTime(description=_("Дата на исполнении"))
        fiksa_termoskribi = graphene.Boolean(description=_('рл стационарный термописец'))
        fiksa_termoskribi_2 = graphene.Boolean(description=_('рл стационарный термописец 2'))
        fiksa_termoskribi_3 = graphene.Boolean(description=_('рл стационарный термописец 3'))
        fiksa_termoskribi_4 = graphene.Boolean(description=_('рл стационарный термописец 4'))
        fiksa_termoskribi_5 = graphene.Boolean(description=_('рл стационарный термописец 5'))
        fiksa_termoskribi_alia = graphene.Boolean(description=_('рл стационарный термописец иной'))
        tipo_mendo = graphene.String(description=_('рл тип заявки'))
                    # "ИдМаршрута": 1,

            # рл_ПодразделениеЗаказчика_Key (Подразделение  (отдел, раздел, секция) sekcio)
            # sekcio_kliento_key = models.UUIDField(_('Sekcio kliento UUID'), default=None, blank=True, null=True)
        sxargxistoj_sxargxado = graphene.Boolean(description=_('рл грузчики погрузка'))
        sxargxistoj_elsxargxado = graphene.Boolean(description=_('рл грузчики выгрузка'))
        # параметры создаваемого проекта
        projekto_statuso_id = graphene.Int(description=_('Статус проекта')) # по умолчанию = 7
        # параметры создаваемой задачи
        tasko_numero = graphene.List(graphene.Int,description=_('Порядковый номер задач')) # "LineNumber": 1, # обязательное поле для маршрута
        tasko_kom_dato = graphene.List(graphene.DateTime,description=_('Дата начала выполнения задач')) # "ДатаНачала": 	"2021-09-02T09:00:00",
        tasko_fin_dato = graphene.List(graphene.DateTime,description=_('Дата окончания выполнения задач')) #  "ДатаОкончания": "2021-09-03T13:00:00",
        tasko_kom_adreso = graphene.List(graphene.String,description=_('Адрес начала выполнения задач')) # "АдресНачала": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
            # "АдресНачала_Type": "Edm.String",
        tasko_fin_adreso = graphene.List(graphene.String,description=_('Адрес окончания выполнения задач')) # "АдресКонца": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
            # "АдресКонца_Type": "Edm.String",
        tasko_statuso_id = graphene.List(graphene.Int,description=_('Статус задач')) # "Состояние_Key": "67896102-d228-11e5-80f8-000c298958d9",
        tasko_priskribo = graphene.List(graphene.String,description=_('Комментарии задач')) # "Комментарий": "принимаем до 12-00",
        tasko_nomo = graphene.List(graphene.String,description=_('Название задач'))# "ИдентификаторМаршрута": "Маршрут № 1"},
        # параметры перевозимого груза
        kargo_numero = graphene.List(graphene.Int,description=_('Порядковый номер'))
        kargo_nomo = graphene.List(graphene.String,description=_('Наименование'))
        kargo_kvanto_pecoj = graphene.List(graphene.Int,description=_('Количество мест'))
        kargo_longo = graphene.List(graphene.Float,description=_('Длина'))
        kargo_largho = graphene.List(graphene.Float,description=_('Ширина'))
        kargo_alto = graphene.List(graphene.Float,description=_('Высота'))
        kargo_pezo_fakta = graphene.List(graphene.Float,description=_('Вес фактический'))
        kargo_volumeno = graphene.List(graphene.Float,description=_('Объем'))
        kargo_pezo_volumena = graphene.List(graphene.Float,description=_('Вес объемный'))
        kargo_tipo_pakumoj_uuid = graphene.List(graphene.String,description=_('UUID типа упаковки'))
        kargo_indikatoro = graphene.List(graphene.Boolean,description=_('Нуженли датчик'))
        kargo_numero_indikatoro = graphene.List(graphene.String,description=_('Серийный номер датчика'))
        kargo_temperatura_reghimo_uuid = graphene.List(graphene.String,description=_('UUID терморежима'))
        kargo_grave = graphene.List(graphene.Boolean,description=_('Опасный/Неопасный'))
        kargo_retropasxo_pakumo = graphene.List(graphene.Boolean,description=_('Возврат упаковки'))
        kargo_retropasxo_indikatoro = graphene.List(graphene.Boolean,description=_('Возврат датчика'))

        
    @staticmethod
    def kontrolanta_argumentoj(root, info, **kwargs):
        """Проверка аргументов
        Что бы не проверять аргументы в create и в edit, проверим отдельно

        Args:
            дублируем как и в create и в edit
        """
        # uzanto = info.context.user
        # возвращаемые данные:
        errors = list()
        # проверяем наличие записей с таким кодом
        if 'posedanto_dokumento_kliento_tipo_id' in kwargs: # тип владельца-клиента документа по умолчанию
            try:
                posedanto_dokumento_kliento_tipo = DokumentoPosedantoTipo.objects.get(
                    id=kwargs.get('posedanto_dokumento_kliento_tipo_id'), forigo=False,
                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_kliento_tipo'] = posedanto_dokumento_kliento_tipo
            except DokumentoPosedantoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_kliento_tipo_id',
                    message=_('Неверный тип документов')
                ))
        if 'posedanto_dokumento_organizo_tipo_id' in kwargs: # тип владельца-поставщика документа по умолчанию
            try:
                posedanto_dokumento_organizo_tipo = DokumentoPosedantoTipo.objects.get(
                    id=kwargs.get('posedanto_dokumento_organizo_tipo_id'), forigo=False,
                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_organizo_tipo'] = posedanto_dokumento_organizo_tipo
            except DokumentoPosedantoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_organizo_tipo_id',
                    message=_('Неверный тип документов')
                ))
        if 'posedanto_dokumento_statuso_id' in kwargs: # статус владельца документа по умолчанию
            try:
                posedanto_dokumento_statuso = DokumentoPosedantoStatuso.objects.get(id=1, forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['posedanto_dokumento_statuso'] = posedanto_dokumento_statuso
            except DokumentoPosedantoStatuso.DoesNotExist:
                errors.append(ErrorNode(
                    field='posedanto_dokumento_statuso',
                    message=_('Неверный статус документов')
                ))
        if 'tipo_dokumento_id' in kwargs: # тип владельца-клиента документа по умолчанию
            try:
                tipo = DokumentoTipo.objects.get(id=kwargs.get('tipo_dokumento_id'), forigo=False,
                                                    arkivo=False, publikigo=True)
                kwargs['tipo_dokumento'] = tipo
            except DokumentoTipo.DoesNotExist:
                errors.append(ErrorNode(
                    field='tipo_dokumento_id',
                    message=_('Неверный тип документов')
                ))
        if 'speco_dokumento_id' in kwargs: # вид владельца-клиента документа по умолчанию
            try:
                kwargs['speco_dokumento'] = DokumentoSpeco.objects.get(id=kwargs.get('speco_dokumento_id'),
                                            forigo=False, arkivo=False, publikigo=True)
                
            except DokumentoSpeco.DoesNotExist:
                errors.append(ErrorNode(
                    field='speco_dokumento_id',
                    message=_('Неверный вид документа')
                ))
        if 'realeco_id' in kwargs:
            realeco_id = set(kwargs.get('realeco_id'))
            if len(realeco_id):
                realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                kwargs['realeco'] = realeco
                if len(dif):
                    errors.append(ErrorNode(
                        field=str(dif),
                        message=_('Указаны несуществующие реальности')
                    ))
        if 'kategorio_id' in kwargs:
            kategorio_id = set(kwargs.get('kategorio_id'))
            if len(kategorio_id):
                kategorio = DokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                kwargs['kategorio'] = kategorio
                if len(dif):
                    errors.append(ErrorNode(
                        field=str(dif),
                        message=_('Указаны несуществующие категория документов')
                    ))
        if 'projekto_statuso_id' in kwargs:
            try:
                projekto_statuso = TaskojProjektoStatuso.objects.get(id=kwargs.get('projekto_statuso_id'), forigo=False,
                                            arkivo=False, publikigo=True)
                kwargs['projekto_statuso'] = projekto_statuso
            except TaskojProjektoStatuso.DoesNotExist:
                errors.append(ErrorNode(
                    field='projekto_statuso_id',
                    message=_('Неверный id статуса проекта')
                ))
        # проверяем параметры для создания задач (маршрута)
        if kwargs.get('tasko_numero', False) and len(kwargs.get('tasko_numero')):
            len_tasko_numero = len(kwargs.get('tasko_numero'))
            # проверяем количество параметров
            if len_tasko_numero != len(kwargs.get('tasko_kom_dato')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_kom_dato',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_fin_dato')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_fin_dato',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_kom_adreso')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_kom_adreso',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_fin_adreso')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_fin_adreso',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_statuso_id')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_statuso_id',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_priskribo')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_priskribo',
                    message=_('Несоответствие количества параметров')
                ))
            if len_tasko_numero != len(kwargs.get('tasko_nomo')):
                errors.append(ErrorNode(
                    field='tasko_numero, tasko_nomo',
                    message=_('Несоответствие количества параметров')
                ))
            # получаем соответствующие записи по индексу
            if not len(errors):
                tasko_statuso_id = set(kwargs.get('tasko_statuso_id',[]))
                tasko_statuso = TaskojTaskoStatuso.objects.filter(id__in=tasko_statuso_id, forigo=False, arkivo=False, publikigo=True)
                set_value = set(tasko_statuso.values_list('id', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(s)
                dif = set(tasko_statuso_id) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('tasko_statuso_id'), str(dif)),
                        message=_('Указаны несуществующие статусы владельцев')
                    ))
                else: #собираем массив статусов владельцев для каждой задачи согласно вошедшего массива, 
                    #  т.к. у нас перечисление, а нужен верно составленный массив
                    tasko_statuso_masivo = []
                    for st in kwargs.get('tasko_statuso_id',[]):
                        for t_s in tasko_statuso:
                            if st == t_s.id:
                                tasko_statuso_masivo.append(t_s)
                                break
                    kwargs['tasko_statuso'] = tasko_statuso_masivo
        # проверяем параметры для создания списка грузов
        if kwargs.get('kargo_numero', False) and len(kwargs.get('kargo_numero')):
            len_kargo_numero = len(kwargs.get('kargo_numero'))
            # проверяем количество параметров
            if kwargs.get('kargo_nomo', False) and len_kargo_numero != len(kwargs.get('kargo_nomo')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_nomo',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_kvanto_pecoj', False) and len_kargo_numero != len(kwargs.get('kargo_kvanto_pecoj')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_kvanto_pecoj',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_longo', False) and len_kargo_numero != len(kwargs.get('kargo_longo')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_longo',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_largho', False) and len_kargo_numero != len(kwargs.get('kargo_largho')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_largho',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_alto', False) and len_kargo_numero != len(kwargs.get('kargo_alto')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_alto',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_pezo_fakta', False) and len_kargo_numero != len(kwargs.get('kargo_pezo_fakta')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_pezo_fakta',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_volumeno', False) and len_kargo_numero != len(kwargs.get('kargo_volumeno')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_volumeno',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_pezo_volumena', False) and len_kargo_numero != len(kwargs.get('kargo_pezo_volumena')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_pezo_volumena',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_tipo_pakumoj_uuid', False) and len_kargo_numero != len(kwargs.get('kargo_tipo_pakumoj_uuid')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_tipo_pakumoj_uuid',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_indikatoro')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_indikatoro',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_numero_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_numero_indikatoro')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_numero_indikatoro',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_temperatura_reghimo_uuid', False) and len_kargo_numero != len(kwargs.get('kargo_temperatura_reghimo_uuid')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_temperatura_reghimo_uuid',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_grave', False) and len_kargo_numero != len(kwargs.get('kargo_grave')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_grave',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_retropasxo_pakumo', False) and len_kargo_numero != len(kwargs.get('kargo_retropasxo_pakumo')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_retropasxo_pakumo',
                    message=_('Несоответствие количества параметров')
                ))
            if kwargs.get('kargo_retropasxo_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_retropasxo_indikatoro')):
                errors.append(ErrorNode(
                    field='kargo_numero, kargo_retropasxo_indikatoro',
                    message=_('Несоответствие количества параметров')
                ))
            # получаем соответствующие записи по индексу
            if not len(errors):
                kargo_tipo_pakumoj_uuid = set(kwargs.get('kargo_tipo_pakumoj_uuid',[]))
                kargo_tipo_pakumoj = ObjektoTipoPakumo.objects.filter(uuid__in=kargo_tipo_pakumoj_uuid, forigo=False, arkivo=False, publikigo=True)
                set_value = set(kargo_tipo_pakumoj.values_list('uuid', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(str(s))
                dif = set(kargo_tipo_pakumoj_uuid) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('kargo_tipo_pakumoj_uuid'), str(dif)),
                        message=_('Указаны несуществующие uuid')
                    ))
                else: #собираем массив для каждой задачи согласно вошедшего массива, 
                    #  т.к. у нас перечисление, а нужен верно составленный массив
                    kargo_tipo_pakumoj_masivo = []
                    for st in kwargs.get('kargo_tipo_pakumoj_uuid',[]):
                        for t_s in kargo_tipo_pakumoj:
                            if st == str(t_s.uuid):
                                kargo_tipo_pakumoj_masivo.append(t_s)
                                break
                    kwargs['kargo_tipo_pakumoj'] = kargo_tipo_pakumoj_masivo
            if not len(errors):
                kargo_temperatura_reghimo_uuid = set(kwargs.get('kargo_temperatura_reghimo_uuid',[]))
                kargo_temperatura_reghimo = ObjektoTemperaturaReghimo.objects.filter(uuid__in=kargo_temperatura_reghimo_uuid, forigo=False, arkivo=False, publikigo=True)
                set_value = set(kargo_temperatura_reghimo.values_list('uuid', flat=True))
                arr_value = []
                for s in set_value:
                    arr_value.append(str(s))
                dif = set(kargo_temperatura_reghimo_uuid) - set(arr_value)
                if len(dif):
                    errors.append(ErrorNode(
                        field='{} ({})'.format(_('kargo_temperatura_reghimo_uuid'), str(dif)),
                        message=_('Указаны несуществующие uuid')
                    ))
                else: #собираем массив для каждой задачи согласно вошедшего массива, 
                    #  т.к. у нас перечисление, а нужен верно составленный массив
                    kargo_temperatura_reghimo_masivo = []
                    for st in kwargs.get('kargo_temperatura_reghimo_uuid',[]):
                        for t_s in kargo_temperatura_reghimo:
                            if st == str(t_s.uuid):
                                kargo_temperatura_reghimo_masivo.append(t_s)
                                break
                    kwargs['kargo_temperatura_reghimo'] = kargo_temperatura_reghimo_masivo
        return errors, kwargs


    @staticmethod
    def argumentoj_kreanta(root, info, **kwargs):
        """argumentoj kiam kreanta - аргументы при создании 

        Args:
            root (_type_): _description_
            info (_type_): _description_
        """
        errors = list()
        # проверяем обязательные параметры
        # установка параметров "по умочанию"
        if not 'projekto_statuso_id' in kwargs: # удалитьРезультатВыполнения_Key
            kwargs['projekto_statuso_id'] = 7
        if not 'tipo_dokumento_id' in kwargs: # тип владельца-клиента документа по умолчанию
            kwargs['tipo_dokumento_id'] = 4
        if 'realeco_id' not in kwargs:
            kwargs['realeco_id'] = [1,]
        elif not len(kwargs.get('realeco_id')): # если передали пустой массив
            errors.append(ErrorNode(
                field='realeco_id',
                message=_('Необходимо указать хотя бы одно значение')
            ))
        if 'kategorio_id' not in kwargs:
            kwargs['kategorio_id'] = [4,]
        elif not len(kwargs.get('kategorio_id')):
            errors.append(ErrorNode(
                field='kategorio_id',
                message=_('Необходимо указать хотя бы одно значение')
            ))
        if not 'posedanto_dokumento_kliento_tipo_id' in kwargs: # тип владельца-клиента документа по умолчанию
            kwargs['posedanto_dokumento_kliento_tipo_id'] = 2
        if not 'posedanto_dokumento_organizo_tipo_id' in kwargs: # тип владельца-поставщика документа по умолчанию
            kwargs['posedanto_dokumento_organizo_tipo_id'] = 1
        if not 'posedanto_dokumento_statuso_id' in kwargs: # статус владельца документа по умолчанию
            kwargs['posedanto_dokumento_statuso_id'] = 1
        return errors, kwargs


    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        uzanto = info.context.user
        kliento = None
        membro = None
        dokumento = None
        dokumento_ekspedo = None
        informlibro = {'uzanto':uzanto,}

        informlibro['autoro'] = uzanto

        # Находим организацию-клиента
        # проверяем наличие записей с таким кодом
        if not len(errors):
            if 'kliento_uuid' in kwargs:
                    try:
                        kliento = Organizo.objects.get(uuid=kwargs.get('kliento_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['kliento'] = kliento
                    except Organizo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='kliento_uuid',
                            message=_('Неверный uuid организации')
                        ))
            else:
                errors.append(ErrorNode(
                    field='kliento_uuid',
                    message=_('Поле обязательно для заполнения')
                ))

        if not uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            try:
                membro = OrganizoMembro.objects.get(organizo=kliento, uzanto=uzanto, forigo=False,
                                                    arkivo=False, publikigo=True)
            except OrganizoMembro.DoesNotExist: # не является членом организации
                pass
        # Создавать может член организации-владельца документа
        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo') or membro:
            # Для непроверенных ставим "Вид документа" - "На синхронизации с 1с"
            kwargs['speco_dokumento_id'] = dokumento_Speco_1c # 4
            # проверяем обязательные параметры
            errors, kwargs = RedaktuDokumentoEkspedo.argumentoj_kreanta(root, info, **kwargs)
            if not len(errors):
                # проверяем наличие записей с таким кодом
                errors, kwargs = RedaktuDokumentoEkspedo.kontrolanta_argumentoj(root, info, **kwargs)
            if not len(errors):
                errors, kwargs = AldoniDokumentoEkspedo.argumentoj_kreanta(root, info, **kwargs)
            if not len(errors):
                # проверяем наличие записей с таким кодом
                errors, kwargs = AldoniDokumentoEkspedo.kontrolanta_argumentoj(root, info, **kwargs)

            with transaction.atomic():
                publikiga_dato = timezone.now()
                dokumento = None
                if not len(errors):
                    dokumento = Dokumento.objects.create(
                        forigo=False,
                        arkivo=False,
                        autoro = uzanto,
                        tipo = kwargs.get('tipo_dokumento'),
                        publikigo=True,
                        publikiga_dato=publikiga_dato,
                    )
                
                    set_enhavo(dokumento.nomo, 'Importo',  'ru_RU')
                    if kwargs.get('realeco', False):
                        dokumento.realeco.set(kwargs.get('realeco'))
                    if kwargs.get('kategorio', False):
                        dokumento.kategorio.set(kwargs.get('kategorio'))
                    if kwargs.get('speco_dokumento', False):
                        dokumento.speco = kwargs.get('speco_dokumento')

                    dokumento.save()
                    kwargs['dokumento'] = dokumento
                
                    if kliento:
                        # если есть клиент, то создаём
                        posedanto_kliento = DokumentoPosedanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            publikigo=True,
                            publikiga_dato=publikiga_dato,
                            tipo=kwargs.get('posedanto_dokumento_kliento_tipo'),
                            statuso=kwargs.get('posedanto_dokumento_statuso'),
                            posedanto_organizo=kliento,
                            dokumento=dokumento
                        )
                    if kwargs.get('organizo', False):
                        # если есть владелец, то создаём
                        posedanto_organizo = DokumentoPosedanto.objects.create(
                            forigo=False,
                            arkivo=False,
                            publikigo=True,
                            publikiga_dato=publikiga_dato,
                            tipo=kwargs.get('posedanto_dokumento_organizo_tipo'),
                            statuso=kwargs.get('posedanto_dokumento_statuso'),
                            posedanto_organizo=kwargs.get('organizo'),
                            dokumento=dokumento
                        )

                    # сохраняем в базу с пометкой о неподтверждённом сохранении в 1с. Регулярно проверяем о сохранении в 1с и если нет, то опять отправляем на сохранение
                    # В УИД записываем id документа (Dokumento) - он уникален по всем документам
                    kwargs['uid'] = dokumento.id
                    
                    status, message, errors, dokumento_ekspedo = RedaktuDokumentoEkspedo.create(root, info, **kwargs)

                    # параметры создаваемой задачи (маршрут)
                    # подготавливаем под задачи+проекты+владельцы
                    kwargoj = {
                        'prj_nomo': 'Экспедирование',
                        'prj_kategorio': [7,],
                        'prj_tipo_id': 3,
                        'prj_statuso_id': kwargs.get('projekto_statuso_id'),
                        'prj_posedanto_tipo_id': [1],
                        'prj_posedanto_statuso_id': [1],
                        'posedanto_dokumento_uuid': [dokumento.uuid],
                        'prj_posedanto_organizo_uuid': [],
                        'tipo_id': 3,
                        # эти в цикле собираем:
                        'kategorio':[],
                        'pozicio': kwargs.get('tasko_numero'),
                        'nomo': kwargs.get('tasko_nomo'),
                        'priskribo': kwargs.get('tasko_priskribo'),
                        'statuso_id': kwargs.get('tasko_statuso_id'),
                        'posedanto_tipo_id':[],
                        'posedanto_statuso_id':[],
                        'kom_dato':kwargs.get('tasko_kom_dato'),
                        'fin_dato':kwargs.get('tasko_fin_dato'),
                        'kom_adreso':kwargs.get('tasko_kom_adreso'),
                        'fin_adreso':kwargs.get('tasko_fin_adreso')
                    }
                    if kliento:
                        kwargoj['prj_posedanto_organizo_uuid'] = [kliento.uuid]
                    # поставщика услуг также во владельцы проекта надо ставить
                    if kwargs.get('organizo'):
                        kwargoj['prj_posedanto_tipo_id'].append(1)
                        kwargoj['prj_posedanto_statuso_id'].append(1)
                        kwargoj['posedanto_dokumento_uuid'].append(dokumento.uuid)
                        kwargoj['prj_posedanto_organizo_uuid'].append(kwargs.get('organizo').uuid)

                    for js_tasko in kwargs.get('tasko_numero'):
                        kwargoj['kategorio'].append(13)
                        kwargoj['posedanto_tipo_id'].append(1)
                        kwargoj['posedanto_statuso_id'].append(1)
                        
                    logger.info('=== kwargoj === ', kwargoj)
                        
                    status, message2, errors2, projekto, taskoj = RedaktuKreiTaskojProjektoTaskojPosedanto.create(root, info, **kwargoj)

                    if errors2 and len(errors2)>0:
                        errors.extend(errors2)

                    # параметры перевозимого груза
                    if not len(errors):
                        i = 0
                        for kargo_numero in kwargs.get('kargo_numero'):
                            ekspedo_kargo = DokumentoEkspedoKargo.objects.create(
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=publikiga_dato,
                                numero=kwargs.get('kargo_numero')[i],
                                kvanto_pecoj=kwargs.get('kargo_kvanto_pecoj')[i],
                                longo=kwargs.get('kargo_longo')[i],
                                largho=kwargs.get('kargo_largho')[i],
                                alto=kwargs.get('kargo_alto')[i],
                                pezo_fakta=kwargs.get('kargo_pezo_fakta')[i],
                                volumeno=kwargs.get('kargo_volumeno')[i],
                                pezo_volumena=kwargs.get('kargo_pezo_volumena')[i],
                                tipo_pakumoj=kwargs.get('kargo_tipo_pakumoj')[i],
                                indikatoro=kwargs.get('kargo_indikatoro')[i],
                                temperatura_reghimo=kwargs.get('kargo_temperatura_reghimo')[i],
                                grave=kwargs.get('kargo_grave')[i],
                                retropasxo_pakumo=kwargs.get('kargo_retropasxo_pakumo')[i],
                                retropasxo_indikatoro=kwargs.get('kargo_retropasxo_indikatoro')[i],
                                ekspedo=dokumento_ekspedo,
                            )

                    if not len(errors):
                        # Создаём запись в 1с через задачу
                        logger.info('=== Создаём запись ===')
                        task_dokumento_ekspedo_al_1c.delay(uzanto.id, dokumento_ekspedo.uuid)
                    # else:
                    #     logger.info('=== errors = ', errors)

        else:
            errors.append(ErrorNode(
                field='',
                message=_('Недостаточно прав')
            ))
        if len(errors):
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        elif not message:
            status = True
            errors = list()
            message =  _('Данные сохранены на всех этапах')

        return status, message, errors, dokumento_ekspedo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        ekspedo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Создаём запись
            status, message, errors, ekspedo = AldoniDokumentoEkspedo.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return AldoniDokumentoEkspedo(status=status, message=message, errors=errors, ekspedo=ekspedo)


# Старое добавление документа экспедирования в 1с 
class OldAldoniDokumentoEkspedo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    aldoni = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        uzanto = info.context.user

        # url = "https://192.168.99.5/skl_test_site/odata/standard.odata/Document_бит_ЗаявкаЭкспедированияММП(guid'eed47c08-3c81-11ec-8bc3-00155d091105')?$format=application/json"
        url = settings.ODATA_URL+'Document_бит_ЗаявкаЭкспедированияММП?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        # Находим организацию-клиента
        if not uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            if kwargs.get('kliento', False):
                try:
                    membro = OrganizoMembro.objects.get(organizo=kwargs.get('kliento'), uzanto=uzanto, forigo=False,
                                                        arkivo=False, publikigo=True)
                except OrganizoMembro.DoesNotExist: # не является членом организации
                    pass
        # Создавать может член организации-владельца документа
        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo') or membro:

            with transaction.atomic():
                dato = timezone.now()
                tempo_s = None
                tempo_elsxargxado = None
                konservejo_ricevado_dato = None
                konservejo_ricevado_tempo_de = None
                konservejo_ricevado_tempo_en = None
                konservejo_fordono_dato = None
                konservejo_fordono_tempo_de = None
                konservejo_fordono_tempo_en = None
                if kwargs.get('tempo_s', False):
                    tempo_s = kwargs.get('tempo_s').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('tempo_elsxargxado', False):
                    tempo_elsxargxado = kwargs.get('tempo_elsxargxado').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_ricevado_dato', False):
                    konservejo_ricevado_dato = kwargs.get('konservejo_ricevado_dato').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_ricevado_tempo_de', False):
                    konservejo_ricevado_tempo_de = kwargs.get('konservejo_ricevado_tempo_de').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_ricevado_tempo_en', False):
                    konservejo_ricevado_tempo_en = kwargs.get('konservejo_ricevado_tempo_en').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_fordono_dato', False):
                    konservejo_fordono_dato = kwargs.get('konservejo_fordono_dato').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_fordono_tempo_de', False):
                    konservejo_fordono_tempo_de = kwargs.get('konservejo_fordono_tempo_de').strftime("%Y-%m-%dT%H:%M:%S")
                if kwargs.get('konservejo_fordono_tempo_en', False):
                    konservejo_fordono_tempo_en = kwargs.get('konservejo_fordono_tempo_en').strftime("%Y-%m-%dT%H:%M:%S")
                fordono_kargo_dato = None
                if kwargs.get('fordono_kargo_dato', False):
                    fordono_kargo_dato = kwargs.get('fordono_kargo_dato').strftime("%Y-%m-%dT%H:%M:%S")
                fordono_kargo_tempo = None
                if kwargs.get('fordono_kargo_tempo', False):
                    fordono_kargo_tempo = kwargs.get('fordono_kargo_tempo').strftime("%Y-%m-%dT%H:%M:%S")
                trajno_venigo_dato = None
                if kwargs.get('trajno_venigo_dato', False):
                    trajno_venigo_dato = kwargs.get('trajno_venigo_dato').strftime("%Y-%m-%dT%H:%M:%S")
                trajno_venigo_tempo = None
                if kwargs.get('trajno_venigo_tempo', False):
                    trajno_venigo_tempo = kwargs.get('trajno_venigo_tempo').strftime("%Y-%m-%dT%H:%M:%S")
                trajno_finigho_dato = None
                if kwargs.get('trajno_finigho_dato', False):
                    trajno_finigho_dato = kwargs.get('trajno_finigho_dato').strftime("%Y-%m-%dT%H:%M:%S")
                trajno_finigho_tempo = None
                if kwargs.get('trajno_finigho_tempo', False):
                    trajno_finigho_tempo = kwargs.get('trajno_finigho_tempo').strftime("%Y-%m-%dT%H:%M:%S")
                plenumado_dato = None
                if kwargs.get('plenumado_dato', False):
                    plenumado_dato = kwargs.get('plenumado_dato').strftime("%Y-%m-%dT%H:%M:%S")
                datoj = {
                    # "Date": "2021-11-01T08:53:36",
                    "Date": dato.strftime("%Y-%m-%dT%H:%M:%S"),
                    # "УИД": "ORDER890509",
                    "СборныйГруз": kwargs.get('rendevuejo', False),
                    "КонтактноеЛицоЗаказчика_Type": "StandardODATA.Catalog_КонтактныеЛица",
                    "ТелефонЗаказчика": kwargs.get('telefono_kliento', ''),
                    "ТелефонПоАдресу": kwargs.get('telefono_adreso', None),
                    "ТелефонПоАдресуРазгрузки": kwargs.get('telefono_adreso_elsxargxado', None),
                    "Объем": kwargs.get('volumeno', None),
                    "Вес": kwargs.get('pezo', None),
                    "ОписаниеГруза": kwargs.get('priskribo_kargo', None),
                    "СтоимостьГруза": kwargs.get('kosto_kargo', None),
                    "СуммаЗаказчика": kwargs.get('monsumo_kliento', None),
                    "СуммаЗаказчикаУстановленаВручную": kwargs.get('monsumo_kliento_fiksa_mane', None),
                    "СуммаПеревозчика": kwargs.get('monsumo_transportisto', None),
                    "СуммаПеревозчикаУстановленаВручную": kwargs.get('monsumo_transportisto_fiksa_mane', None),
                    "АдресЗагрузки": kwargs.get('adreso_kargado', None),
                    "АдресЗагрузки_Type": "Edm.String",
                    "АдресВыгрузки": kwargs.get('adreso_elsxargxado', None),
                    "АдресВыгрузки_Type": "Edm.String",
                    
                    "ВремяС": tempo_s,
                    "ВремяВыгрузкиС": tempo_elsxargxado,
                    "СКЛ_ДатаПриемки": konservejo_ricevado_dato,
                    "СКЛ_ВремяПриемкиС": konservejo_ricevado_tempo_de,
                    "СКЛ_ВремяПриемкиПо": konservejo_ricevado_tempo_en,
                    "СКЛ_ДатаПередачи": konservejo_fordono_dato,
                    "СКЛ_ВремяПередачиС": konservejo_fordono_tempo_de,
                    "СКЛ_ВремяПередачиПо": konservejo_fordono_tempo_en,
                    "ОрганизацияПоАдресу": kwargs.get('organizo_adreso', None),
                    "ОрганизацияПоАдресуРазгрузки": kwargs.get('organizo_adreso_elsxargxado', None),
                    "КонтактноеЛицоПоАдресу": kwargs.get('kombatanto_adresoj', None),
                    "КонтактноеЛицоПоАдресуРазгрузки": 	kwargs.get('kombatanto_adresoj_elsxargxado', None),
                    "ЗаказчикИзвещен": kwargs.get('kliento_sciigi', None),
                    "Комментарий": kwargs.get('komento', None),
                    "КомментарийАдресаВыгрузки": kwargs.get('komento_adreso_elsxargxado', None),
                    "КомментарийАдресаЗагрузки": kwargs.get('komento_adreso_kargado', None),
                    "КомментарийОтмены": kwargs.get('komento_nuligo', None),
                    "Факт": kwargs.get('fakto', None),
                    "ЦенаЗаказчика": kwargs.get('kosto_kliento', None),
                    "ЦенаПеревозчика": kwargs.get('kosto_transportisto', None),
                    "АвансЗаказчика": kwargs.get('partopago_kliento', None),
                    "АвансПеревозчику": kwargs.get('partopago_transportisto', None),
                    "ДопУсловия": kwargs.get('aldona_kondicxaro', None),
                    "СтавкаНДСЗаказчик": kwargs.get('impostokvoto_kliento', None),
                    "СтавкаНДСПеревозчик": kwargs.get('impostokvoto_transportisto', None),
                    "КомментарийРасчета": kwargs.get('priskribo_kalkulado', None),
                    "НомерНакладнойСК": kwargs.get('numero_frajtoletero', None),
                    "ВесФакт": kwargs.get('pezo_fakto', None),
                    "ВесРасч": kwargs.get('pezo_kalkula', None),
                    "ОпасныйГруз": kwargs.get('dangxera_kargo', None),
                    "ОписаниеОпасногоГруза": kwargs.get('priskribo_dangxera_kargo', None),
                    "КлассОпасности": kwargs.get('klaso_dangxera', None),
                    "ВозвратТСД": kwargs.get('retropasxo_tsd', None),
                    "ПриемДоставкаВНерабочееВремя": kwargs.get('akcepto_liveri_nelabora_tempo', None),
                    "ДополнительнаяУпаковка": kwargs.get('aldone_pakumo', None),
                    "ДатчикТемпературы": kwargs.get('indikatoro_temperatura', None),
                    "ПредоставлениеТП": kwargs.get('livero_tp', None),
                    "скл_ТемпературныйРежим1": kwargs.get('konservejo_temperatura_reghimo_1', None),
                    "скл_ТемпературныйРежим2": kwargs.get('konservejo_temperatura_reghimo_2', None),
                    "скл_ТемпературныйРежим3": kwargs.get('konservejo_temperatura_reghimo_3', None),
                    "скл_ТемпературныйРежим4": kwargs.get('konservejo_temperatura_reghimo_4', None),
                    "скл_ТемпературныйРежим5": kwargs.get('konservejo_temperatura_reghimo_5', None),
                    "скл_ТемпературныйРежим6": kwargs.get('konservejo_temperatura_reghimo_6', None),
                    "скл_ТемпературныйРежимИной": kwargs.get('konservejo_temperatura_reghimo_alia', None),
                    "ПредоставлениеТП2": kwargs.get('Livero_tp_2', None),
                    "ПредоставлениеТП3": kwargs.get('Livero_tp_3', None),
                    "ПредоставлениеТП4": kwargs.get('Livero_tp_4', None),
                    "ПредоставлениеТП5": kwargs.get('Livero_tp_5', None),
                    "ПредоставлениеТПИной": kwargs.get('Livero_tp_alia', None),
                    "ДопТребованияХЦ": kwargs.get('aldone_postulo_hc', None),
                    "ИныеУслуги": kwargs.get('aliaj_servo', None),
                    "ПередачаГрузаФИО": kwargs.get('fordono_kargo_fio', None),
                    "ФПередачаГрузаДата": fordono_kargo_dato,
                    "ФПередачаГрузаВремя": fordono_kargo_tempo,
                    "ТребуетсяСтрахование": kwargs.get('asekurado', None),
                    "КоличествоЧасовГрузчики": kwargs.get('horoj_sxargxistoj', None),
                    "КоличествоГрузчиков": kwargs.get('kvanto_sxargxistoj', None),
                    "КоличествоМест": kwargs.get('kvanto_pecoj', None),
                    "ТребуютсяГрузчикиТакелажники": kwargs.get('sxargxisto_rigilaro', None),
                    "Тяжеловес": kwargs.get('en_peza_kategorio', None),
                    "НаименованиеГруза": kwargs.get('nomo_kargo', None),
                    "скл_Задание": kwargs.get('konservejo_tasko', None),
                    "скл_Комментарий": kwargs.get('konservejo_komento', None),
                    "рл_ДатаПодачиТранспорт": trajno_venigo_dato,
                    "рл_ВремяПодачиТранспорт": trajno_venigo_tempo,
                    "рл_ДатаОкончанияТранспорт": trajno_finigho_dato,
                    "рл_ВремяОкончанияТранспорт": trajno_finigho_tempo,
                    "ДатчикТемпературы2": kwargs.get('sensoro_temperatura_2', None),
                    "ДатчикТемпературы3": kwargs.get('sensoro_temperatura_3', None),
                    "ДатчикТемпературы4": kwargs.get('sensoro_temperatura_4', None),
                    "ДатчикТемпературы5": kwargs.get('sensoro_temperatura_5', None),
                    "ДатчикТемпературыИной": kwargs.get('sensoro_temperatura_alia', None),
                    "скл_КомментарийКлиента": kwargs.get('priskribo_kliento', None),
                    "НомерНакладнойКлиента": kwargs.get('numero_frajtoletero_kliento', None),
                    "EmailОтправителя": kwargs.get('email_ekspedinto', None),
                    "EmailПолучателя": kwargs.get('email_ricevanto', None),
                    "ДатаНаИсполнении": plenumado_dato,
                    "рл_СтационарныйТермописец": kwargs.get('fiksa_termoskribi', None),
                    "рл_СтационарныйТермописец2": kwargs.get('fiksa_termoskribi_2', None),
                    "рл_СтационарныйТермописец3": kwargs.get('fiksa_termoskribi_3', None),
                    "рл_СтационарныйТермописец4": kwargs.get('fiksa_termoskribi_4', None),
                    "рл_СтационарныйТермописец5": kwargs.get('fiksa_termoskribi_5', None),
                    "рл_СтационарныйТермописецИной": kwargs.get('fiksa_termoskribi_alia', None),
                    "рл_ТипЗаявки": kwargs.get('tipo_mendo', None),
                    "рл_ГрузчикиПогрузка": kwargs.get('sxargxistoj_sxargxado', None),
                    "рл_ГрузчикиВыгрузка": kwargs.get('sxargxistoj_elsxargxado', None),
                    "Маршрут": [],
                    "ГрузыММП": [],
                }
                if kwargs.get('kliento', False):
                    datoj["Заказчик_Key"] = kwargs.get('kliento_uuid')
                if kwargs.get('valuto_kliento', False):
                    datoj['ВалютаЗаказчика_Key'] = str(kwargs.get('valuto_kliento').uuid)
                if kwargs.get('ekspedinto', False):
                    datoj['Грузоотправитель_Key'] = str(kwargs.get('ekspedinto').uuid)
                if kwargs.get('ricevanto', False):
                    datoj['Грузополучатель_Key'] = str(kwargs.get('ricevanto').uuid)
                if kwargs.get('kontrakto_kliento', False):
                    datoj['ДоговорЗаказчика_Key'] = kwargs.get('kontrakto_kliento_uuid')
                if kwargs.get('kombatanto_kliento', False):
                    datoj['КонтактноеЛицоЗаказчика'] = kwargs.get('kombatanto_kliento_uuid')
                if kwargs.get('organizo', False):
                    datoj['Организация_Key'] = kwargs.get('organizo_uuid')
                if kwargs.get('urboj_recivado_uuid', False):
                    datoj['ГородПриемки'] = kwargs.get('urboj_recivado_uuid')
                    datoj["ГородПриемки_Type"] = "StandardODATA.Catalog_СКЛ_Города"
                else: 
                    datoj['ГородПриемки'] = kwargs.get('urboj_recivado_nomo')
                    datoj["ГородПриемки_Type"] = "Edm.String"
                if kwargs.get('urboj_transdono_uuid', False):
                    datoj['ГородПередачи'] = kwargs.get('urboj_transdono_uuid')
                    datoj["ГородПередачи_Type"] = "StandardODATA.Catalog_СКЛ_Города"
                else: 
                    datoj['ГородПередачи'] = kwargs.get('urboj_transdono_nomo')
                    datoj["ГородПередачи_Type"] = "Edm.String"
                if kwargs.get('prj_statuso', False):
                    datoj['Организация_Key'] = kwargs.get('organizo_uuid')
                if kwargs.get('temperatura_reghimo', False):
                    datoj['ТемпературныйРежим_Key'] = kwargs.get('temperatura_reghimo_uuid')
                if kwargs.get('liveranto', False):
                    datoj['рл_Поставщик_Key'] = kwargs.get('liveranto_uuid')
                if kwargs.get('liveranto_kontrakto', False):
                    datoj['рл_ДоговорПоставщика_Key'] = kwargs.get('liveranto_kontrakto_uuid')
                if kwargs.get('kombatanto_kontrakto', False):
                    datoj['рл_КонтактноеЛицоПоставщика_Key'] = kwargs.get('kombatanto_kontrakto_uuid')
                # из проекта
                if kwargs.get('projekto_statuso', False):
                    datoj['удалитьРезультатВыполнения_Key'] = str(kwargs.get('projekto_statuso').uuid)

                # добавление маршрута
                if kwargs.get('tasko_statuso', False):
                    i = 0
                    for tasko_numero in kwargs.get('tasko_numero'):
                        tasko_kom_dato = kwargs.get('tasko_kom_dato')[i].strftime("%Y-%m-%dT%H:%M:%S")
                        tasko_fin_dato = kwargs.get('tasko_fin_dato')[i].strftime("%Y-%m-%dT%H:%M:%S")
                        datoj['Маршрут'].append({
                            "LineNumber": tasko_numero, # обязательное поле для маршрута
                            "ДатаНачала": 	tasko_kom_dato,
                            "ДатаОкончания": tasko_fin_dato,
                            "АдресНачала": kwargs.get('tasko_kom_adreso')[i],
                            "АдресНачала_Type": "Edm.String",
                            "АдресКонца": kwargs.get('tasko_fin_adreso')[i],
                            "АдресКонца_Type": "Edm.String",
                            "Состояние_Key": str(kwargs.get('tasko_statuso')[i].uuid),
                            "Комментарий": kwargs.get('tasko_priskribo')[i],
                            "ИдентификаторМаршрута": kwargs.get('tasko_nomo')[i]
                        })
                        i += 1
                    datoj["ИдМаршрута"] = 1
                if kwargs.get('kargo_numero', False):
                    # добавление груза ММП
                    i = 0
                    for kargo_numero in kwargs.get('kargo_numero'):
                        kargo = {"LineNumber": kargo_numero}
                        if kwargs.get('kargo_nomo', False):
                            kargo["Наименование"] = kwargs.get('kargo_nomo')[i]
                        if kwargs.get('kargo_kvanto_pecoj', False):
                            kargo["КоличествоМест"] = kwargs.get('kargo_kvanto_pecoj')[i] 
                        if kwargs.get('kargo_longo', False):
                            kargo["Длина"] = kwargs.get('kargo_longo')[i] 
                        if kwargs.get('kargo_largho', False):
                            kargo["Ширина"] = kwargs.get('kargo_largho')[i] 
                        if kwargs.get('kargo_alto', False):
                            kargo["Высота"] = kwargs.get('kargo_alto')[i] 
                        if kwargs.get('kargo_pezo_fakta', False):
                            kargo["ВесФактический"] = kwargs.get('kargo_pezo_fakta')[i] 
                        if kwargs.get('kargo_volumeno', False):
                            kargo["Объем"] = kwargs.get('kargo_volumeno')[i] 
                        if kwargs.get('kargo_pezo_volumena', False):
                            kargo["ВесОбъемный"] = kwargs.get('kargo_pezo_volumena')[i] 
                        if kwargs.get('kargo_tipo_pakumoj', False):
                            kargo["ТипУпаковки_Key"] = str(kwargs.get('kargo_tipo_pakumoj')[i].uuid) 
                        if kwargs.get('kargo_indikatoro', False):
                            kargo["НуженДатчик"] = kwargs.get('kargo_indikatoro')[i] 
                        if kwargs.get('kargo_numero_indikatoro', False):
                            kargo["СерийныйНомерДатчика"] = kwargs.get('kargo_numero_indikatoro')[i] 
                        if kwargs.get('kargo_temperatura_reghimo', False):
                            kargo["Терморежим_Key"] = str(kwargs.get('kargo_temperatura_reghimo')[i].uuid) 
                        if kwargs.get('kargo_grave', False):
                            kargo["ОпасныйНеопасный"] = kwargs.get('kargo_grave')[i] 
                        if kwargs.get('kargo_retropasxo_pakumo', False):
                            kargo["ВозвратУпаковки"] = kwargs.get('kargo_retropasxo_pakumo')[i] 
                        if kwargs.get('kargo_retropasxo_indikatoro', False):
                            kargo["ВозвратДатчика"] = kwargs.get('kargo_retropasxo_indikatoro')[i] 
                        
                        datoj['ГрузыММП'].append(kargo)
                        i += 1
                # logger.info('=== datoj[ГрузыММП] === ', datoj['ГрузыММП'])
                logger.info('=== datoj === ', datoj)
                json_string = json.dumps(datoj)

                # r = requests.delete(url, auth=(login, password), verify=False)
                r = requests.post(url, data=json_string, auth=(login, password), verify=False)
                if ((r.status_code < 200) or (r.status_code > 299)):
                    # logger.info('Ошибка связи с 1С сервером = ', r.status_code)
                    message = 'Ошибка связи с 1С сервером = ' + str(r.status_code)
                    status = False
                    errors.append(ErrorNode(
                        field = str(r.status_code),
                        message = _('Ошибка связи с 1С сервером')
                    ))
                    aldoni = None
                    return status, message, errors, aldoni 
                jsj = r.json()
                errors = list()
                message = jsj
                if jsj.get('Ref_Key',False):
                    status = True
                    aldoni = jsj
                else:
                    status = False
                    errors.append(ErrorNode(
                        field='',
                        message=_('Не получен ответ от 1С')
                    ))

        else:
            message = _('Недостаточно прав OldAldoniDokumentoEkspedo1c')
            errors.append(ErrorNode(
                field='OldAldoniDokumentoEkspedo1c.create',
                message=_('Недостаточно прав')
            ))

        return status, message, errors, aldoni

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Добавляем данные
            status, message, errors, aldoni = OldAldoniDokumentoEkspedo1c.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return OldAldoniDokumentoEkspedo1c(status=status, message=message, errors=errors, aldoni=aldoni)


# Старое добавление документа экспедирования вместе с созданием документа, проекта, задачи
class OldAldoniDokumentoEkspedo(graphene.Mutation):
    """ Вначале данные сохраняются в 1с, получая ответ с uuid - сохраняем себе в базу (так будет синхронизация)

    Args:
        graphene ([type]): [description]

    Returns:
        [type]: [description]
    """
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    ekspedo = graphene.Field(DokumentoEkspedoNode)

    class Arguments:
        # для создания документа
        tipo_dokumento_id = graphene.Int(description=_('ID типа документов (по умолчанию = 4 (Экспедирование))'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров (по умолчанию = [1])'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий документов (по умолчанию = [4])'))
        # для создания владельцев документов
        posedanto_dokumento_kliento_tipo_id = graphene.Int(description=_('ID типа владельцев-клиентов документов (по умолчанию = 2)'))
        posedanto_dokumento_organizo_tipo_id = graphene.Int(description=_('ID типа владельцев-поставщиков документов (по умолчанию = 1)'))
        posedanto_dokumento_statuso_id = graphene.Int(description=_('ID типа  владельцев документов (по умолчанию = 1)'))
        # для создание характеристик экспедирования
        # kodo = graphene.String(description=_('Kodo')) создаётся в 1с
        adreso_elsxargxado = graphene.String(description=_('Адрес выгрузки'))
        adreso_kargado = graphene.String(description=_('Адрес загрузки'))
        valuto_kliento_id = graphene.Int(description=_('Валюта заказчика'))
        pezo = graphene.Int(description=_('Вес'))
        tempo_elsxargxado = graphene.DateTime(description=_("Время выгрузки с"))
        tempo_s = graphene.DateTime(description=_("Время с"))
        ekspedinto_uuid = graphene.String(description=_('Грузоотправитель'))
        ricevanto_uuid = graphene.String(description=_('Грузополучатель'))
        kontrakto_kliento_uuid = graphene.String(description=_('Договор заказчика'))
        kliento_uuid = graphene.String(description=_('Заказчик'))
        kliento_sciigi = graphene.Boolean(description=_("Признак: заказчик извещен"))
        komento = graphene.String(description=_('Комментарий'))
        komento_adreso_elsxargxado = graphene.String(description=_('Комментарий адреса выгрузки'))
        komento_adreso_kargado = graphene.String(description=_('Комментарий адреса загрузки'))
        komento_nuligo = graphene.String(description=_('Комментарий отмены'))
        kombatanto_kliento_uuid = graphene.String(description=_('Контактное лицо заказчика'))
        kombatanto_adresoj = graphene.String(description=_('Контактное лицо по адресу загрузки'))
        kombatanto_adresoj_elsxargxado = graphene.String(description=_('Контактное лицо по адресу разгрузки'))
        volumeno = graphene.Float(description=_('Объем'))
        priskribo_kargo = graphene.String(description=_('Описание груза'))
        organizo_uuid = graphene.String(description=_('uuid обслуживающей организации'))
        organizo_adreso = graphene.String(description=_('Организация по адресу'))
        organizo_adreso_elsxargxado = graphene.String(description=_('Организация по адресу разгрузки'))
        # ПричинаОтмены_Key - ссылка ??? (причи́н||а kaŭzo; kialo) (отме́н||а nuligo)
        # kialo_nuligo_key = models.UUIDField(_('Kialo nuligo'), default=None, blank=True, null=True)
        kosto_kargo = graphene.Float(description=_('Стоимость груза'))
        monsumo_kliento = graphene.Float(description=_('Сумма заказчика'))
        monsumo_kliento_fiksa_mane = graphene.Boolean(description=_('Сумма заказчика установлена вручную'))
        monsumo_transportisto = graphene.Float(description=_('Сумма перевозчика'))
        monsumo_transportisto_fiksa_mane = graphene.Boolean(description=_('Сумма перевозчика установлена вручную'))
        telefono_kliento = graphene.String(description=_('Телефон заказчика'))
        telefono_adreso = graphene.String(description=_('Телефон по адресу'))
        telefono_adreso_elsxargxado = graphene.String(description=_('Телефон по адресу разгрузки'))
        fakto = graphene.Boolean(description=_('Факт'))
        kosto_kliento = graphene.Float(description=_('Цена заказчика'))
        kosto_transportisto = graphene.Float(description=_('Цена перевозчика'))
        partopago_kliento = graphene.Float(description=_('Аванс заказчика'))
        partopago_transportisto = graphene.Float(description=_('Аванс перевозчика'))
        aldona_kondicxaro = graphene.String(description=_('Дополнительные условия'))
        # АвторЛогист_Key - ссылка
        # autoro_logist_key = models.UUIDField(_('Autoro logist key'), default=None, blank=True, null=True)
        impostokvoto_kliento = graphene.String(description=_('Ставка НДС заказчика'))
        impostokvoto_transportisto = graphene.String(description=_('Ставка НДС перевозчика'))
        priskribo_kalkulado = graphene.String(description=_('Комментарий расчета'))
        rendevuejo = graphene.Boolean(description=_('Сборный груз'))
        numero_frajtoletero = graphene.String(description=_('Номер накладной СК'))
        pezo_fakto = graphene.Float(description=_('Вес фактический'))
        pezo_kalkula = graphene.Float(description=_('Вес расчетный'))
        dangxera_kargo = graphene.Boolean(description=_('Опасный груз'))
        priskribo_dangxera_kargo = graphene.String(description=_('Описание опасного груза'))
        klaso_dangxera = graphene.String(description=_('Класс опасности'))
        retropasxo_tsd = graphene.Boolean(description=_('Возврат ТСД'))
        akcepto_liveri_nelabora_tempo = graphene.Boolean(description=_('Прием доставка в нерабочее время'))
        aldone_pakumo = graphene.Boolean(description=_('Дополнительная упаковка'))
        temperatura_reghimo_uuid = graphene.String(description=_('UUID температурного режима'))
        # СозданиеХЦ_Key (Создание - faro)
        # faro_hc_key = models.UUIDField(_('Faro HC UUID'), default=None, blank=True, null=True)
        indikatoro_temperatura = graphene.Boolean(description=_('Датчик температуры'))
        livero_tp = graphene.Boolean(description=_('Предоставление ТП'))
        aldone_postulo_hc = graphene.String(description=_('Дополнительные требования ХЦ'))
        urboj_recivado_uuid = graphene.String(description=_('UUID города приемки'))
        urboj_recivado_nomo = graphene.String(description=_('Город приемки'))
        urboj_transdono_uuid = graphene.String(description=_('UUID города передачи'))
        urboj_transdono_nomo = graphene.String(description=_('Город передачи'))
        aliaj_servo = graphene.String(description=_('Иные услуги'))
        fordono_kargo_fio = graphene.String(description=_('Передача груза ФИО'))
        fordono_kargo_dato = graphene.Date(description=_("ФПередача груза дата"))
        fordono_kargo_tempo = graphene.Time(description=_("ФПередача груза время"))
        konservejo_ricevado_dato = graphene.Date(description=_("СКЛ дата приемки"))
        konservejo_ricevado_tempo_de = graphene.Time(description=_("СКЛ время приемки с"))
        konservejo_ricevado_tempo_en = graphene.Time(description=_("СКЛ время приемки по"))
        konservejo_fordono_dato = graphene.Date(description=_("СКЛ дата передачи"))
        konservejo_fordono_tempo_de = graphene.Time(description=_("СКЛ время передачи с"))
        konservejo_fordono_tempo_en = graphene.Time(description=_("СКЛ время передачи по"))
        asekurado = graphene.Boolean(description=_('Требуется страхование'))
        horoj_sxargxistoj = graphene.Int(description=_('Количество часов грузчики'))
        kvanto_sxargxistoj = graphene.Int(description=_('Количество грузчиков'))
        kvanto_pecoj = graphene.Int(description=_('Количество мест'))
        sxargxisto_rigilaro = graphene.Int(description=_('Требуются грузчики такелажники'))
        en_peza_kategorio = graphene.Boolean(description=_('Тяжеловес'))
        nomo_kargo = graphene.String(description=_('Наименование груза'))
        # Транспорт_Key (тра́нспорт trajno)
        # trajno_key = models.UUIDField(_('Trajno UUID'), default=None, blank=True, null=True)
        # Водитель_Key (Водитель kondukisto)
        # kondukisto_key = models.UUIDField(_('Kondukisto UUID'), default=None, blank=True, null=True)
        konservejo_temperatura_reghimo_1 = graphene.Boolean(description=_('скл температурный режим 1'))
        konservejo_temperatura_reghimo_2 = graphene.Boolean(description=_('скл температурный режим 2'))
        konservejo_temperatura_reghimo_3 = graphene.Boolean(description=_('скл температурный режим 3'))
        konservejo_temperatura_reghimo_4 = graphene.Boolean(description=_('скл температурный режим 4'))
        konservejo_temperatura_reghimo_alia = graphene.String(description=_('скл температурный режим иной'))
        konservejo_temperatura_reghimo_5 = graphene.Boolean(description=_('скл температурный режим 5'))
        konservejo_temperatura_reghimo_6 = graphene.Boolean(description=_('скл температурный режим 6'))
        konservejo_tasko = graphene.String(description=_('скл задание'))
        uid = graphene.String(description=_('УИД'))
        konservejo_komento = graphene.String(description=_('скл комментарий'))
        liveranto_uuid = graphene.String(description=_('UUID рл поставщик'))
        liveranto_kontrakto_uuid = graphene.String(description=_('Kontrakto liveranto UUID'))
        kombatanto_kontrakto_uuid = graphene.String(description=_('UUID рл контактное лицо поставщика'))
        trajno_venigo_dato = graphene.Date(description=_("рл дата подачи транспорт"))
        trajno_venigo_tempo = graphene.Time(description=_("рл время подачи транспорт"))
        trajno_finigho_dato = graphene.Date(description=_("рл дата окончания транспорт"))
        trajno_finigho_tempo = graphene.Time(description=_("рл время окончания транспорт"))
        # ТранспортСоСкладаГК_Key (тра́нспорт trajno)
        # trajno_konservejo_gk_key = models.UUIDField(_('Trajno konservejo gk UUID'), default=None, blank=True, null=True)
        # ВодительСоСкладаГК_Key (Водитель kondukisto)
        # kondukisto_konservejo_gk_key = models.UUIDField(_('Kondukisto konservejo gk UUID'), default=None, blank=True, null=True)
        sensoro_temperatura_2 = graphene.Boolean(description=_('Датчик температуры 2'))
        sensoro_temperatura_3 = graphene.Boolean(description=_('Датчик температуры 3'))
        sensoro_temperatura_4 = graphene.Boolean(description=_('Датчик температуры 4'))
        sensoro_temperatura_5 = graphene.Boolean(description=_('Датчик температуры 5'))
        sensoro_temperatura_alia = graphene.Boolean(description=_('Датчик температуры иной'))
        Livero_tp_2 = graphene.Boolean(description=_('Предоставление ТП 2'))
        Livero_tp_3 = graphene.Boolean(description=_('Предоставление ТП 3'))
        Livero_tp_4 = graphene.Boolean(description=_('Предоставление ТП 4'))
        Livero_tp_5 = graphene.Boolean(description=_('Предоставление ТП 5'))
        Livero_tp_alia = graphene.Boolean(description=_('Предоставление ТП иной'))
        priskribo_kliento = graphene.String(description=_('скл комментарий клиента'))
        numero_frajtoletero_kliento = graphene.String(description=_('Номер накладной клиента'))
        email_ekspedinto = graphene.String(description=_('Email отправителя'))
        email_ricevanto = graphene.String(description=_('Email получателя'))
        plenumado_dato = graphene.DateTime(description=_("Дата на исполнении"))
        fiksa_termoskribi = graphene.Boolean(description=_('рл стационарный термописец'))
        fiksa_termoskribi_2 = graphene.Boolean(description=_('рл стационарный термописец 2'))
        fiksa_termoskribi_3 = graphene.Boolean(description=_('рл стационарный термописец 3'))
        fiksa_termoskribi_4 = graphene.Boolean(description=_('рл стационарный термописец 4'))
        fiksa_termoskribi_5 = graphene.Boolean(description=_('рл стационарный термописец 5'))
        fiksa_termoskribi_alia = graphene.Boolean(description=_('рл стационарный термописец иной'))
        tipo_mendo = graphene.String(description=_('рл тип заявки'))
                    # "ИдМаршрута": 1,

            # рл_ПодразделениеЗаказчика_Key (Подразделение  (отдел, раздел, секция) sekcio)
            # sekcio_kliento_key = models.UUIDField(_('Sekcio kliento UUID'), default=None, blank=True, null=True)
        sxargxistoj_sxargxado = graphene.Boolean(description=_('рл грузчики погрузка'))
        sxargxistoj_elsxargxado = graphene.Boolean(description=_('рл грузчики выгрузка'))
        # параметры создаваемого проекта
        projekto_statuso_id = graphene.Int(description=_('Статус проекта')) # по умолчанию = 7
        # параметры создаваемой задачи
        tasko_numero = graphene.List(graphene.Int,description=_('Порядковый номер задач')) # "LineNumber": 1, # обязательное поле для маршрута
        tasko_kom_dato = graphene.List(graphene.DateTime,description=_('Дата начала выполнения задач')) # "ДатаНачала": 	"2021-09-02T09:00:00",
        tasko_fin_dato = graphene.List(graphene.DateTime,description=_('Дата окончания выполнения задач')) #  "ДатаОкончания": "2021-09-03T13:00:00",
        tasko_kom_adreso = graphene.List(graphene.String,description=_('Адрес начала выполнения задач')) # "АдресНачала": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
            # "АдресНачала_Type": "Edm.String",
        tasko_fin_adreso = graphene.List(graphene.String,description=_('Адрес окончания выполнения задач')) # "АдресКонца": "г.Москва, проезд Добролю….8А, корпус -, оф.5 эт.",
            # "АдресКонца_Type": "Edm.String",
        tasko_statuso_id = graphene.List(graphene.Int,description=_('Статус задач')) # "Состояние_Key": "67896102-d228-11e5-80f8-000c298958d9",
        tasko_priskribo = graphene.List(graphene.String,description=_('Комментарии задач')) # "Комментарий": "принимаем до 12-00",
        tasko_nomo = graphene.List(graphene.String,description=_('Название задач'))# "ИдентификаторМаршрута": "Маршрут № 1"},
        # параметры перевозимого груза
        kargo_numero = graphene.List(graphene.Int,description=_('Порядковый номер'))
        kargo_nomo = graphene.List(graphene.String,description=_('Наименование'))
        kargo_kvanto_pecoj = graphene.List(graphene.Int,description=_('Количество мест'))
        kargo_longo = graphene.List(graphene.Float,description=_('Длина'))
        kargo_largho = graphene.List(graphene.Float,description=_('Ширина'))
        kargo_alto = graphene.List(graphene.Float,description=_('Высота'))
        kargo_pezo_fakta = graphene.List(graphene.Float,description=_('Вес фактический'))
        kargo_volumeno = graphene.List(graphene.Float,description=_('Объем'))
        kargo_pezo_volumena = graphene.List(graphene.Float,description=_('Вес объемный'))
        kargo_tipo_pakumoj_uuid = graphene.List(graphene.String,description=_('UUID типа упаковки'))
        kargo_indikatoro = graphene.List(graphene.Boolean,description=_('Нуженли датчик'))
        kargo_numero_indikatoro = graphene.List(graphene.String,description=_('Серийный номер датчика'))
        kargo_temperatura_reghimo_uuid = graphene.List(graphene.String,description=_('UUID терморежима'))
        kargo_grave = graphene.List(graphene.Boolean,description=_('Опасный/Неопасный'))
        kargo_retropasxo_pakumo = graphene.List(graphene.Boolean,description=_('Возврат упаковки'))
        kargo_retropasxo_indikatoro = graphene.List(graphene.Boolean,description=_('Возврат датчика'))
        

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        ekspedo = None
        uzanto = info.context.user
        kliento = None
        membro = None
        kontrakto_kliento = None
        kombatanto_kliento = None
        organizo = None
        valuto_kliento = None
        urboj_recivado = None
        urboj_transdono = None
        realeco = None
        kategorio = None
        posedanto_dokumento_organizo_tipo = None
        informlibro = {'uzanto':uzanto,}

        informlibro['autoro'] = uzanto

        # Находим организацию-клиента
        # проверяем наличие записей с таким кодом
        if not len(errors):
            if 'kliento_uuid' in kwargs:
                    try:
                        kliento = Organizo.objects.get(uuid=kwargs.get('kliento_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['kliento'] = kliento
                    except Organizo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='kliento_uuid',
                            message=_('Неверный uuid организации')
                        ))
            else:
                errors.append(ErrorNode(
                    field='kliento_uuid',
                    message=_('Поле обязательно для заполнения')
                ))

        if not uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo'):
            try:
                membro = OrganizoMembro.objects.get(organizo=kliento, uzanto=uzanto, forigo=False,
                                                    arkivo=False, publikigo=True)
            except OrganizoMembro.DoesNotExist: # не является членом организации
                pass
        # Создавать может член организации-владельца документа
        if uzanto.has_perm('dokumentoj.povas_krei_dokumentoj_ekspedo') or membro:
            # проверяем обязательные параметры
            # if not ('rendevuejo' in kwargs): #СборныйГруз
            #     errors.append(ErrorNode(
            #         field='rendevuejo',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('telefono_kliento' in kwargs): #ТелефонЗаказчика
            #     errors.append(ErrorNode(
            #         field='telefono_kliento',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('volumeno' in kwargs): # Объем
            #     errors.append(ErrorNode(
            #         field='volumeno',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('pezo' in kwargs): #Вес
            #     errors.append(ErrorNode(
            #         field='pezo',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('priskribo_kargo' in kwargs): # ОписаниеГруза
            #     errors.append(ErrorNode(
            #         field='priskribo_kargo',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('kosto_kargo' in kwargs): # СтоимостьГруза
            #     errors.append(ErrorNode(
            #         field='kosto_kargo',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('monsumo_kliento' in kwargs): # СуммаЗаказчика
            #     errors.append(ErrorNode(
            #         field='monsumo_kliento',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('monsumo_transportisto' in kwargs): # СуммаПеревозчика
            #     errors.append(ErrorNode(
            #         field='monsumo_transportisto',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            if not ('adreso_kargado' in kwargs): # АдресЗагрузки
                errors.append(ErrorNode(
                    field='adreso_kargado',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('adreso_elsxargxado' in kwargs): # АдресВыгрузки
                errors.append(ErrorNode(
                    field='adreso_elsxargxado',
                    message=_('Поле обязательно для заполнения')
                ))
            # if not ('tempo_s' in kwargs): # ВремяС
            #     errors.append(ErrorNode(
            #         field='tempo_s',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            # if not ('tempo_elsxargxado' in kwargs): # ВремяВыгрузкиС
            #     errors.append(ErrorNode(
            #         field='tempo_elsxargxado',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            if not ('konservejo_ricevado_dato' in kwargs): # СКЛ_ДатаПриемки
                errors.append(ErrorNode(
                    field='konservejo_ricevado_dato',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('konservejo_ricevado_tempo_de' in kwargs): # СКЛ_ВремяПриемкиС
                errors.append(ErrorNode(
                    field='konservejo_ricevado_tempo_de',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('konservejo_ricevado_tempo_en' in kwargs): # СКЛ_ВремяПриемкиПо
                errors.append(ErrorNode(
                    field='konservejo_ricevado_tempo_en',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('konservejo_fordono_dato' in kwargs): # СКЛ_ДатаПередачи
                errors.append(ErrorNode(
                    field='konservejo_fordono_dato',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('konservejo_fordono_tempo_de' in kwargs): # СКЛ_ВремяПередачиС
                errors.append(ErrorNode(
                    field='konservejo_fordono_tempo_de',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('konservejo_fordono_tempo_en' in kwargs): # СКЛ_ВремяПередачиПо
                errors.append(ErrorNode(
                    field='konservejo_fordono_tempo_en',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('organizo_adreso' in kwargs): # ОрганизацияПоАдресу
                errors.append(ErrorNode(
                    field='organizo_adreso',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('organizo_adreso_elsxargxado' in kwargs): # ОрганизацияПоАдресуРазгрузки
                errors.append(ErrorNode(
                    field='organizo_adreso_elsxargxado',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('kombatanto_adresoj' in kwargs): # КонтактноеЛицоПоАдресу
                errors.append(ErrorNode(
                    field='kombatanto_adresoj',
                    message=_('Поле обязательно для заполнения')
                ))
            if not ('kombatanto_adresoj_elsxargxado' in kwargs): # КонтактноеЛицоПоАдресуРазгрузки
                errors.append(ErrorNode(
                    field='kombatanto_adresoj_elsxargxado',
                    message=_('Поле обязательно для заполнения')
                ))
            # проверяем наличие записей с таким кодом
            if 'kontrakto_kliento_uuid' in kwargs:#ДоговорЗаказчика_Key
                if not len(errors):
                    try:
                        kontrakto_kliento = DokumentoContract.objects.get(uuid=kwargs.get('kontrakto_kliento_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['kontrakto_kliento'] = kontrakto_kliento
                    except DokumentoContract.DoesNotExist:
                        errors.append(ErrorNode(
                            field='kontrakto_kliento_uuid',
                            message=_('Неверный uuid в DokumentoContract')
                        ))
            if 'kombatanto_kliento_uuid' in kwargs: # КонтактноеЛицоЗаказчика
                if not len(errors):
                    try:
                        kombatanto_kliento = Kombatanto.objects.get(uuid=kwargs.get('kombatanto_kliento_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['kombatanto_kliento'] = kombatanto_kliento
                    except Kombatanto.DoesNotExist:
                        errors.append(ErrorNode(
                            field='kombatanto_kliento_uuid',
                            message=_('Неверный uuid')
                        ))
            # else:
            #     errors.append(ErrorNode(
            #         field='kombatanto_kliento_uuid',
            #         message=_('Поле обязательно для заполнения')
            #     ))
            if 'organizo_uuid' in kwargs: # Организация_Key
                if not len(errors):
                    try:
                        organizo = Organizo.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['organizo'] = organizo
                    except Organizo.DoesNotExist:
                        errors.append(ErrorNode(
                            field='organizo_uuid',
                            message=_('Неверный uuid')
                        ))
            else:
                errors.append(ErrorNode(
                    field='organizo_uuid',
                    message=_('Поле обязательно для заполнения')
                ))
            if 'valuto_kliento_uuid' in kwargs: # ВалютаЗаказчика_Key
                if not len(errors):
                    try:
                        valuto_kliento = MonoValuto.objects.get(uuid=kwargs.get('valuto_kliento_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['valuto_kliento'] = valuto_kliento
                    except MonoValuto.DoesNotExist:
                        errors.append(ErrorNode(
                            field='valuto_kliento_uuid',
                            message=_('Неверный uuid')
                        ))
            if 'urboj_recivado_uuid' in kwargs: # ГородПриемки
                if not len(errors):
                    try:
                        urboj_recivado = InformilojUrboj.objects.get(uuid=kwargs.get('urboj_recivado_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['urboj_recivado'] = urboj_recivado
                    except InformilojUrboj.DoesNotExist:
                        errors.append(ErrorNode(
                            field='urboj_recivado_uuid',
                            message=_('Неверный uuid')
                        ))
            else:
                if not ('urboj_recivado_nomo' in kwargs): # ГородПриемки_nomo
                    errors.append(ErrorNode(
                        field='urboj_recivado_uuid или urboj_recivado_nomo',
                        message=_('Одно из полей обязательно для заполнения')
                    ))
            if 'urboj_transdono_uuid' in kwargs: # ГородПередачи
                if not len(errors):
                    try:
                        urboj_transdono = InformilojUrboj.objects.get(uuid=kwargs.get('urboj_transdono_uuid'), forigo=False,
                                                    arkivo=False, publikigo=True)
                        kwargs['urboj_transdono'] = urboj_transdono
                    except InformilojUrboj.DoesNotExist:
                        errors.append(ErrorNode(
                            field='urboj_transdono_uuid',
                            message=_('Неверный uuid')
                        ))
            else:
                if not ('urboj_transdono_nomo' in kwargs): # ГородПередачи_nomo
                    errors.append(ErrorNode(
                        field='urboj_transdono_uuid или urboj_transdono_nomo',
                        message=_('Одно из полей обязательно для заполнения')
                    ))
            if not 'projekto_statuso_id' in kwargs: # удалитьРезультатВыполнения_Key
                kwargs['projekto_statuso_id'] = 7
            if not len(errors):
                try:
                    projekto_statuso = TaskojProjektoStatuso.objects.get(id=kwargs.get('projekto_statuso_id'), forigo=False,
                                                arkivo=False, publikigo=True)
                    kwargs['projekto_statuso'] = projekto_statuso
                except TaskojProjektoStatuso.DoesNotExist:
                    errors.append(ErrorNode(
                        field='projekto_statuso_id',
                        message=_('Неверный id статуса проекта')
                    ))
            if not 'tipo_dokumento_id' in kwargs: # тип владельца-клиента документа по умолчанию
                kwargs['tipo_dokumento_id'] = 4
            if not len(errors):
                try:
                    tipo = DokumentoTipo.objects.get(id=kwargs.get('tipo_dokumento_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    informlibro['tipo_dokumento'] = tipo
                except DokumentoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='tipo_dokumento_id',
                        message=_('Неверный тип документов')
                    ))
            if not len(errors):
                if 'realeco' not in kwargs:
                    realeco_id = [1,]
                elif not len(kwargs.get('realeco')):
                    errors.append(ErrorNode(
                        field='realeco',
                        message=_('Необходимо указать хотя бы одно значение')
                    ))
                else:
                    realeco_id = set(kwargs.get('realeco'))
            if not len(errors):
                if len(realeco_id):
                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                    informlibro['realeco'] = realeco
                    if len(dif):
                        errors.append(ErrorNode(
                            field=str(dif),
                            message=_('Указаны несуществующие реальности')
                        ))
            if not len(errors):
                if 'kategorio' not in kwargs:
                    kwargs['kategorio'] = [4,]
                elif not len(kwargs.get('kategorio')):
                    errors.append(ErrorNode(
                        field='kategorio',
                        message=_('Необходимо указать хотя бы одно значение')
                    ))
            if not len(errors):
                kategorio_id = set(kwargs.get('kategorio'))
                if len(kategorio_id):
                    kategorio = DokumentoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                    informlibro['kategorio'] = kategorio
                    if len(dif):
                        errors.append(ErrorNode(
                            field=str(dif),
                            message=_('Указаны несуществующие категория документов')
                        ))
            if not 'posedanto_dokumento_kliento_tipo_id' in kwargs: # тип владельца-клиента документа по умолчанию
                kwargs['posedanto_dokumento_kliento_tipo_id'] = 2
            if not len(errors):
                try:
                    posedanto_dokumento_kliento_tipo = DokumentoPosedantoTipo.objects.get(
                        id=kwargs.get('posedanto_dokumento_kliento_tipo_id'), forigo=False,
                        arkivo=False, publikigo=True)
                    informlibro['posedanto_dokumento_kliento_tipo'] = posedanto_dokumento_kliento_tipo
                except DokumentoPosedantoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='posedanto_dokumento_kliento_tipo_id',
                        message=_('Неверный тип документов')
                    ))
            if not 'posedanto_dokumento_organizo_tipo_id' in kwargs: # тип владельца-поставщика документа по умолчанию
                kwargs['posedanto_dokumento_organizo_tipo_id'] = 1
            if not len(errors):
                try:
                    posedanto_dokumento_organizo_tipo = DokumentoPosedantoTipo.objects.get(
                        id=kwargs.get('posedanto_dokumento_organizo_tipo_id'), forigo=False,
                        arkivo=False, publikigo=True)
                    informlibro['posedanto_dokumento_organizo_tipo'] = posedanto_dokumento_organizo_tipo
                except DokumentoPosedantoTipo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='posedanto_dokumento_organizo_tipo_id',
                        message=_('Неверный тип документов')
                    ))
            if not 'posedanto_dokumento_statuso_id' in kwargs: # статус владельца документа по умолчанию
                kwargs['posedanto_dokumento_statuso_id'] = 1
            if not len(errors):
                try:
                    posedanto_dokumento_statuso = DokumentoPosedantoStatuso.objects.get(id=1, forigo=False,
                                                        arkivo=False, publikigo=True)
                    informlibro['posedanto_dokumento_statuso'] = posedanto_dokumento_statuso
                except DokumentoPosedantoStatuso.DoesNotExist:
                    errors.append(ErrorNode(
                        field='posedanto_dokumento_statuso',
                        message=_('Неверный статус документов')
                    ))
            if (not len(errors)) and kwargs.get('valuto_kliento_id', False):
                try:
                    valuto_kliento = MonoValuto.objects.get(id=kwargs.get('valuto_kliento_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['valuto_kliento'] = valuto_kliento
                except MonoValuto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='valuto_kliento_id',
                        message=_('Неверный id валюты')
                    ))
            if (not len(errors)) and kwargs.get('ekspedinto_uuid', False):
                try:
                    ekspedinto = Organizo.objects.get(uuid=kwargs.get('ekspedinto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['ekspedinto'] = ekspedinto
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='ekspedinto_uuid',
                        message=_('Неверный uuid организации')
                    ))
            if (not len(errors)) and kwargs.get('ricevanto_uuid', False):
                try:
                    ricevanto = Organizo.objects.get(uuid=kwargs.get('ricevanto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['ricevanto'] = ricevanto
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='ricevanto_uuid',
                        message=_('Неверный uuid организации')
                    ))
            if (not len(errors)) and kwargs.get('temperatura_reghimo_uuid', False):
                try:
                    temperatura_reghimo = ObjektoTemperaturaReghimo.objects.get(uuid=kwargs.get('temperatura_reghimo_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['temperatura_reghimo'] = temperatura_reghimo
                except ObjektoTemperaturaReghimo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='temperatura_reghimo_uuid',
                        message=_('Неверный uuid температурного режима')
                    ))
            if (not len(errors)) and kwargs.get('liveranto_uuid', False):
                try:
                    liveranto = Organizo.objects.get(uuid=kwargs.get('liveranto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['liveranto'] = liveranto
                except Organizo.DoesNotExist:
                    errors.append(ErrorNode(
                        field='liveranto_uuid',
                        message=_('Неверный uuid организации')
                    ))
            if (not len(errors)) and kwargs.get('liveranto_kontrakto_uuid', False):
                try:
                    liveranto_kontrakto = DokumentoContract.objects.get(uuid=kwargs.get('liveranto_kontrakto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['liveranto_kontrakto'] = liveranto_kontrakto
                except DokumentoContract.DoesNotExist:
                    errors.append(ErrorNode(
                        field='liveranto_kontrakto_uuid',
                        message=_('Неверный uuid договора поставщика в DokumentoContract')
                    ))
            if (not len(errors)) and kwargs.get('kombatanto_kontrakto_uuid', False):
                try:
                    kombatanto_kontrakto = Kombatanto.objects.get(uuid=kwargs.get('kombatanto_kontrakto_uuid'), forigo=False,
                                                        arkivo=False, publikigo=True)
                    kwargs['kombatanto_kontrakto'] = kombatanto_kontrakto
                except Kombatanto.DoesNotExist:
                    errors.append(ErrorNode(
                        field='kombatanto_kontrakto_uuid',
                        message=_('Неверный uuid контактного лица поставщика')
                    ))
            # проверяем параметры для создания задач (маршрута)
            if kwargs.get('tasko_numero', False) and len(kwargs.get('tasko_numero')):
                len_tasko_numero = len(kwargs.get('tasko_numero'))
                # проверяем количество параметров
                if len_tasko_numero != len(kwargs.get('tasko_kom_dato')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_kom_dato',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_fin_dato')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_fin_dato',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_kom_adreso')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_kom_adreso',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_fin_adreso')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_fin_adreso',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_statuso_id')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_statuso_id',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_priskribo')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_priskribo',
                        message=_('Несоответствие количества параметров')
                    ))
                if len_tasko_numero != len(kwargs.get('tasko_nomo')):
                    errors.append(ErrorNode(
                        field='tasko_numero, tasko_nomo',
                        message=_('Несоответствие количества параметров')
                    ))
                # получаем соответствующие записи по индексу
                if not len(errors):
                    tasko_statuso_id = set(kwargs.get('tasko_statuso_id',[]))
                    tasko_statuso = TaskojTaskoStatuso.objects.filter(id__in=tasko_statuso_id, forigo=False, arkivo=False, publikigo=True)
                    set_value = set(tasko_statuso.values_list('id', flat=True))
                    arr_value = []
                    for s in set_value:
                        arr_value.append(s)
                    dif = set(tasko_statuso_id) - set(arr_value)
                    if len(dif):
                        errors.append(ErrorNode(
                            field='{} ({})'.format(_('tasko_statuso_id'), str(dif)),
                            message=_('Указаны несуществующие статусы владельцев')
                        ))
                    else: #собираем массив статусов владельцев для каждой задачи согласно вошедшего массива, 
                        #  т.к. у нас перечисление, а нужен верно составленный массив
                        tasko_statuso_masivo = []
                        for st in kwargs.get('tasko_statuso_id',[]):
                            for t_s in tasko_statuso:
                                if st == t_s.id:
                                    tasko_statuso_masivo.append(t_s)
                                    break
                        kwargs['tasko_statuso'] = tasko_statuso_masivo

            # проверяем параметры для создания списка грузов
            if kwargs.get('kargo_numero', False) and len(kwargs.get('kargo_numero')):
                len_kargo_numero = len(kwargs.get('kargo_numero'))
                # проверяем количество параметров
                if kwargs.get('kargo_nomo', False) and len_kargo_numero != len(kwargs.get('kargo_nomo')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_nomo',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_kvanto_pecoj', False) and len_kargo_numero != len(kwargs.get('kargo_kvanto_pecoj')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_kvanto_pecoj',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_longo', False) and len_kargo_numero != len(kwargs.get('kargo_longo')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_longo',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_largho', False) and len_kargo_numero != len(kwargs.get('kargo_largho')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_largho',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_alto', False) and len_kargo_numero != len(kwargs.get('kargo_alto')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_alto',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_pezo_fakta', False) and len_kargo_numero != len(kwargs.get('kargo_pezo_fakta')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_pezo_fakta',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_volumeno', False) and len_kargo_numero != len(kwargs.get('kargo_volumeno')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_volumeno',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_pezo_volumena', False) and len_kargo_numero != len(kwargs.get('kargo_pezo_volumena')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_pezo_volumena',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_tipo_pakumoj_uuid', False) and len_kargo_numero != len(kwargs.get('kargo_tipo_pakumoj_uuid')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_tipo_pakumoj_uuid',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_indikatoro')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_indikatoro',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_numero_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_numero_indikatoro')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_numero_indikatoro',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_temperatura_reghimo_uuid', False) and len_kargo_numero != len(kwargs.get('kargo_temperatura_reghimo_uuid')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_temperatura_reghimo_uuid',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_grave', False) and len_kargo_numero != len(kwargs.get('kargo_grave')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_grave',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_retropasxo_pakumo', False) and len_kargo_numero != len(kwargs.get('kargo_retropasxo_pakumo')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_retropasxo_pakumo',
                        message=_('Несоответствие количества параметров')
                    ))
                if kwargs.get('kargo_retropasxo_indikatoro', False) and len_kargo_numero != len(kwargs.get('kargo_retropasxo_indikatoro')):
                    errors.append(ErrorNode(
                        field='kargo_numero, kargo_retropasxo_indikatoro',
                        message=_('Несоответствие количества параметров')
                    ))
                # получаем соответствующие записи по индексу
                if not len(errors):
                    kargo_tipo_pakumoj_uuid = set(kwargs.get('kargo_tipo_pakumoj_uuid',[]))
                    kargo_tipo_pakumoj = ObjektoTipoPakumo.objects.filter(uuid__in=kargo_tipo_pakumoj_uuid, forigo=False, arkivo=False, publikigo=True)
                    set_value = set(kargo_tipo_pakumoj.values_list('uuid', flat=True))
                    arr_value = []
                    for s in set_value:
                        arr_value.append(str(s))
                    dif = set(kargo_tipo_pakumoj_uuid) - set(arr_value)
                    if len(dif):
                        errors.append(ErrorNode(
                            field='{} ({})'.format(_('kargo_tipo_pakumoj_uuid'), str(dif)),
                            message=_('Указаны несуществующие uuid')
                        ))
                    else: #собираем массив для каждой задачи согласно вошедшего массива, 
                        #  т.к. у нас перечисление, а нужен верно составленный массив
                        kargo_tipo_pakumoj_masivo = []
                        for st in kwargs.get('kargo_tipo_pakumoj_uuid',[]):
                            for t_s in kargo_tipo_pakumoj:
                                if st == str(t_s.uuid):
                                    kargo_tipo_pakumoj_masivo.append(t_s)
                                    break
                        kwargs['kargo_tipo_pakumoj'] = kargo_tipo_pakumoj_masivo
                if not len(errors):
                    kargo_temperatura_reghimo_uuid = set(kwargs.get('kargo_temperatura_reghimo_uuid',[]))
                    kargo_temperatura_reghimo = ObjektoTemperaturaReghimo.objects.filter(uuid__in=kargo_temperatura_reghimo_uuid, forigo=False, arkivo=False, publikigo=True)
                    set_value = set(kargo_temperatura_reghimo.values_list('uuid', flat=True))
                    arr_value = []
                    for s in set_value:
                        arr_value.append(str(s))
                    dif = set(kargo_temperatura_reghimo_uuid) - set(arr_value)
                    if len(dif):
                        errors.append(ErrorNode(
                            field='{} ({})'.format(_('kargo_temperatura_reghimo_uuid'), str(dif)),
                            message=_('Указаны несуществующие uuid')
                        ))
                    else: #собираем массив для каждой задачи согласно вошедшего массива, 
                        #  т.к. у нас перечисление, а нужен верно составленный массив
                        kargo_temperatura_reghimo_masivo = []
                        for st in kwargs.get('kargo_temperatura_reghimo_uuid',[]):
                            for t_s in kargo_temperatura_reghimo:
                                if st == str(t_s.uuid):
                                    kargo_temperatura_reghimo_masivo.append(t_s)
                                    break
                        kwargs['kargo_temperatura_reghimo'] = kargo_temperatura_reghimo_masivo

            with transaction.atomic():
                publikiga_dato = timezone.now()
                
                if not len(errors):
                    # Создаём запись
                    status, message, errors, aldonij = OldAldoniDokumentoEkspedo1c.create(root, info, **kwargs)
                
                if status: # сохранили в 1с, теперь сохраняем себе в базу
                    kvanto = 0
                    status, message, errors, ekspedo, kvanto = ImportoDokumentoEkspedo1c.aldoni(root, info, kvanto, informlibro, aldonij)

        else:
            errors.append(ErrorNode(
                field='',
                message=_('Недостаточно прав')
            ))
        if len(errors):
            for error in errors:
                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                    error.message, _('в поле'), error.field)
        elif not message:
            status = True
            errors = list()
            message =  _('Данные сохранены на всех этапах')

        return status, message, errors, ekspedo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        ekspedo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Создаём запись
            status, message, errors, ekspedo = OldAldoniDokumentoEkspedo.create(root, info, **kwargs)
        else:
            message = _('Требуется авторизация')

        return OldAldoniDokumentoEkspedo(status=status, message=message, errors=errors, ekspedo=ekspedo)


class DokumentoMutations(graphene.ObjectType):
    redaktu_dokumento_dosiero = RedaktuDokumentoDosiero.Field(
        description=_('''Создаёт или редактирует файлы к документам''')
    )
    redaktu_dokumento_kategorio = RedaktuDokumentoKategorio.Field(
        description=_('''Создаёт или редактирует категории документов''')
    )
    redaktu_dokumento_kategorio_ligiloj_tipo = RedaktuDokumentoKategorioLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей категорий документов между собой''')
    )
    redaktu_dokumento_kategorio_ligiloj = RedaktuDokumentoKategorioLigilo.Field(
        description=_('''Создаёт или редактирует модели связей категорий документов между собой''')
    )
    redaktu_dokumento_tipo = RedaktuDokumentoTipo.Field(
        description=_('''Создаёт или редактирует типы документов''')
    )
    redaktu_dokumento_speco = RedaktuDokumentoSpeco.Field(
        description=_('''Создаёт или редактирует виды документов''')
    )
    redaktu_dokumento_stokejoj_tipo = RedaktuDokumentoStokejoTipo.Field(
        description=_('''Создаёт или редактирует типы мест хранения документов''')
    )
    redaktu_dokumento = RedaktuDokumento.Field(
        description=_('''Создаёт или редактирует документы''')
    )
    redaktu_dokumento_stokejoj = RedaktuDokumentoStokejo.Field(
        description=_('''Создаёт или редактирует логические места хранения документов''')
    )
    redaktu_dokumento_posedanto_tipo = RedaktuDokumentoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует модели типов владельцев документов''')
    )
    redaktu_dokumento_posedanto_statuso = RedaktuDokumentoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует модели статусов владельцев документов''')
    )
    redaktu_dokumento_posedantoj = RedaktuDokumentoPosedanto.Field(
        description=_('''Создаёт или редактирует модели владельцев документов''')
    )
    redaktu_dokumento_ligiloj_tipo = RedaktuDokumentoLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей документов между собой''')
    )
    redaktu_dokumento_ligiloj = RedaktuDokumentoLigilo.Field(
        description=_('''Создаёт или редактирует модели связей документов между собой''')
    )
    redaktu_dokumento_ekspedo = RedaktuDokumentoEkspedo.Field(
        description=_('''Создаёт или редактирует модели документов экспедирования''')
    )
    redaktu_dokumento_contract = RedaktuDokumentoContract.Field(
        description=_('''Создаёт или редактирует модели документов - контрактов''')
    )
    # importo_dokumento_ekspedo1c = ImportoDokumentoEkspedo1c.Field(
    #     description=_('''Импорт документов экспедирования из 1с''')
    # )
    test_aldoni_dokumento_ekspedo1c = TestAldoniDokumentoEkspedo1c.Field(
        description=_('''Тестовое создание документов в 1с''')
    )
    # aldoni_dokumento_ekspedo1c = AldoniDokumentoEkspedo1c.Field(
    #     description=_('''Создание документов в 1с''')
    # )
    importo_speco_servo_ekspedo1c = ImportoSpecoServoEkspedo1c.Field(
        description=_('''Импорт видов услуг экспедирования из 1с''')
    )
    aldoni_dokumento_ekspedo = AldoniDokumentoEkspedo.Field(
        description=_('''Создание документов экспедирования''')
    )
    old_aldoni_dokumento_ekspedo = OldAldoniDokumentoEkspedo.Field(
        description=_('''Старое создание документов экспедирования''')
    )
    importo_importo_dokumento_contract1c = ImportoDokumentoContract1c.Field(
        description=_('''Импорт договоров на экспедирование из 1с''')
    )
