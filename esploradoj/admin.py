"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from esploradoj.models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from siriuso.utils.admin_mixins import TekstoMixin


# Форма для многоязычных названий типов категорий Конференций
class EsploradojKategorioTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = EsploradojKategorioTipo
        fields = [field.name for field in EsploradojKategorioTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы категорий Конференций
@admin.register(EsploradojKategorioTipo)
class EsploradojKategorioTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojKategorioTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = EsploradojKategorioTipo


# Форма для многоязычнных полей категорий Конференций
class EsploradojKategorioFormo(forms.ModelForm):

    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=True)

    class Meta:
        model = EsploradojKategorio
        fields = [field.name for field in EsploradojKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категории Конференций
@admin.register(EsploradojKategorio)
class EsploradojKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojKategorioFormo
    list_display = ('id', 'nomo_teksto', 'priskribo_teksto')
    exclude = ('id',)

    class Meta:
        model = EsploradojKategorio


# Форма для многоязычных названий типов тем Конференций
class EsploradojTemoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = EsploradojTemoTipo
        fields = [field.name for field in EsploradojTemoTipo._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы тем Конференций
@admin.register(EsploradojTemoTipo)
class EsploradojTemoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojTemoTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = EsploradojTemoTipo


# Форма для многоязычнных полей тем Конференций
class EsploradojTemoFormo(forms.ModelForm):
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=True)
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=True)

    class Meta:
        model = EsploradojTemo
        fields = [field.name for field in EsploradojTemo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Темы Конференций
@admin.register(EsploradojTemo)
class EsploradojTemoAdmin(admin.ModelAdmin, TekstoMixin):
    form = EsploradojTemoFormo
    list_display = ('id', 'priskribo_teksto', 'nomo_teksto')
    exclude = ('id',)

    class Meta:
        model = EsploradojTemo
