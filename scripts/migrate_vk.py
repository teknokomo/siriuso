"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from http.client import HTTPSConnection
from urllib.parse import urlparse
from komunumoj.models import *
from muroj.models import *
from siriuso.utils import set_enhavo

from datetime import datetime
import pytz
import json
import re


def convert_to_editorjs(data):
    paragraphs = re.split('[\r]?\n', data)

    return str(json.dumps({
        'time': int(datetime.utcnow().timestamp() * 1000),
        'blocks': [
            {'type': 'paragraph', 'data': {'text': _}} for _ in paragraphs
        ],
        "version": "2.12.3"
    }, ensure_ascii=False, separators=(',', ':')))


def migrate_vk(to, from_url, cnt=20):
    try:
        kom = Komunumo.objects.get(uuid=to)
        kom_aliro = KomunumojAliro.objects.get(kodo="chiuj")
        kom_muro = Muro.objects.get(posedanto=kom)

        url = urlparse(from_url)

        client = HTTPSConnection(url.netloc)
        client.request("GET", "{}?{}".format(url.path, url.query))
        resp = client.getresponse()

        if resp.status == 200:
            data = resp.read()
            data = json.loads(data)

            for rec in data['response']['items']:
                vk_url = "https://vk.com/wall{}_{}".format(rec["owner_id"], rec["id"])
                text = rec["text"]
                pub_data = datetime.fromtimestamp(rec["date"], pytz.utc)

                enskribo = MuroEnskribo.objects.create(
                    muro=kom_muro,
                    autoro_id=1,
                    aliro=kom_aliro,
                    komentado_aliro=kom_aliro,
                    posedanto=kom,
                    arkivo=False,
                    forigo=False,
                    publikigo=True,
                    publikiga_dato=pub_data,
                    vk_referenco=vk_url
                )

                set_enhavo(enskribo.teksto, convert_to_editorjs(text), "ru_RU")
                print(enskribo.teksto)
                enskribo.save()

    except (Komunumo.DoesNotExist, Muro.DoesNotExist, KomunumojAliro.DoesNotExist):
        pass
