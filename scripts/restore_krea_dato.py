"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from komunumoj.models import *
from muroj.models import *


def main():
    def get_kd(inst):
        if isinstance(inst, Komunumo):
            try:
                if inst.tipo.kodo == 'iniciato':
                    return KomunumojSociaprojekto.objects.get(uuid=inst.uuid).krea_dato
                elif inst.tipo.kodo == 'grupo':
                    return KomunumojGrupo.objects.get(uuid=inst.uuid).krea_dato
                elif inst.tipo.kodo == 'soveto':
                    return KomunumojSoveto.objects.get(uuid=inst.uuid).krea_dato
                elif inst.tipo.kodo in ('entrepreno', 'sindikato', 'gvardio', 'partio', 'tribunalo', 'resursacentro'):
                    return KomunumojOrganizo.objects.get(uuid=inst.uuid).krea_dato
            except:
                return None
        elif isinstance(inst, MuroEnskribo):
            try:
                if inst.posedanto.tipo.kodo == 'iniciato':
                    return MurojSociaprojektoEnskribo.objects.get(uuid=inst.uuid).krea_dato
                elif inst.posedanto.tipo.kodo == 'grupo':
                    return MurojGrupoEnskribo.objects.get(uuid=inst.uuid).krea_dato
                elif inst.posedanto.tipo.kodo == 'soveto':
                    return MurojSovetoEnskribo.objects.get(uuid=inst.uuid).krea_dato
                elif inst.posedanto.tipo.kodo in ('entrepreno', 'sindikato', 'gvardio', 'partio', 'tribunalo', 'resursacentro'):
                    return MurojOrganizoEnskribo.objects.get(uuid=inst.uuid).krea_dato
            except:
                return None

    total = 0
    for _ in Komunumo.objects.all():
        nd = get_kd(_)

        if nd:
            _.krea_dato = nd
            _.save()
            total += 1

    print("Komunumo total: {}".format(total))
    total = 0

    for _ in MuroEnskribo.objects.all():
        nd = get_kd(_)

        if nd:
            _.krea_dato = get_kd(_)
            _.save()
            total += 1

    print("Muro total: {}".format(total))
