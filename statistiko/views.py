"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from siriuso.views import SiriusoTemplateView


class StatistikoView(SiriusoTemplateView):
    template_name = 'statistiko/statistiko.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_admin:
                return super().dispatch(request, *args, **kwargs)

            raise PermissionDenied()

        return redirect('ensaluti', permanent=True)
